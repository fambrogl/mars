rad_dir=$PWD
mkdir build
cd build
echo "Configuration  with cmake!"
cmake -DSTATIC_BUILD=OFF -DMARS_INSTALL_PREFIX=$rad_dir $rad_dir
echo "Compilation of the code with make!"
make -j4
echo "Installation of the code!"
make install




