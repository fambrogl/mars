#----------------------------------------------------------------------------
# Setup the project
#
cmake_minimum_required(VERSION 2.6 FATAL_ERROR)
project(MARS)


#----------------------------------------------------------------------------
# Find Geant4 package, activating all available UI and Vis drivers by default
# You can set WITH_GEANT4_UIVIS to OFF via the command line or ccmake/cmake-gui
# to build a batch mode only executable
#
option(WITH_GEANT4_UIVIS "Build example with Geant4 UI and Vis drivers" ON)
if(WITH_GEANT4_UIVIS)
  find_package(Geant4 REQUIRED ui_all vis_all)
else()
  find_package(Geant4 REQUIRED)
endif()

#----------------------------------------------------------------------------
# - Prepend local CMake Modules to the search path

set(CMAKE_MODULE_PATH
    ${PROJECT_SOURCE_DIR}/cmake/Modules
    ${CMAKE_MODULE_PATH})



#----------------------------------------------------------------------------
# Setup Geant4 include directories and compile definitions
# Setup include directory for this project
#
include(${Geant4_USE_FILE})
set(mars_include_dirs ${CMAKE_CURRENT_SOURCE_DIR}/source/analysis/include 
            ${CMAKE_CURRENT_SOURCE_DIR}/source/dataformats/include 
            ${CMAKE_CURRENT_SOURCE_DIR}/source/astronaut/include 
            ${CMAKE_CURRENT_SOURCE_DIR}/source/global/include 
            ${CMAKE_CURRENT_SOURCE_DIR}/source/shield/include 
            ${CMAKE_CURRENT_SOURCE_DIR}/source/spacecraft/include 
            ${Geant4_INCLUDE_DIR})   

include_directories(${mars_include_dirs})
#----------------------------------------------------------------------------
# Source subdirectory
#
set(headers "")
set(sources "")
add_subdirectory(source)
get_directory_property(headers DIRECTORY source DEFINITION headers)
get_directory_property(sources DIRECTORY source DEFINITION sources)

include(FindROOT)
if(ROOT_FOUND)
	MESSAGE( STATUS "MARS will be compiled with ROOT.")
	include_directories(${ROOT_INCLUDE_DIR})
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DUSE_ANALYSIS_ROOT")
	set(EXTRA_LIBRARIES "${EXTRA_LIBRARIES} ${ROOT_LIBRARIES}")
	set (MARS_BUILTWITH_ROOT "yes")
else()
	MESSAGE(FATAL_ERROR "ROOT installation not found!Installation interrupted!")
endif()

#---------------------------------------------------------
#set directories where library and executable will be set

set(EXECUTABLE_OUTPUT_PATH ${PROJECT_BINARY_DIR}/bin)
set(LIBRARY_OUTPUT_PATH ${PROJECT_BINARY_DIR}/lib)

set(DataFormats_LINKDEF ${CMAKE_CURRENT_SOURCE_DIR}/source/dataformats/include/LinkDef.h )
set(DataFormats_DICTIONARY ${CMAKE_CURRENT_BINARY_DIR}/DataFormatDict.cxx)
set(DataFormatsHeaders "")
get_directory_property(DataFormatsHeaders DIRECTORY source/dataformats DEFINITION rootdfheaders)
ROOT_GENERATE_DICTIONARY("${DataFormatsHeaders}" "${DataFormats_LINKDEF}" "${DataFormats_DICTIONARY}" "${mars_include_dirs}")

# add the dictionary to the list of source files
set(sources ${sources} ${DataFormats_DICTIONARY}) 

if(Geant4_gdml_FOUND)
    MESSAGE( STATUS "MARS will be compiled with GDML!")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DMARS_USE_G4GDML") 
    find_package(XercesC)
    include_directories(${XERCESC_INCLUDE_DIR})
endif(Geant4_gdml_FOUND)


#----------------------------------------------------------------------------
# MARS library
option(STATIC_BUILD "Build mars static library" ON)
STRING(TOUPPER ${STATIC_BUILD} staticbuild)
STRING(COMPARE EQUAL ${staticbuild} "ON" test_res)

if (test_res)
    add_library(mars_lib STATIC ${sources} ${headers})

else()
    add_library(mars_lib SHARED ${sources} ${headers})
    
endif()
set_target_properties(mars_lib PROPERTIES OUTPUT_NAME "mars")
target_link_libraries(mars_lib  ${Geant4_LIBRARIES} ${ROOT_LIBRARIES}) 


#----------------------------------------------------------------------------
# MARS executable
add_executable(mars_exe ${PROJECT_SOURCE_DIR}/source/executable/mars.cc ${headers}) 
add_dependencies(mars_exe mars_lib)

set_target_properties(mars_exe PROPERTIES OUTPUT_NAME "mars")
target_link_libraries(mars_exe mars_lib)


#----------------------------------------------------------------------------
#set MARS_INSTALL_PREFIX
#
set(MARS_INSTALL_PREFIX ${PROJECT_BINARY_DIR} CACHE PATH "Prefix for the MARS installation")

#--------------------------------------
#Building of mars-env.sh 

set (MARS_BASH_BINPATH_SETUP "export PATH=${MARS_INSTALL_PREFIX}/bin:$PATH")
if(${CMAKE_SYSTEM_NAME} STREQUAL "Darwin")
    set(libpathname DYLD_LIBRARY_PATH)
else()
    set(libpathname LD_LIBRARY_PATH)
endif()
set (MARS_BASH_LIBPATH_SETUP "export ${libpathname}=${MARS_INSTALL_PREFIX}/lib:$${libpathname}")   
configure_file(${PROJECT_SOURCE_DIR}/cmake/Templates/mars-env.sh.in
    ${PROJECT_BINARY_DIR}/config/mars-env.sh
    @ONLY)

#--------------------------------------
#Building of mars-env.csh 

set (MARS_CSH_BINPATH_SETUP "setenv  PATH ${MARS_INSTALL_PREFIX}/bin:$PATH")     
if(${CMAKE_SYSTEM_NAME} STREQUAL "Darwin")
    set(libpathname DYLD_LIBRARY_PATH)
else()
    set(libpathname LD_LIBRARY_PATH)
endif()
set (MARS_CSH_LIBPATH_SETUP "setenv ${libpathname} ${MARS_INSTALL_PREFIX}/lib:$${libpathname}")   
configure_file(${PROJECT_SOURCE_DIR}/cmake/Templates/mars-env.csh.in
    ${PROJECT_BINARY_DIR}/config/mars-env.csh
    @ONLY) 


#----------------------------------------------------------------------------
# Install the executable to 'bin' directory under CMAKE_INSTALL_PREFIX
#
install(TARGETS mars_exe DESTINATION ${MARS_INSTALL_PREFIX}/bin)
install(TARGETS mars_lib DESTINATION ${MARS_INSTALL_PREFIX}/lib)
install(FILES ${PROJECT_BINARY_DIR}/config/mars-env.sh
    DESTINATION ${MARS_INSTALL_PREFIX}/bin
    PERMISSIONS
      OWNER_READ OWNER_WRITE OWNER_EXECUTE
      GROUP_READ GROUP_EXECUTE
      WORLD_READ WORLD_EXECUTE
    COMPONENT Development
    )
install(FILES ${PROJECT_BINARY_DIR}/config/mars-env.csh
    DESTINATION ${MARS_INSTALL_PREFIX}/bin
    PERMISSIONS
      OWNER_READ OWNER_WRITE OWNER_EXECUTE
      GROUP_READ GROUP_EXECUTE
      WORLD_READ WORLD_EXECUTE
    COMPONENT Development
    )
