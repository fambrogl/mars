//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Jul 22 15:19:30 2013 by ROOT version 5.34/09
// from TTree Dose/The Tree with the variable used to performe the dose calculation on astronaut 
// found on file: FreeSpace_Z_3_28_10KevtCylinderMale.root
//////////////////////////////////////////////////////////

#ifndef CompleteDoseAnalyzer_h
#define CompleteDoseAnalyzer_h

#include <TROOT.h>
#include <TChain.h>
#include <TString.h>
#include <TFile.h>
#include <TDirectory.h>
#include <TH1F.h>
#include <TH2F.h>

// Header file for the classes stored in the TTree if any.

#include "MarsRootEvent.hh"
#include "MarsRootTrack.hh"
#include "MarsRootHit.hh"
#include "MarsRootFluenceHit.hh"


#include <TObject.h>
#include <TVector3.h>
#include <vector>
#include <iostream>
#include <fstream>
#include <map>
#include <string>

using namespace std;

// Fixed size dimensions of array or collections stored in the TTree if any.

class CompleteDoseAnalyzer{
 public :
  TChain          *fTreeDose;             //!pointer to the analyzed TTree or TChain
  TChain          *fTreeSetup;            //!pointer to the analyzed TTree or TChain
  TChain          *fTreeAstronaut;        //!pointer to the analyzed TTree or TChain
  Int_t           fTreeCurrentDose;      //!current Tree number in a TChain
  Int_t           fTreeCurrentSetup;     //!current Tree number in a TChain
  Int_t           fTreeCurrentAstronaut; //!current Tree number in a TChain
  // Declaration of leaf types

  bool fluxAlreadyComputed;

  TString         fluxFile;
  
  bool onlyBarrel;
  int totalDetectorNumber;
  std::vector<double>  Energy;

  std::map<int,std::map<int,std::map<TString,TH1F*> > >myFluenceSpectraMap;
  std::map<int,std::map<int,std::map<TString,TH1F*> > >myDoseSpectraMap;

  std::map<TString,TH1F*> finalFluxSecondarySpectraMap;
  std::map<TString,TH1F*> finalDoseSecondarySpectraMap;
  std::map<TString,TH1F*> finalFluxSecondaryMap;
  std::map<TString,TH1F*> finalDoseSecondaryMap;

  std::map<TString,TH2F*> detailedSecondaryVertexMap;

  std::map<int,std::vector<double> > ZFlux;
  std::map<int,std::map<int,int> > ZFluxPartNumb;
  std::map<int,std::map<int,int> > ZFluxPartNumbGen;
  
  std::map<int,std::map<int,std::map<int,double> > >ZEnergyAbDoseBFO;
  std::map<int,std::map<int,std::map<int,double> > >ZEnergyAbDoseSKIN;
  std::map<int,std::map<int,std::map<int,double> > >ZEnergyAbDoseTOTAL;
  std::map<int,std::map<int,std::map<int,double> > >ZEnergyDoseEqBFO;
  std::map<int,std::map<int,std::map<int,double> > >ZEnergyDoseEqSKIN;
  std::map<int,std::map<int,std::map<int,double> > >ZEnergyDoseEqTOTAL;

  std::map<int,std::map<int,std::map<int,double> > >ZEnergyAbDoseBFO_P;
  std::map<int,std::map<int,std::map<int,double> > >ZEnergyAbDoseSKIN_P;
  std::map<int,std::map<int,std::map<int,double> > >ZEnergyAbDoseTOTAL_P;
  std::map<int,std::map<int,std::map<int,double> > >ZEnergyDoseEqBFO_P;
  std::map<int,std::map<int,std::map<int,double> > >ZEnergyDoseEqSKIN_P;
  std::map<int,std::map<int,std::map<int,double> > >ZEnergyDoseEqTOTAL_P;

  std::map<int,std::map<int,std::map<int,double> > >ZEnergyAbDoseBFO_S;
  std::map<int,std::map<int,std::map<int,double> > >ZEnergyAbDoseSKIN_S;
  std::map<int,std::map<int,std::map<int,double> > >ZEnergyAbDoseTOTAL_S;
  std::map<int,std::map<int,std::map<int,double> > >ZEnergyDoseEqBFO_S;
  std::map<int,std::map<int,std::map<int,double> > >ZEnergyDoseEqSKIN_S;
  std::map<int,std::map<int,std::map<int,double> > >ZEnergyDoseEqTOTAL_S;

  std::map<int,std::map<int,std::map<int,std::map<TString,double> > > >ZEnergyAbDoseBFO_S_D;
  std::map<int,std::map<int,std::map<int,std::map<TString,double> > > >ZEnergyAbDoseSKIN_S_D;
  std::map<int,std::map<int,std::map<int,std::map<TString,double> > > >ZEnergyAbDoseTOTAL_S_D;
  std::map<int,std::map<int,std::map<int,std::map<TString,double> > > >ZEnergyDoseEqBFO_S_D;
  std::map<int,std::map<int,std::map<int,std::map<TString,double> > > >ZEnergyDoseEqSKIN_S_D;
  std::map<int,std::map<int,std::map<int,std::map<TString,double> > > >ZEnergyDoseEqTOTAL_S_D;

  std::map<TString,std::map<int,double> > totalAbDoseProtonS_D;
  std::map<TString,std::map<int,double> > totalDoseEqProtonS_D;
  std::map<TString,std::map<int,double> > bfoAbDoseProtonS_D;
  std::map<TString,std::map<int,double> > bfoDoseEqProtonS_D;
  std::map<TString,std::map<int,double> > skinAbDoseProtonS_D;
  std::map<TString,std::map<int,double> > skinDoseEqProtonS_D;
  
  std::map<TString,std::map<int,double> > totalAbDoseHeS_D;
  std::map<TString,std::map<int,double> > totalDoseEqHeS_D;
  std::map<TString,std::map<int,double> > bfoAbDoseHeS_D;
  std::map<TString,std::map<int,double> > bfoDoseEqHeS_D;
  std::map<TString,std::map<int,double> > skinAbDoseHeS_D;
  std::map<TString,std::map<int,double> > skinDoseEqHeS_D;
  
  std::map<TString,std::map<int,double> > totalAbDoseZ3_10S_D;
  std::map<TString,std::map<int,double> > totalDoseEqZ3_10S_D;
  std::map<TString,std::map<int,double> > bfoAbDoseZ3_10S_D;
  std::map<TString,std::map<int,double> > bfoDoseEqZ3_10S_D;
  std::map<TString,std::map<int,double> > skinAbDoseZ3_10S_D;
  std::map<TString,std::map<int,double> > skinDoseEqZ3_10S_D;
  
  std::map<TString,std::map<int,double> > totalAbDoseZ11_20S_D;
  std::map<TString,std::map<int,double> > totalDoseEqZ11_20S_D;
  std::map<TString,std::map<int,double> > bfoAbDoseZ11_20S_D;
  std::map<TString,std::map<int,double> > bfoDoseEqZ11_20S_D;
  std::map<TString,std::map<int,double> > skinAbDoseZ11_20S_D;
  std::map<TString,std::map<int,double> > skinDoseEqZ11_20S_D;
  
  std::map<TString,std::map<int,double> > totalAbDoseZ21_28S_D;
  std::map<TString,std::map<int,double> > totalDoseEqZ21_28S_D;
  std::map<TString,std::map<int,double> > bfoAbDoseZ21_28S_D;
  std::map<TString,std::map<int,double> > bfoDoseEqZ21_28S_D;
  std::map<TString,std::map<int,double> > skinAbDoseZ21_28S_D;
  std::map<TString,std::map<int,double> > skinDoseEqZ21_28S_D;

  ///////

  std::map<TString,std::pair<double,double> > totalAbDoseProtonAVGSTD_S_D;
  std::map<TString,std::pair<double,double> > totalDoseEqProtonAVGSTD_S_D;
  std::map<TString,std::pair<double,double> > bfoAbDoseProtonAVGSTD_S_D;
  std::map<TString,std::pair<double,double> > bfoDoseEqProtonAVGSTD_S_D;
  std::map<TString,std::pair<double,double> > skinAbDoseProtonAVGSTD_S_D;
  std::map<TString,std::pair<double,double> > skinDoseEqProtonAVGSTD_S_D;
  
  std::map<TString,std::pair<double,double> > totalAbDoseHeAVGSTD_S_D;
  std::map<TString,std::pair<double,double> > totalDoseEqHeAVGSTD_S_D;
  std::map<TString,std::pair<double,double> > bfoAbDoseHeAVGSTD_S_D;
  std::map<TString,std::pair<double,double> > bfoDoseEqHeAVGSTD_S_D;
  std::map<TString,std::pair<double,double> > skinAbDoseHeAVGSTD_S_D;
  std::map<TString,std::pair<double,double> > skinDoseEqHeAVGSTD_S_D;
  
  std::map<TString,std::pair<double,double> > totalAbDoseZ3_10AVGSTD_S_D;
  std::map<TString,std::pair<double,double> > totalDoseEqZ3_10AVGSTD_S_D;
  std::map<TString,std::pair<double,double> > bfoAbDoseZ3_10AVGSTD_S_D;
  std::map<TString,std::pair<double,double> > bfoDoseEqZ3_10AVGSTD_S_D;
  std::map<TString,std::pair<double,double> > skinAbDoseZ3_10AVGSTD_S_D;
  std::map<TString,std::pair<double,double> > skinDoseEqZ3_10AVGSTD_S_D;
  
  std::map<TString,std::pair<double,double> > totalAbDoseZ11_20AVGSTD_S_D;
  std::map<TString,std::pair<double,double> > totalDoseEqZ11_20AVGSTD_S_D;
  std::map<TString,std::pair<double,double> > bfoAbDoseZ11_20AVGSTD_S_D;
  std::map<TString,std::pair<double,double> > bfoDoseEqZ11_20AVGSTD_S_D;
  std::map<TString,std::pair<double,double> > skinAbDoseZ11_20AVGSTD_S_D;
  std::map<TString,std::pair<double,double> > skinDoseEqZ11_20AVGSTD_S_D;
  
  std::map<TString,std::pair<double,double> > totalAbDoseZ21_28AVGSTD_S_D;
  std::map<TString,std::pair<double,double> > totalDoseEqZ21_28AVGSTD_S_D;
  std::map<TString,std::pair<double,double> > bfoAbDoseZ21_28AVGSTD_S_D;
  std::map<TString,std::pair<double,double> > bfoDoseEqZ21_28AVGSTD_S_D;
  std::map<TString,std::pair<double,double> > skinAbDoseZ21_28AVGSTD_S_D;
  std::map<TString,std::pair<double,double> > skinDoseEqZ21_28AVGSTD_S_D;

  vector<double> totalAbDoseProton;
  vector<double> totalAbDoseProtonP;
  vector<double> totalAbDoseProtonS;
  vector<double> totalDoseEqProton;
  vector<double> totalDoseEqProtonP;
  vector<double> totalDoseEqProtonS;
  vector<double> bfoAbDoseProton;
  vector<double> bfoAbDoseProtonP;
  vector<double> bfoAbDoseProtonS;
  vector<double> bfoDoseEqProton;
  vector<double> bfoDoseEqProtonP;
  vector<double> bfoDoseEqProtonS;
  vector<double> skinAbDoseProton;
  vector<double> skinAbDoseProtonP;
  vector<double> skinAbDoseProtonS;
  vector<double> skinDoseEqProton;
  vector<double> skinDoseEqProtonP;
  vector<double> skinDoseEqProtonS;
  vector<double> totalAbDoseHe;
  vector<double> totalAbDoseHeP;
  vector<double> totalAbDoseHeS;
  vector<double> totalDoseEqHe;
  vector<double> totalDoseEqHeP;
  vector<double> totalDoseEqHeS;
  vector<double> bfoAbDoseHe;
  vector<double> bfoAbDoseHeP;
  vector<double> bfoAbDoseHeS;
  vector<double> bfoDoseEqHe;
  vector<double> bfoDoseEqHeP;
  vector<double> bfoDoseEqHeS;
  vector<double> skinAbDoseHe;
  vector<double> skinAbDoseHeP;
  vector<double> skinAbDoseHeS;
  vector<double> skinDoseEqHe;
  vector<double> skinDoseEqHeP;
  vector<double> skinDoseEqHeS;
  vector<double> totalAbDoseZ3_10;
  vector<double> totalAbDoseZ3_10P;
  vector<double> totalAbDoseZ3_10S;
  vector<double> totalDoseEqZ3_10;
  vector<double> totalDoseEqZ3_10P;
  vector<double> totalDoseEqZ3_10S;
  vector<double> bfoAbDoseZ3_10;
  vector<double> bfoAbDoseZ3_10P;
  vector<double> bfoAbDoseZ3_10S;
  vector<double> bfoDoseEqZ3_10;
  vector<double> bfoDoseEqZ3_10P;
  vector<double> bfoDoseEqZ3_10S;
  vector<double> skinAbDoseZ3_10;
  vector<double> skinAbDoseZ3_10P;
  vector<double> skinAbDoseZ3_10S;
  vector<double> skinDoseEqZ3_10;
  vector<double> skinDoseEqZ3_10P;
  vector<double> skinDoseEqZ3_10S;
  vector<double> totalAbDoseZ11_20;
  vector<double> totalAbDoseZ11_20P;
  vector<double> totalAbDoseZ11_20S;
  vector<double> totalDoseEqZ11_20;
  vector<double> totalDoseEqZ11_20P;
  vector<double> totalDoseEqZ11_20S;
  vector<double> bfoAbDoseZ11_20;
  vector<double> bfoAbDoseZ11_20P;
  vector<double> bfoAbDoseZ11_20S;
  vector<double> bfoDoseEqZ11_20;
  vector<double> bfoDoseEqZ11_20P;
  vector<double> bfoDoseEqZ11_20S;
  vector<double> skinAbDoseZ11_20;
  vector<double> skinAbDoseZ11_20P;
  vector<double> skinAbDoseZ11_20S;
  vector<double> skinDoseEqZ11_20;
  vector<double> skinDoseEqZ11_20P;
  vector<double> skinDoseEqZ11_20S;
  vector<double> totalAbDoseZ21_28;
  vector<double> totalAbDoseZ21_28P;
  vector<double> totalAbDoseZ21_28S;
  vector<double> totalDoseEqZ21_28;
  vector<double> totalDoseEqZ21_28P;
  vector<double> totalDoseEqZ21_28S;
  vector<double> bfoAbDoseZ21_28;
  vector<double> bfoAbDoseZ21_28P;
  vector<double> bfoAbDoseZ21_28S;
  vector<double> bfoDoseEqZ21_28;
  vector<double> bfoDoseEqZ21_28P;
  vector<double> bfoDoseEqZ21_28S;
  vector<double> skinAbDoseZ21_28;
  vector<double> skinAbDoseZ21_28P;
  vector<double> skinAbDoseZ21_28S;
  vector<double> skinDoseEqZ21_28;
  vector<double> skinDoseEqZ21_28P;
  vector<double> skinDoseEqZ21_28S;

  MarsRootEvent *Event;

  vector<string>  *HumanAstronautVolumeName;
  vector<double>  *HumanAstronautVolumeMass;

  vector<double> *WorldDimensions;
  vector<int>    *ActiveWall;

  string          *GeometryConfiguration;
  string          *BFieldConfiguration;

  string          outputFileName;
  string          outputFileNameRoot;

  // List of branches
  TBranch        *b_Event; //!
  TBranch        *b_HumanAstronautVolumeName;   //!
  TBranch        *b_HumanAstronautVolumeMass;   //!
  TBranch        *b_GeometryConfiguration;   //!
  TBranch        *b_BFieldConfiguration;   //!
  TBranch        *b_ActiveWall;   //!
  TBranch        *b_WorldDimensions;   //!

 
  CompleteDoseAnalyzer(TString fileName,TString dirName,bool useChain=false);
  virtual ~CompleteDoseAnalyzer();
  virtual Int_t    GetDoseEntry(Long64_t entry);
  virtual Int_t    GetAstronautEntry(Long64_t entry);
  virtual Int_t    GetSetupEntry(Long64_t entry);
  virtual Long64_t LoadDoseTree(Long64_t entry);
  virtual Long64_t LoadSetupTree(Long64_t entry);
  virtual Long64_t LoadAstronautTree(Long64_t entry);
  virtual double   QFactor(double L);
  virtual void     SetGCRFlux(TString flux);
  virtual void     SetCustomGCRFlux(TString flux);
  virtual void     ComputeGCRFlux();
  virtual void     ComputeCustomGCRFlux();
  virtual std::pair<double,double>   ComputeAvgStd(vector<double> v,double norm);
  virtual void     SetBarrelAnalysis();
  virtual void     SetDetectorNumber(int detNumb);
  virtual void     InitDose();
  virtual void     InitAstronaut();
  virtual void     InitSetup();
  virtual void     Loop();
  virtual void     ShowDose(Long64_t entry = -1);
  virtual void     ShowAstronaut(Long64_t entry = -1);
  virtual void     ShowSetup(Long64_t entry = -1);

};

#endif

#ifdef CompleteDoseAnalyzer_cxx
CompleteDoseAnalyzer::CompleteDoseAnalyzer(TString fileName,TString dirName,bool useChain): 
fTreeDose(0),fTreeSetup(0),fTreeAstronaut(0){
  fluxFile="creme96_Z1_28_solarmin.flx";
  fluxAlreadyComputed=false;
  onlyBarrel=false;
  totalDetectorNumber=1;
  TString dosePath = dirName+"/Dose";
  TString setupPath = dirName+"/Setup";
  TString astroPath = dirName+"/AstronautGeometry";  
  fTreeDose = new TChain(dosePath);
  fTreeSetup = new TChain(setupPath);
  fTreeAstronaut = new TChain(astroPath);
  if(useChain){
    std::ifstream File_Input(fileName);
    if (!File_Input){
      std::cout<<"List of ROOT file NOT found"<<std::endl;
      return;
    }
    while(!File_Input.eof()){
      TString singlefile = "noName";
      File_Input>> singlefile;
      if(singlefile!="noName"&&singlefile!=""){
        fTreeDose->Add(singlefile);
        fTreeSetup->Add(singlefile);
        fTreeAstronaut->Add(singlefile);
      }
    }
    File_Input.close();
    InitDose();
    InitSetup();
    InitAstronaut();
  }else{
    fTreeDose->Add(fileName);
    fTreeSetup->Add(fileName);
    fTreeAstronaut->Add(fileName);
    InitDose();
    InitSetup();
    InitAstronaut();
  }
  ComputeGCRFlux();
  string temp(fileName);
  std::size_t pos = temp.find(".");
  outputFileName = temp.substr(0,pos);
  outputFileNameRoot = outputFileName;
  outputFileName.append("DoseResults.dat");
  outputFileNameRoot.append("SpectraAndVertexResults.root");
  
}

CompleteDoseAnalyzer::~CompleteDoseAnalyzer(){
  if (!fTreeDose) return;
  delete fTreeDose->GetCurrentFile();
  if (!fTreeSetup) return;
  delete fTreeSetup->GetCurrentFile();
  if (!fTreeAstronaut) return;
  delete fTreeAstronaut->GetCurrentFile();
}
 
 
Int_t CompleteDoseAnalyzer::GetDoseEntry(Long64_t entry){

  // Read contents of entry.
  if (!fTreeDose) return 0;
  return fTreeDose->GetEntry(entry);
}

Int_t CompleteDoseAnalyzer::GetAstronautEntry(Long64_t entry){

  // Read contents of entry.
  if (!fTreeAstronaut) return 0;
  return fTreeAstronaut->GetEntry(entry);
}
  
Int_t CompleteDoseAnalyzer::GetSetupEntry(Long64_t entry){

  // Read contents of entry.
  if (!fTreeSetup) return 0;
  return fTreeSetup->GetEntry(entry);
}

Long64_t CompleteDoseAnalyzer::LoadDoseTree(Long64_t entry){

  // Set the environment to read one entry
  if (!fTreeDose) return -5;

  Long64_t centry = fTreeDose->LoadTree(entry);
  if (centry < 0) return centry;
  if (fTreeDose->GetTreeNumber() != fTreeCurrentDose) {
    fTreeCurrentDose = fTreeDose->GetTreeNumber();
  }
  return centry;

}

Long64_t CompleteDoseAnalyzer::LoadAstronautTree(Long64_t entry){

  // Set the environment to read one entry
  if (!fTreeAstronaut) return -5;
  Long64_t centry = fTreeAstronaut->LoadTree(entry);
  if (centry < 0) return centry;
  if (fTreeAstronaut->GetTreeNumber() != fTreeCurrentAstronaut) {
    fTreeCurrentAstronaut = fTreeAstronaut->GetTreeNumber();
  }
  return centry;
}

Long64_t CompleteDoseAnalyzer::LoadSetupTree(Long64_t entry){

  // Set the environment to read one entry
  if (!fTreeSetup) return -5;
  Long64_t centry = fTreeSetup->LoadTree(entry);
  if (centry < 0) return centry;
  if (fTreeSetup->GetTreeNumber() != fTreeCurrentSetup) {
    fTreeCurrentSetup = fTreeSetup->GetTreeNumber();
  }
  return centry;
}

void CompleteDoseAnalyzer::InitDose(){
  // The Init() function is called when the selector needs to initialize
  // a new tree or chain. Typically here the branch addresses and branch
  // pointers of the tree will be set.
  // It is normally not necessary to make changes to the generated
  // code, but the routine can be extended by the user if needed.
  // Init() will be called many times when running on PROOF
  // (once per file to be processed).
  
  // Set object pointer
  Event = 0;
  // Set branch addresses and branch pointers
  if (!fTreeDose) return;
  fTreeCurrentDose = -1;
  
  fTreeDose->SetBranchAddress("Event", &Event, &b_Event);
}



void CompleteDoseAnalyzer::InitAstronaut(){
  // The Init() function is called when the selector needs to initialize
  // a new tree or chain. Typically here the branch addresses and branch
  // pointers of the tree will be set.
  // It is normally not necessary to make changes to the generated
  // code, but the routine can be extended by the user if needed.
  // Init() will be called many times when running on PROOF
  // (once per file to be processed).
  
  // Set object pointer
  HumanAstronautVolumeName = 0;
  HumanAstronautVolumeMass = 0;
  // Set branch addresses and branch pointers
  if (!fTreeAstronaut) return;
  fTreeCurrentAstronaut = -1;
  
  fTreeAstronaut->SetBranchAddress("HumanAstronautVolumeName", &HumanAstronautVolumeName, &b_HumanAstronautVolumeName);
  fTreeAstronaut->SetBranchAddress("HumanAstronautVolumeMass", &HumanAstronautVolumeMass, &b_HumanAstronautVolumeMass);
}

void CompleteDoseAnalyzer::InitSetup(){

  // The Init() function is called when the selector needs to initialize
  // a new tree or chain. Typically here the branch addresses and branch
  // pointers of the tree will be set.
  // It is normally not necessary to make changes to the generated
  // code, but the routine can be extended by the user if needed.
  // Init() will be called many times when running on PROOF
  // (once per file to be processed).
  
  // Set object pointer
  GeometryConfiguration = 0;
  BFieldConfiguration = 0;
  // Set branch addresses and branch pointers
  if (!fTreeSetup) return;
  fTreeCurrentSetup = -1;
  
  fTreeSetup->SetBranchAddress("GeometryConfiguration", &GeometryConfiguration, &b_GeometryConfiguration);
  fTreeSetup->SetBranchAddress("BFieldConfiguration", &BFieldConfiguration, &b_BFieldConfiguration);
  fTreeSetup->SetBranchAddress("WorldDimensions", &WorldDimensions, &b_WorldDimensions);
  fTreeSetup->SetBranchAddress("ActiveWall", &ActiveWall, &b_ActiveWall);
}

void CompleteDoseAnalyzer::ShowDose(Long64_t entry){

  // Print contents of entry.
  // If entry is not specified, print current entry
  if (!fTreeDose) return;
  fTreeDose->Show(entry);
}

void CompleteDoseAnalyzer::ShowSetup(Long64_t entry){

  // Print contents of entry.
  // If entry is not specified, print current entry
  if (!fTreeSetup) return;
  fTreeSetup->Show(entry);
}

void CompleteDoseAnalyzer::ShowAstronaut(Long64_t entry){

  // Print contents of entry.
  // If entry is not specified, print current entry
  if (!fTreeAstronaut) return;
  fTreeAstronaut->Show(entry);
}
#endif // #ifdef CompleteDoseAnalyzer_cxx
