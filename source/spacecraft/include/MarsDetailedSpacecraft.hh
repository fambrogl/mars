//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id$
//
// Code developed by: S.Guatelli, guatelli@ge.infn.it
//

#ifndef MarsDetailedSpacecraft_h
#define MarsDetailedSpacecraft_h 1

#include "globals.hh"
#include "MarsVGeometryComponent.hh"
#include "MarsFluenceSD.hh"

class G4VPhysicalVolume;
class G4Tubs;
class G4SubtractionSolid;
class G4LogicalVolume;
class G4Material;
class MarsMaterial;
class G4VisAttributes;
class MarsDetailedSpacecraftMessenger;

class MarsDetailedSpacecraft: public MarsVGeometryComponent
{
public:
  MarsDetailedSpacecraft();
  ~MarsDetailedSpacecraft();
  void ConstructComponent(G4VPhysicalVolume*);
  void DestroyComponent(); 
  
  void ConfigureSpacecraft(G4double aOuterRadius,G4double aHalfLength,G4double afluenceStructureDistance);

private:
  MarsMaterial* pMaterial;
  MarsDetailedSpacecraftMessenger* messenger;

  G4VPhysicalVolume* spacecraftPhys;  
  G4VPhysicalVolume* spacecraftFluenceVolumePhys;  
  G4VPhysicalVolume* layerHabitatPhys;  
  G4VPhysicalVolume* layer1Phys;
  G4VPhysicalVolume* layer2Phys; 
  G4VPhysicalVolume* layer3Phys;
  G4VPhysicalVolume* layer4Phys;
  G4VPhysicalVolume* layer5Phys;
  G4VPhysicalVolume* layer6Phys;
  G4VPhysicalVolume* layer7Phys;
  G4VPhysicalVolume* layer8Phys;
  G4VPhysicalVolume* layer9Phys;
  G4VPhysicalVolume* layer10Phys;
  G4VPhysicalVolume* layer11Phys; 
  G4VPhysicalVolume* layer12Phys;
  G4VPhysicalVolume* layer13Phys; 
  G4VPhysicalVolume* layer14Phys;
  G4VPhysicalVolume* layer15Phys;
  G4VPhysicalVolume* layerPhys;
  G4VPhysicalVolume* layer16Phys; 
  G4VPhysicalVolume* layer17Phys;
  G4VPhysicalVolume* layer18Phys;
  G4VPhysicalVolume* layer19Phys; 
  G4VPhysicalVolume* layer20Phys;
  G4VPhysicalVolume* layer21Phys;
  G4VPhysicalVolume* layer22Phys;
  G4VPhysicalVolume* layer23Phys;
  G4VPhysicalVolume* layer24Phys;
  G4VPhysicalVolume* layer25Phys;
  G4VPhysicalVolume* layer26Phys;
  G4VPhysicalVolume* layer27Phys;
  G4Tubs* spacecraft;
  G4LogicalVolume* spacecraftLog;
  G4Tubs* spacecraftFluenceVolume;
  G4LogicalVolume* spacecraftFluenceVolumeLog;
  G4Tubs* layer1O;
  G4Tubs* layer1I;
  G4SubtractionSolid* layer1;
  G4LogicalVolume* layer1Log;
  G4Tubs* layer2O;
  G4Tubs* layer2I;
  G4SubtractionSolid* layer2;
  G4LogicalVolume* layer2Log;
  G4Tubs* layer3O;
  G4Tubs* layer3I;
  G4SubtractionSolid* layer3;
  G4LogicalVolume* layer3Log;
  G4Tubs* layer4O;
  G4Tubs* layer4I;
  G4SubtractionSolid* layer4;
  G4LogicalVolume* layer4Log;
  G4Tubs* layer5O;
  G4Tubs* layer5I;
  G4SubtractionSolid* layer5;
  G4LogicalVolume* layer5Log;
  G4Tubs* layer6O;
  G4Tubs* layer6I;
  G4SubtractionSolid* layer6;
  G4LogicalVolume* layer6Log;
  G4Tubs* layer7O;
  G4Tubs* layer7I;
  G4SubtractionSolid* layer7;
  G4LogicalVolume* layer7Log;
  G4Tubs* layer8O;
  G4Tubs* layer8I;
  G4SubtractionSolid* layer8;
  G4LogicalVolume* layer8Log;
  G4Tubs* layer9O;
  G4Tubs* layer9I;
  G4SubtractionSolid* layer9;
  G4LogicalVolume* layer9Log;
  G4Tubs* layer10O;
  G4Tubs* layer10I;
  G4SubtractionSolid* layer10;
  G4LogicalVolume* layer10Log;
  G4Tubs* layer11O;
  G4Tubs* layer11I;
  G4SubtractionSolid* layer11;
  G4LogicalVolume* layer11Log;
  G4Tubs* layer12O;
  G4Tubs* layer12I;
  G4SubtractionSolid* layer12;
  G4LogicalVolume* layer12Log;
  G4Tubs* layer13O;
  G4Tubs* layer13I;
  G4SubtractionSolid* layer13;
  G4LogicalVolume* layer13Log;
  G4Tubs* layer14O;
  G4Tubs* layer14I;
  G4SubtractionSolid* layer14;
  G4LogicalVolume* layer14Log;
  G4Tubs* layer15O;
  G4Tubs* layer15I;
  G4SubtractionSolid* layer15;
  G4LogicalVolume* layer15Log;
  G4Tubs* layerO;
  G4Tubs* layerI;
  G4SubtractionSolid* layer;
  G4LogicalVolume* layerLog;
  G4Tubs* layer16O;
  G4Tubs* layer16I;
  G4SubtractionSolid* layer16;
  G4LogicalVolume* layer16Log;
  G4Tubs* layer17O;
  G4Tubs* layer17I;
  G4SubtractionSolid* layer17;
  G4LogicalVolume* layer17Log;
  G4Tubs* layer18O;
  G4Tubs* layer18I;
  G4SubtractionSolid* layer18;
  G4LogicalVolume* layer18Log;
  G4Tubs* layer19O;
  G4Tubs* layer19I;
  G4SubtractionSolid* layer19;
  G4LogicalVolume* layer19Log;
  G4Tubs* layer20O;
  G4Tubs* layer20I;
  G4SubtractionSolid* layer20;
  G4LogicalVolume* layer20Log;
  G4Tubs* layer21O;
  G4Tubs* layer21I;
  G4SubtractionSolid* layer21;
  G4LogicalVolume* layer21Log;
  G4Tubs* layer22O;
  G4Tubs* layer22I;
  G4SubtractionSolid* layer22;
  G4LogicalVolume* layer22Log;
  G4Tubs* layer23O;
  G4Tubs* layer23I;
  G4SubtractionSolid* layer23;
  G4LogicalVolume* layer23Log;
  G4Tubs* layer24O;
  G4Tubs* layer24I;
  G4SubtractionSolid* layer24;
  G4LogicalVolume* layer24Log;
  G4Tubs* layer25O;
  G4Tubs* layer25I;
  G4SubtractionSolid* layer25;
  G4LogicalVolume* layer25Log;
  G4Tubs* layer26O;
  G4Tubs* layer26I;
  G4SubtractionSolid* layer26;
  G4LogicalVolume* layer26Log;
  G4Tubs* layer27O;
  G4Tubs* layer27I;
  G4SubtractionSolid* layer27;
  G4LogicalVolume* layer27Log;
  G4Tubs* layerHabitat;
  G4LogicalVolume* layerHabitatLog;  

  G4double structureOuterRadius;
  G4double structureHalfLength;
  G4double fluenceStructureDistance;

  MarsFluenceSD* fluenceSD;




};
#endif
