//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id$
//
// Code developed by: S.Guatelli, guatelli@ge.infn.it
//

#ifndef MarsSimplifiedSpacecraft_h
#define MarsSimplifiedSpacecraft_h 1
#include "MarsVGeometryComponent.hh"
#include "globals.hh"
#include "MarsFluenceSD.hh"

class G4VPhysicalVolume;
class G4Tubs;
class G4LogicalVolume;
class G4Material;
class MarsMaterial;
class G4VisAttributes;
class MarsSimplifiedSpacecraftMessenger;

class MarsSimplifiedSpacecraft: public MarsVGeometryComponent
{
public:
  MarsSimplifiedSpacecraft();
  ~MarsSimplifiedSpacecraft();
  void ConstructComponent(G4VPhysicalVolume*);
  void DestroyComponent(); 
  void ConfigureSpacecraft(G4String aMaterial,G4double aThickness, G4double aInnerRadius, G4double aHalfLength, G4double afluenceStructureDistance);
  
private:
  MarsMaterial* pMaterial;
  MarsSimplifiedSpacecraftMessenger* messenger;
  G4VPhysicalVolume* spacecraftBarrelPhys;
  G4VPhysicalVolume* spacecraftFluenceVolumePhys;
  G4VPhysicalVolume* spacecraftEndcapMinusPhys;
  G4VPhysicalVolume* spacecraftEndcapPlusPhys;
  G4VPhysicalVolume* spacecraftHabitatPhys;

  G4Tubs* spacecraftHabitat;
  G4Tubs* spacecraftFluenceVolume;
  G4Tubs* spacecraftBarrel;
  G4Tubs* spacecraftEndcap;

  G4LogicalVolume* spacecraftHabitatLog;
  G4LogicalVolume* spacecraftFluenceVolumeLog;
  G4LogicalVolume* spacecraftBarrelLog;
  G4LogicalVolume* spacecraftEndcapLog;

  G4String structureMaterial;
  G4double structureThickness;
  G4double structureInnerRadius;
  G4double structureHalfLength;
  G4double fluenceStructureDistance;

  MarsFluenceSD* fluenceSD;

};
#endif
