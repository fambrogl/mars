//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id$
//
// Code developed by: S.Guatelli, guatelli@ge.infn.it and F. Ambroglini filippo.ambroglini@pg.infn.it 
//
#include "MarsDetailedSpacecraft.hh"
#include "MarsDetailedSpacecraftMessenger.hh"
#include "MarsMaterial.hh"

#include "G4SystemOfUnits.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "G4UserLimits.hh"
#include "G4SubtractionSolid.hh"
#include "G4SDManager.hh"


MarsDetailedSpacecraft::MarsDetailedSpacecraft():
  spacecraftPhys(0),spacecraftFluenceVolumePhys(0),layerHabitatPhys(0),
  layer1Phys(0),layer2Phys(0),layer3Phys(0),layer4Phys(0),
  layer5Phys(0),layer6Phys(0),layer7Phys(0),layer8Phys(0),
  layer9Phys(0),layer10Phys(0),layer11Phys(0),layer12Phys(0), 
  layer13Phys(0),layer14Phys(0),layer15Phys(0),layerPhys(0),
  layer16Phys(0),layer17Phys(0),layer18Phys(0),layer19Phys(0), 
  layer20Phys(0),layer21Phys(0),layer22Phys(0),layer23Phys(0), 
  layer24Phys(0),layer25Phys(0),layer26Phys(0),layer27Phys(0),
  spacecraft(0),spacecraftLog(0),spacecraftFluenceVolume(0),spacecraftFluenceVolumeLog(0),
  layer1O(0),layer1I(0),layer1(0),layer1Log(0),
  layer2O(0),layer2I(0),layer2(0),layer2Log(0),
  layer3O(0),layer3I(0),layer3(0),layer3Log(0),
  layer4O(0),layer4I(0),layer4(0),layer4Log(0),
  layer5O(0),layer5I(0),layer5(0),layer5Log(0),
  layer6O(0),layer6I(0),layer6(0),layer6Log(0),
  layer7O(0),layer7I(0),layer7(0),layer7Log(0),
  layer8O(0),layer8I(0),layer8(0),layer8Log(0),
  layer9O(0),layer9I(0),layer9(0),layer9Log(0),
  layer10O(0),layer10I(0),layer10(0),layer10Log(0),
  layer11O(0),layer11I(0),layer11(0),layer11Log(0),
  layer12O(0),layer12I(0),layer12(0),layer12Log(0),
  layer13O(0),layer13I(0),layer13(0),layer13Log(0),
  layer14O(0),layer14I(0),layer14(0),layer14Log(0),
  layer15O(0),layer15I(0),layer15(0),layer15Log(0),
  layerO(0),layerI(0),layer(0),layerLog(0),
  layer16O(0),layer16I(0),layer16(0),layer16Log(0),
  layer17O(0),layer17I(0),layer17(0),layer17Log(0),
  layer18O(0),layer18I(0),layer18(0),layer18Log(0),
  layer19O(0),layer19I(0),layer19(0),layer19Log(0),
  layer20O(0),layer20I(0),layer20(0),layer20Log(0),
  layer21O(0),layer21I(0),layer21(0),layer21Log(0),
  layer22O(0),layer22I(0),layer22(0),layer22Log(0),
  layer23O(0),layer23I(0),layer23(0),layer23Log(0),
  layer24O(0),layer24I(0),layer24(0),layer24Log(0),
  layer25O(0),layer25I(0),layer25(0),layer25Log(0),
  layer26O(0),layer26I(0),layer26(0),layer26Log(0),
  layer27O(0),layer27I(0),layer27(0),layer27Log(0),
  layerHabitat(0),layerHabitatLog(0)
{
  pMaterial = new MarsMaterial();
  messenger = new MarsDetailedSpacecraftMessenger(this);
  
  structureOuterRadius = 3*m;
  structureHalfLength  = 5*m;

  G4SDManager* SDManager = G4SDManager::GetSDMpointer();
  fluenceSD   = new MarsFluenceSD("fluenceSD");
  SDManager->AddNewDetector(fluenceSD);
}
MarsDetailedSpacecraft::~MarsDetailedSpacecraft()
{
  delete pMaterial;
  delete messenger;
}

void MarsDetailedSpacecraft::ConfigureSpacecraft(G4double aOuterRadius,G4double aHalfLength,G4double aFluenceStructureDistance)
{
  structureOuterRadius = aOuterRadius;
  structureHalfLength = aHalfLength;
  fluenceStructureDistance= aFluenceStructureDistance;
}

void MarsDetailedSpacecraft::ConstructComponent(G4VPhysicalVolume* motherVolume)
{

  G4double thick = 0.03*cm;
  G4double rMax = structureOuterRadius;
  G4double rMin = rMax -thick;
  G4double zLength = structureHalfLength;
  
  
  G4Material* betacloth = pMaterial->GetMaterial("betacloth");  
  G4Material* vacuum = pMaterial->GetMaterial("Galactic") ;
  
  spacecraft = new G4Tubs("spacecraft",0,rMax,zLength,0,2*pi);
  spacecraftLog = new G4LogicalVolume(spacecraft,
				      vacuum,
				      "spacecraftLog",
				      0,0,0);
  spacecraftPhys = new G4PVPlacement(0,
				     G4ThreeVector(0.,0.,0.),
				     "spacecraftPhys", 
				     spacecraftLog,
				     motherVolume,
				     false,
				     0,true);
  //betacloth layer
  layer1O = new G4Tubs("layer1O",0,rMax,zLength,0,2*pi);
  layer1I = new G4Tubs("layer1I",0,rMin,zLength-thick,0,2*pi);
  
  layer1 = new G4SubtractionSolid("layer1",layer1O,layer1I);
  
  layer1Log = new G4LogicalVolume(layer1,
				  betacloth,
				  "layer1Log",
				  0,0,0);
  
  layer1Phys = new G4PVPlacement(0,
                                 G4ThreeVector(0.,0.,0.),
                                 "layer1Phys", 
				 layer1Log,
				 spacecraftPhys,
				 false,
				 0,true);
    
  // Mylar layer
  rMax = rMin;
  zLength -= thick;
  thick = 0.01*cm;
  rMin -= thick;
  
  G4Material* mylar = pMaterial->GetMaterial("mylar"); 
    
  layer2O = new G4Tubs("layer2O",0,rMax,zLength,0,2*pi);
  layer2I = new G4Tubs("layer2I",0,rMin,zLength-thick,0,2*pi);

  layer2 = new G4SubtractionSolid("layer2",layer2O,layer2I);

  layer2Log = new G4LogicalVolume(layer2,
				  mylar,
				  "layer2Log",
				  0,0,0);
  layer2Phys = new G4PVPlacement(0,
                                 G4ThreeVector(0.,0.,0.),
                                 "layer2Phys", 
                                 layer2Log,
                                 spacecraftPhys,
				 false,0,true);
  // Nextel layer
  rMax = rMin;
  zLength -= thick;
  thick = 0.1*cm;
  rMin -= thick;
  
  G4Material* nextel = pMaterial->GetMaterial("Nextel312AF62"); 
  
  layer3O = new G4Tubs("layer3O",0,rMax,zLength,0,2*pi);
  layer3I = new G4Tubs("layer3I",0,rMin,zLength-thick,0,2*pi);

  layer3 = new G4SubtractionSolid("layer3",layer3O,layer3I);

  layer3Log = new G4LogicalVolume(layer3,
				  nextel,
				  "layer3Log",
				  0,0,0);

  layer3Phys = new G4PVPlacement(0,
				 G4ThreeVector(0.,0.,0.),
				 "layer3Phys", 
				 layer3Log,
				 spacecraftPhys,
				 false,0,true);
  rMax = rMin;
  zLength -= thick;
  thick = 10.06*cm;
  rMin -= thick;
 
  layer4O = new G4Tubs("layer4O",0,rMax,zLength,0,2*pi);
  layer4I = new G4Tubs("layer4I",0,rMin,zLength-thick,0,2*pi);

  layer4 = new G4SubtractionSolid("layer4",layer4O,layer4I);

  layer4Log = new G4LogicalVolume(layer4,
				  vacuum,
				  "layer4Log",
				  0,0,0);
  layer4Phys = new G4PVPlacement(0,
                                 G4ThreeVector(0.,0.,0.),
                                 "layer4Phys", 
                                 layer4Log,
                                 spacecraftPhys,
				 false,0,true);
  // Nextel layer
  rMax = rMin;
  zLength -= thick;
  thick = 0.1*cm;
  rMin -= thick;
  layer5O = new G4Tubs("layer5O",0,rMax,zLength,0,2*pi);
  layer5I = new G4Tubs("layer5I",0,rMin,zLength-thick,0,2*pi);

  layer5 = new G4SubtractionSolid("layer5",layer5O,layer5I);

  layer5Log = new G4LogicalVolume(layer5,
				  nextel,
				  "layer5Log",
				  0,0,0);
  layer5Phys = new G4PVPlacement(0,
                                 G4ThreeVector(0.,0.,0.),
                                 "layer5Phys", 
                                 layer5Log,
                                 spacecraftPhys,
				 false,0,true);
  //Vacuum layer 
  rMax = rMin;
  zLength -= thick;
  thick = 10.06 * cm;
  rMin -= thick;
  layer6O = new G4Tubs("layer6O",0,rMax,zLength,0,2*pi);
  layer6I = new G4Tubs("layer6I",0,rMin,zLength-thick,0,2*pi);

  layer6 = new G4SubtractionSolid("layer6",layer6O,layer6I);

  layer6Log = new G4LogicalVolume(layer6,
				  vacuum,
				  "layer6Log",
				  0,0,0);
  layer6Phys = new G4PVPlacement(0,
                                 G4ThreeVector(0.,0.,0),
                                 "layer6Phys", 
                                 layer6Log,
                                 spacecraftPhys,
				 false,0,true);
  // Nextel layer 
  rMax = rMin;
  zLength -= thick;
  thick = 0.2*cm;
  rMin -= thick;
  layer7O = new G4Tubs("layer7O",0,rMax,zLength,0,2*pi);
  layer7I = new G4Tubs("layer7I",0,rMin,zLength-thick,0,2*pi);

  layer7 = new G4SubtractionSolid("layer7",layer7O,layer7I);

  layer7Log = new G4LogicalVolume(layer7,
				  nextel,
				  "layer7Log",
				  0,0,0);
  layer7Phys = new G4PVPlacement(0,
                                 G4ThreeVector(0.,0.,0.),
                                 "layer7Phys", 
                                 layer7Log,
                                 spacecraftPhys,
				 false,0,true);
  // mylar layer
  rMax = rMin;
  zLength -= thick;
  thick = 0.2*cm;
  rMin -= thick;
  layer8O = new G4Tubs("layer8O",0,rMax,zLength,0,2*pi);
  layer8I = new G4Tubs("layer8I",0,rMin,zLength-thick,0,2*pi);

  layer8 = new G4SubtractionSolid("layer8",layer8O,layer8I);

  layer8Log = new G4LogicalVolume(layer8,
				  mylar,
				  "layer8Log",
				  0,0,0);
  layer8Phys = new G4PVPlacement(0,
				 G4ThreeVector(0.,0.,0.),
				 "layer8Phys", 
				 layer8Log,
				 spacecraftPhys,
				 false,0,true);
  //Vacuum layer
  rMax = rMin;
  zLength -= thick;
  thick = 10.06*cm;
  rMin -= thick;
  layer9O = new G4Tubs("layer9O",0,rMax,zLength,0,2*pi);
  layer9I = new G4Tubs("layer9I",0,rMin,zLength-thick,0,2*pi);

  layer9 = new G4SubtractionSolid("layer9",layer9O,layer9I);

  layer9Log = new G4LogicalVolume(layer9,
				  vacuum,
				  "layer9Log",
				  0,0,0);
  layer9Phys = new G4PVPlacement(0,
                                 G4ThreeVector(0.,0.,0.),
                                 "layer9Phys", 
                                 layer9Log,
                                 spacecraftPhys,
				 false,0,true);
  // mylar layer
  rMax = rMin;
  zLength -= thick;
  thick = 0.2*cm;
  rMin -= thick;
  layer10O = new G4Tubs("layer10O",0,rMax,zLength,0,2*pi);
  layer10I = new G4Tubs("layer10I",0,rMin,zLength-thick,0,2*pi);

  layer10 = new G4SubtractionSolid("layer10",layer10O,layer10I);

  layer10Log = new G4LogicalVolume(layer10,
				   mylar,
				   "layer10Log",
				   0,0,0);
  layer10Phys = new G4PVPlacement(0,
                                  G4ThreeVector(0.,0.,0.),
                                  "layer10Phys", 
                                  layer10Log,
                                  spacecraftPhys,
				  false,0,true);
  //Redundant Bladder
  //Polyethylene
  G4Material* polyethilene = pMaterial->GetMaterial("polyethylene"); 
  rMax = rMin;
  zLength -= thick;
  thick = 0.0028*cm;
  rMin -= thick;
  layer11O = new G4Tubs("layer11O",0,rMax,zLength,0,2*pi);
  layer11I = new G4Tubs("layer11I",0,rMin,zLength-thick,0,2*pi);

  layer11 = new G4SubtractionSolid("layer11",layer11O,layer11I);

  layer11Log = new G4LogicalVolume(layer11,
				   polyethilene,
				   "layer11Log",
				   0,0,0);
  layer11Phys = new G4PVPlacement(0,
                                  G4ThreeVector(0.,0.,0.),
                                  "layer11Phys", 
                                  layer11Log,
                                  spacecraftPhys,
  				  false,0,true);
  //ployacrylate
  rMax = rMin;
  zLength -= thick;
  thick = 0.0028*cm;
  rMin -= thick;

  G4Material* polyacrylate = pMaterial ->GetMaterial("polyacrylate"); 
  
  layer12O = new G4Tubs("layer12O",0,rMax,zLength,0,2*pi);
  layer12I = new G4Tubs("layer12I",0,rMin,zLength-thick,0,2*pi);

  layer12 = new G4SubtractionSolid("layer12",layer12O,layer12I);

  layer12Log = new G4LogicalVolume(layer12,
				   polyacrylate,
				   "layer12Log",
				   0,0,0);
  layer12Phys = new G4PVPlacement(0,
                                  G4ThreeVector(0.,0.,0.),
                                  "layer12Phys", 
                                  layer12Log,
                                  spacecraftPhys,
  				  false,0,true);
  //evoh layer
  rMax = rMin;
  zLength -= thick;
  thick = 0.0028*cm;
  rMin -= thick;
  
  G4Material* evoh = pMaterial ->GetMaterial("evoh"); 
  
  layer13O = new G4Tubs("layer13O",0,rMax,zLength,0,2*pi);
  layer13I = new G4Tubs("layer13I",0,rMin,zLength-thick,0,2*pi);

  layer13 = new G4SubtractionSolid("layer13",layer13O,layer13I);

  layer13Log = new G4LogicalVolume(layer13,
				   evoh,
				   "layer13Log",
				   0,0,0);
  layer13Phys = new G4PVPlacement(0,
                                  G4ThreeVector(0.,0.,0.),
                                  "layer13Phys", 
                                  layer13Log,
                                  spacecraftPhys,
  				  false,0,true);
  //polyacrylate
  rMax = rMin;
  zLength -= thick;
  thick = 0.0028*cm;
  rMin -= thick;
  layer14O = new G4Tubs("layer14O",0,rMax,zLength,0,2*pi);
  layer14I = new G4Tubs("layer14I",0,rMin,zLength-thick,0,2*pi);

  layer14 = new G4SubtractionSolid("layer14",layer14O,layer14I);

  layer14Log = new G4LogicalVolume(layer14,
				   polyacrylate,
				   "layer14Log",
				   0,0,0);
  layer14Phys = new G4PVPlacement(0,
                                  G4ThreeVector(0.,0.,0.),
                                  "layer14Phys", 
                                  layer14Log,
                                  spacecraftPhys,
  				  false,0,true);
  //polyethilene
  rMax = rMin;
  zLength -= thick;
  thick = 0.0028*cm;
  rMin -= thick;
  layer15O = new G4Tubs("layer15O",0,rMax,zLength,0,2*pi);
  layer15I = new G4Tubs("layer15I",0,rMin,zLength-thick,0,2*pi);

  layer15 = new G4SubtractionSolid("layer15",layer15O,layer15I);

  layer15Log = new G4LogicalVolume(layer15,
				   polyethilene,
				   "layer15Log",
				   0,0,0);
  layer15Phys = new G4PVPlacement(0,
                                  G4ThreeVector(0.,0.,0.),
                                  "layer15Phys", 
                                  layer15Log,
                                  spacecraftPhys,
  				  false,0,true);
  //kevlar layer
  rMax = rMin;
  zLength -= thick;
  thick = 0.026*cm;
  rMin -= thick;

  G4Material* kevlarAir = pMaterial->GetMaterial("kevlarAir"); 
  
  layerO = new G4Tubs("layerO",0,rMax,zLength,0,2*pi);
  layerI = new G4Tubs("layerI",0,rMin,zLength-thick,0,2*pi);

  layer = new G4SubtractionSolid("layer",layerO,layerI);

  layerLog = new G4LogicalVolume(layer,
				 kevlarAir,
				 "layerLog",
				 0,0,0);
  layerPhys = new G4PVPlacement(0,
                                G4ThreeVector(0.,0.,0.),
                                "layerPhys", 
                                layerLog,
                                spacecraftPhys,
  			        false,0,true);
  //polyethilene layer
  rMax = rMin;
  zLength -= thick;
  thick = 0.0028*cm;
  rMin -= thick;
  layer16O = new G4Tubs("layer16O",0,rMax,zLength,0,2*pi);
  layer16I = new G4Tubs("layer16I",0,rMin,zLength-thick,0,2*pi);

  layer16 = new G4SubtractionSolid("layer16",layer16O,layer16I);

  layer16Log = new G4LogicalVolume(layer16,
				   polyethilene,				    
				   "layer16Log",
				   0,0,0);
  layer16Phys = new G4PVPlacement(0,
                                  G4ThreeVector(0.,0.,0.),
				  "layer16Phys", 
				  layer16Log,
				  spacecraftPhys,
				  false,0,true);
  //polyacrylate layer
  rMax = rMin;
  zLength -= thick;
  thick = 0.0028*cm;
  rMin -= thick;
  layer17O = new G4Tubs("layer17O",0,rMax,zLength,0,2*pi);
  layer17I = new G4Tubs("layer17I",0,rMin,zLength-thick,0,2*pi);

  layer17 = new G4SubtractionSolid("layer17",layer17O,layer17I);

  layer17Log = new G4LogicalVolume(layer17,
				   polyacrylate,
				   "layer17Log",
				   0,0,0);
  layer17Phys = new G4PVPlacement(0,
                                  G4ThreeVector(0.,0.,0.),
				  "layer17Phys", 
				  layer17Log,
				  spacecraftPhys,
				  false,0,true);
  //evoh
  rMax = rMin;
  zLength -= thick;
  thick = 0.0028*cm;
  rMin -= thick;
  layer18O = new G4Tubs("layer18O",0,rMax,zLength,0,2*pi);
  layer18I = new G4Tubs("layer18I",0,rMin,zLength-thick,0,2*pi);

  layer18 = new G4SubtractionSolid("layer18",layer18O,layer18I);

  layer18Log = new G4LogicalVolume(layer18,
				   evoh,
				   "layer18Log",
				   0,0,0);
  layer18Phys = new G4PVPlacement(0,
                                  G4ThreeVector(0.,0.,0.),
				  "layer18Phys", 
				  layer18Log,
				  spacecraftPhys,
				  false,0,true);
  //polyacrylate layer
  rMax = rMin;
  zLength -= thick;
  thick = 0.0028*cm;
  rMin -= thick;
  layer19O = new G4Tubs("layer19O",0,rMax,zLength,0,2*pi);
  layer19I = new G4Tubs("layer19I",0,rMin,zLength-thick,0,2*pi);

  layer19 = new G4SubtractionSolid("layer19",layer19O,layer19I);

  layer19Log = new G4LogicalVolume(layer19,
				   polyacrylate,
				   "layer19Log",
				   0,0,0);
  layer19Phys = new G4PVPlacement(0,
                                  G4ThreeVector(0.,0.,0.),
				  "layer19Phys", 
				  layer19Log,
				  spacecraftPhys,
				  false,0,true);
  //polyethilene layer
  rMax = rMin;
  zLength -= thick;
  thick = 0.0028*cm;
  rMin -= thick;
  layer20O = new G4Tubs("layer20O",0,rMax,zLength,0,2*pi);
  layer20I = new G4Tubs("layer20I",0,rMin,zLength-thick,0,2*pi);

  layer20 = new G4SubtractionSolid("layer20",layer20O,layer20I);

  layer20Log = new G4LogicalVolume(layer20,
				   polyethilene,
				   "layer20Log",
				   0,0,0);
  layer20Phys = new G4PVPlacement(0,
                                  G4ThreeVector(0.,0.,0.),
				  "layer20Phys", 
				  layer20Log,
				  spacecraftPhys,
				  false,0,true);
  //kevlar layer
  rMax = rMin;
  zLength -= thick;
  thick = 0.026*cm;
  rMin -= thick;
  layer21O = new G4Tubs("layer21O",0,rMax,zLength,0,2*pi);
  layer21I = new G4Tubs("layer21I",0,rMin,zLength-thick,0,2*pi);

  layer21 = new G4SubtractionSolid("layer21",layer21O,layer21I);

  layer21Log = new G4LogicalVolume(layer21,
				   kevlarAir,
				   "layer21Log",
				   0,0,0);
  layer21Phys = new G4PVPlacement(0,
                                  G4ThreeVector(0.,0.,0.),
				  "layer21Phys", 
				  layer21Log,
				  spacecraftPhys,
				  false,0,true);
  //polyethilene layer
  rMax = rMin;
  zLength -= thick;
  thick = 0.0028*cm;
  rMin -= thick;
  layer22O = new G4Tubs("layer22O",0,rMax,zLength,0,2*pi);
  layer22I = new G4Tubs("layer22I",0,rMin,zLength-thick,0,2*pi);

  layer22 = new G4SubtractionSolid("layer22",layer22O,layer22I);

  layer22Log = new G4LogicalVolume(layer22,
				   polyethilene,
				   "layer22Log",
				   0,0,0);
  layer22Phys = new G4PVPlacement(0,
                                  G4ThreeVector(0.,0.,0.),
				  "layer22Phys", 
				  layer22Log,
				  spacecraftPhys,
				  false,0,true);
  //polyacrylate layer
  rMax = rMin;
  zLength -= thick;
  thick = 0.0028*cm;
  rMin -= thick;
  layer23O = new G4Tubs("layer23O",0,rMax,zLength,0,2*pi);
  layer23I = new G4Tubs("layer23I",0,rMin,zLength-thick,0,2*pi);

  layer23 = new G4SubtractionSolid("layer23",layer23O,layer23I);

  layer23Log = new G4LogicalVolume(layer23,
				   polyacrylate,
				   "layer23Log",
				   0,0,0);
  layer23Phys = new G4PVPlacement(0,
                                  G4ThreeVector(0.,0.,0.),
				  "layer23Phys", 
				  layer23Log,
				  spacecraftPhys,
				  false,0,true); 
  //evoh layer
  rMax = rMin;
  zLength -= thick;
  thick = 0.0028*cm;
  rMin -= thick;
  layer24O = new G4Tubs("layer24O",0,rMax,zLength,0,2*pi);
  layer24I = new G4Tubs("layer24I",0,rMin,zLength-thick,0,2*pi);

  layer24 = new G4SubtractionSolid("layer24",layer24O,layer24I);

  layer24Log = new G4LogicalVolume(layer24,
				   evoh,
				   "layer24Log",
				   0,0,0);
  layer24Phys = new G4PVPlacement(0,
                                  G4ThreeVector(0.,0.,0.),
				  "layer24Phys", 
				  layer24Log,
				  spacecraftPhys,
				  false,0,true);
  //polyacrylate layer
  rMax = rMin;
  zLength -= thick;
  thick = 0.0028*cm;
  rMin -= thick;
  layer25O = new G4Tubs("layer25O",0,rMax,zLength,0,2*pi);
  layer25I = new G4Tubs("layer25I",0,rMin,zLength-thick,0,2*pi);

  layer25 = new G4SubtractionSolid("layer25",layer25O,layer25I);

  layer25Log = new G4LogicalVolume(layer25,
				   polyacrylate,
				   "layer25Log",
				   0,0,0);
  layer25Phys = new G4PVPlacement(0,
                                  G4ThreeVector(0.,0.,0.),
				  "layer25Phys", 
				  layer25Log,
				  spacecraftPhys,
				  false,0,true);
  //polyethilene layer
  rMax = rMin;
  zLength -= thick;
  thick = 0.0028*cm;
  rMin -= thick;
  layer26O = new G4Tubs("layer26O",0,rMax,zLength,0,2*pi);
  layer26I = new G4Tubs("layer26I",0,rMin,zLength-thick,0,2*pi);

  layer26 = new G4SubtractionSolid("layer26",layer26O,layer26I);

  layer26Log = new G4LogicalVolume(layer26,
				   polyethilene,
				   "layer26Log",
				   0,0,0);
  layer26Phys = new G4PVPlacement(0,
                                  G4ThreeVector(0.,0.,0.),
				  "layer26Phys", 
				  layer26Log,
				  spacecraftPhys,
				  false,0,true);
  //nomex layer
  rMax = rMin;
  zLength -= thick;
  thick = 0.03*cm;
  rMin -= thick;
  
  G4Material* nomex= pMaterial ->GetMaterial("nomexAir"); 
  
  layer27O = new G4Tubs("layer27O",0,rMax,zLength,0,2*pi);
  layer27I = new G4Tubs("layer27I",0,rMin,zLength-thick,0,2*pi);

  layer27 = new G4SubtractionSolid("layer27",layer27O,layer27I);

  layer27Log = new G4LogicalVolume(layer27,
				   nomex,
				   "layer27Log",
				   0,0,0);
  layer27Phys = new G4PVPlacement(0,
                                  G4ThreeVector(0.,0.,0.),
				  "layer27Phys", 
				  layer27Log,
				  spacecraftPhys,
				  false,0,true);
  //habitat air
  rMax = rMin;
  zLength -= thick;

  G4Material* air= pMaterial ->GetMaterial("Air"); 
  
  layerHabitat = new G4Tubs("layerHabitat",0,rMax,zLength,0,2*pi);
  
  layerHabitatLog = new G4LogicalVolume(layerHabitat,
					air,
					"layerHabitatLog",
					0,0,0);
  
  layerHabitatPhys = new G4PVPlacement(0,
				       G4ThreeVector(0.,0.,0.),
				       "spacecraftHabitatPhys", 
				       layerHabitatLog,
				       spacecraftPhys,
				       false,0,true);

  //fluence volume
  rMax -= fluenceStructureDistance ;
  zLength -= fluenceStructureDistance;

  spacecraftFluenceVolume = new G4Tubs("spacecraftFluenceVolume",0,rMax,zLength,0,2*pi);

  spacecraftFluenceVolumeLog = new G4LogicalVolume(spacecraftFluenceVolume,
						   air,
						   "spacecraftFluenceVolumeLog",
						   0,0,0);
  
  spacecraftFluenceVolumeLog->SetSensitiveDetector(fluenceSD);

  spacecraftFluenceVolumePhys = new G4PVPlacement(0,
						  G4ThreeVector(0.,0.,0.),
						  "spacecraftFluenceVolumePhys", 
						  spacecraftFluenceVolumeLog,
						  layerHabitatPhys,
						  false,0,true);


  G4VisAttributes* layerVisAttInvisible = new G4VisAttributes(); //habitat
  layerVisAttInvisible->SetVisibility(false);
  G4VisAttributes* layerVisAttMagenta = new G4VisAttributes(G4Colour::Magenta()); //betacloth
  layerVisAttMagenta->SetVisibility(true);
  //layerVisAttMagenta->SetForceSolid(true);
  layerVisAttMagenta->SetForceAuxEdgeVisible(true);
  G4VisAttributes* layerVisAttGreen = new G4VisAttributes(G4Colour::Green()); //Mylar
  layerVisAttGreen->SetVisibility(true);
  //layerVisAttGreen->SetForceSolid(true);
  layerVisAttGreen->SetForceAuxEdgeVisible(true);
  G4VisAttributes* layerVisAttBlue = new G4VisAttributes(G4Colour::Blue()); //Nextel
  layerVisAttBlue->SetVisibility(true);
  //layerVisAttBlue->SetForceSolid(true);
  layerVisAttBlue->SetForceAuxEdgeVisible(true);
  G4VisAttributes* layerVisAttYellow = new G4VisAttributes(G4Colour::Yellow()); //Vaccum
  layerVisAttYellow->SetVisibility(true);
  //layerVisAttYellow->SetForceSolid(true);
  layerVisAttYellow->SetForceAuxEdgeVisible(true);
  G4VisAttributes* layerVisAttRed = new G4VisAttributes(G4Colour::Red()); //Polyethylene
  layerVisAttRed->SetVisibility(true);
  //layerVisAttRed->SetForceSolid(true);
  layerVisAttRed->SetForceAuxEdgeVisible(true);
  G4VisAttributes* layerVisAttCyan = new G4VisAttributes(G4Colour::Cyan()); //ployacrylate 
  layerVisAttCyan->SetVisibility(true);
  //layerVisAttCyan->SetForceSolid(true);
  layerVisAttCyan->SetForceAuxEdgeVisible(true);
  G4VisAttributes* layerVisAttGrey = new G4VisAttributes(G4Colour::Grey()); //evoh
  layerVisAttGrey->SetVisibility(true);
  //layerVisAttGrey->SetForceSolid(true);
  layerVisAttGrey->SetForceAuxEdgeVisible(true);
  G4VisAttributes* layerVisAttGray = new G4VisAttributes(G4Colour::Gray()); //kevlar
  layerVisAttGray->SetVisibility(true);
  //layerVisAttGray->SetForceSolid(true);
  layerVisAttGray->SetForceAuxEdgeVisible(true);
  G4VisAttributes* layerVisAttWhite = new G4VisAttributes(G4Colour::White());//nomex
  layerVisAttWhite->SetVisibility(true);
  //layerVisAttWhite->SetForceSolid(true);
  layerVisAttWhite->SetForceAuxEdgeVisible(true);

  
  spacecraftFluenceVolumeLog->SetVisAttributes(layerVisAttInvisible);
  layerHabitatLog->SetVisAttributes(layerVisAttInvisible);
  //betacloth
  layer1Log->SetVisAttributes(layerVisAttMagenta);
  //mylar
  layer2Log->SetVisAttributes(layerVisAttGreen);
  layer8Log->SetVisAttributes(layerVisAttGreen);
  layer10Log->SetVisAttributes(layerVisAttGreen);
  //nextel
  layer3Log->SetVisAttributes(layerVisAttBlue);
  layer5Log->SetVisAttributes(layerVisAttBlue);
  layer7Log->SetVisAttributes(layerVisAttBlue);
  //vacuum
  layer4Log->SetVisAttributes(layerVisAttYellow);
  layer6Log->SetVisAttributes(layerVisAttYellow);
  layer9Log->SetVisAttributes(layerVisAttYellow);
  //Polyethylene
  layer11Log->SetVisAttributes(layerVisAttRed);
  layer15Log->SetVisAttributes(layerVisAttRed);
  layer16Log->SetVisAttributes(layerVisAttRed);
  layer20Log->SetVisAttributes(layerVisAttRed);
  layer22Log->SetVisAttributes(layerVisAttRed);
  layer26Log->SetVisAttributes(layerVisAttRed);
  //ployacrylate
  layer12Log->SetVisAttributes(layerVisAttCyan);
  layer14Log->SetVisAttributes(layerVisAttCyan);
  layer17Log->SetVisAttributes(layerVisAttCyan);
  layer19Log->SetVisAttributes(layerVisAttCyan);
  layer23Log->SetVisAttributes(layerVisAttCyan);
  layer25Log->SetVisAttributes(layerVisAttCyan);
  //evoh
  layer13Log->SetVisAttributes(layerVisAttGrey);
  layer18Log->SetVisAttributes(layerVisAttGrey);
  layer24Log->SetVisAttributes(layerVisAttGrey);
  //kevlar
  layer21Log->SetVisAttributes(layerVisAttGray);
  layerLog->SetVisAttributes(layerVisAttGray);
  //nomex
  layer27Log->SetVisAttributes(layerVisAttWhite);
}

void MarsDetailedSpacecraft::DestroyComponent()
{
  
  delete spacecraftPhys;  
  spacecraftPhys=0;  
  delete layerHabitatPhys;  
  layerHabitatPhys=0;  
  delete spacecraftFluenceVolumePhys;
  spacecraftFluenceVolumePhys=0;
  delete layer1Phys;
  layer1Phys=0;
  delete layer2Phys; 
  layer2Phys=0; 
  delete layer3Phys;
  layer3Phys=0;
  delete layer4Phys;
  layer4Phys=0;
  delete layer5Phys;
  layer5Phys=0;
  delete layer6Phys;
  layer6Phys=0;
  delete layer7Phys;
  layer7Phys=0;
  delete layer8Phys;
  layer8Phys=0;
  delete layer9Phys;
  layer9Phys=0;
  delete layer10Phys;
  layer10Phys=0;
  delete layer11Phys; 
  layer11Phys=0; 
  delete layer12Phys;
  layer12Phys=0;
  delete layer13Phys; 
  layer13Phys=0; 
  delete layer14Phys;
  layer14Phys=0;
  delete layer15Phys;
  layer15Phys=0;
  delete layerPhys;
  layerPhys=0;
  delete layer16Phys; 
  layer16Phys=0; 
  delete layer17Phys;
  layer17Phys=0;
  delete layer18Phys;
  layer18Phys=0;
  delete layer19Phys; 
  layer19Phys=0; 
  delete layer20Phys;
  layer20Phys=0;
  delete layer21Phys;
  layer21Phys=0;
  delete layer22Phys;
  layer22Phys=0;
  delete layer23Phys;
  layer23Phys=0;
  delete layer24Phys;
  layer24Phys=0;
  delete layer25Phys;
  layer25Phys=0;
  delete layer26Phys;
  layer26Phys=0;
  delete layer27Phys;
  layer27Phys=0;

  delete spacecraftLog;
  spacecraftLog=0;
  delete layer1Log;
  layer1Log=0;
  delete layer2Log;
  layer2Log=0;
  delete layer3Log;
  layer3Log=0;
  delete layer4Log;
  layer4Log=0;
  delete layer5Log;
  layer5Log=0;
  delete layer6Log;
  layer6Log=0;
  delete layer7Log;
  layer7Log=0;
  delete layer8Log;
  layer8Log=0;
  delete layer9Log;
  layer9Log=0;
  delete layer10Log;
  layer10Log=0;
  delete layer11Log;
  layer11Log=0;
  delete layer12Log;
  layer12Log=0;
  delete layer13Log;
  layer13Log=0;
  delete layer14Log;
  layer14Log=0;
  delete layer15Log;
  layer15Log=0;
  delete layerLog;
  layerLog=0;
  delete layer16Log;
  layer16Log=0;
  delete layer17Log;
  layer17Log=0;
  delete layer18Log;
  layer18Log=0;
  delete layer19Log;
  layer19Log=0;
  delete layer20Log;
  layer20Log=0;
  delete layer21Log;
  layer21Log=0;
  delete layer22Log;
  layer22Log=0;
  delete layer23Log;
  layer23Log=0;
  delete layer24Log;
  layer24Log=0;
  delete layer25Log;
  layer25Log=0;
  delete layer26Log;
  layer26Log=0;
  delete layer27Log;
  layer27Log=0;
  delete layerHabitatLog;
  layerHabitatLog=0;
  delete spacecraftFluenceVolumeLog;
  spacecraftFluenceVolumeLog=0;
  

  delete layer1;
  layer1=0;
  delete layer2;
  layer2=0;
  delete layer3;
  layer3=0;
  delete layer4;
  layer4=0;
  delete layer5;
  layer5=0;
  delete layer6;
  layer6=0;
  delete layer7;
  layer7=0;
  delete layer8;
  layer8=0;
  delete layer9;
  layer9=0;
  delete layer10;
  layer10=0;
  delete layer11;
  layer11=0;
  delete layer12;
  layer12=0;
  delete layer13;
  layer13=0;
  delete layer14;
  layer14=0;
  delete layer15;
  layer15=0;
  delete layer;
  layer=0;
  delete layer16;
  layer16=0;
  delete layer17;
  layer17=0;
  delete layer18;
  layer18=0;
  delete layer19;
  layer19=0;
  delete layer20;
  layer20=0;
  delete layer21;
  layer21=0;
  delete layer22;
  layer22=0;
  delete layer23;
  layer23=0;
  delete layer24;
  layer24=0;
  delete layer25;
  layer25=0;
  delete layer26;
  layer26=0;
  delete layer27;
  layer27=0;

  delete spacecraft;
  spacecraft=0;
  delete layer1O;
  layer1O=0;
  delete layer1I;
  layer1I=0;
  delete layer2O;
  layer2O=0;
  delete layer2I;
  layer2I=0;
  delete layer3O;
  layer3O=0;
  delete layer3I;
  layer3I=0;
  delete layer4O;
  layer4O=0;
  delete layer4I;
  layer4I=0;
  delete layer5O;
  layer5O=0;
  delete layer5I;
  layer5I=0;
  delete layer6O;
  layer6O=0;
  delete layer6I;
  layer6I=0;
  delete layer7O;
  layer7O=0;
  delete layer7I;
  layer7I=0;
  delete layer8O;
  layer8O=0;
  delete layer8I;
  layer8I=0;
  delete layer9O;
  layer9O=0;
  delete layer9I;
  layer9I=0;
  delete layer10O;
  layer10O=0;
  delete layer10I;
  layer10I=0;
  delete layer11O;
  layer11O=0;
  delete layer11I;
  layer11I=0;
  delete layer12O;
  layer12O=0;
  delete layer12I;
  layer12I=0;
  delete layer13O;
  layer13O=0;
  delete layer13I;
  layer13I=0;
  delete layer14O;
  layer14O=0;
  delete layer14I;
  layer14I=0;
  delete layer15O;
  layer15O=0;
  delete layer15I;
  layer15I=0;
  delete layerO;
  layerO=0;
  delete layerI;
  layerI=0;
  delete layer16O;
  layer16O=0;
  delete layer16I;
  layer16I=0;
  delete layer17O;
  layer17O=0;
  delete layer17I;
  layer17I=0;
  delete layer18O;
  layer18O=0;
  delete layer18I;
  layer18I=0;
  delete layer19O;
  layer19O=0;
  delete layer19I;
  layer19I=0;
  delete layer20O;
  layer20O=0;
  delete layer20I;
  layer20I=0;
  delete layer21O;
  layer21O=0;
  delete layer21I;
  layer21I=0;
  delete layer22O;
  layer22O=0;
  delete layer22I;
  layer22I=0;
  delete layer23O;
  layer23O=0;
  delete layer23I;
  layer23I=0;
  delete layer24O;
  layer24O=0;
  delete layer24I;
  layer24I=0;
  delete layer25O;
  layer25O=0;
  delete layer25I;
  layer25I=0;
  delete layer26O;
  layer26O=0;
  delete layer26I;
  layer26I=0;
  delete layer27O;
  layer27O=0;
  delete layer27I;
  layer27I=0;
  delete layerHabitat;
  layerHabitat=0;
  delete spacecraftFluenceVolume;
  spacecraftFluenceVolume=0;
}
