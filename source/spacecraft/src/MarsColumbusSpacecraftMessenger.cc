//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// Code developed by:  Filippo Ambroglini : filippo.ambroglini@pg.infn.it
//
//    *****************************************
//    *                                       *
//    *    MarsColumbusSpacecraftMessenger.cc *
//    *                                       *
//    *****************************************
//
//
// $Id$
//
// 
#include "MarsColumbusSpacecraftMessenger.hh"
#include "MarsColumbusSpacecraft.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcommand.hh"
#include "G4UIparameter.hh"
#include "G4UnitsTable.hh"

MarsColumbusSpacecraftMessenger::MarsColumbusSpacecraftMessenger( MarsColumbusSpacecraft* space): spacecraft(space)
{  
  G4UIparameter* param;

  spacecraftDir = new G4UIdirectory("/columbusSpacecraft/");
  spacecraftDir->SetGuidance("Columbus spacecraft.");

  spacecraftConfigurationCmd = new G4UIcommand("/columbusspacecraft/spacecraftConfiguration",this);
  spacecraftConfigurationCmd->SetGuidance("Configure the Columbus spacecraft passing the material and the dimension"); 
  spacecraftConfigurationCmd->SetGuidance("[usage] /columbusspacecraft/spacecraftConfiguration FluenceDistaneStructure unit"); 
  param = new G4UIparameter("fluenceDistance",'d',false);
  spacecraftConfigurationCmd->SetParameter(param);
  param = new G4UIparameter("unit_length",'s',false);
  spacecraftConfigurationCmd->SetParameter(param);
  spacecraftConfigurationCmd->AvailableForStates(G4State_PreInit,G4State_Idle); 

   
}

MarsColumbusSpacecraftMessenger::~MarsColumbusSpacecraftMessenger()
{
  delete spacecraftConfigurationCmd;
  delete spacecraftDir;
} 

void MarsColumbusSpacecraftMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 
  if (command == spacecraftConfigurationCmd){
    const char* paramString=newValue;
    G4String unit_fd;
    G4double fluenceDistance;
    std::istringstream is((char*)paramString);
    is >>fluenceDistance>>unit_fd;
    fluenceDistance*=G4UnitDefinition::GetValueOf(unit_fd);
    spacecraft->ConfigureSpacecraft(fluenceDistance);
  }
}

