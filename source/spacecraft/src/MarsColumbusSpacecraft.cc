//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id$
//
// Code developed by: Filippo Ambroglini, filippo.ambroglini@pg.infn.it
//
#include "MarsColumbusSpacecraft.hh"
#include "MarsColumbusSpacecraftMessenger.hh"
#include "MarsMaterial.hh"

#include "G4SystemOfUnits.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Cons.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "G4UserLimits.hh"
#include "G4SDManager.hh"

MarsColumbusSpacecraft::MarsColumbusSpacecraft():
  spacecraftBarrelPhys(0),spacecraftFluenceVolumePhys(0),spacecraftEndcapMinusPhys(0), 
  spacecraftEndcapPlusPhys(0),spacecraftHabitatBarrelPhys(0),
  spacecraftHabitatEndcapMinusPhys(0),spacecraftHabitatEndcapPlusPhys(0),
  spacecraftShaftMinusPhys(0),spacecraftShaftPlusPhys(0),
  spacecraftHabitatBarrel(0),spacecraftFluenceVolume(0),spacecraftBarrel(0),spacecraftEndcap(0),
  spacecraftHabitatEndcap(0),spacecraftShaft(0),
  spacecraftHabitatBarrelLog(0),spacecraftFluenceVolumeLog(0),spacecraftBarrelLog(0),spacecraftEndcapLog(0),
  spacecraftHabitatEndcapLog(0),spacecraftShaftLog(0)
{
  pMaterial = new MarsMaterial();
  messenger = new MarsColumbusSpacecraftMessenger(this);
  
  G4SDManager* SDManager = G4SDManager::GetSDMpointer();
  fluenceSD   = new MarsFluenceSD("fluenceSD");
  SDManager->AddNewDetector(fluenceSD);
  fluenceStructureDistance=20*cm;

}
MarsColumbusSpacecraft::~MarsColumbusSpacecraft()
{
  delete pMaterial;
  delete messenger;
}

void MarsColumbusSpacecraft::ConfigureSpacecraft(G4double aFluenceStructureDistance)
{
  fluenceStructureDistance = aFluenceStructureDistance;
}


void MarsColumbusSpacecraft::ConstructComponent(G4VPhysicalVolume* motherVolume)
{

  G4double thickBarrel   = 15*mm;
  G4double rMinBarrel    = 2235*mm;
  G4double rMaxBarrel    = rMinBarrel+thickBarrel;
  G4double zLengthBarrel = 2387.5*mm;

  G4double rMinS       = 965.145*mm;
  G4double rMaxS       = 1000*mm;
  G4double rMinB       = 2215.145*mm;
  G4double rMaxB       = 2250*mm;
  G4double heightEndcap = 298*mm; 

  G4double shaftR     = 1000*mm;
  G4double shaftThick = 30*mm;

  G4Material* structure = pMaterial->GetMaterial("Aluminium");  
  G4Material* air = pMaterial->GetMaterial("Air");  

  
  spacecraftHabitatBarrel = new G4Tubs("spacecraftHabitaBarrel",0,rMinBarrel,zLengthBarrel,0,2*pi);

  spacecraftHabitatBarrelLog = new G4LogicalVolume(spacecraftHabitatBarrel,
						   air,
						   "spacecraftHabitatBarrelLog",
						   0,0,0);
  
  spacecraftHabitatBarrelPhys = new G4PVPlacement(0,
						  G4ThreeVector(0.,0.,0),
						  "spacecraftHabitatPhys", 
						  spacecraftHabitatBarrelLog,
						  motherVolume,
						  false,0,true);
    
  spacecraftFluenceVolume = new G4Tubs("spacecraftFluenceVolume",0,rMinBarrel-fluenceStructureDistance,zLengthBarrel-fluenceStructureDistance,0,2*pi);

  spacecraftFluenceVolumeLog = new G4LogicalVolume(spacecraftFluenceVolume,
						   air,
						   "spacecraftFluenceVolumeLog",
						   0,0,0);

  spacecraftFluenceVolumeLog->SetSensitiveDetector(fluenceSD);

  spacecraftFluenceVolumePhys = new G4PVPlacement(0,
						  G4ThreeVector(0.,0.,0),
						  "spacecraftFluenceVolumePhys", 
						  spacecraftFluenceVolumeLog,
						  spacecraftHabitatBarrelPhys,
						  false,0,true);
 
  spacecraftHabitatEndcap = new G4Cons("spacecraftHabitatEndcap",0,rMinS,0,rMinB,heightEndcap,0,2*pi);
  
  spacecraftHabitatEndcapLog = new G4LogicalVolume(spacecraftHabitatEndcap,
						   air,
						   "spacecraftHabitatEndcapLog",
						   0,0,0);
  G4RotationMatrix* endcapPlusRot = new G4RotationMatrix;
  endcapPlusRot->rotateX(pi);
  spacecraftHabitatEndcapPlusPhys = new G4PVPlacement(endcapPlusRot,
						      G4ThreeVector(0.,0.,zLengthBarrel+heightEndcap),
						      "spacecraftHabitatEnedcapPlusPhys", 
						      spacecraftHabitatEndcapLog,
						      motherVolume,
						      false,0,true);

  spacecraftHabitatEndcapMinusPhys = new G4PVPlacement(0,
						      G4ThreeVector(0.,0.,-zLengthBarrel-heightEndcap),
						      "spacecraftHabitatEndcapMinusPhys", 
						      spacecraftHabitatEndcapLog,
						      motherVolume,
						      false,0,true);

  spacecraftBarrel = new G4Tubs("spacecraftBarrel",rMinBarrel,rMaxBarrel,zLengthBarrel,0,2*pi);
  
  spacecraftBarrelLog = new G4LogicalVolume(spacecraftBarrel,
					    structure,
					    "spacecraftBarrelLog",
					    0,0,0);
  
  spacecraftBarrelPhys = new G4PVPlacement(0,
					   G4ThreeVector(0.,0.,0),
					   "spacecraftBarrelPhys", 
					   spacecraftBarrelLog,
					   motherVolume,
					   false,0,true);

  spacecraftEndcap = new G4Cons("spacecraftEndcap",rMinS,rMaxS,rMinB,rMaxB,heightEndcap,0,2*pi);
  
  spacecraftEndcapLog = new G4LogicalVolume(spacecraftEndcap,
					    structure,
					    "spacecraftEndcapLog",
					    0,0,0);


  spacecraftEndcapPlusPhys = new G4PVPlacement(endcapPlusRot,
					       G4ThreeVector(0.,0.,zLengthBarrel+heightEndcap),
					       "spacecraftEndcapPlusPhys", 
					       spacecraftEndcapLog,
					       motherVolume,
					       false,0,true);

  spacecraftEndcapMinusPhys = new G4PVPlacement(0,
						G4ThreeVector(0.,0.,-zLengthBarrel-heightEndcap),
						"spacecraftEndcapMinusPhys", 
						spacecraftEndcapLog,
						motherVolume,
						false,0,true);


  spacecraftShaft = new G4Tubs("spacecraftShaft",0,shaftR,shaftThick/2.,0,2*pi);
  
  spacecraftShaftLog = new G4LogicalVolume(spacecraftShaft,
					  structure,
					  "spacecraftShaftLog",
					  0,0,0);

  spacecraftShaftPlusPhys = new G4PVPlacement(0,
					      G4ThreeVector(0.,0.,zLengthBarrel+heightEndcap*2.+shaftThick/2.),
					      "spacecraftShaftPlusPhys", 
					      spacecraftShaftLog,
					      motherVolume,
					      false,0,true);

  spacecraftShaftMinusPhys = new G4PVPlacement(0,
					      G4ThreeVector(0.,0.,-zLengthBarrel-heightEndcap*2.-shaftThick/2.),
					      "spacecraftShaftMinusPhys", 
					      spacecraftShaftLog,
					      motherVolume,
					      false,0,true);

  
  G4VisAttributes* spacecraftVisAttBarrel = new G4VisAttributes(G4Colour::Magenta());
  spacecraftVisAttBarrel->SetVisibility(true);
  //  spacecraftVisAttBarrel->SetForceSolid(true);
  spacecraftVisAttBarrel->SetForceAuxEdgeVisible(true);
  spacecraftBarrelLog->SetVisAttributes(spacecraftVisAttBarrel); 
  
  G4VisAttributes* spacecraftVisAttEndcap = new G4VisAttributes(G4Colour::Green());
  spacecraftVisAttEndcap->SetVisibility(true);
  //  spacecraftVisAttEndcap->SetForceSolid(true);
  spacecraftVisAttEndcap->SetForceAuxEdgeVisible(true);
  spacecraftEndcapLog->SetVisAttributes(spacecraftVisAttEndcap);  
  
  G4VisAttributes* spacecraftVisAttShaft = new G4VisAttributes(G4Colour::Blue());
  spacecraftVisAttShaft->SetVisibility(true);
  //  spacecraftVisAttShaft->SetForceSolid(true);
  spacecraftVisAttShaft->SetForceAuxEdgeVisible(true);
  spacecraftShaftLog->SetVisAttributes(spacecraftVisAttShaft);  
  
  G4VisAttributes * spacecraftVisAttInvisible = new G4VisAttributes();
  spacecraftVisAttInvisible->SetVisibility(false);
  spacecraftVisAttInvisible->SetForceAuxEdgeVisible(false);
  spacecraftHabitatBarrelLog->SetVisAttributes(spacecraftVisAttInvisible); 
  spacecraftHabitatEndcapLog->SetVisAttributes(spacecraftVisAttInvisible); 
}

void MarsColumbusSpacecraft::DestroyComponent()
{
  /*
  delete spacecraftBarrelPhys;
  spacecraftBarrelPhys=0;
  delete spacecraftFluenceVolumePhys;
  spacecraftFluenceVolumePhys=0;
  delete spacecraftEndcapMinusPhys;
  spacecraftEndcapMinusPhys=0;
  delete spacecraftEndcapPlusPhys;
  spacecraftEndcapPlusPhys=0;
  delete spacecraftShaftMinusPhys;
  spacecraftShaftMinusPhys=0;
  delete spacecraftShaftPlusPhys;
  spacecraftShaftPlusPhys=0;
  delete spacecraftHabitatBarrelPhys;
  spacecraftHabitatBarrelPhys=0;
  delete spacecraftHabitatEndcapMinusPhys;
  spacecraftHabitatEndcapMinusPhys=0;
  delete spacecraftHabitatEndcapPlusPhys;
  spacecraftHabitatEndcapPlusPhys=0;

  delete spacecraftHabitatBarrel;
  spacecraftHabitatBarrel=0;
  delete spacecraftFluenceVolume;
  spacecraftFluenceVolume=0;
  delete spacecraftHabitatEndcap;
  spacecraftHabitatEndcap=0;
  delete spacecraftBarrel;
  spacecraftBarrel=0;
  delete spacecraftEndcap;
  spacecraftEndcap=0;
  delete spacecraftShaft;
  spacecraftShaft=0;

  delete spacecraftHabitatBarrelLog;
  spacecraftHabitatBarrelLog=0;
  delete spacecraftFluenceVolumeLog;
  spacecraftFluenceVolumeLog=0;
  delete spacecraftHabitatEndcapLog;
  spacecraftHabitatEndcapLog=0;
  delete spacecraftBarrelLog;
  spacecraftBarrelLog=0;
  delete spacecraftEndcapLog;
  spacecraftEndcapLog=0;
  delete spacecraftShaftLog;
  spacecraftShaftLog=0;
  */
}
