//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// Code developed by:  S.Guatelli, guatelli@ge.infn.it
//
//    *****************************************
//    *                                       *
//    *    MarsDetailedSpacecraftMessenger.cc *
//    *                                       *
//    *****************************************
//
//
// $Id$
//
// 

#include "MarsDetailedSpacecraftMessenger.hh"
#include "MarsDetailedSpacecraft.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcommand.hh"
#include "G4UIparameter.hh"
#include "G4UnitsTable.hh"

MarsDetailedSpacecraftMessenger::MarsDetailedSpacecraftMessenger( MarsDetailedSpacecraft* space): spacecraft(space)
{  

  G4UIparameter* param;
  
  spacecraftDir = new G4UIdirectory("/detailedspacecraft/");
  spacecraftDir->SetGuidance("Detailed spacecraft.");
 
  spacecraftConfigurationCmd = new G4UIcommand("/detailedspacecraft/spacecraftConfiguration",this);
  spacecraftConfigurationCmd->SetGuidance("Configure the detailed spacecraft passing the material and the dimension"); 
  spacecraftConfigurationCmd->SetGuidance("[usage] /detailedspacecraft/spacecraftConfiguration OuterRadius unit HalfLength unit FluenceDistaneStructure unit"); 
  param = new G4UIparameter("outerRadius",'d',false);
  spacecraftConfigurationCmd->SetParameter(param);
  param = new G4UIparameter("unit_length",'s',false);
  spacecraftConfigurationCmd->SetParameter(param);
  param = new G4UIparameter("halfLength",'d',false);
  spacecraftConfigurationCmd->SetParameter(param);
  param = new G4UIparameter("unit_length",'s',false);
  spacecraftConfigurationCmd->SetParameter(param);
  param = new G4UIparameter("fluenceDistance",'d',false);
  spacecraftConfigurationCmd->SetParameter(param);
  param = new G4UIparameter("unit_length",'s',false);
  spacecraftConfigurationCmd->SetParameter(param);
  spacecraftConfigurationCmd->AvailableForStates(G4State_PreInit,G4State_Idle); 

  
}

MarsDetailedSpacecraftMessenger::~MarsDetailedSpacecraftMessenger()
{
  delete spacecraftConfigurationCmd;
  delete spacecraftDir;
} 

void MarsDetailedSpacecraftMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 
  if (command == spacecraftConfigurationCmd){
    const char* paramString=newValue;
    G4String unit_or,unit_hl,unit_fd;
    G4double outerRadius,halfLength,fluenceDistance;
    std::istringstream is((char*)paramString);
    is >>outerRadius>>unit_or>>halfLength>>unit_hl>>fluenceDistance>>unit_fd;
    outerRadius*=G4UnitDefinition::GetValueOf(unit_or);
    halfLength*=G4UnitDefinition::GetValueOf(unit_hl);
    fluenceDistance*=G4UnitDefinition::GetValueOf(unit_fd);
    spacecraft->ConfigureSpacecraft(outerRadius,halfLength,fluenceDistance);
  }
}

