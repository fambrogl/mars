//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id$
//
// Code developed by: S.Guatelli, guatelli@ge.infn.it
//
#include "MarsSimplifiedSpacecraft.hh"
#include "MarsSimplifiedSpacecraftMessenger.hh"
#include "MarsMaterial.hh"

#include "G4SystemOfUnits.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "G4UserLimits.hh"
#include "G4SDManager.hh"

MarsSimplifiedSpacecraft::MarsSimplifiedSpacecraft():
  spacecraftBarrelPhys(0), spacecraftEndcapMinusPhys(0), 
  spacecraftEndcapPlusPhys(0), spacecraftHabitatPhys(0),
  spacecraftHabitat(0),spacecraftBarrel(0),spacecraftEndcap(0),
  spacecraftHabitatLog(0),spacecraftBarrelLog(0),spacecraftEndcapLog(0)
{
  pMaterial = new MarsMaterial();
  messenger = new MarsSimplifiedSpacecraftMessenger(this);
  
  structureMaterial="Aluminium";
  structureThickness=1.8*cm;
  structureInnerRadius=3*m;
  structureHalfLength=5*m;

  G4SDManager* SDManager = G4SDManager::GetSDMpointer();
  fluenceSD   = new MarsFluenceSD("fluenceSD");
  SDManager->AddNewDetector(fluenceSD);

  
}
MarsSimplifiedSpacecraft::~MarsSimplifiedSpacecraft()
{
  delete pMaterial;
  delete messenger;
}

void MarsSimplifiedSpacecraft::ConfigureSpacecraft(G4String aMaterial,G4double aThickness, G4double aInnerRadius, G4double aHalfLength, G4double aFluenceStructureDistance)
{
  structureMaterial    = aMaterial;
  structureThickness   = aThickness;
  structureInnerRadius = aInnerRadius;
  structureHalfLength  = aHalfLength;
  fluenceStructureDistance = aFluenceStructureDistance;
}

void MarsSimplifiedSpacecraft::ConstructComponent(G4VPhysicalVolume* motherVolume)
{

  G4double thick   = structureThickness;
  G4double rMax    = structureInnerRadius+structureThickness;
  G4double rMin    = structureInnerRadius;
  G4double zLength = structureHalfLength;


  G4Material* structure = pMaterial->GetMaterial(structureMaterial);  
  G4Material* air = pMaterial->GetMaterial("Air");  

  
  spacecraftHabitat = new G4Tubs("spacecraftHabitat",0,rMin,zLength,0,2*pi);

  spacecraftHabitatLog = new G4LogicalVolume(spacecraftHabitat,
							       air,
							       "spacecraftHabitatLog",
							       0,0,0);
  
  spacecraftHabitatPhys = new G4PVPlacement(0,
					    G4ThreeVector(0.,0.,0),
					    "spacecraftHabitatPhys", 
					    spacecraftHabitatLog,
					    motherVolume,
					    false,0,true);

  spacecraftFluenceVolume = new G4Tubs("spacecraftFluenceVolume",0,rMin-fluenceStructureDistance,zLength-fluenceStructureDistance,0,2*pi);

  spacecraftFluenceVolumeLog = new G4LogicalVolume(spacecraftFluenceVolume,
						   air,
						   "spacecraftFluenceVolumeLog",
						   0,0,0);
  
  spacecraftFluenceVolumeLog->SetSensitiveDetector(fluenceSD);

  spacecraftFluenceVolumePhys = new G4PVPlacement(0,
						  G4ThreeVector(0.,0.,0.),
						  "spacecraftFluenceVolumePhys", 
						  spacecraftFluenceVolumeLog,
						  spacecraftHabitatPhys,
						  false,0,true);

  
  spacecraftBarrel = new G4Tubs("spacecraftBarrel",rMin,rMax,zLength,0,2*pi);

  spacecraftBarrelLog = new G4LogicalVolume(spacecraftBarrel,
							       structure,
							       "spacecraftBarrelLog",
							       0,0,0);
  
  spacecraftBarrelPhys = new G4PVPlacement(0,
					   G4ThreeVector(0.,0.,0),
					   "spacecraftBarrelPhys", 
					   spacecraftBarrelLog,
					   motherVolume,
					   false,0,true);
  
  
  
  spacecraftEndcap = new G4Tubs("spacecraftEndcap",0,rMax,thick/2.,0,2*pi);
  spacecraftEndcapLog = new G4LogicalVolume(spacecraftEndcap,
							     structure,
							     "spacecraftEndcapLog",
							     0,0,0);
  
  spacecraftEndcapPlusPhys = new G4PVPlacement(0,
					       G4ThreeVector(0.,0.,zLength+thick/2.),
					       "spacecraftEndcapPlusPhys", 
					       spacecraftEndcapLog,
					       motherVolume,
					       false,0,true);

  spacecraftEndcapMinusPhys = new G4PVPlacement(0,
						G4ThreeVector(0.,0.,-zLength-thick/2.),
						"spacecraftEndcapMinusPhys", 
						spacecraftEndcapLog,
						motherVolume,
						false,0,true);

  
  G4VisAttributes* spacecraftVisAttBarrel = new G4VisAttributes(G4Colour::Magenta());
  spacecraftVisAttBarrel->SetVisibility(true);
  //  spacecraftVisAttBarrel->SetForceSolid(true);
  spacecraftVisAttBarrel->SetForceAuxEdgeVisible(true);
  spacecraftBarrelLog->SetVisAttributes(spacecraftVisAttBarrel); 
  
  G4VisAttributes* spacecraftVisAttEndcap = new G4VisAttributes(G4Colour::Green());
  spacecraftVisAttEndcap->SetVisibility(true);
  //  spacecraftVisAttEndcap->SetForceSolid(true);
  spacecraftVisAttEndcap->SetForceAuxEdgeVisible(true);
  spacecraftEndcapLog->SetVisAttributes(spacecraftVisAttEndcap);  
  
  G4VisAttributes * spacecraftVisAttInvisible = new G4VisAttributes();
  spacecraftVisAttInvisible->SetVisibility(false);
  spacecraftVisAttInvisible->SetForceAuxEdgeVisible(false);
  spacecraftHabitatLog->SetVisAttributes(spacecraftVisAttInvisible); 
}

void MarsSimplifiedSpacecraft::DestroyComponent()
{
  
  delete spacecraftBarrelPhys;
  spacecraftBarrelPhys=0;
  delete spacecraftEndcapMinusPhys;
  spacecraftEndcapMinusPhys=0;
  delete spacecraftEndcapPlusPhys;
  spacecraftEndcapPlusPhys=0;
  delete spacecraftHabitatPhys;
  spacecraftHabitatPhys=0;

  delete spacecraftHabitat;
  spacecraftHabitat=0;
  delete spacecraftBarrel;
  spacecraftBarrel=0;
  delete spacecraftEndcap;
  spacecraftEndcap=0;

  delete spacecraftHabitatLog;
  spacecraftHabitatLog=0;
  delete spacecraftBarrelLog;
  spacecraftBarrelLog=0;
  delete spacecraftEndcapLog;
  spacecraftEndcapLog=0;

}
