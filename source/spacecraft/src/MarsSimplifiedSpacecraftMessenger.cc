//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// Code developed by:  S.Guatelli, guatelli@ge.infn.it
//
//    *****************************************
//    *                                       *
//    *    MarsSimplifiedSpacecraftMessenger.cc *
//    *                                       *
//    *****************************************
//
//
// $Id$
//
// 
#include "MarsSimplifiedSpacecraftMessenger.hh"
#include "MarsSimplifiedSpacecraft.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcommand.hh"
#include "G4UIparameter.hh"
#include "G4UnitsTable.hh"

MarsSimplifiedSpacecraftMessenger::MarsSimplifiedSpacecraftMessenger( MarsSimplifiedSpacecraft* space): spacecraft(space)
{  

  G4UIparameter* param;
  
  spacecraftDir = new G4UIdirectory("/simplifiedspacecraft/");
  spacecraftDir->SetGuidance("Simplified spacecraft.");
 
  spacecraftConfigurationCmd = new G4UIcommand("/simplifiedspacecraft/spacecraftConfiguration",this);
  spacecraftConfigurationCmd->SetGuidance("Configure the simplified spacecraft passing the material and the dimension"); 
  spacecraftConfigurationCmd->SetGuidance("[usage] /simplifiedspacecraft/spacecraftConfiguration material thickness unit innerRadius unit HalfLength unit FluenceDistaneStructure unit"); 
  param = new G4UIparameter("material",'s',false);
  spacecraftConfigurationCmd->SetParameter(param);
  param = new G4UIparameter("thickness",'d',false);
  spacecraftConfigurationCmd->SetParameter(param);
  param = new G4UIparameter("unit_length",'s',false);
  spacecraftConfigurationCmd->SetParameter(param);
  param = new G4UIparameter("innerRadius",'d',false);
  spacecraftConfigurationCmd->SetParameter(param);
  param = new G4UIparameter("unit_length",'s',false);
  spacecraftConfigurationCmd->SetParameter(param);
  param = new G4UIparameter("halfLength",'d',false);
  spacecraftConfigurationCmd->SetParameter(param);
  param = new G4UIparameter("unit_length",'s',false);
  spacecraftConfigurationCmd->SetParameter(param);
  param = new G4UIparameter("fluenceDistance",'d',false);
  spacecraftConfigurationCmd->SetParameter(param);
  param = new G4UIparameter("unit_length",'s',false);
  spacecraftConfigurationCmd->SetParameter(param);
  spacecraftConfigurationCmd->AvailableForStates(G4State_PreInit,G4State_Idle); 

  
}

MarsSimplifiedSpacecraftMessenger::~MarsSimplifiedSpacecraftMessenger()
{
  delete spacecraftConfigurationCmd;
  delete spacecraftDir;
} 

void MarsSimplifiedSpacecraftMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 
  if (command == spacecraftConfigurationCmd){
    const char* paramString=newValue;
    G4String material,unit_t,unit_ir,unit_hl,unit_fd;
    G4double thickness,innerRadius,halfLength,fluenceDistance;
    std::istringstream is((char*)paramString);
    is >>material>>thickness>>unit_t>>innerRadius>>unit_ir>>halfLength>>unit_hl>>fluenceDistance>>unit_fd;
    thickness*=G4UnitDefinition::GetValueOf(unit_t);
    innerRadius*=G4UnitDefinition::GetValueOf(unit_ir);
    halfLength*=G4UnitDefinition::GetValueOf(unit_hl);
    fluenceDistance*=G4UnitDefinition::GetValueOf(unit_fd);
    spacecraft->ConfigureSpacecraft(material,thickness,innerRadius,halfLength,fluenceDistance);
  }
}


