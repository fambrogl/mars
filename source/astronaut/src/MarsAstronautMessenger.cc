//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// Authors: S. Guatelli and M. G. Pia, INFN Genova, Italy
// 
// Based on code developed by the undergraduate student G. Guerrieri 
// Note: this is a preliminary beta-version of the code; an improved 
// version will be distributed in the next Geant4 public release, compliant
// with the design in a forthcoming publication, and subject to a 
// design and code review.
//
#include "MarsAstronautMessenger.hh"
#include "MarsAstronaut.hh"

#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcommand.hh"
#include "G4UIparameter.hh"
#include "G4UnitsTable.hh"

#include "globals.hh"

#include "G4RunManager.hh"

MarsAstronautMessenger::MarsAstronautMessenger(MarsAstronaut* aAstronaut)
  :myAstronaut(aAstronaut),bps(false)
{ 

  astronautDir = new G4UIdirectory("/astronaut/");
  astronautDir->SetGuidance("Set Your Astronaut.");
  
  bpDir = new G4UIdirectory("/bodypart/");
  bpDir->SetGuidance("Add Body Part to Astronaut");

  stepLengthCmd = new G4UIcmdWithADoubleAndUnit("/astronaut/setMaxStepLength",this);
  stepLengthCmd->SetGuidance("Set the maximun step length value");
  stepLengthCmd->SetParameterName("MaxStepLength",false);
  stepLengthCmd->SetUnitCategory("Length");
  stepLengthCmd->SetRange("MaxStepLength>=0.0");;
  stepLengthCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  modelCmd = new G4UIcmdWithAString("/astronaut/setAstronautModel",this);
  modelCmd->SetGuidance("Set sex of Astronaut: MIRD,ORNLFemale,ORNLMale,MIX,MIRDHead,ORNLHead,Cylinder,ICRU.");
  modelCmd->SetParameterName("astronautModel",true);
  modelCmd->SetDefaultValue("Cylinder");
  modelCmd->SetCandidates("MIRD ORNLFemale ORNLMale MIX MIRDHead ORNLHead Cylinder ICRU");
  modelCmd->AvailableForStates(G4State_PreInit,G4State_Idle); 

  sexCmd = new G4UIcmdWithAString("/astronaut/setAstronautSex",this);
  sexCmd->SetGuidance("Set sex of Astronaut: Male or Female.");
  sexCmd->SetParameterName("astronautSex",true);
  sexCmd->SetDefaultValue("Female");
  sexCmd->SetCandidates("Male Female");
  sexCmd->AvailableForStates(G4State_PreInit,G4State_Idle); 
  
  bodypartCmd = new G4UIcmdWithAString("/bodypart/addBodyPart",this);
  bodypartCmd->SetGuidance("Add a Body Part to Astronaut");
  bodypartCmd->SetParameterName("bpName",true);
  bodypartCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  endCmd = new G4UIcmdWithoutParameter("/astronaut/buildNewAstronaut",this);
  endCmd->SetGuidance("Build your Astronaut.");
  endCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  numberAndPositionCmd = new G4UIcommand("/astronaut/setAstronautNumberAndPosition",this);
  numberAndPositionCmd->SetGuidance("Set number of astronaut model and the position of each");
  numberAndPositionCmd->AvailableForStates(G4State_Idle);
  param = new G4UIparameter("NumberAstronaut",'d',false);
  numberAndPositionCmd->SetParameter(param);
  param = new G4UIparameter("length_unit",'s',false);
  numberAndPositionCmd->SetParameter(param);
  param = new G4UIparameter("X1pos",'d',false);
  numberAndPositionCmd->SetParameter(param);
  param = new G4UIparameter("Y1pos",'d',false);
  numberAndPositionCmd->SetParameter(param);
  param = new G4UIparameter("Z1pos",'d',false);
  numberAndPositionCmd->SetParameter(param);
  param = new G4UIparameter("X2pos",'d',false);
  numberAndPositionCmd->SetParameter(param);
  param = new G4UIparameter("Y2pos",'d',false);
  numberAndPositionCmd->SetParameter(param);
  param = new G4UIparameter("Z2pos",'d',false);
  numberAndPositionCmd->SetParameter(param);
  param = new G4UIparameter("X3pos",'d',false);
  numberAndPositionCmd->SetParameter(param);
  param = new G4UIparameter("Y3pos",'d',false);
  numberAndPositionCmd->SetParameter(param);
  param = new G4UIparameter("Z3pos",'d',false);
  numberAndPositionCmd->SetParameter(param);
  param = new G4UIparameter("X4pos",'d',false);
  numberAndPositionCmd->SetParameter(param);
  param = new G4UIparameter("Y4pos",'d',false);
  numberAndPositionCmd->SetParameter(param);
  param = new G4UIparameter("Z4pos",'d',false);
  numberAndPositionCmd->SetParameter(param);
  param = new G4UIparameter("X5pos",'d',false);
  numberAndPositionCmd->SetParameter(param);
  param = new G4UIparameter("Y5pos",'d',false);
  numberAndPositionCmd->SetParameter(param);
  param = new G4UIparameter("Z5pos",'d',false);
  numberAndPositionCmd->SetParameter(param);
  param = new G4UIparameter("X6pos",'d',false);
  numberAndPositionCmd->SetParameter(param);
  param = new G4UIparameter("Y6pos",'d',false);
  numberAndPositionCmd->SetParameter(param);
  param = new G4UIparameter("Z6pos",'d',false);
  numberAndPositionCmd->SetParameter(param);
}

MarsAstronautMessenger::~MarsAstronautMessenger()
{
  delete  stepLengthCmd;
  delete  modelCmd;
  delete  sexCmd;
  delete  bodypartCmd;
  delete  endCmd;
  delete  astronautDir;
  delete  bpDir;
}

void MarsAstronautMessenger::SetNewValue(G4UIcommand* command,G4String newValue){ 

  if( command == modelCmd ){ 
    myAstronaut->SetAstronautModel(newValue); 
  }else if( command == sexCmd ){ 
    myAstronaut->SetAstronautSex(newValue); 
  }else if( command == bodypartCmd ){
    AddBodyPart(newValue);
  }else if( command == endCmd ){ 
    G4cout << 
      " ****************>>>> NEW ASTRONAUT CONSTRUCTION <<<<***************** " 
	   << G4endl;
  }else if( command == numberAndPositionCmd){
   const char* paramString;
    paramString=newValue;
    G4int aNumb;
    G4String unit;
    G4double X,Y,Z;
    std::vector<G4ThreeVector> aPos;
    std::istringstream is((char*)paramString);
    is >> aNumb >> unit;
    for(int i=0;i<aNumb;i++){
      is >> X >> Y >> Z;
      X*=G4UnitDefinition::GetValueOf(unit);
      Y*=G4UnitDefinition::GetValueOf(unit);
      Z*=G4UnitDefinition::GetValueOf(unit);
      aPos.push_back(G4ThreeVector(X,Y,Z));
    }
    myAstronaut->SetAstronautNumberAndPosition(aNumb,aPos);
  }else if( command == stepLengthCmd ){
    myAstronaut->SetAstronautMaxStepLength(stepLengthCmd->GetNewDoubleValue(newValue));
  }
}

void  MarsAstronautMessenger::AddBodyPart(G4String newBodyPartSensitivity)
{

  char* str = new char[newBodyPartSensitivity.length()+1];

  strcpy(str, newBodyPartSensitivity.c_str()); 
  
  std::string bpart = strtok(str," ");

  std::string sensitivity = strtok(NULL," ");

  if(sensitivity=="yes"){
    bps=true;
  }else{
    bps=false;
  }

  G4cout << " >>> Body Part = " << bpart << "\n"
	 << " >>> Sensitivity = " << sensitivity << G4endl;

  myAstronaut->SetBodyPartSensitivity(bpart,bps);
}

