//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//    **********************************
//    *                                *
//    *    MarsAstronaut.cc *
//    *                                *
//    **********************************
//
// $Id$
//
// Author:Filippo Ambroglini, filippo.ambroglini@pg.infn.it 

#include "MarsVGeometryComponent.hh"
#include "MarsMaterial.hh"
#include "G4SystemOfUnits.hh"
#include "G4Material.hh"
#include "G4MaterialTable.hh"
#include "MarsAstronaut.hh"
#include "G4Tubs.hh"
#include "G4Sphere.hh"
#include "G4SubtractionSolid.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "G4SDManager.hh"
#include "G4RunManager.hh"
#include "MarsAstronautMessenger.hh"
#include "G4PhantomHeadBuilder.hh"
#include "G4CustomFemaleBuilder.hh"
#include "G4FemaleBuilder.hh"
#include "G4MaleBuilder.hh"
#include "G4UserLimits.hh"


MarsAstronaut::MarsAstronaut()
  : solidSkin(0),solidBody(0),solidBFO(0),solidOrgans(0),
    astronaut(0),skinOuter(0),skinInner(0),bodyOuter(0),bodyInner(0),
    bfoOuter(0),bfoInner(0),organs(0),skin(0),body(0),bfo(0),
    astronautLog(0),skinLog(0),bodyLog(0),bfoLog(0),organsLog(0),
  astronautPhys(0),skinPhys(0),bodyPhys(0),bfoPhys(0),organsPhys(0),material(0),messenger(0),builder(0),
  theAstronautUserLimits(0)
{ 
  G4SDManager* SDManager = G4SDManager::GetSDMpointer();

  astronautSD   = new MarsDoseSD("astronautSD");

  SDManager->AddNewDetector(astronautSD);
  material = new MarsMaterial();
  messenger = new MarsAstronautMessenger(this);
  astronautNumber=1;
  astronautPosition.push_back(G4ThreeVector(0,0,0));
  theAstronautUserLimits = new G4UserLimits (1.*cm);
}

MarsAstronaut::~MarsAstronaut()
{
  delete material;
  delete messenger;
}
void MarsAstronaut::ConstructComponent(G4VPhysicalVolume* motherVolume)
{
  ConstructAstronaut(motherVolume);
}

void MarsAstronaut::DestroyComponent()
{
  /*
  delete solidSkin;
  solidSkin=0;
  delete solidBody;
  solidBody=0;
  delete solidBFO;
  solidBFO=0;
  delete solidOrgans;
  solidOrgans=0;
  delete astronaut;
  astronaut=0;
  delete skinOuter;
  skinOuter=0;
  delete skinInner;
  skinInner=0;
  delete bodyOuter;
  bodyOuter=0;
  delete bodyInner;
  bodyInner=0;
  delete bfoOuter;
  bfoOuter=0;
  delete bfoInner;
  bfoInner=0;
  delete organs;
  organs=0;
  delete skin;
  skin=0;
  delete body;
  body=0;
  delete bfo;
  bfo=0;
  delete astronautLog;
  astronautLog=0;
  delete skinLog;
  skinLog=0;
  delete bodyLog;
  bodyLog=0;
  delete bfoLog;
  bfoLog=0;
  delete organsLog;
  organsLog=0;
  for(unsigned int i=0;i<astronautPhys.size();i++)
    delete astronautPhys[i];
  astronautPhys.clear();
  delete skinPhys;
  skinPhys=0;
  delete bodyPhys;
  bodyPhys=0;
  delete bfoPhys;
  bfoPhys=0;
  delete organsPhys;
  organsPhys=0;
  */
}


void MarsAstronaut::SetAstronautModel(G4String aModel){
  model = aModel;

  if (model == "MIRD")
    {
      G4cout<<" >> Astronaut " << model << " will be built."<<G4endl;
    }

  if (model == "ORNLFemale")
    {
      G4cout<<" >> Astronaut " << model << " will be built."<<G4endl;
    }

  if (model == "ORNLMale")
    {
      G4cout<<" >> Astronaut " << model << " will be built."<<G4endl;
    }

  if (model == "MIX")
    {
      G4cout<<" >> Astronaut " << model << " will be built."<<G4endl;
    }

  if (model == "MIRDHead")
    {
      G4cout<<" >> Astronaut " << model << " will be built."<<G4endl;
    }

  if (model == "ORNLHead")
    {
      G4cout<<" >> Astronaut " << model << " will be built."<<G4endl;
    }

  if (model == "Cylinder")
    {
      G4cout<<" >> Astronaut " << model << " will be built."<<G4endl;
    }

  if (model == "ICRU")
    {
      G4cout<<" >> Astronaut " << model << " will be built."<<G4endl;
    }
}

void MarsAstronaut::SetAstronautSex(G4String aSex){
  sex=aSex;

  if (sex == "Male")
    {
      G4cout << ">> Male Astronaut will be built." << G4endl;
    }
  if (sex == "Female")
    {
      G4cout << ">> Female Astronaut will be built." << G4endl;
    }
  if ((sex != "Female") && (sex != "Male"))
    G4cout << sex << " can not be defined!" << G4endl;
}


void MarsAstronaut::SetAstronautNumberAndPosition(G4int aNumb,std::vector<G4ThreeVector> aPos){
  astronautNumber = aNumb;
  astronautPosition.clear();
  for(unsigned int i = 0;i<aPos.size();i++)
    astronautPosition.push_back(aPos[i]);
}

void  MarsAstronaut::SetBodyPartSensitivity(G4String bodyPartName, G4bool bodyPartSensitivity)
{
  sensitivities[bodyPartName] = bodyPartSensitivity;
  if(bodyPartSensitivity==true) 
    G4cout << " >>> " << bodyPartName << " added as sensitive volume." << G4endl;
}

void MarsAstronaut::SetAstronautMaxStepLength(G4double maxStep)
{//G4cout<<maxStep/km<<std::endl; 
  theAstronautUserLimits->SetMaxAllowedStep(maxStep);
}

void MarsAstronaut::ConstructAstronaut(G4VPhysicalVolume* motherVolume)
{
  // Astronaut definition: Box of water
  // The Astronaut is the sensitive detector
  material->DefineMaterials();

  G4Material* vacuum = material -> GetMaterial("Galactic");
  // Astronaut sizes
  G4double astronautRmin = 0.*cm;
  G4double astronautRmax = 24.*cm;
  G4double astronautZ    = 200./2.*cm;

  
  astronaut = new G4Tubs("astronaut",astronautRmin,astronautRmax,astronautZ,0,2*pi);
  
  astronautLog = new G4LogicalVolume(astronaut,
				     vacuum,
				     "astronaut",
				     0,0,0);
  
  astronautPhys.push_back(new G4PVPlacement(0,astronautPosition[0],
					    "astronaut",astronautLog, 
					    motherVolume,false,0,true));
  
  G4Region* astronautRegion = new G4Region("Astronaut");
  astronautLog->SetRegion(astronautRegion);
  astronautRegion->AddRootLogicalVolume(astronautLog);
  astronautRegion->SetUserLimits(theAstronautUserLimits);

  if (model == "Cylinder"){
    BuildCylinderModel(astronautPhys[0]);
  }else if(model== "ICRU"){
    BuildICRUModel(astronautPhys[0]);
  }else{
    if (model == "MIRDHead" || model == "ORNLHead"){ 
      G4cout << "HeadBuilder instantiated" << G4endl;
      builder = new G4PhantomHeadBuilder;
      if (model ==  "MIRDHead") builder->SetModel("MIRD");
      else if (model ==  "ORNLHead") builder->SetModel("ORNLMale");
    }else{  
      if(sex =="Female"){ 
	if (model == "MIX"){
	  builder = new G4CustomFemaleBuilder;
	}else{
	  builder = new G4FemaleBuilder;
	}
	builder->SetModel(model);
	G4cout <<model << " "<< sex << G4endl;
      }else if (sex == "Male"){
	builder = new G4MaleBuilder;
	builder->SetModel(model);
	if (model == "MIX"){ 
	  G4cout<< "Custom Male is not available!!! MIRD model is selected !" 
		<< G4endl;
	  model = "MIRD";  
	  builder->SetModel(model);
	}
      }
    }
    builder->SetMotherVolume(astronautPhys[0]);
    BuildPhantomModel(builder);
  }
  if(astronautNumber>1){
    for(unsigned int j=1;j<astronautPosition.size();j++){
      astronautPhys.push_back(new G4PVPlacement(0,astronautPosition[j],
						"astronaut",astronautLog, 
						motherVolume,false,j,true));
    }
  }
}

void MarsAstronaut::BuildPhantomModel(G4BasePhantomBuilder*  builder){
  
  
  builder->BuildHead("black", false, sensitivities["Head"]);
  builder->BuildSkull("orange", false,sensitivities["Skull"]); 
  builder->BuildBrain("yellow", true,sensitivities["Brain"]); 
  
  if(model != "MIRDHead" && model != "ORNLHead"){ 
    //  builder->SetModel(model);
    builder->BuildTrunk("yellow", false, sensitivities["Trunk"]);
    
    builder->BuildLeftLeg("yellow", false,sensitivities["LeftLeg"]);
    builder->BuildRightLeg("yellow", false,sensitivities["RightLeg"]);
    
    builder->BuildLeftArmBone("grey", true,sensitivities["LeftArmBone"]);
    builder->BuildRightArmBone("grey", true, sensitivities["RightArmBone"]);  
    
    builder->BuildLeftLegBone("grey", true,sensitivities["LeftLegBone"]);
    builder ->BuildRightLegBone("grey", true,sensitivities["RightLegBone"]);
    
    builder->BuildLeftClavicle("grey", true,sensitivities["LeftClavicle"]);
    builder ->BuildRightClavicle("grey", true,sensitivities["RightClavicle"]);
    
    builder->BuildUpperSpine("yellow", true,sensitivities["UpperSpine"]); 
     
    if (model == "MIRD" || model == "MIX"){
      builder->BuildLeftScapula("grey", true, sensitivities["LeftScapula"]); 
      builder->BuildRightScapula("grey", true, sensitivities["RightScapula"]);
      builder->BuildLeftAdrenal("yellow", true, sensitivities["LeftAdrenal"]);
      builder->BuildRightAdrenal("yellow", true, sensitivities["RightAdrenal"]);
    }
    
    builder->BuildMiddleLowerSpine("yellow", true,sensitivities["MiddleLowerSpine"]);

    builder->BuildPelvis("grey", true,sensitivities["Pelvis"]); 

    builder->BuildStomach("orange", true,sensitivities["Stomach"]); 
    builder->BuildSmallIntestine("orange", true,sensitivities["SmallIntestine"]);
    builder->BuildUpperLargeIntestine("lightBlue", true,sensitivities["UpperLargeIntestine"]);
    builder->BuildLowerLargeIntestine("lightBlue", true,sensitivities["LowerLargeIntestine"]);
    if (model == "MIRD" || model == "MIX") 
      builder->BuildRibCage("grey", true,sensitivities["RibCage"]); 
    
    builder->BuildSpleen("green", true,sensitivities["Spleen"]);
    builder->BuildPancreas("purple", true,sensitivities["Pancreas"]); 
    builder->BuildLiver("orange", true,sensitivities["Liver"]); // da fare MIRD
    
    builder->BuildLeftKidney("green", true,sensitivities["LeftKidney"]);
    builder->BuildRightKidney("green", true,sensitivities["RightKidney"]);
    builder->BuildUrinaryBladder("green", true,sensitivities["UrinaryBladder"]);
    
    builder->BuildHeart("red", true,sensitivities["Hearth"]);//dafare MIRD
    builder->BuildLeftLung("blue", true,sensitivities["LeftLung"]);
    builder->BuildRightLung("blue", true,sensitivities["RightLung"]);
    builder->BuildThyroid("orange", true,sensitivities["Thyroid"]); 
    builder->BuildThymus("orange", true,sensitivities["Thymus"]); 
    
    if(sex=="Female"){
      builder->BuildLeftOvary("purple", true,sensitivities["LeftOvary"]);
      builder->BuildRightOvary("purple", true,sensitivities["RightOvary"]);
      builder->BuildUterus("purple", true,sensitivities["Uterus"]);
      
      if (model == "ORNLFemale" || model == "MIRD"){
	builder->BuildLeftBreast("purple", true,sensitivities["LeftBreast"]); 
	builder->BuildRightBreast("purple", true,sensitivities["RightBreast"]);
      }
      else if (model == "MIX"){
	builder->BuildVoxelLeftBreast("purple",false, sensitivities["LeftBreast"]); 
	builder->BuildVoxelRightBreast("purple", false, sensitivities["RightBreast"]);  
      } 
    }
    if(sex=="Male"){
      builder->BuildMaleGenitalia("yellow",false,sensitivities["MaleGenitalia"]);
      builder->BuildLeftTeste("purple",true,sensitivities["LeftTeste"]);
      builder->BuildRightTeste("purple",true,sensitivities["RightTeste"]);
    }
  }
}

void MarsAstronaut::BuildICRUModel(G4VPhysicalVolume* motherVolume)
{
  G4Material* g4skin    = material->GetMaterial("G4SKIN");
  G4Material* g4tissue  = material->GetMaterial("G4TISSUE4");
  
  G4double skinRmax = 15*cm;
  G4double skinRmin = skinRmax - 0.2*cm;
  
  solidSkin = new G4Sphere("skin",skinRmin,skinRmax,0,2*pi,0,pi);
  
  skinLog = new G4LogicalVolume(solidSkin,
				g4skin,
				"skin",
				0,0,0);

  skinLog->SetSensitiveDetector(astronautSD);

  G4double bodyRmin = skinRmin - 4.8*cm;
  G4double bodyRmax = skinRmin;
      
  solidBody = new G4Sphere("body",bodyRmin,bodyRmax,0,2*pi,0,pi);
      
  bodyLog = new G4LogicalVolume(solidBody,
				g4tissue,
				"body",
				0,0,0);
      
  bodyLog->SetSensitiveDetector(astronautSD);
      
  G4double bfoRmin = bodyRmin - 0.2*cm;
  G4double bfoRmax = bodyRmin;
  
  solidBFO = new G4Sphere("bfo",bfoRmin,bfoRmax,0,2*pi,0,pi);
  
  bfoLog = new G4LogicalVolume(solidBFO,
			       g4tissue,
			       "bfo",
			       0,0,0);
  
  bfoLog->SetSensitiveDetector(astronautSD);
  
  G4double organsRmin = 0;
  G4double organsRmax = bfoRmin;
  
  solidOrgans = new G4Sphere("organs",organsRmin,organsRmax,0,2*pi,0,pi);
  
  organsLog = new G4LogicalVolume(solidOrgans,
				  g4tissue,
				  "organs",
				  0,0,0);
  
  organsLog->SetSensitiveDetector(astronautSD);
  
  
  skinPhys = new G4PVPlacement(0,
			       G4ThreeVector(0.,0.,0),
			       "skin",skinLog, 
			       motherVolume,false,0,true);
  
  bodyPhys = new G4PVPlacement(0,
			       G4ThreeVector(0.,0.,0),
			       "body",bodyLog, 
			       motherVolume,false,0,true);
  
  bfoPhys = new G4PVPlacement(0,
			      G4ThreeVector(0.,0.,0),
			      "bfo",bfoLog, 
			      motherVolume,false,0,true);
  
  organsPhys = new G4PVPlacement(0,
				 G4ThreeVector(0.,0.,0),
				 "organs",organsLog, 
				 motherVolume,false,0,true);
      
  // Visualisation attributes
  G4VisAttributes* layerVisAttMagenta = new G4VisAttributes(G4Colour::Magenta());
  layerVisAttMagenta->SetVisibility(true);
  layerVisAttMagenta->SetForceAuxEdgeVisible(true);
  skinLog->SetVisAttributes(layerVisAttMagenta);
      
  G4VisAttributes* layerVisAttGreen = new G4VisAttributes(G4Colour::Green());
  layerVisAttGreen->SetVisibility(true);
  layerVisAttGreen->SetForceAuxEdgeVisible(true);
  bodyLog->SetVisAttributes(layerVisAttGreen); 
      
  G4VisAttributes* layerVisAttYellow = new G4VisAttributes(G4Colour::Yellow());
  layerVisAttYellow->SetVisibility(true);
  layerVisAttYellow->SetForceAuxEdgeVisible(true);
  bfoLog->SetVisAttributes(layerVisAttYellow); 
      
  G4VisAttributes* layerVisAttBlue = new G4VisAttributes(G4Colour::Blue());
  layerVisAttBlue->SetVisibility(true);
  layerVisAttBlue->SetForceAuxEdgeVisible(true);
  organsLog->SetVisAttributes(layerVisAttBlue); 

}

void MarsAstronaut::BuildCylinderModel(G4VPhysicalVolume* motherVolume)
{

  G4Material* water = material -> GetMaterial("Water");

  G4double skinRmax = 12*cm;
  G4double skinRmin = skinRmax - 0.2*cm;
  G4double skinZ    = 180./2.*cm;
  
  skinOuter = new G4Tubs("skinOuter",0,skinRmax,skinZ,0,2*pi);
  skinInner = new G4Tubs("skinInner",0,skinRmin,skinZ-0.2*cm,0,2*pi);
  
  skin = new G4SubtractionSolid("skin", skinOuter, skinInner);
  
  skinLog = new G4LogicalVolume(skin,
				water,
				"skin",
				0,0,0);

  skinLog->SetSensitiveDetector(astronautSD);

  G4double bodyRmin = skinRmin - 4.8*cm;
  G4double bodyRmax = skinRmin;
  G4double bodyZ    = skinZ - 0.2*cm;
  
  bodyOuter = new G4Tubs("bodyOuter",0,bodyRmax,bodyZ,0,2*pi);
  bodyInner = new G4Tubs("bodyInner",0,bodyRmin,bodyZ-4.8*cm,0,2*pi);

  body = new G4SubtractionSolid("body", bodyOuter, bodyInner);
  
  bodyLog = new G4LogicalVolume(body,
				water,
				"body",
				0,0,0);

  bodyLog->SetSensitiveDetector(astronautSD);
  
  G4double bfoRmin = bodyRmin - 0.2*cm;
  G4double bfoRmax = bodyRmin;
  G4double bfoZ    = bodyZ - 4.8*cm;
  
  bfoOuter = new G4Tubs("bfoOuter",0,bfoRmax,bfoZ,0,2*pi);
  bfoInner = new G4Tubs("bfoInner",0,bfoRmin,bfoZ-0.2*cm,0,2*pi);

  bfo = new G4SubtractionSolid("bfo", bfoOuter, bfoInner);
  
  bfoLog = new G4LogicalVolume(bfo,
			       water,
			       "bfo",
			       0,0,0);

  bfoLog->SetSensitiveDetector(astronautSD);

  G4double organsRmin = 0;
  G4double organsRmax = bfoRmin;
  G4double organsZ    = bfoZ - 0.2*cm;
  
  organs = new G4Tubs("organs",organsRmin,organsRmax,organsZ,0,2*pi);
  
  organsLog = new G4LogicalVolume(organs,
				  water,
				  "organs",
				  0,0,0);
  
  organsLog->SetSensitiveDetector(astronautSD);
  
  skinPhys = new G4PVPlacement(0,
			       G4ThreeVector(0.,0.,0),
			       "skin",skinLog, 
			       motherVolume,false,0,true);
  
  bodyPhys = new G4PVPlacement(0,
			       G4ThreeVector(0.,0.,0),
			       "body",bodyLog, 
			       motherVolume,false,0,true);
  
  bfoPhys = new G4PVPlacement(0,
			      G4ThreeVector(0.,0.,0),
			      "bfo",bfoLog, 
			      motherVolume,false,0,true);

  organsPhys = new G4PVPlacement(0,
				 G4ThreeVector(0.,0.,0),
				 "organs",organsLog, 
				 motherVolume,false,0,true);
   
  // Visualisation attributes
  G4VisAttributes* layerVisAttMagenta = new G4VisAttributes(G4Colour::Magenta());
  layerVisAttMagenta->SetVisibility(true);
  layerVisAttMagenta->SetForceAuxEdgeVisible(true);
  skinLog->SetVisAttributes(layerVisAttMagenta);
 
  G4VisAttributes* layerVisAttGreen = new G4VisAttributes(G4Colour::Green());
  layerVisAttGreen->SetVisibility(true);
  layerVisAttGreen->SetForceAuxEdgeVisible(true);
  bodyLog->SetVisAttributes(layerVisAttGreen); 

  G4VisAttributes* layerVisAttYellow = new G4VisAttributes(G4Colour::Yellow());
  layerVisAttYellow->SetVisibility(true);
  layerVisAttYellow->SetForceAuxEdgeVisible(true);
  bfoLog->SetVisAttributes(layerVisAttYellow); 

  G4VisAttributes* layerVisAttBlue = new G4VisAttributes(G4Colour::Blue());
  layerVisAttBlue->SetVisibility(true);
  layerVisAttBlue->SetForceAuxEdgeVisible(true);
  organsLog->SetVisAttributes(layerVisAttBlue); 
}

