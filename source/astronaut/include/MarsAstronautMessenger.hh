//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// Authors: S. Guatelli and M. G. Pia, INFN Genova, Italy
// 
// Based on code developed by the undergraduate student G. Guerrieri 
// Note: this is a preliminary beta-version of the code; an improved 
// version will be distributed in the next Geant4 public release, compliant
// with the design in a forthcoming publication, and subject to a 
// design and code review.
//
#ifndef MarsAstronautMessenger_h
#define MarsAstronautMessenger_h 1

#include "G4UImessenger.hh"
#include "globals.hh"
#include <iostream>

class MarsAstronaut;
class G4UIcommand;
class G4UIparameter;
class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithoutParameter;

class MarsAstronautMessenger: public G4UImessenger
{
public:
  MarsAstronautMessenger(MarsAstronaut*);
  ~MarsAstronautMessenger();
  
  void SetNewValue(G4UIcommand* command, G4String newValue);
  
  void AddBodyPart(G4String);	      // Set Body Parts Sensitivity
  
private:
  MarsAstronaut*           myAstronaut;

  G4UIdirectory*             astronautDir;
  G4UIdirectory*             bpDir;
  G4UIparameter*             param;

  G4UIcmdWithADoubleAndUnit* stepLengthCmd;
  G4UIcmdWithAString*        modelCmd; 
  G4UIcmdWithAString*        sexCmd;  
  G4UIcmdWithAString*        bodypartCmd;
  G4UIcmdWithoutParameter*   endCmd;
  G4UIcommand*               numberAndPositionCmd;
  G4String                   bodypart;
  G4bool                     bps;

};

#endif

