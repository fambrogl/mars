//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//    **********************************
//    *                                *
//    *    MarsAstronaut.hh *
//    *                                *
//    **********************************
//
// $Id$
//
// Author:Filippo Ambroglini: filippo.ambroglini@pg.infn.it

#ifndef MarsAstronaut_h
#define MarsAstronaut_h 1

#include "MarsVGeometryComponent.hh"
//#include "globals.hh"
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"
#include "MarsDoseSD.hh"
#include "G4BasePhantomBuilder.hh"
class G4VPhysicalVolume;
class G4Tubs;
class G4Sphere;
class G4SubtractionSolid;
class G4LogicalVolume;
class G4Material;
class G4UserLimits;
class MarsMaterial;
class MarsVGeometryComponent;
class G4VPhysicalVolume;
class G4VisAttributes;
class MarsAstronautMessenger;

class MarsAstronaut: public MarsVGeometryComponent
{
public:
  MarsAstronaut();
  ~MarsAstronaut();
  void ConstructComponent(G4VPhysicalVolume*);
  void DestroyComponent(); 
  void SetBodyPartSensitivity(G4String, G4bool);
  void SetAstronautSex(G4String);
  void SetAstronautModel(G4String);
  void SetAstronautNumberAndPosition(G4int,std::vector<G4ThreeVector>);
  void SetAstronautMaxStepLength(G4double);

private:
  void ConstructAstronaut(G4VPhysicalVolume*);  
  void BuildPhantomModel(G4BasePhantomBuilder*);
  void BuildCylinderModel(G4VPhysicalVolume*);
  void BuildICRUModel(G4VPhysicalVolume*);

  G4Sphere* solidSkin;
  G4Sphere* solidBody;
  G4Sphere* solidBFO;
  G4Sphere* solidOrgans;
  G4Tubs* astronaut;
  G4Tubs* skinOuter;
  G4Tubs* skinInner;
  G4Tubs* bodyOuter;
  G4Tubs* bodyInner;
  G4Tubs* bfoOuter;
  G4Tubs* bfoInner;
  G4Tubs* organs;
  G4SubtractionSolid* skin;
  G4SubtractionSolid* body;
  G4SubtractionSolid* bfo;
  G4LogicalVolume* astronautLog;
  G4LogicalVolume* skinLog;
  G4LogicalVolume* bodyLog;
  G4LogicalVolume* bfoLog;
  G4LogicalVolume* organsLog;
  std::vector<G4VPhysicalVolume*> astronautPhys;
  G4VPhysicalVolume* skinPhys;
  G4VPhysicalVolume* bodyPhys;
  G4VPhysicalVolume* bfoPhys;
  G4VPhysicalVolume* organsPhys;

  G4String model;
  G4String sex;
  std::map<std::string,G4bool> sensitivities;
  std::vector<G4ThreeVector> astronautPosition;
  G4int astronautNumber;


  MarsMaterial* material;
  MarsAstronautMessenger* messenger;
  MarsDoseSD* astronautSD;
  G4BasePhantomBuilder*  builder;

  G4UserLimits* theAstronautUserLimits;

};
#endif
