//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id$
//
// Code developed by: Filippo Ambroglini, filippo.ambroglini@pg.infn.it
//
#include "SR2SOpenToroidalMagneticShieldPumpkinV2.hh"
#include "MarsMaterial.hh"

#include "G4SystemOfUnits.hh"
#include "G4Material.hh"
#include "G4MaterialTable.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "G4RunManager.hh"
#include "G4VisAttributes.hh"
#include "SR2SOpenToroidalMagneticShieldPumpkinV2Messenger.hh"
#include "G4Torus.hh"
#include "G4UnionSolid.hh"
#include "G4SubtractionSolid.hh"

SR2SOpenToroidalMagneticShieldPumpkinV2::SR2SOpenToroidalMagneticShieldPumpkinV2(G4double halfX,G4double halfY,G4double halfZ)
{

  worldHalfX = halfX;
  worldHalfY = halfY;
  worldHalfZ = halfZ;

  racetrackSupportRingMiddlePosition      = 2756*mm;
  racetrackSupportRingThickness           = 100*mm;
  racetrackSupportRingLength              = 400*mm;
  racetrackSupportRingInnerRadius         = racetrackSupportRingMiddlePosition-racetrackSupportRingThickness/2.;
  racetrackSupportRingOuterRadius         = racetrackSupportRingMiddlePosition+racetrackSupportRingThickness/2.;
  racetrackSupportRingZposition           = 985.5*mm;
  
  racetrackSupportSmallCylinderRadius     = 100*mm;
  racetrackSupportSmallCylinderHeight     = 1000*mm;
  racetrackSupportBigCylinderRadius     = 201*mm;
  racetrackSupportBigCylinderHeight     = 3100*mm;
 
  bandageThickness                         = 0.5*mm;
  shortBandageLength                       = 4021*mm;
  shortBandageHeight                       = 3100*mm;
  shortBandageWidth                        = 661*mm;
  longBandageLength                        = 2560*mm;
  longBandageHeight                        = 4561*mm;
  longBandageWidth                         = 662*mm;
  
  coilBarrelLength                         = 2560*mm;
  coilBarrelWidth                          = 660*mm;
  coilThickness                            = 110*mm;
  coilBarrelHeight                         = 4560*mm;
  coilEndcapRadius                         = 730*mm;
  coilSupportThickness                     = 30*mm;

  racetrackCoilNumber                      = 3;
  coilAngle                                = (2*pi)/float(racetrackCoilNumber);
  
  racetrackNumber                          = 4;
  racetrackAngle                           = (2*pi)/float(racetrackNumber/2);
    
  cableBarrelLength                        = coilBarrelLength;
  cableBarrelWidth                         = coilBarrelWidth-coilSupportThickness*2;
  cableThickness                           = coilThickness-coilSupportThickness*2;
  cableBarrelHeight                        = coilBarrelHeight-coilSupportThickness*2;
  cableEndcapRadius                        = coilEndcapRadius-coilSupportThickness;

  racetrackTubeR                            = (shortBandageLength+racetrackSupportBigCylinderRadius+10*cm);
  racetrackTubeZ                            = racetrackSupportSmallCylinderHeight-coilEndcapRadius+longBandageHeight;

  coilDistanceFromCenter                   = racetrackSupportBigCylinderRadius;
  
  magneticFieldVolumeRmax                  = racetrackSupportRingInnerRadius-racetrackSupportRingThickness;
  magneticFieldVolumeHalfZ                 = 3.10*m;

  coilMaterial                            ="Aluminium";
  coreCableMaterial                       ="SCCable";
  bandageMaterial                         ="kevlar";
  SupportRingMaterial                     ="Aluminium";
  SupportCylinderMaterial                 ="Aluminium";
  
  materialShield=true;
  
  pMaterial = new MarsMaterial();
  messenger = new SR2SOpenToroidalMagneticShieldPumpkinV2Messenger(this);
}


void SR2SOpenToroidalMagneticShieldPumpkinV2::SetSR2SSupportRingParameter(G4double radius,G4double thickness,
									G4double length, G4double Zposition,
									G4String material)
{
  racetrackSupportRingMiddlePosition = radius;
  racetrackSupportRingThickness      = thickness;
  racetrackSupportRingInnerRadius    = racetrackSupportRingMiddlePosition-racetrackSupportRingThickness/2.;
  racetrackSupportRingOuterRadius    = racetrackSupportRingMiddlePosition+racetrackSupportRingThickness/2.;
  racetrackSupportRingLength         = length;
  racetrackSupportRingZposition      = Zposition;
  SupportRingMaterial                = material; 
}

void SR2SOpenToroidalMagneticShieldPumpkinV2::SetSR2SSupportCylinderParameter(G4double smallRadius,G4double smallHeight,
									    G4double bigRadius, G4double bigHeight,
									    G4String material)
{
  racetrackSupportSmallCylinderRadius     = smallRadius;
  racetrackSupportSmallCylinderHeight     = smallHeight;
  racetrackSupportBigCylinderRadius       = bigRadius;
  racetrackSupportBigCylinderHeight       = bigHeight;
  SupportCylinderMaterial                 = material;
  coilDistanceFromCenter                  = racetrackSupportBigCylinderRadius;

}

void SR2SOpenToroidalMagneticShieldPumpkinV2::SetSR2SCoilStructureParameter(G4double length,
									  G4double width,G4double thickness,
									  G4double  height,G4double endcapR,
									  G4double envelopeThickness,
									  G4int nCoil, G4int nRacetrack, G4String materialCoil,
									  G4String materialCable)
{
  coilBarrelLength                         = length;
  coilBarrelWidth                          = width;
  coilThickness                            = thickness;
  coilBarrelHeight                         = height;
  coilEndcapRadius                         = endcapR;
  coilSupportThickness                     = envelopeThickness;

  cableBarrelLength                        = coilBarrelLength;
  cableBarrelWidth                         = coilBarrelWidth-coilSupportThickness*2;
  cableThickness                           = coilThickness-coilSupportThickness*2;
  cableBarrelHeight                        = coilBarrelHeight-coilSupportThickness*2;
  cableEndcapRadius                        = coilEndcapRadius-coilSupportThickness;
  
  racetrackCoilNumber                      = nCoil;
  coilAngle                                = (2*pi)/float(racetrackCoilNumber);

  racetrackNumber                          = nRacetrack;
  racetrackAngle                           = (2*pi)/float(racetrackNumber/2);
    

  coilMaterial                             = materialCoil;
  coreCableMaterial                        = materialCable;
}

void SR2SOpenToroidalMagneticShieldPumpkinV2::SetSR2SBandageStructureParameter(G4double thickness,
									     G4double short_length,
									     G4double short_height,
									     G4double short_width,
									     G4double long_length,
									     G4double long_height,
									     G4double long_width,
									     G4String material)
{
  
  bandageThickness                        = thickness;
  shortBandageLength                      = short_length;
  shortBandageHeight                      = short_height;
  shortBandageWidth                       = short_width;
  longBandageLength                       = long_length;
  longBandageHeight                       = long_height;
  longBandageWidth                        = long_width;
  
  bandageMaterial                         = material;
}



SR2SOpenToroidalMagneticShieldPumpkinV2::~SR2SOpenToroidalMagneticShieldPumpkinV2()
{
  delete pMaterial;
  delete messenger;
}

void SR2SOpenToroidalMagneticShieldPumpkinV2::DestroyComponent()
{
  /*
  delete racetrackSupportStructure;
  racetrackSupportStructure=0;

  delete magneticFieldVolume;
  magneticFieldVolume=0;
  delete racetrackCoilBoxBarrelExternal;
  racetrackCoilBoxBarrelExternal=0;
  delete racetrackCoilBoxBarrelInternal;
  racetrackCoilBoxBarrelInternal=0;
  delete racetrackCoilBoxBarrel;
  racetrackCoilBoxBarrel=0;
  delete racetrackCoilBoxEndcapTubs;
  racetrackCoilBoxEndcapTubs=0;
  delete racetrackCoilBoxEndcapBox;
  racetrackCoilBoxEndcapBox=0;
  delete racetrackCoilBoxEndcap;
  racetrackCoilBoxEndcap=0;
  delete racetrackCoil;
  racetrackCoil=0;
  delete racetrackCableBoxBarrelExternal;
  racetrackCableBoxBarrelExternal=0;
  delete racetrackCableBoxBarrelInternal;
  racetrackCableBoxBarrelInternal=0;
  delete racetrackCableBoxBarrel;
  racetrackCableBoxBarrel=0;
  delete racetrackCableBoxEndcapTubs;
  racetrackCableBoxEndcapTubs=0;
  delete racetrackCableBoxEndcapBox;
  racetrackCableBoxEndcapBox=0;
  delete racetrackCableBoxEndcap;
  racetrackCableBoxEndcap=0;
  delete racetrackCable;
  racetrackCable=0;
  delete  bandageBoxLongExternal;
  bandageBoxLongExternal=0;
  delete  bandageBoxLongInternal;
  bandageBoxLongInternal=0;
  delete  bandageBoxLong;
  bandageBoxLong=0;
  delete  bandageBoxShortExternal;
  bandageBoxShortExternal=0;
  delete  bandageBoxShortInternal;
  bandageBoxShortInternal=0;
  delete  bandageBoxShort;
  bandageBoxShort=0;


  delete racetrackSupportStructureLog;
  racetrackSupportStructureLog=0;
  delete magneticFieldVolumeLog;
  magneticFieldVolumeLog=0;
  delete racetrackCoilLog;
  racetrackCoilLog=0;
  delete racetrackCableLog;
  racetrackCableLog=0;
  delete  bandageBoxShortLog;
  bandageBoxShortLog=0;
  delete  bandageBoxLongLog;
  bandageBoxLongLog=0;

  delete racetrackSupportStructurePhys;
  racetrackSupportStructurePhys=0;
  delete magneticFieldVolumePhys;
  magneticFieldVolumePhys=0;
  delete racetrackCoilPhys;
  racetrackCoilPhys=0;
  delete racetrackCablePhys;
  racetrackCablePhys=0;
  delete  bandageBoxShortPhys;
  bandageBoxShortPhys=0;
  delete  bandageBoxLongPhys;
  bandageBoxLongPhys=0;
  */

}

void SR2SOpenToroidalMagneticShieldPumpkinV2::ConstructComponent(G4VPhysicalVolume* motherVolume)
{
  // Geometry definition
  pMaterial->DefineMaterials();

  G4Material* vacuum =   pMaterial -> GetMaterial("Galactic");
  G4Material* supportR = pMaterial -> GetMaterial(SupportRingMaterial);	      
  G4Material* supportC = pMaterial -> GetMaterial(SupportCylinderMaterial);	      
  G4Material* bandage = pMaterial -> GetMaterial(bandageMaterial);	      
  G4Material* cable = pMaterial -> GetMaterial(coreCableMaterial);	      
  G4Material* coil =  pMaterial -> GetMaterial(coilMaterial);	      

  if(!materialShield){
    supportR = pMaterial -> GetMaterial("VACUUM");
    supportC = pMaterial -> GetMaterial("VACUUM");
    bandage =  pMaterial -> GetMaterial("VACUUM");
    cable = pMaterial -> GetMaterial("VACUUM");
    coil = pMaterial -> GetMaterial("VACUUM");
  }
  

  racetrackTubeR                            = (shortBandageLength+racetrackSupportBigCylinderRadius+10*cm);
  racetrackTubeZ                            = racetrackSupportSmallCylinderHeight-coilEndcapRadius+longBandageHeight;

  magneticFieldVolume = new G4SubtractionSolid("magneticFieldVolume",
					       new G4Box("magneticFieldVolumeBox",worldHalfX,worldHalfY,worldHalfZ),
					       new G4Tubs("magneticFieldVolumeTubs",0,magneticFieldVolumeRmax,magneticFieldVolumeHalfZ,0,2*pi));
  
 
  
  magneticFieldVolumeLog = new G4LogicalVolume(magneticFieldVolume,
					       vacuum,
					       "magneticFieldVolumeLog",
					       0,0,0);

  magneticFieldVolumePhys = new G4PVPlacement(0,
					      G4ThreeVector(0.,0.,0.),
					      "magneticFieldVolumePhys",
					      magneticFieldVolumeLog,
					      motherVolume,false,0,true);

  racetrackTube = new G4Tubs("raceTrackTube",0,racetrackTubeR,racetrackTubeZ/2.,0,2*pi);
  
  racetrackTubePlusLog =  new G4LogicalVolume(racetrackTube,
					      vacuum,
					      "racetrackTubePlusLog",
					      0,0,0);

  racetrackTubeMinusLog =  new G4LogicalVolume(racetrackTube,
					       vacuum,
					       "racetrackTubeMinusLog",
					       0,0,0);
  
  for(int i=0;i<racetrackNumber/2;i++){
    G4double localAngleMinus = i*racetrackAngle+pi/2.;
    G4double localAnglePlus = i*racetrackAngle;
    G4RotationMatrix* boxRotPlus = new G4RotationMatrix;
    G4RotationMatrix* boxRotMinus = new G4RotationMatrix;
    G4ThreeVector boxTrasPlus= G4ThreeVector((racetrackTubeZ/2.+racetrackSupportRingOuterRadius)*sin(localAnglePlus),
					     (racetrackTubeZ/2.+racetrackSupportRingOuterRadius)*cos(localAnglePlus),
					     racetrackSupportRingZposition);
    G4ThreeVector boxTrasMinus= G4ThreeVector((racetrackTubeZ/2.+racetrackSupportRingOuterRadius)*sin(localAngleMinus),
					     (racetrackTubeZ/2.+racetrackSupportRingOuterRadius)*cos(localAngleMinus),
					     -racetrackSupportRingZposition);
    boxRotPlus->rotateZ(localAnglePlus);
    boxRotPlus->rotateX(pi/2.);
    
    racetrackTubePlusPhys = new G4PVPlacement(boxRotPlus,boxTrasPlus,
					      "racetrackTubePlusPhys",
					      racetrackTubePlusLog,
					      magneticFieldVolumePhys,
					      false,i,true);
    
    boxRotMinus->rotateZ(localAngleMinus);
    boxRotMinus->rotateX(pi/2.);
    
    racetrackTubeMinusPhys = new G4PVPlacement(boxRotMinus,boxTrasMinus,
					       "racetrackTubeMinusPhys",
					       racetrackTubeMinusLog,
					       magneticFieldVolumePhys,
					       false,i,true);    
  }

  racetrackSupportRing = new G4Tubs("racetrackSupportRing",
				    racetrackSupportRingInnerRadius,
				    racetrackSupportRingOuterRadius,
				    racetrackSupportRingLength/2.,0,2*pi);
  
  racetrackSupportRingLog = new G4LogicalVolume(racetrackSupportRing,
						supportR,
						"racetrackSupportRingLog",
						0,0,0);
    
  racetrackSupportRingPhysPlus = new G4PVPlacement(0,
						   G4ThreeVector(0.,0.,racetrackSupportRingZposition),
						   "racetrackSupportRingPhys",
						   racetrackSupportRingLog,
						   magneticFieldVolumePhys,false,0,true);
  
  racetrackSupportRingPhysMinus = new G4PVPlacement(0,
						    G4ThreeVector(0.,0.,-racetrackSupportRingZposition),
						    "racetrackSupportRingPhys",
						    racetrackSupportRingLog,
						    magneticFieldVolumePhys,false,0,true);
  
  racetrackCoilBoxBarrelExternal = new G4Box("racetrackCoilBoxBarrelExternal",
					     coilBarrelWidth/2.,
					     coilBarrelHeight/2.,
					     coilBarrelLength/2.);

  racetrackCoilBoxBarrelInternal = new G4Box("racetrackCoilBoxBarrelInternal",
					     coilBarrelWidth/2.+10*mm,
					     (coilBarrelHeight-coilThickness*2)/2.,
					     coilBarrelLength/2.+10*mm);

  racetrackCoilBoxBarrel = new G4SubtractionSolid("racetrackCoilBoxBarrel",
						  racetrackCoilBoxBarrelExternal,
						  racetrackCoilBoxBarrelInternal);
  
  racetrackCoilBoxEndcapTubs = new G4Tubs("racetrackCoilBoxEndcapTubs",
					  coilEndcapRadius-coilThickness,
					  coilEndcapRadius,
					  coilBarrelWidth/2.,0,pi/2.);
  
  racetrackCoilBoxEndcapBox = new G4Box("racetrackCoilBoxEndcapBox",
					coilBarrelHeight/2.-coilEndcapRadius,
					coilThickness/2.,
					coilBarrelWidth/2);
  
  G4ThreeVector transEP = G4ThreeVector(coilBarrelHeight/2.-coilEndcapRadius,-(coilEndcapRadius-coilThickness/2.),0);
  G4ThreeVector transEM = G4ThreeVector(-(coilBarrelHeight/2.-coilEndcapRadius),-(coilEndcapRadius-coilThickness/2.),0);
  G4RotationMatrix* rotEP = new G4RotationMatrix;
  G4RotationMatrix* rotEM = new G4RotationMatrix;
  rotEM->rotateY(-pi);
  
  racetrackCoilBoxEndcap = new G4UnionSolid("racetrackCoilBoxEndcap",
					    new G4UnionSolid("partialracetrackCoilBoxEndcap",
							     racetrackCoilBoxEndcapBox,
							     racetrackCoilBoxEndcapTubs,
							     rotEP,transEP),
					    racetrackCoilBoxEndcapTubs,rotEM,transEM);
  
  G4ThreeVector transCM = G4ThreeVector(0,0,-(coilBarrelLength/2.+coilEndcapRadius-coilThickness/2.));
  G4RotationMatrix* rotCM = new G4RotationMatrix;
  rotCM->rotateZ(pi/2);
  rotCM->rotateX(pi/2);
  
  G4ThreeVector transCP = G4ThreeVector(0,0,coilBarrelLength/2.+coilEndcapRadius-coilThickness/2.);
  G4RotationMatrix* rotCP = new G4RotationMatrix;
  rotCP->rotateZ(pi/2);
  rotCP->rotateX(-pi/2);
  
  racetrackCoil = new G4UnionSolid("racetrackCoil",
				   new G4UnionSolid("partialracetrackCoil",
						    racetrackCoilBoxBarrel,
						    racetrackCoilBoxEndcap,
						    rotCP,transCP),
				   racetrackCoilBoxEndcap,rotCM,transCM);
  

  bandageBoxLongExternal = new G4Box("BandageBoxLongExternal",
				     longBandageWidth/2.,
				     longBandageHeight/2.,
				     longBandageLength/2.);

  bandageBoxLongInternal = new G4Box("BandageBoxLongInternal",
				     longBandageWidth/2.-bandageThickness,
				     longBandageHeight/2.-bandageThickness,
				     longBandageLength/2.+20*mm);
    
  bandageBoxLong = new G4SubtractionSolid("BandageBoxLong",
					  bandageBoxLongExternal,
					  bandageBoxLongInternal);

  bandageBoxLongLog = new G4LogicalVolume(bandageBoxLong,
					  bandage,
					  "bandageBoxLongLog",
					  0,0,0);

  bandageBoxShortExternal = new G4Box("BandageBoxShortExternal",
				     shortBandageWidth/2.,
				     shortBandageHeight/2.,
				     shortBandageLength/2.);

  bandageBoxShortInternal = new G4Box("BandageBoxShortInternal",
				      shortBandageWidth/2.-bandageThickness,
				      shortBandageHeight/2.+20*mm,
				      shortBandageLength/2.-bandageThickness);
  
  bandageBoxShort = new G4SubtractionSolid("BandageBoxShort",
					   bandageBoxShortExternal,
					   bandageBoxShortInternal);
  
  bandageBoxShortLog = new G4LogicalVolume(bandageBoxShort,
					   bandage,
					   "bandageBoxShortLog",
					   0,0,0);

  racetrackCoilLog = new G4LogicalVolume(racetrackCoil,
					 coil,
					 "racetrackCoilLog",
					 0,0,0);


  G4RotationMatrix* nullRot = new G4RotationMatrix; 
  G4ThreeVector cylinderZTrans(0, 0,-(racetrackSupportBigCylinderHeight/2.+racetrackSupportSmallCylinderHeight/2.));
  racetrackCylinderSupport = new G4UnionSolid("racetrackCylinderSupport",
					      new G4Tubs("bigCylinderSupport",0,racetrackSupportBigCylinderRadius,racetrackSupportBigCylinderHeight/2.,0,2*pi),
					      new G4Tubs("smallCylinderSupport",0,racetrackSupportSmallCylinderRadius,racetrackSupportSmallCylinderHeight/2.,0,2*pi),nullRot,cylinderZTrans);

  racetrackCylinderSupportLog = new G4LogicalVolume(racetrackCylinderSupport,
						    supportC,
						    "racetrackCylinderSupportLog",
						    0,0,0);

  racetrackCylinderSupportPlusPhys = new  G4PVPlacement(nullRot,
						    G4ThreeVector(0.,0.,(racetrackSupportSmallCylinderHeight-coilEndcapRadius)/2.),
						    "racetrackCylinderSupportPhys",
						    racetrackCylinderSupportLog,
						    racetrackTubePlusPhys,
						    false,0,true);

    racetrackCylinderSupportMinusPhys = new  G4PVPlacement(nullRot,
						    G4ThreeVector(0.,0.,(racetrackSupportSmallCylinderHeight-coilEndcapRadius)/2.),
						    "racetrackCylinderSupportPhys",
						    racetrackCylinderSupportLog,
						    racetrackTubeMinusPhys,
						    false,0,true);

  for(int i=0;i<racetrackCoilNumber;i++){
    G4double localAngle = i*coilAngle;
    G4RotationMatrix* boxRot = new G4RotationMatrix;
    G4ThreeVector boxTras= G4ThreeVector((coilDistanceFromCenter+shortBandageLength/2.)*sin(localAngle),
					 (coilDistanceFromCenter+shortBandageLength/2.)*cos(localAngle),
					 racetrackTubeZ/2.-longBandageHeight/2.);
    boxRot->rotateZ(localAngle);
    boxRot->rotateX(pi/2.);
    racetrackCoilPlusPhys = new G4PVPlacement(boxRot,boxTras,
					  "racetrackCoilPlusPhys",
					  racetrackCoilLog,
					  racetrackTubePlusPhys,
					  false,i,true);    
    
    bandageBoxShortPlusPhys = new G4PVPlacement(boxRot,boxTras,
					    "bandageBoxShortPlusPhys",
					    bandageBoxShortLog,
					    racetrackTubePlusPhys,
					    false,i,true);    
    
    bandageBoxLongPlusPhys = new G4PVPlacement(boxRot,boxTras,
					   "bandageBoxLongPlusPhys",
					   bandageBoxLongLog,
					   racetrackTubePlusPhys,
					   false,i,true);    
  }

    for(int i=0;i<racetrackCoilNumber;i++){
    G4double localAngle = i*coilAngle+pi;
    G4RotationMatrix* boxRot = new G4RotationMatrix;
    G4ThreeVector boxTras= G4ThreeVector((coilDistanceFromCenter+shortBandageLength/2.)*sin(localAngle),
					 (coilDistanceFromCenter+shortBandageLength/2.)*cos(localAngle),
					 racetrackTubeZ/2.-longBandageHeight/2.);
    boxRot->rotateZ(localAngle);
    boxRot->rotateX(pi/2.);
    racetrackCoilMinusPhys = new G4PVPlacement(boxRot,boxTras,
					  "racetrackCoilMinusPhys",
					  racetrackCoilLog,
					  racetrackTubeMinusPhys,
					  false,i,true);    
    
    bandageBoxShortMinusPhys = new G4PVPlacement(boxRot,boxTras,
					    "bandageBoxShortMinusPhys",
					    bandageBoxShortLog,
					    racetrackTubeMinusPhys,
					    false,i,true);    
    
    bandageBoxLongMinusPhys = new G4PVPlacement(boxRot,boxTras,
					   "bandageBoxLongMinusPhys",
					   bandageBoxLongLog,
					   racetrackTubeMinusPhys,
					   false,i,true);    
  }

    
  racetrackCableBoxBarrelExternal = new G4Box("racetrackCableBoxBarrelExternal",
					     cableBarrelWidth/2.,
					     cableBarrelHeight/2.,
					     cableBarrelLength/2.);

  racetrackCableBoxBarrelInternal = new G4Box("racetrackCableBoxBarrelInternal",
					     cableBarrelWidth/2.+10*mm,
					     (cableBarrelHeight-cableThickness*2)/2.,
					     cableBarrelLength/2.+10*mm);

  racetrackCableBoxBarrel = new G4SubtractionSolid("racetrackCableBoxBarrel",
						  racetrackCableBoxBarrelExternal,
						  racetrackCableBoxBarrelInternal);
  
  racetrackCableBoxEndcapTubs = new G4Tubs("racetrackCableBoxEndcapTubs",
					  cableEndcapRadius-cableThickness,
					  cableEndcapRadius,
					  cableBarrelWidth/2.,0,pi/2.);
  
  racetrackCableBoxEndcapBox = new G4Box("racetrackCableBoxEndcapBox",
					cableBarrelHeight/2.-cableEndcapRadius,
					cableThickness/2.,
					cableBarrelWidth/2);

  transEP = G4ThreeVector(cableBarrelHeight/2.-cableEndcapRadius,-(cableEndcapRadius-cableThickness/2.),0);
  transEM = G4ThreeVector(-(cableBarrelHeight/2.-cableEndcapRadius),-(cableEndcapRadius-cableThickness/2.),0);

  racetrackCableBoxEndcap = new G4UnionSolid("racetrackCableBoxEndcap",
					    new G4UnionSolid("partialracetrackCableBoxEndcap",
							     racetrackCableBoxEndcapBox,
							     racetrackCableBoxEndcapTubs,
							     rotEP,transEP),
					    racetrackCableBoxEndcapTubs,rotEM,transEM);
  
  transCM = G4ThreeVector(0,0,-(cableBarrelLength/2.+cableEndcapRadius-cableThickness/2.));
    
  transCP = G4ThreeVector(0,0,cableBarrelLength/2.+cableEndcapRadius-cableThickness/2.);
    
  racetrackCable = new G4UnionSolid("racetrackCable",
				   new G4UnionSolid("partilaracetrackCable",
						    racetrackCableBoxBarrel,
						    racetrackCableBoxEndcap,
						    rotCP,transCP),
				   racetrackCableBoxEndcap,rotCM,transCM);
  
  
  racetrackCableLog = new G4LogicalVolume(racetrackCable,
					 cable,
					 "racetrackCableLog",
					 0,0,0);
  
  racetrackCablePlusPhys = new G4PVPlacement(0,
					 G4ThreeVector(0.,0.,0.),
					 "racetrackCablePhys",
					 racetrackCableLog,
					 racetrackCoilPlusPhys,
					 false,0,true);

  racetrackCableMinusPhys = new G4PVPlacement(0,
					 G4ThreeVector(0.,0.,0.),
					 "racetrackCablePhys",
					 racetrackCableLog,
					 racetrackCoilMinusPhys,
					 false,0,true);

  
  G4VisAttributes* layerVisAttMagenta = new G4VisAttributes(G4Colour::Magenta());
  layerVisAttMagenta->SetVisibility(true);
  //layerVisAttMagenta->SetForceAuxEdgeVisible(true);
  layerVisAttMagenta->SetForceSolid(true);
  G4VisAttributes* layerVisAttGreen = new G4VisAttributes(G4Colour::Green());
  layerVisAttGreen->SetVisibility(true);
  //layerVisAttGreen->SetForceAuxEdgeVisible(true);
  layerVisAttGreen->SetForceSolid(true);
  G4VisAttributes* layerVisAttBlue = new G4VisAttributes(G4Colour::Blue());
  layerVisAttBlue->SetVisibility(true);
  //layerVisAttBlue->SetForceAuxEdgeVisible(true);
  layerVisAttBlue->SetForceSolid(true);
  G4VisAttributes* layerVisAttYellow = new G4VisAttributes(G4Colour::Yellow());
  layerVisAttYellow->SetVisibility(true);
  //layerVisAttYellow->SetForceAuxEdgeVisible(true);
  layerVisAttYellow->SetForceSolid(true);
  G4VisAttributes* layerVisAttRed = new G4VisAttributes(G4Colour::Red());
  layerVisAttRed->SetVisibility(true);
  //layerVisAttRed->SetForceAuxEdgeVisible(true);
  layerVisAttRed->SetForceSolid(true);
  G4VisAttributes* layerVisAttGray = new G4VisAttributes(G4Colour::Gray());
  layerVisAttGray->SetVisibility(true);
  //layerVisAttGray->SetForceAuxEdgeVisible(true);
  layerVisAttGray->SetForceSolid(true);
  G4VisAttributes* layerVisAttCyan = new G4VisAttributes(G4Colour::Cyan());
  layerVisAttCyan->SetVisibility(true);
  //layerVisAttCyan->SetForceAuxEdgeVisible(true);
  layerVisAttCyan->SetForceSolid(true);
  G4VisAttributes* layerVisAttInvisible = new G4VisAttributes();
  layerVisAttInvisible->SetVisibility(false);
  //layerVisAttInvisible->SetForceAuxEdgeVisible(false);
  layerVisAttInvisible->SetForceSolid(false);

  racetrackCoilLog->SetVisAttributes(layerVisAttYellow);
  racetrackCableLog->SetVisAttributes(layerVisAttRed);
  bandageBoxLongLog->SetVisAttributes(layerVisAttCyan);
  bandageBoxShortLog->SetVisAttributes(layerVisAttCyan);
  racetrackCylinderSupportLog->SetVisAttributes(layerVisAttGreen);
  magneticFieldVolumeLog->SetVisAttributes(layerVisAttInvisible);
  racetrackTubePlusLog->SetVisAttributes(layerVisAttInvisible);
  racetrackTubeMinusLog->SetVisAttributes(layerVisAttBlue);
  racetrackSupportRingLog->SetVisAttributes(layerVisAttGray); 

  G4double coilSuppMass = ((racetrackCoilLog->GetSolid()->GetCubicVolume())-(racetrackCableLog->GetSolid()->GetCubicVolume()))*(racetrackCoilLog->GetMaterial()->GetDensity())/kg;
  G4double racetrackSuppMass = (racetrackCylinderSupportLog->GetSolid()->GetCubicVolume())*(racetrackCylinderSupportLog->GetMaterial()->GetDensity())/kg;
  G4double cableMass = (racetrackCableLog->GetSolid()->GetCubicVolume())*(racetrackCableLog->GetMaterial()->GetDensity())/kg;
  G4double supportMass = (racetrackSupportRingLog->GetSolid()->GetCubicVolume())*(racetrackSupportRingLog->GetMaterial()->GetDensity())/kg;
  G4double bandageMass = ((bandageBoxLongLog->GetSolid()->GetCubicVolume())+(bandageBoxShortLog->GetSolid()->GetCubicVolume()))*(bandageBoxLongLog->GetMaterial()->GetDensity())/kg;

  std::cout<<"Mass of Geometry"<<std::endl;
  std::cout<<"Single Cable Mass = "<<cableMass/1000<<" Tons; Total Mass = "<<cableMass*racetrackCoilNumber*racetrackNumber/1000<<" Tons;"<<std::endl;
  std::cout<<"Single CoilSupport Mass = "<<coilSuppMass/1000<<" Tons; Total Mass = "<<coilSuppMass*racetrackCoilNumber*racetrackNumber/1000<<" Tons;"<<std::endl;
  std::cout<<"Single RaceTrack Mass = "<<(cableMass+coilSuppMass)/1000<<" Tons; Total Mass = "<<(cableMass+coilSuppMass)*racetrackCoilNumber*racetrackNumber/1000<<" Tons;"<<std::endl;
  std::cout<<"Single RaceTrack Support Mass = "<<racetrackSuppMass/1000<<" Tons; Total Mass = "<<racetrackSuppMass*racetrackNumber/1000<<" Tons;"<<std::endl;
  std::cout<<"Single Bandage Mass = "<<bandageMass/1000<<" Tons; Total Mass = "<<bandageMass*racetrackCoilNumber*racetrackNumber/1000<<" Tons;"<<std::endl;
  std::cout<<"Support structure Mass = "<<2*supportMass/1000<<" Tons;"<<std::endl;
  std::cout<<"Total Shield Mass = "<<((cableMass+coilSuppMass+bandageMass)*racetrackCoilNumber*racetrackNumber+racetrackSuppMass*racetrackNumber+2*supportMass)/1000<<" Tons;"<<std::endl;
}

void SR2SOpenToroidalMagneticShieldPumpkinV2::SetFieldManager(G4FieldManager* fieldManager)
{
  magneticFieldVolumeLog->SetFieldManager(fieldManager,true);
}
 
