//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id$
//
// Code developed by: S.Guatelli, susanna@uow.edu.au
//
#include "NIACMagneticShield.hh"
#include "MarsMaterial.hh"

#include "G4SystemOfUnits.hh"
#include "G4Material.hh"
#include "G4MaterialTable.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "G4RunManager.hh"
#include "G4VisAttributes.hh"
#include "NIACMagneticShieldMessenger.hh"

NIACMagneticShield::NIACMagneticShield()
{
  compensationSolenoidRadius = 3.2*m;
  compensationSolenoidExternalThickness = 0.0111*cm;
  compensationSolenoidInternalThickness = 0.24*cm;
  halfSolenoidLength = 10.*m;
  solenoidRadius = 400.7555*cm;
  solenoidThickness = 0.0111*cm;
  solenoidCentralSupportRadius = 50.0*cm;
  solenoidCentralSupportThickness = 1.0*cm;
  solenoidRadialSupportThickness = 0.25*cm;
  nSolenoid = 6;
  nRadialSolenoidSupport = 6;
  solenoidRadialSupportShortSide = (solenoidRadius-2*solenoidThickness-solenoidCentralSupportRadius);
  radialDistanceToSpacecraft = solenoidRadius/sin(pi/float(nSolenoid));//+0.1*solenoidRadius/sin(pi/float(nSolenoid)); 
  angleSolenoid = 2*pi/float(nSolenoid);
  angleRadialSupport = 2*pi/float(nRadialSolenoidSupport);

  materialShield=true;
  pMaterial = new MarsMaterial();
  messenger = new NIACMagneticShieldMessenger(this);
}

void NIACMagneticShield::SetNIACShieldStructureParameter(G4double length, G4double compradius,G4double eThick, G4double iThick,G4int number,G4double radius,G4double thick,G4double cradius,G4double cthick,G4double rthick,G4int rnumb,G4bool aMaterial)
{
  halfSolenoidLength = length;

  compensationSolenoidRadius = compradius;
  compensationSolenoidExternalThickness = eThick;
  compensationSolenoidInternalThickness = iThick;
  
  nSolenoid = number;
  solenoidRadius = radius;
  solenoidThickness = thick;
  solenoidCentralSupportRadius = cradius;
  solenoidCentralSupportThickness = cthick;
  solenoidRadialSupportThickness = rthick;
  nRadialSolenoidSupport = rnumb;
  solenoidRadialSupportShortSide = (solenoidRadius-2*solenoidThickness-solenoidCentralSupportRadius);
  radialDistanceToSpacecraft = solenoidRadius/sin(pi/float(nSolenoid));//+0.1*solenoidRadius/sin(pi/float(nSolenoid)); 
  angleSolenoid = 2*pi/float(nSolenoid);
  angleRadialSupport = 2*pi/float(nRadialSolenoidSupport);
  materialShield=aMaterial;

}

NIACMagneticShield::~NIACMagneticShield()
{
  delete pMaterial;
  delete messenger;
}

void NIACMagneticShield::DestroyComponent()
{
  delete compensationSolenoidExternal;
  compensationSolenoidExternal=0;
  delete compensationSolenoidInternal;
  compensationSolenoidInternal=0;
  delete solenoid;
  solenoid=0;
  delete superconductingCoil;
  superconductingCoil=0;
  delete centralSolenoidSupport;
  centralSolenoidSupport=0;
  delete radialSolenoidSupport;
  radialSolenoidSupport=0;
  
  delete compensationSolenoidExternalLog;
  compensationSolenoidExternalLog=0;
  delete compensationSolenoidInternalLog;
  compensationSolenoidInternalLog=0;
  delete solenoidLog;
  solenoidLog=0;
  delete superconductingCoilLog;
  superconductingCoilLog=0;
  delete centralSolenoidSupportLog;
  centralSolenoidSupportLog=0;
  delete radialSolenoidSupportLog;
  radialSolenoidSupportLog=0;

  delete compensationSolenoidExternalPhys;
  compensationSolenoidExternalPhys=0;
  delete compensationSolenoidInternalPhys;
  compensationSolenoidInternalPhys=0;

  delete solenoidPhys;
  solenoidPhys=0;
  delete superconductingCoilPhys;
  superconductingCoilPhys=0;
  delete centralSolenoidSupportPhys;
  centralSolenoidSupportPhys=0;
  delete radialSolenoidSupportPhys;
  radialSolenoidSupportPhys=0;
}

void NIACMagneticShield::ConstructComponent(G4VPhysicalVolume* motherVolume)
{
  // Geometry definition
  pMaterial->DefineMaterials();

  G4Material* vacuum = pMaterial -> GetMaterial("Galactic");
  G4Material* copper = pMaterial -> GetMaterial("Copper");
  G4Material* carbon = pMaterial -> GetMaterial("Graphite");

  if(!materialShield){
    copper = pMaterial -> GetMaterial("Galactic");
    carbon = pMaterial -> GetMaterial("Galactic");
  }

  compensationSolenoidExternal = new G4Tubs("compensationSolenoidExternal",
					    compensationSolenoidRadius-compensationSolenoidExternalThickness,
					    compensationSolenoidRadius,
					    halfSolenoidLength,0.,2.*pi);

  compensationSolenoidExternalLog = new G4LogicalVolume(compensationSolenoidExternal,
							copper,
							"compensationSolenoidExternalLog",
							0,0,0);
  
  compensationSolenoidExternalPhys = new G4PVPlacement(0,
						       G4ThreeVector(0.,0.,0.),
						       "compensationSolenoidExternalPhys", 
						       compensationSolenoidExternalLog,
						       motherVolume,false,0,true); 

  compensationSolenoidInternal = new G4Tubs("compensationSolenoidInternal",
					    compensationSolenoidRadius-compensationSolenoidExternalThickness-compensationSolenoidInternalThickness,
					    compensationSolenoidRadius-compensationSolenoidExternalThickness,
					    halfSolenoidLength,
					    0.,2.*pi);

  compensationSolenoidInternalLog = new G4LogicalVolume(compensationSolenoidInternal,
							carbon,
							"compensationSolenoidInternalLog",
							0,0,0);
  
  compensationSolenoidInternalPhys = new G4PVPlacement(0,
						       G4ThreeVector(0.,0.,0.),
						       "compensationSolenoidInternalPhys", 
						       compensationSolenoidInternalLog,
						       motherVolume,false,0,true); 

  solenoid = new G4Tubs("solenoid",0,
			solenoidRadius,
			halfSolenoidLength,
			0,2*pi);

  solenoidLog = new G4LogicalVolume(solenoid,
				    vacuum,
				    "solenoidLog",
				    0,0,0);

  superconductingCoil = new G4Tubs("superconductingCoil",
				   solenoidRadius-solenoidThickness,
				   solenoidRadius,
				   halfSolenoidLength,
				   0,2*pi);

  superconductingCoilLog = new G4LogicalVolume(superconductingCoil,
					       copper,
					       "superconductingCoilLog",
					       0,0,0);
  
  centralSolenoidSupport = new G4Tubs("centralSolenoidSupport",
				      solenoidCentralSupportRadius-solenoidCentralSupportThickness,
				      solenoidCentralSupportRadius,
				      halfSolenoidLength,
				      0,2*pi);
  
  centralSolenoidSupportLog = new G4LogicalVolume(centralSolenoidSupport,
						  carbon,
						  "centralSolenoidSupportLog",
						  0,0,0);

  radialSolenoidSupport = new G4Box("radialSolenoidSupport",
				    solenoidRadialSupportShortSide/2.,
				    solenoidRadialSupportThickness/2.,
				    halfSolenoidLength);

  radialSolenoidSupportLog = new G4LogicalVolume(radialSolenoidSupport,
						 carbon,
						 "radialSolenoidSupportLog",
						 0,0,0);

  for(int i=0;i<nSolenoid;i++){
    G4double localAngle = (i+1)*angleSolenoid;
    
    solenoidPhys = new G4PVPlacement(0,
				     G4ThreeVector(radialDistanceToSpacecraft*cos(localAngle),radialDistanceToSpacecraft*sin(localAngle),0.),
				     "solenoidPhys",
				     solenoidLog,
				     motherVolume,
				     false,i,true);
  }
  superconductingCoilPhys = new G4PVPlacement(0,
					      G4ThreeVector(0.,0.,0.),
					      "superconductingCoilPhys", 
					      superconductingCoilLog,
					      solenoidPhys,false,0,true); 
    
  centralSolenoidSupportPhys = new G4PVPlacement(0,
						 G4ThreeVector(0.,0.,0.),
						 "centralSolenoidSupportPhys", 
						 centralSolenoidSupportLog,
						 solenoidPhys,false,0,true);
  
  for(int j=0;j<nRadialSolenoidSupport;j++){
    G4double localRadialSupportAngle = (j+1)*angleRadialSupport;
    G4RotationMatrix* rotation = new G4RotationMatrix(G4ThreeVector(cos(localRadialSupportAngle),sin(localRadialSupportAngle),0.),
						      G4ThreeVector(-sin(localRadialSupportAngle),cos(localRadialSupportAngle),0),
						      G4ThreeVector(0.,0.,1.));
    radialSolenoidSupportPhys = new G4PVPlacement(G4Transform3D(*rotation,
								G4ThreeVector((solenoidRadialSupportShortSide/2.+solenoidCentralSupportRadius)*cos(localRadialSupportAngle),
									      (solenoidRadialSupportShortSide/2.+solenoidCentralSupportRadius)*sin(localRadialSupportAngle),
									      0.)),
						  "radialSolenoidSupportPhys", 
						  radialSolenoidSupportLog,
						  solenoidPhys,false,j,true);
  }
  
  G4VisAttributes* layerVisAttMagenta = new G4VisAttributes(G4Colour::Magenta());
  layerVisAttMagenta->SetVisibility(true);
  //  layerVisAttMagenta->SetForceSolid(true);
  layerVisAttMagenta->SetForceAuxEdgeVisible(true);
  G4VisAttributes* layerVisAttGreen = new G4VisAttributes(G4Colour::Green());
  layerVisAttGreen->SetVisibility(true);
  //  layerVisAttGreen->SetForceSolid(true);
  layerVisAttGreen->SetForceAuxEdgeVisible(true);
  G4VisAttributes* layerVisAttBlue = new G4VisAttributes(G4Colour::Blue());
  layerVisAttBlue->SetVisibility(true);
  //  layerVisAttBlue->SetForceSolid(true);
  layerVisAttBlue->SetForceAuxEdgeVisible(true);
  G4VisAttributes* layerVisAttYellow = new G4VisAttributes(G4Colour::Yellow());
  layerVisAttYellow->SetVisibility(true);
  //  layerVisAttYellow->SetForceSolid(true);
  layerVisAttYellow->SetForceAuxEdgeVisible(true);
  G4VisAttributes* layerVisAttRed = new G4VisAttributes(G4Colour::Red());
  layerVisAttRed->SetVisibility(true);
  //  layerVisAttRed->SetForceSolid(true);
  layerVisAttRed->SetForceAuxEdgeVisible(true);
  G4VisAttributes* layerVisAttInvisible = new G4VisAttributes();
  layerVisAttInvisible->SetVisibility(false);
  //  layerVisAttInvisible->SetForceSolid(true);
  layerVisAttInvisible->SetForceAuxEdgeVisible(false);
 
  compensationSolenoidExternalLog->SetVisAttributes(layerVisAttBlue);
  compensationSolenoidInternalLog->SetVisAttributes(layerVisAttGreen);
  solenoidLog->SetVisAttributes(layerVisAttInvisible);
  //solenoidLog->SetVisAttributes(layerVisAttRed);
  superconductingCoilLog->SetVisAttributes(layerVisAttRed);
  centralSolenoidSupportLog->SetVisAttributes(layerVisAttYellow);
  radialSolenoidSupportLog->SetVisAttributes(layerVisAttYellow);

}

void NIACMagneticShield::SetFieldManager(G4FieldManager* fieldManager)
{
  solenoidLog->SetFieldManager(fieldManager,true);
}
 
