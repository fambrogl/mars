//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// Code developed by:
//  S.Guatelli
//
//    *********************************
//    *                               *
//    *    SR2SToroidalMagneticShieldSimplifiedMessenger.cc *
//    *                               *
//    *********************************
//
//
// $Id$
//
// 

#include "SR2SToroidalMagneticShieldSimplifiedMessenger.hh"
#include "SR2SToroidalMagneticShieldSimplified.hh"
#include "G4UIdirectory.hh"
#include "G4UIparameter.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcommand.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UnitsTable.hh"

SR2SToroidalMagneticShieldSimplifiedMessenger::SR2SToroidalMagneticShieldSimplifiedMessenger( SR2SToroidalMagneticShieldSimplified* sr2s): sr2sShield(sr2s)
{ 
  sr2sShieldDir = new G4UIdirectory("/SR2SShieldSimplified/");
  sr2sShieldDir->SetGuidance("SR2S Shield control.");
   
  sr2sTitaniumStructureCmd = new G4UIcommand("/SR2SShieldSimplified/TitaniumStructure",this);
  sr2sTitaniumStructureCmd->SetGuidance("[usage] /SR2SShieldSimplified/TitaniumStructure Radius Thicknes Length unit Material");
  sr2sTitaniumStructureCmd->AvailableForStates(G4State_Idle); 
  param = new G4UIparameter("Radius",'d',false);
  sr2sTitaniumStructureCmd->SetParameter(param);
  param = new G4UIparameter("Thickness",'d',false);
  sr2sTitaniumStructureCmd->SetParameter(param);
  param = new G4UIparameter("Length",'d',false);
  sr2sTitaniumStructureCmd->SetParameter(param);
  param = new G4UIparameter("length_unit",'s',false);
  sr2sTitaniumStructureCmd->SetParameter(param);
  param = new G4UIparameter("Material",'s',false);
  sr2sTitaniumStructureCmd->SetParameter(param);
  

  sr2sHydrogenStructureCmd = new G4UIcommand("/SR2SShieldSimplified/HydrogenStructure",this);
  sr2sHydrogenStructureCmd->SetGuidance("[usage] /SR2SShieldSimplified/HydrogenStructure Radius Thicknes Length unit Material");
  sr2sHydrogenStructureCmd->AvailableForStates(G4State_Idle); 
  param = new G4UIparameter("Radius",'d',false);
  sr2sHydrogenStructureCmd->SetParameter(param);
  param = new G4UIparameter("Thickness",'d',false);
  sr2sHydrogenStructureCmd->SetParameter(param);
  param = new G4UIparameter("Length",'d',false);
  sr2sHydrogenStructureCmd->SetParameter(param);
  param = new G4UIparameter("length_unit",'s',false);
  sr2sHydrogenStructureCmd->SetParameter(param);
  param = new G4UIparameter("Material",'s',false);
  sr2sHydrogenStructureCmd->SetParameter(param);

  sr2sSupportStructureCmd = new G4UIcommand("/SR2SShieldSimplified/SupportStructure",this);
  sr2sSupportStructureCmd->SetGuidance("[usage] /SR2SShieldSimplified/SupportStructure Radius Thicknes Length unit Material");
  sr2sSupportStructureCmd->AvailableForStates(G4State_Idle); 
  param = new G4UIparameter("Radius",'d',false);
  sr2sSupportStructureCmd->SetParameter(param);
  param = new G4UIparameter("Thickness",'d',false);
  sr2sSupportStructureCmd->SetParameter(param);
  param = new G4UIparameter("Length",'d',false);
  sr2sSupportStructureCmd->SetParameter(param);
  param = new G4UIparameter("length_unit",'s',false);
  sr2sSupportStructureCmd->SetParameter(param);
  param = new G4UIparameter("Material",'s',false);
  sr2sSupportStructureCmd->SetParameter(param);
    
  sr2sCoilStructureCmd = new G4UIcommand("/SR2SShieldSimplified/CoilStructure",this);
  sr2sCoilStructureCmd->SetGuidance("[usage] /SR2SShieldSimplified/CoilStructure RadialDistance Length Width Thickness Heigth EndcapRadius unit CoilNumber Material");
  sr2sCoilStructureCmd->AvailableForStates(G4State_Idle); 
  param = new G4UIparameter("RadialDistance",'d',false);
  sr2sCoilStructureCmd->SetParameter(param);
  param = new G4UIparameter("Length",'d',false);
  sr2sCoilStructureCmd->SetParameter(param);
  param = new G4UIparameter("Width",'d',false);
  sr2sCoilStructureCmd->SetParameter(param);
  param = new G4UIparameter("Thickness",'d',false);
  sr2sCoilStructureCmd->SetParameter(param);
  param = new G4UIparameter("Height",'d',false);
  sr2sCoilStructureCmd->SetParameter(param);
  param = new G4UIparameter("EndcapRadius",'d',false);
  sr2sCoilStructureCmd->SetParameter(param);
  param = new G4UIparameter("length_unit",'s',false);
  sr2sCoilStructureCmd->SetParameter(param);
  param = new G4UIparameter("CoilNumber",'i',false);
  sr2sCoilStructureCmd->SetParameter(param);
  param = new G4UIparameter("Material",'s',false);
  sr2sCoilStructureCmd->SetParameter(param);
  
  sr2sNoMaterialCmd = new G4UIcmdWithoutParameter("/SR2SShieldSimplified/NoMaterial",this);
  sr2sNoMaterialCmd->SetGuidance("Switched off all teh materila used for the shield");
  sr2sNoMaterialCmd->AvailableForStates(G4State_Idle); 
}

SR2SToroidalMagneticShieldSimplifiedMessenger::~SR2SToroidalMagneticShieldSimplifiedMessenger()
{
  delete sr2sShield;
  delete sr2sShieldDir; 
  delete sr2sTitaniumStructureCmd;
  delete sr2sHydrogenStructureCmd;
  delete sr2sSupportStructureCmd;
  delete sr2sCoilStructureCmd;
  delete sr2sNoMaterialCmd;
}

void SR2SToroidalMagneticShieldSimplifiedMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 
  if(command == sr2sTitaniumStructureCmd){
    const char* paramString;
    paramString=newValue;
    G4double Radius,Thickness,Length; 
    G4String unit,material;
    std::istringstream is((char*)paramString);
    is>>Radius>>Thickness>>Length>>unit>>material;
    Radius*=G4UnitDefinition::GetValueOf(unit);
    Thickness*=G4UnitDefinition::GetValueOf(unit);
    Length*=G4UnitDefinition::GetValueOf(unit);
    
    sr2sShield->SetSR2STitaniumStructureParameter(Radius,Thickness,Length,material);
  }else if(command == sr2sHydrogenStructureCmd){
    const char* paramString;
    paramString=newValue;
    G4double Radius,Thickness,Length; 
    G4String unit,material;
    std::istringstream is((char*)paramString);
    is>>Radius>>Thickness>>Length>>unit>>material;
    Radius*=G4UnitDefinition::GetValueOf(unit);
    Thickness*=G4UnitDefinition::GetValueOf(unit);
    Length*=G4UnitDefinition::GetValueOf(unit);
    
    sr2sShield->SetSR2SHydrogenStructureParameter(Radius,Thickness,Length,material);
  }else if(command == sr2sSupportStructureCmd){
    const char* paramString;
    paramString=newValue;
    G4double Radius,Thickness,Length; 
    G4String unit,material;
    std::istringstream is((char*)paramString);
    is>>Radius>>Thickness>>Length>>unit>>material;
    Radius*=G4UnitDefinition::GetValueOf(unit);
    Thickness*=G4UnitDefinition::GetValueOf(unit);
    Length*=G4UnitDefinition::GetValueOf(unit);
    
    sr2sShield->SetSR2SSupportStructureParameter(Radius,Thickness,Length,material);
}else if(command == sr2sCoilStructureCmd){
    const char* paramString;
    paramString=newValue;
    G4double RadialDistance,Length,Width,Thickness,Heigth,EndcapRadius;
    G4int CoilNumber;
    G4String unit,material;
    std::istringstream is((char*)paramString);
    is>>RadialDistance>>Length>>Width>>Thickness>>Heigth>>EndcapRadius>>unit>>CoilNumber>>material;
    RadialDistance*=G4UnitDefinition::GetValueOf(unit);
    Length*=G4UnitDefinition::GetValueOf(unit);
    Width*=G4UnitDefinition::GetValueOf(unit);
    Thickness*=G4UnitDefinition::GetValueOf(unit);
    Heigth*=G4UnitDefinition::GetValueOf(unit);
    EndcapRadius*=G4UnitDefinition::GetValueOf(unit);

    sr2sShield->SetSR2SCoilStructureParameter(RadialDistance,Length,Width,Thickness,Heigth,EndcapRadius,CoilNumber,material);
  }else if(command == sr2sNoMaterialCmd)
    sr2sShield->SetSR2SNoMaterial();
}

