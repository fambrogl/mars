#include "MarsMagneticFieldSetup.hh"
#include "MarsMapMagneticField.hh"
#include "MarsToroidalMagneticField.hh"
#include "MarsSolenoidalMagneticField.hh"
#include "MarsMagneticFieldSetupMessenger.hh"
#include "globals.hh"
#include "geomdefs.hh"
#include "time.h"
#include "G4ios.hh"
#include "fstream"
#include "G4MagIntegratorStepper.hh"
#include "G4FieldManager.hh"
#include "G4ChordFinder.hh"
#include "G4Mag_UsualEqRhs.hh"
#include "G4TransportationManager.hh"
#include "G4Navigator.hh"
#include "G4PropagatorInField.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"
#include "G4ExplicitEuler.hh"
#include "G4ImplicitEuler.hh"
#include "G4SimpleRunge.hh"
#include "G4ClassicalRK4.hh"
#include "G4CashKarpRKF45.hh"
#include "G4HelixSimpleRunge.hh"
#include "G4HelixImplicitEuler.hh"
#include "G4HelixHeum.hh"
#include "G4UImanager.hh"
#include "G4RunManager.hh"
#include "G4SolidStore.hh"
#include "G4Box.hh"
#include "G4FieldManager.hh"
#include "G4UniformMagField.hh"

////////////////////////////////////////////////////////////////////////////////
//
MarsMagneticFieldSetup::MarsMagneticFieldSetup():
  fChordFinder(0),
  fLocalChordFinder(0),
  fEquationOfMotion(0),
  fLocalEquationOfMotion(0),
  fStepper(0),
  fLocalStepper(0),
  fFieldMessenger(0),
  fMagneticField(0),
  fLocalMagneticField(0),
  fFieldManager(0),
  fLocalFieldManager(0)
{ 
  ListOfFieldModels.clear();
  ListOfFieldModels.push_back("TOROIDAL");
  ListOfFieldModels.push_back("SOLENOIDAL");
  ListOfFieldModels.push_back("BYMAP");
  
  fFieldMessenger = new MarsMagneticFieldSetupMessenger(this);
  
  G4TransportationManager::GetTransportationManager()->GetPropagatorInField()->SetLargestAcceptableStep(2*m);
  
  fMagneticField = new G4UniformMagField(G4ThreeVector(0.0,0.0,0.01*tesla));
  fEquationOfMotion = new G4Mag_UsualEqRhs(fMagneticField);
    
  fFieldManager = G4TransportationManager::GetTransportationManager()->GetFieldManager();
  fLocalFieldManager = new G4FieldManager();

  fFieldManager->SetDetectorField(fMagneticField);
  

  DefaultDeltaChord=1*mm;
  DeltaChord=DefaultDeltaChord;
  DefaultDeltaIntersection= .1*mm;
  DefaultEpsilonMin= 1.e-6;
  DefaultEpsilonMax= 1.e-6*1.0001;
  
  SetMagneticField("SOLENOIDAL");
  SetStepper("ClassicalRK4");
  SetDeltaIntersection(DefaultDeltaIntersection);
  SetEpsilon(DefaultEpsilonMin,DefaultEpsilonMax);
  
}
 
////////////////////////////////////////////////////////////////////////
//
MarsMagneticFieldSetup::~MarsMagneticFieldSetup()
{ 
  delete fFieldMessenger;
  /*
  if (fEquationOfMotion) delete fEquationOfMotion;
  if (fLocalEquationOfMotion) delete fLocalEquationOfMotion;
  if (fChordFinder) delete fChordFinder;
  if (fLocalChordFinder) delete fLocalChordFinder;
  if (fStepper) delete fStepper;
  if (fLocalStepper) delete fLocalStepper;
  if (fMagneticField) delete fMagneticField;
  if (fLocalMagneticField) delete fLocalMagneticField;
  if (fFieldManager) delete fFieldManager;
  if (fLocalFieldManager) delete fLocalFieldManager;
  */
}

////////////////////////////////////////////////////////////////////////////////
//
void MarsMagneticFieldSetup::SetMagneticField(G4String aModel)
{
  if(fLocalMagneticField) delete fLocalMagneticField;
  if(aModel=="TOROIDAL"){
    fLocalMagneticField = new MarsToroidalMagneticField();
  }else if(aModel=="SOLENOIDAL"){
    fLocalMagneticField = new MarsSolenoidalMagneticField();
  }else if(aModel=="BYMAP"){
    fLocalMagneticField = new MarsMapMagneticField();
  }else{
    G4cout<<"You have choose a wrong model "<<aModel<<"; the possible value are SOLENOIDAL or TOROIDAL."<<std::endl;
  }

  fLocalEquationOfMotion = new G4Mag_UsualEqRhs(fLocalMagneticField);

  fLocalFieldManager->SetDetectorField(fLocalMagneticField);

  FieldModelName=aModel;
}

void MarsMagneticFieldSetup::SetMagneticFieldParameter(G4String aParam)
{
  fLocalMagneticField->SetMagneticFieldParameter(aParam);
}
////////////////////////////////////////////////////////////////////////////////
//
void MarsMagneticFieldSetup::SetStepper(G4String aString)
{ 
  if (fStepper) delete fStepper;
  if (fLocalStepper) delete fLocalStepper;
   
  if (aString == "ExplicitEuler"){
    fStepper = new G4ExplicitEuler(fEquationOfMotion);
    fLocalStepper = new G4ExplicitEuler(fLocalEquationOfMotion);
    G4cout<<"G4ExplicitEuler is called"<<G4endl;
  }
  else if  (aString == "ImplicitEuler"){
    fStepper = new G4ImplicitEuler(fEquationOfMotion);
    fLocalStepper = new G4ImplicitEuler(fLocalEquationOfMotion);
    G4cout<<"G4ImplicitEuler is called"<<G4endl;
  }
  else if  (aString == "SimpleRunge"){
    fStepper = new G4SimpleRunge(fEquationOfMotion);
    fLocalStepper = new G4SimpleRunge(fLocalEquationOfMotion);
    G4cout<<"G4SimpleRunge is called"<<G4endl;
  } 
  else if  (aString == "ClassicalRK4"){
    fStepper = new G4ClassicalRK4(fEquationOfMotion);
    fLocalStepper = new G4ClassicalRK4(fLocalEquationOfMotion);
    G4cout<<"G4ClassicalRK4 is called"<<G4endl;
  }  
  else if  (aString == "CashKarpRKF45"){
    fStepper = new G4CashKarpRKF45(fEquationOfMotion);
    fLocalStepper = new G4CashKarpRKF45(fLocalEquationOfMotion);
    G4cout<<"G4CashKarpRKF45 is called"<<G4endl;
  }   
  else if  (aString == "HelixSimpleRunge"){
    fStepper = new G4HelixSimpleRunge(fEquationOfMotion);
    fLocalStepper = new G4HelixSimpleRunge(fLocalEquationOfMotion);
    G4cout<<"G4HelixSimpleRunge is called"<<G4endl;
  }   
  else if  (aString == "HelixImplicitEuler"){
    fStepper = new G4HelixImplicitEuler(fEquationOfMotion);
    fLocalStepper = new G4HelixImplicitEuler(fLocalEquationOfMotion);
    G4cout<<"G4HelixImplicitEuler is called"<<G4endl;
  }
  else if  (aString == "HelixHeum"){
    fStepper = new G4HelixHeum(fEquationOfMotion);
    fLocalStepper = new G4HelixHeum(fLocalEquationOfMotion);
    G4cout<<"G4HelixHeum is called"<<G4endl;
  }
  else {
    fStepper = new G4ClassicalRK4(fEquationOfMotion);
    fLocalStepper = new G4ClassicalRK4(fLocalEquationOfMotion);
    G4cout<<"The selected stepper is not available"<<G4endl;
    G4cout<<"G4CalssicalRK4 is called"<<G4endl;
  }
  
  if (fChordFinder) delete fChordFinder;
  if (fLocalChordFinder) delete fLocalChordFinder;

  G4double min_step= 0.1*mm;
  
  fChordFinder = new G4ChordFinder(fMagneticField,min_step,fStepper);
  fChordFinder->SetDeltaChord(DeltaChord); 
  fFieldManager->SetChordFinder(fChordFinder); 

  fLocalChordFinder = new G4ChordFinder(fLocalMagneticField,min_step,fLocalStepper);
  fLocalChordFinder->SetDeltaChord(DeltaChord); 
  fLocalFieldManager->SetChordFinder(fLocalChordFinder); 
}
////////////////////////////////////////////////////////////////////////////////
//
void MarsMagneticFieldSetup::SetDeltaChord(G4double aVal)
{ 
  if (fChordFinder) fChordFinder->SetDeltaChord(aVal);
  if (fLocalChordFinder) fLocalChordFinder->SetDeltaChord(aVal);
  DeltaChord=aVal;
}
////////////////////////////////////////////////////////////////////////////////
//
void MarsMagneticFieldSetup::SetDeltaIntersection(G4double aVal)
{ ///////Delta intersection for G4Integration method
  fLocalFieldManager->SetDeltaIntersection(aVal);
  fFieldManager->SetDeltaIntersection(aVal);
}
////////////////////////////////////////////////////////////////////////////////
//
void MarsMagneticFieldSetup::SetEpsilon(G4double aEpsMin,G4double aEpsMax)
{ //precision for G4Integration methods
  G4TransportationManager::GetTransportationManager()->GetPropagatorInField()->SetMinimumEpsilonStep(aEpsMin);
  G4TransportationManager::GetTransportationManager()->GetPropagatorInField()->SetMaximumEpsilonStep(aEpsMax);
  
}
////////////////////////////////////////////////////////////////////////////////
//
void MarsMagneticFieldSetup::ResetIntegrationParameters()
{
  SetStepper("ClassicalRK4");
  SetDeltaChord(DefaultDeltaChord);
  SetDeltaIntersection(DefaultDeltaIntersection);
  SetEpsilon(DefaultEpsilonMin,DefaultEpsilonMax);
}

////////////////////////////////////////////////////////////////////////////////
//

////////////////////////////////////////////////////////////////////
void MarsMagneticFieldSetup::SwitchOn()
{
  fFieldManager->SetDetectorField(fMagneticField);
  fLocalFieldManager->SetDetectorField(fLocalMagneticField);
}
////////////////////////////////////////////////////////////////////
void MarsMagneticFieldSetup::SwitchOff()
{
  fFieldManager->SetDetectorField(0);
  fLocalFieldManager->SetDetectorField(0);
}

G4FieldManager* MarsMagneticFieldSetup::GetGlobalFieldManager()
{
  return G4TransportationManager::GetTransportationManager()->GetFieldManager();
}

G4FieldManager* MarsMagneticFieldSetup::GetLocalFieldManager()
{
  return fLocalFieldManager;
}



