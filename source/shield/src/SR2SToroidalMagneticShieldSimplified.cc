//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id$
//
// Code developed by: Filippo Ambroglini, filippo.ambroglini@pg.infn.it
//
#include "SR2SToroidalMagneticShieldSimplified.hh"
#include "MarsMaterial.hh"

#include "G4SystemOfUnits.hh"
#include "G4Material.hh"
#include "G4MaterialTable.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "G4RunManager.hh"
#include "G4VisAttributes.hh"
#include "SR2SToroidalMagneticShieldSimplifiedMessenger.hh"
#include "G4Torus.hh"
#include "G4UnionSolid.hh"
#include "G4SubtractionSolid.hh"

SR2SToroidalMagneticShieldSimplified::SR2SToroidalMagneticShieldSimplified()
{

  racetrackSupportStructureMiddlePosition = 2500*mm;
  racetrackSupportStructureThickness      = 100*mm;
  racetrackSupportStructureInnerRadius    = racetrackSupportStructureMiddlePosition
    -racetrackSupportStructureThickness/2.;
  racetrackSupportStructureOuterRadius    = racetrackSupportStructureMiddlePosition
    +racetrackSupportStructureThickness/2.;
  racetrackSupportStructureLength         = 7000*mm;

  racetrackTitaniumStructureMiddlePosition = 3935*mm;
  racetrackTitaniumStructureThickness      = 130*mm;
  racetrackTitaniumStructureInnerRadius    = racetrackTitaniumStructureMiddlePosition
    -racetrackTitaniumStructureThickness/2.;
  racetrackTitaniumStructureOuterRadius    = racetrackTitaniumStructureMiddlePosition
    +racetrackTitaniumStructureThickness/2.;
  racetrackTitaniumStructureLength         = 9000*mm;

  racetrackHydrogenStructureMiddlePosition = 3000*mm;
  racetrackHydrogenStructureThickness      = 155*mm;
  racetrackHydrogenStructureInnerRadius    = racetrackHydrogenStructureMiddlePosition
    -racetrackHydrogenStructureThickness/2.;
  racetrackHydrogenStructureOuterRadius    = racetrackHydrogenStructureMiddlePosition
    +racetrackHydrogenStructureThickness/2.;
  racetrackHydrogenStructureLength         = 9000*mm;

  coilDistanceFromCenter                   = 2745*mm;
  coilBarrelLength                         = 6600*mm;
  coilBarrelWidth                          = 130*mm;
  coilThickness                            = 110*mm;
  coilBarrleHeight                         = 3610*mm;
  coilEndcapRadius                         = 1695*mm;
  racetrackCoilNumber                      = 120;
  coilAngle                                = (2*pi)/float(racetrackCoilNumber);
  
  coilMaterial                             ="coil_equivalent";
  Ti_StructureMaterial                     ="Titanium";
  H_StructureMaterial                      ="Galactic";
  SupportStructureMaterial                 ="carbo_epoxy";
  
  materialShield=true;
  
  pMaterial = new MarsMaterial();
  messenger = new SR2SToroidalMagneticShieldSimplifiedMessenger(this);
}

void SR2SToroidalMagneticShieldSimplified::SetSR2STitaniumStructureParameter(G4double radius,G4double thickness,
									     G4double length, G4String material)
{
  racetrackTitaniumStructureMiddlePosition = radius;
  racetrackTitaniumStructureThickness      = thickness;
  racetrackTitaniumStructureInnerRadius    = racetrackTitaniumStructureMiddlePosition
    -racetrackTitaniumStructureThickness/2.;
  racetrackTitaniumStructureOuterRadius    = racetrackTitaniumStructureMiddlePosition
    +racetrackTitaniumStructureThickness/2.;
  racetrackTitaniumStructureLength         = length;
  
  Ti_StructureMaterial                     =material; 
}

void SR2SToroidalMagneticShieldSimplified::SetSR2SHydrogenStructureParameter(G4double radius,G4double thickness,
									     G4double length, G4String material)
{
  racetrackHydrogenStructureMiddlePosition = radius;
  racetrackHydrogenStructureThickness      = thickness;
  racetrackHydrogenStructureInnerRadius    = racetrackHydrogenStructureMiddlePosition
    -racetrackHydrogenStructureThickness/2.;
  racetrackHydrogenStructureOuterRadius    = racetrackHydrogenStructureMiddlePosition
    +racetrackHydrogenStructureThickness/2.;
  racetrackHydrogenStructureLength         = length;
  
  H_StructureMaterial                     =material; 
}

void SR2SToroidalMagneticShieldSimplified::SetSR2SSupportStructureParameter(G4double radius,G4double thickness,
									     G4double length, G4String material)
{
  racetrackSupportStructureMiddlePosition = radius;
  racetrackSupportStructureThickness      = thickness;
  racetrackSupportStructureInnerRadius    = racetrackSupportStructureMiddlePosition
    -racetrackSupportStructureThickness/2.;
  racetrackSupportStructureOuterRadius    = racetrackSupportStructureMiddlePosition
    +racetrackSupportStructureThickness/2.;
  racetrackSupportStructureLength         = length;
  
  SupportStructureMaterial                = material; 
}

void SR2SToroidalMagneticShieldSimplified::SetSR2SCoilStructureParameter(G4double radialDistance,G4double length,
									 G4double width,G4double thickness,
									 G4double  height,G4double endcapR,
									 G4int nCoil, G4String material)
{
  coilDistanceFromCenter                   = radialDistance;
  coilBarrelLength                         = length;
  coilBarrelWidth                          = width;
  coilThickness                            = thickness;
  coilBarrleHeight                         = height;
  coilEndcapRadius                         = endcapR;
  racetrackCoilNumber                      = nCoil;
  coilAngle                                = (2*pi)/float(racetrackCoilNumber);

  coilMaterial                             = material;
}

SR2SToroidalMagneticShieldSimplified::~SR2SToroidalMagneticShieldSimplified()
{
  delete pMaterial;
  delete messenger;
}

void SR2SToroidalMagneticShieldSimplified::DestroyComponent()
{

  delete racetrackSupportStructure;
  racetrackSupportStructure=0;
  delete racetrackTitaniumStructure;
  racetrackTitaniumStructure=0;
  delete racetrackHydrogenStructure;
  racetrackHydrogenStructure=0;

  delete  magneticFieldVolumeBarrel;
  magneticFieldVolumeBarrel=0;
  delete magneticFieldVolumeEndcap;
  magneticFieldVolumeEndcap=0;
  delete magneticFieldVolume;
  magneticFieldVolume=0;
  delete racetrackCoilBoxBarrelExternal;
  racetrackCoilBoxBarrelExternal=0;
  delete racetrackCoilBoxBarrelInternal;
  racetrackCoilBoxBarrelInternal=0;
  delete racetrackCoilBoxBarrel;
  racetrackCoilBoxBarrel=0;
  delete racetrackCoilBoxEndcapTubs;
  racetrackCoilBoxEndcapTubs=0;
  delete racetrackCoilBoxEndcapBox;
  racetrackCoilBoxEndcapBox=0;
  delete racetrackCoilBoxEndcap;
  racetrackCoilBoxEndcap=0;
  delete racetrackCoil;
  racetrackCoil=0;

  delete racetrackSupportStructureLog;
  racetrackSupportStructureLog=0;
  delete racetrackTitaniumStructureLog;
  racetrackTitaniumStructureLog=0;
  delete racetrackHydrogenStructureLog;
  racetrackHydrogenStructureLog=0;
  delete magneticFieldVolumeLog;
  magneticFieldVolumeLog=0;
  delete racetrackCoilLog;
  racetrackCoilLog=0;

  delete racetrackSupportStructurePhys;
  racetrackSupportStructurePhys=0;
  delete racetrackTitaniumStructurePhys;
  racetrackTitaniumStructurePhys=0;
  delete racetrackHydrogenStructurePhys;
  racetrackHydrogenStructurePhys=0;
  delete magneticFieldVolumePhys;
  magneticFieldVolumePhys=0;
  delete racetrackCoilPhys;
  racetrackCoilPhys=0;

}

void SR2SToroidalMagneticShieldSimplified::ConstructComponent(G4VPhysicalVolume* motherVolume)
{
  // Geometry definition
  pMaterial->DefineMaterials();

  G4Material* vacuum =   pMaterial -> GetMaterial("Galactic");
  G4Material* support = pMaterial -> GetMaterial(SupportStructureMaterial);	      
  G4Material* hydrogen = pMaterial -> GetMaterial(H_StructureMaterial);	      
  G4Material* titanium = pMaterial -> GetMaterial(Ti_StructureMaterial);	      
  G4Material* coil =  pMaterial -> GetMaterial(coilMaterial);	      

  if(!materialShield){
    support = pMaterial -> GetMaterial("Galactic");
    hydrogen =  pMaterial -> GetMaterial("Galactic");
    titanium = pMaterial -> GetMaterial("Galactic");
    coil = pMaterial -> GetMaterial("Galactic");
  }
  

  magneticFieldVolumeBarrel = new G4Tubs("magneticFieldVolumeBarrel",
					 coilDistanceFromCenter-10*mm,
					 coilDistanceFromCenter+coilBarrleHeight+coilThickness+10*mm,
					 coilBarrelLength/2.,0,2*pi);
  

  magneticFieldVolumeEndcap = new G4Torus("magneticFieldVolumeEndcap",
					  0,coilBarrleHeight/2.+coilThickness/2.+5*mm,
					  coilDistanceFromCenter+coilBarrleHeight/2.+coilThickness/2.,
					  0,2*pi);

  G4ThreeVector transM = G4ThreeVector(0,0,-(coilBarrelLength/2.));
  G4RotationMatrix* rotM = new G4RotationMatrix;

  G4ThreeVector transP = G4ThreeVector(0,0,coilBarrelLength/2.);
  G4RotationMatrix* rotP = new G4RotationMatrix;
  
  magneticFieldVolume = new G4UnionSolid("magneticFieldVolume",
					 new G4UnionSolid("partialmagneticFieldVolume",
							  magneticFieldVolumeBarrel,
							  magneticFieldVolumeEndcap,
							  rotM,transM),
					 magneticFieldVolumeEndcap,rotP,transP);
 
  
  magneticFieldVolumeLog = new G4LogicalVolume(magneticFieldVolume,
					       vacuum,
					       "magneticFieldVolumeLog",
					       0,0,0);

  magneticFieldVolumePhys = new G4PVPlacement(0,
					      G4ThreeVector(0.,0.,0.),
					      "magneticFieldVolumePhys",
					      magneticFieldVolumeLog,
					      motherVolume,false,0,true); 

  racetrackSupportStructure = new G4Tubs("racetrackSupportStructure",
					 racetrackSupportStructureInnerRadius,
					 racetrackSupportStructureOuterRadius,
					 racetrackSupportStructureLength/2.,0,2*pi);
  
  racetrackTitaniumStructure = new G4Tubs("racetrackTitaniumStructure",
					 racetrackTitaniumStructureInnerRadius,
					 racetrackTitaniumStructureOuterRadius,
					 racetrackTitaniumStructureLength/2.,0,2*pi);

  racetrackHydrogenStructure = new G4Tubs("racetrackHydrogenStructure",
					 racetrackHydrogenStructureInnerRadius,
					 racetrackHydrogenStructureOuterRadius,
					 racetrackHydrogenStructureLength/2.,0,2*pi);

  
  racetrackSupportStructureLog = new G4LogicalVolume(racetrackSupportStructure,
						     support,
						     "racetrackSupportStructureLog",
						     0,0,0);

  racetrackTitaniumStructureLog = new G4LogicalVolume(racetrackTitaniumStructure,
						      titanium,
						      "racetrackTitaniumStructureLog",
						      0,0,0);

  racetrackHydrogenStructureLog = new G4LogicalVolume(racetrackHydrogenStructure,
						      hydrogen,
						      "racetrackHydrogenStructureLog",
						      0,0,0);

  
  racetrackSupportStructurePhys = new G4PVPlacement(0,
						    G4ThreeVector(0.,0.,0.),
						    "racetrackSupportStructurePhys",
						    racetrackSupportStructureLog,
						    motherVolume,false,0,true); 
  
  racetrackTitaniumStructurePhys = new G4PVPlacement(0,
						    G4ThreeVector(0.,0.,0.),
						    "racetrackTitaniumStructurePhys",
						    racetrackTitaniumStructureLog,
						    magneticFieldVolumePhys,false,0,true); 

  racetrackHydrogenStructurePhys = new G4PVPlacement(0,
						    G4ThreeVector(0.,0.,0.),
						    "racetrackHydrogenStructurePhys",
						    racetrackHydrogenStructureLog,
						    magneticFieldVolumePhys,false,0,true);     
  
  racetrackCoilBoxBarrelExternal = new G4Box("racetrackCoilBoxBarrelExternal",
					     coilBarrelWidth/2.,
					     coilBarrleHeight/2.,
					     coilBarrelLength/2.);

  racetrackCoilBoxBarrelInternal = new G4Box("racetrackCoilBoxBarrelInternal",
					     coilBarrelWidth/2.+10*mm,
					     (coilBarrleHeight-coilThickness*2)/2.,
					     coilBarrelLength/2.+10*mm);

  racetrackCoilBoxBarrel = new G4SubtractionSolid("racetrackCoilBoxBarrel",
						  racetrackCoilBoxBarrelExternal,
						  racetrackCoilBoxBarrelInternal);
  
  racetrackCoilBoxEndcapTubs = new G4Tubs("racetrackCoilBoxEndcapTubs",
					    coilEndcapRadius-coilThickness/2.,
					    coilEndcapRadius+coilThickness/2.,
					    coilBarrelWidth/2.,0,pi/2.);
  
  racetrackCoilBoxEndcapBox = new G4Box("racetrackCoilBoxEndcapBox",
					coilThickness/2.,
					coilThickness/2.,
					coilBarrelWidth/2);

  G4ThreeVector transEP = G4ThreeVector(coilThickness/2.,-coilEndcapRadius,0);
  G4ThreeVector transEM = G4ThreeVector(-coilThickness/2.,-coilEndcapRadius,0);
  G4RotationMatrix* rotEP = new G4RotationMatrix;
  G4RotationMatrix* rotEM = new G4RotationMatrix;
  rotEM->rotateY(-pi);

  racetrackCoilBoxEndcap = new G4UnionSolid("racetrackCoilBoxEndcap",
					    new G4UnionSolid("partialracetrackCoilBoxEndcap",
							     racetrackCoilBoxEndcapBox,
							     racetrackCoilBoxEndcapTubs,
							     rotEP,transEP),
					    racetrackCoilBoxEndcapTubs,rotEM,transEM);
  
  G4ThreeVector transCM = G4ThreeVector(0,0,-(coilBarrelLength/2.+coilEndcapRadius));
  G4RotationMatrix* rotCM = new G4RotationMatrix;
  rotCM->rotateZ(pi/2);
  rotCM->rotateX(pi/2);
  
  G4ThreeVector transCP = G4ThreeVector(0,0,coilBarrelLength/2.+coilEndcapRadius);
  G4RotationMatrix* rotCP = new G4RotationMatrix;
  rotCP->rotateZ(pi/2);
  rotCP->rotateX(-pi/2);
  
  racetrackCoil = new G4UnionSolid("racetrackCoil",
				   new G4UnionSolid("partilaracetrackCoil",
						    racetrackCoilBoxBarrel,
						    racetrackCoilBoxEndcap,
						    rotCP,transCP),
				   racetrackCoilBoxEndcap,rotCM,transCM);
  
  
  racetrackCoilLog = new G4LogicalVolume(racetrackCoil,
					 coil,
					 "racetrackCoilLog",
					 0,0,0);
  
  
  for(int i=0;i<racetrackCoilNumber;i++){
    G4double localAngle = i*coilAngle;
    G4RotationMatrix* boxRot = new G4RotationMatrix;
    boxRot->rotateZ(localAngle);
    racetrackCoilPhys = new G4PVPlacement(boxRot,
					  G4ThreeVector((coilDistanceFromCenter+coilBarrleHeight/2.)*sin(localAngle),
							(coilDistanceFromCenter+coilBarrleHeight/2.)*cos(localAngle),
							0.),
					  "racetrackCoilPhys",
					  racetrackCoilLog,
					  magneticFieldVolumePhys,
					  false,i,true);
    
  }
  
  G4VisAttributes* layerVisAttMagenta = new G4VisAttributes(G4Colour::Magenta());
  layerVisAttMagenta->SetVisibility(true);
  //layerVisAttMagenta->SetForceAuxEdgeVisible(true);
  layerVisAttMagenta->SetForceSolid(true);
  G4VisAttributes* layerVisAttGreen = new G4VisAttributes(G4Colour::Green());
  layerVisAttGreen->SetVisibility(true);
  //layerVisAttGreen->SetForceAuxEdgeVisible(true);
  layerVisAttGreen->SetForceSolid(true);
  G4VisAttributes* layerVisAttBlue = new G4VisAttributes(G4Colour::Blue());
  layerVisAttBlue->SetVisibility(true);
  //layerVisAttBlue->SetForceAuxEdgeVisible(true);
  layerVisAttBlue->SetForceSolid(true);
  G4VisAttributes* layerVisAttYellow = new G4VisAttributes(G4Colour::Yellow());
  layerVisAttYellow->SetVisibility(true);
  //layerVisAttYellow->SetForceAuxEdgeVisible(true);
  layerVisAttYellow->SetForceSolid(true);
  G4VisAttributes* layerVisAttRed = new G4VisAttributes(G4Colour::Red());
  layerVisAttRed->SetVisibility(true);
  //layerVisAttRed->SetForceAuxEdgeVisible(true);
  layerVisAttRed->SetForceSolid(true);
  G4VisAttributes* layerVisAttGray = new G4VisAttributes(G4Colour::Gray());
  layerVisAttGray->SetVisibility(true);
  //layerVisAttGray->SetForceAuxEdgeVisible(true);
  layerVisAttGray->SetForceSolid(true);
  G4VisAttributes* layerVisAttInvisible = new G4VisAttributes();
  layerVisAttInvisible->SetVisibility(false);
  //layerVisAttInvisible->SetForceAuxEdgeVisible(false);
  layerVisAttInvisible->SetForceSolid(false);

  racetrackCoilLog->SetVisAttributes(layerVisAttYellow);
  magneticFieldVolumeLog->SetVisAttributes(layerVisAttInvisible);
  racetrackSupportStructureLog->SetVisAttributes(layerVisAttGray); 
  racetrackTitaniumStructureLog->SetVisAttributes(layerVisAttBlue);
  racetrackHydrogenStructureLog->SetVisAttributes(layerVisAttMagenta);  


  G4double coilMass = (racetrackCoilLog->GetSolid()->GetCubicVolume())*(racetrackCoilLog->GetMaterial()->GetDensity())/kg;
  G4double tiMass = (racetrackTitaniumStructureLog->GetSolid()->GetCubicVolume())*(racetrackTitaniumStructureLog->GetMaterial()->GetDensity())/kg;
  G4double supportMass = (racetrackSupportStructureLog->GetSolid()->GetCubicVolume())*(racetrackSupportStructureLog->GetMaterial()->GetDensity())/kg;
  G4double solidHMass = (racetrackHydrogenStructureLog->GetSolid()->GetCubicVolume())*(racetrackHydrogenStructureLog->GetMaterial()->GetDensity())/kg;

  std::cout<<"Mass of Geometry"<<std::endl;
  std::cout<<"Single Coil Mass = "<<coilMass/1000<<" Tons; Total mass = "<<coilMass*racetrackCoilNumber/1000<<" Tons;"<<std::endl;
  std::cout<<"Ti structure Mass = "<<tiMass/1000<<" Tons;"<<std::endl;
  std::cout<<"Support structure Mass = "<<supportMass/1000<<" Tons;"<<std::endl;
  std::cout<<"Solid H Mass = "<<solidHMass/1000<<" Tons;"<<std::endl;

}

void SR2SToroidalMagneticShieldSimplified::SetFieldManager(G4FieldManager* fieldManager)
{
  magneticFieldVolumeLog->SetFieldManager(fieldManager,true);
}
 
