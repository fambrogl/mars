//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// Code developed by:
//  S.Guatelli
//
//    *********************************
//    *                               *
//    *    SR2SSolenoidalMagneticShieldMessenger.cc *
//    *                               *
//    *********************************
//
//
// $Id$
//
// 

#include "SR2SSolenoidalMagneticShieldMessenger.hh"
#include "SR2SSolenoidalMagneticShield.hh"
#include "G4UIdirectory.hh"
#include "G4UIparameter.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcommand.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UnitsTable.hh"

SR2SSolenoidalMagneticShieldMessenger::SR2SSolenoidalMagneticShieldMessenger( SR2SSolenoidalMagneticShield* sr2s): sr2sShield(sr2s)
{ 
  sr2sShieldDir = new G4UIdirectory("/SR2SShield/");
  sr2sShieldDir->SetGuidance("SR2S Shield control.");
   
  sr2sMagneticShieldStructureCmd = new G4UIcommand("/SR2SShield/SolenoidalShieldStructure",this);
  sr2sMagneticShieldStructureCmd->SetGuidance("[usage] /SR2SShield/SolenoidalShieldStructure InnerSolenoidRadius unit InnerSolenoidThickness unit OuterSolenoidRadius unit OuterSolenoidThickness unit SolenoidHalfLength unit");
  sr2sMagneticShieldStructureCmd->AvailableForStates(G4State_Idle); 
  param = new G4UIparameter("InnerSolenoidRadius",'d',false);
  sr2sMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("length_unit",'s',false);
  sr2sMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("InnerSolenoidThickness",'d',false);
  sr2sMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("length_unit",'s',false);
  sr2sMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("OuterSolenoidRadius",'d',false);
  sr2sMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("length_unit",'s',false);
  sr2sMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("OuterSolenoidThickness",'d',false);
  sr2sMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("length_unit",'s',false);
  sr2sMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("SolenoidHalfLength",'d',false);
  sr2sMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("length_unit",'s',false);
  sr2sMagneticShieldStructureCmd->SetParameter(param);  
  param = new G4UIparameter("material",'s',false);
  sr2sMagneticShieldStructureCmd->SetParameter(param);  
}

SR2SSolenoidalMagneticShieldMessenger::~SR2SSolenoidalMagneticShieldMessenger()
{
  delete sr2sShield;
  delete sr2sShieldDir; 
  delete sr2sMagneticShieldStructureCmd;
}

void SR2SSolenoidalMagneticShieldMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 
  if(command == sr2sMagneticShieldStructureCmd){
    const char* paramString;
    paramString=newValue;
    G4double aInnerSolenoidRadius,aInnerSolenoidThickness,aOuterSolenoidRadius,aOuterSolenoidThickness,aSolenoidHalfLength;
    G4String unit1,unit2,unit3,unit4,unit5,material;
    G4bool existMaterial = true;
    std::istringstream is((char*)paramString);
    is >> aInnerSolenoidRadius >> unit1
       >> aInnerSolenoidThickness >> unit2
       >> aOuterSolenoidRadius >> unit3
       >> aOuterSolenoidThickness >> unit4
       >> aSolenoidHalfLength >> unit5
       >> material;
      
    aInnerSolenoidRadius*=G4UnitDefinition::GetValueOf(unit1);
    aInnerSolenoidThickness*=G4UnitDefinition::GetValueOf(unit2);
    aOuterSolenoidRadius*=G4UnitDefinition::GetValueOf(unit3);
    aOuterSolenoidThickness*=G4UnitDefinition::GetValueOf(unit4);
    aSolenoidHalfLength*=G4UnitDefinition::GetValueOf(unit5);
    if(material=="materialOff")
      existMaterial=false;
    else if(material=="materialOn")
      existMaterial=true;
    
    sr2sShield->SetSR2SShieldStructureParameter(aInnerSolenoidRadius,aInnerSolenoidThickness,
						aOuterSolenoidRadius,aOuterSolenoidThickness,
						aSolenoidHalfLength,existMaterial);
  }
}
 
 
