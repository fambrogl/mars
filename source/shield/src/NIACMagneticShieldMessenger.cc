//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// Code developed by:
//  S.Guatelli
//
//    *********************************
//    *                               *
//    *    NIACMagneticShieldMessenger.cc *
//    *                               *
//    *********************************
//
//
// $Id$
//
// 

#include "NIACMagneticShieldMessenger.hh"
#include "NIACMagneticShield.hh"
#include "G4UIdirectory.hh"
#include "G4UIparameter.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcommand.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UnitsTable.hh"

NIACMagneticShieldMessenger::NIACMagneticShieldMessenger( NIACMagneticShield* niac): niacShield(niac)
{ 
  niacShieldDir = new G4UIdirectory("/NIACShield/");
  niacShieldDir->SetGuidance("NIAC Shield control.");
   
  niacMagneticShieldStructureCmd = new G4UIcommand("/NIACShield/SolenoidalShieldStructure",this);
  niacMagneticShieldStructureCmd->SetGuidance("[usage] /NIACShield/SolenoidalShieldStructure length unit comp_radius unit comp_ext_thick unit comp_int_thick unit nSolenoid radius unit thickness unit central_radius unit central_thickness unit radial_thickness unit nRadii");
  niacMagneticShieldStructureCmd->AvailableForStates(G4State_Idle); 
  param = new G4UIparameter("length",'d',false);
  niacMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("length_unit",'s',false);
  niacMagneticShieldStructureCmd->SetParameter(param);

  param = new G4UIparameter("compensation_radius",'d',false);
  niacMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("length_unit",'s',false);
  niacMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("external_thickness",'d',false);
  niacMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("length_unit",'s',false);
  niacMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("internal_thickness",'d',false);
  niacMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("length_unit",'s',false);
  niacMagneticShieldStructureCmd->SetParameter(param);

  param = new G4UIparameter("number_solenoid",'i',false);
  niacMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("radius",'d',false);
  niacMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("length_unit",'s',false);
  niacMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("thickness",'d',false);
  niacMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("length_unit",'s',false);
  niacMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("central_radius",'d',false);
  niacMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("length_unit",'s',false);
  niacMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("central_thickness",'d',false);
  niacMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("length_unit",'s',false);
  niacMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("radial_thickness",'d',false);
  niacMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("length_unit",'s',false);
  niacMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("number_radii",'i',false);
  niacMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("material",'s',false);
  niacMagneticShieldStructureCmd->SetParameter(param);

}

NIACMagneticShieldMessenger::~NIACMagneticShieldMessenger()
{
  delete niacShield;
  delete niacShieldDir; 
  delete niacMagneticShieldStructureCmd;
}

void NIACMagneticShieldMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 
  if(command == niacMagneticShieldStructureCmd){
    const char* paramString;
    paramString=newValue;
    G4int nsolenoid,nradii;
    G4double length,comp_radius,comp_ext_thick,comp_int_thick,radius,thick,cradius,cthick,rthick;
    G4String unit_length,unit_compradius,unit_comp_ext_thick,unit_comp_int_thick,unit_radius,unit_thick,unit_cradius,unit_cthick,unit_rthick,material;
    G4bool  existMaterial = true;
    std::istringstream is((char*)paramString);
    is >> length >> unit_length
       >> comp_radius >> unit_compradius >> comp_ext_thick >> unit_comp_ext_thick >> comp_int_thick >> unit_comp_int_thick
       >> nsolenoid >> radius >> unit_radius >> thick >> unit_thick >> cradius >> unit_cradius >> cthick >> unit_cthick >> rthick >> unit_rthick >> nradii >> material;
    length         *=G4UnitDefinition::GetValueOf(unit_length);
    comp_radius    *=G4UnitDefinition::GetValueOf(unit_compradius);
    comp_ext_thick *=G4UnitDefinition::GetValueOf(unit_comp_ext_thick);
    comp_int_thick *=G4UnitDefinition::GetValueOf(unit_comp_int_thick); 
    radius         *=G4UnitDefinition::GetValueOf(unit_radius);
    thick          *=G4UnitDefinition::GetValueOf(unit_thick);
    cradius        *=G4UnitDefinition::GetValueOf(unit_cradius);
    cthick         *=G4UnitDefinition::GetValueOf(unit_cthick);
    rthick         *=G4UnitDefinition::GetValueOf(unit_rthick);

    if(material=="materialOff")
      existMaterial=false;
    else if(material=="materialOn")
      existMaterial=true;

    niacShield->SetNIACShieldStructureParameter(length,comp_radius,comp_ext_thick,comp_int_thick,nsolenoid,radius,thick,cradius,cthick,rthick,nradii,existMaterial);
  }
}
 
 
