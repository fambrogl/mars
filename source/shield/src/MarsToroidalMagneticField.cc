#include "MarsToroidalMagneticField.hh"
#include "globals.hh"
#include "geomdefs.hh"
#include "time.h"
#include "G4ios.hh"
#include "fstream"
#include "G4UnitsTable.hh"

////////////////////////////////////////////////////////////////////////////////
//
MarsToroidalMagneticField::MarsToroidalMagneticField()
{ 
  BdLParameter=0;
}

////////////////////////////////////////////////////////////////////////
//
MarsToroidalMagneticField::~MarsToroidalMagneticField()
{ 

}
///////////////////////////////////////////////////////////////////////
//
void MarsToroidalMagneticField::GetFieldValue( const G4double yTrack[7], G4double B[3]) const 
{ 
  G4ThreeVector position = G4ThreeVector(yTrack[0],yTrack[1],yTrack[2]);
  G4ThreeVector BField = GetFieldValue(position); 
  B[0]=BField.x();
  B[1]=BField.y();
  B[2]=BField.z();
 
  return ;
 
}
/////////////////////////////////////////////////////////////////////////////////
//
G4ThreeVector MarsToroidalMagneticField::GetFieldValue(const G4ThreeVector position) const 
{ 
  G4double r = position.getR()/m; 
  G4double phi = position.getPhi();
  G4double Br = (BdLParameter/r)*tesla;
  return G4ThreeVector(-Br*sin(phi),Br*cos(phi),0.); 
}


////////////////////////////////////////////////////////////////////////////////
//
void MarsToroidalMagneticField::SetMagneticFieldParameter(const G4String name_file)
{
  std::fstream File_Input(name_file,  std::ios::in);
  while (!File_Input.eof()){
    File_Input>>BdLParameter;
  }
  File_Input.close(); 
}
