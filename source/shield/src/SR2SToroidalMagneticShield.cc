//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id$
//
// Code developed by: S.Guatelli, susanna@uow.edu.au
//
#include "SR2SToroidalMagneticShield.hh"
#include "MarsMaterial.hh"

#include "G4SystemOfUnits.hh"
#include "G4Material.hh"
#include "G4MaterialTable.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "G4RunManager.hh"
#include "G4VisAttributes.hh"
#include "SR2SToroidalMagneticShieldMessenger.hh"
#include "G4Torus.hh"
#include "G4UnionSolid.hh"
#include "G4SubtractionSolid.hh"

SR2SToroidalMagneticShield::SR2SToroidalMagneticShield()
{
  structureBarrelHalfLength            = 2.0935*m;
  racetrackSupportStructureInnerRadius = 2.337*m;
  racetrackSupportStructureThickness   = 7.75*cm;
  racetrackCoilNumber                  = 24;
  coilAngle                            = (2*pi)/float(racetrackCoilNumber);
  racetrackCoilInnerRadius             = 2.9315*m;
  racetrackCoilThickness               = 0.05*m;
  racetrackCoilLength                  = 0.55*m;
  racetrackTiProtectionThickness       = 0.55*cm;
  racetrackRadialDistanceFromOrigin    = racetrackSupportStructureInnerRadius+racetrackSupportStructureThickness+racetrackTiProtectionThickness+racetrackCoilInnerRadius+racetrackCoilThickness;
  racetrackBottomTiProtectionInnerRadius = racetrackSupportStructureInnerRadius+racetrackSupportStructureThickness;
  racetrackTiProtectionInnerRadius       = racetrackCoilInnerRadius+racetrackCoilThickness;
  
  microMeteoriteProtectionInnerRadius  = racetrackTiProtectionInnerRadius+racetrackTiProtectionThickness;
  microMeteoriteProtectionThickness    = 0.53*cm;
  
  tierodsNumber                        = 40; 
  tierodsRadius                        = 0.4*cm;
  tierodsHalfLength                    = racetrackTiProtectionInnerRadius;
  tierodsStep                          = (structureBarrelHalfLength*2)/(float(tierodsNumber)/2.);
  magneticFieldVolumeInnerRadius       = racetrackSupportStructureInnerRadius+racetrackSupportStructureThickness;
  magneticFieldVolumeThickness         = (racetrackTiProtectionInnerRadius+racetrackTiProtectionThickness)*2;

  materialShield=true;
  
  pMaterial = new MarsMaterial();
  messenger = new SR2SToroidalMagneticShieldMessenger(this);
}

void SR2SToroidalMagneticShield::SetSR2SShieldStructureParameter(G4double aBarrelHalfLength,G4double rtSuppIR,G4double rtSuppThick,
							 G4int rtcNumber,G4double rtcInnerRadius,G4double rtcThick,G4double rtcLength,
							 G4double rtcTiPThick,
							 G4int tieNumber,G4double tieRadius,
								 G4double mmpThick,G4bool aMaterial)
{
  structureBarrelHalfLength            = aBarrelHalfLength;
  racetrackSupportStructureInnerRadius = rtSuppIR;
  racetrackSupportStructureThickness   = rtSuppThick;
  racetrackCoilNumber                  = rtcNumber;
  coilAngle                            = (2*pi)/float(racetrackCoilNumber);
  racetrackCoilInnerRadius             = rtcInnerRadius;
  racetrackCoilThickness               = rtcThick;
  racetrackCoilLength                  = rtcLength;
  racetrackTiProtectionThickness       = rtcTiPThick;
  racetrackRadialDistanceFromOrigin    = racetrackSupportStructureInnerRadius+racetrackSupportStructureThickness+racetrackTiProtectionThickness+racetrackCoilInnerRadius+racetrackCoilThickness;
  racetrackBottomTiProtectionInnerRadius = racetrackSupportStructureInnerRadius+racetrackSupportStructureThickness;
  racetrackTiProtectionInnerRadius       = racetrackCoilInnerRadius+racetrackCoilThickness;
  
  tierodsNumber                        = tieNumber; 
  tierodsRadius                        = tieRadius;
  tierodsHalfLength                    = racetrackTiProtectionInnerRadius;
  tierodsStep                          = (structureBarrelHalfLength*2)/(float(tierodsNumber)/2.);
  
  microMeteoriteProtectionInnerRadius  = racetrackTiProtectionInnerRadius+racetrackTiProtectionThickness;
  microMeteoriteProtectionThickness    = mmpThick;
  
  magneticFieldVolumeInnerRadius       = racetrackSupportStructureInnerRadius+racetrackSupportStructureThickness;
  magneticFieldVolumeThickness         = (racetrackTiProtectionInnerRadius+racetrackTiProtectionThickness)*2;

  materialShield                       = aMaterial;

}

SR2SToroidalMagneticShield::~SR2SToroidalMagneticShield()
{
  delete pMaterial;
  delete messenger;
}

void SR2SToroidalMagneticShield::DestroyComponent()
{
  delete racetrackSupportStructure;
  racetrackSupportStructure=0;
  delete magneticFieldVolumeBarrel;
  magneticFieldVolumeBarrel=0;
  delete magneticFieldVolumeEndcapTorus;
  magneticFieldVolumeEndcapTorus=0;
  delete magneticFieldVolumeEndcapBox;
  magneticFieldVolumeEndcapBox=0;
  delete magneticFieldVolumeEndcap;
  magneticFieldVolumeEndcap=0;
  delete magneticFieldVolume;
  magneticFieldVolume=0;
  delete racetrackBottomTiProtection;
  racetrackBottomTiProtection=0;
  delete racetrackCoilBarrelBox;
  racetrackCoilBarrelBox=0;
  delete racetrackCoilEndcapBox;
  racetrackCoilEndcapBox=0;
  delete racetrackCoilBox;
  racetrackCoilBox=0;
  delete racetrackCoilBarrel;
  racetrackCoilBarrel=0;
  delete racetrackCoilEndcap;
  racetrackCoilEndcap=0;
  delete racetrackCoil;
  racetrackCoil=0;
  delete racetrackCoilTiProtectionBarrel;
  racetrackCoilTiProtectionBarrel=0;
  delete racetrackCoilTiProtectionEndcap;
  racetrackCoilTiProtectionEndcap=0;
  delete racetrackCoilTiProtection;
  racetrackCoilTiProtection=0;
  delete tierods;
  tierods=0;
  delete microMeteoriteProtectionBarrel;
  microMeteoriteProtectionBarrel=0;
  delete microMeteoriteProtectionEndcapTorus;
  microMeteoriteProtectionEndcapTorus=0;
  delete microMeteoriteProtectionEndcapBox;
  microMeteoriteProtectionEndcapBox=0;
  delete microMeteoriteProtectionEndcap;
  microMeteoriteProtectionEndcap=0;
  delete microMeteoriteProtection;
  microMeteoriteProtection=0;


  delete racetrackSupportStructureLog;
  racetrackSupportStructureLog=0;
  delete magneticFieldVolumeLog;
  magneticFieldVolumeLog=0;
  delete racetrackBottomTiProtectionLog;
  racetrackBottomTiProtectionLog=0;
  delete racetrackCoilBoxLog;
  racetrackCoilBoxLog=0;
  delete racetrackCoilLog;
  racetrackCoilLog=0;
  delete racetrackCoilTiProtectionLog;
  racetrackCoilTiProtectionLog=0;
  delete tierodsLog;
  tierodsLog=0;
  delete microMeteoriteProtectionLog;
  microMeteoriteProtectionLog=0;

  delete racetrackSupportStructurePhys;
  racetrackSupportStructurePhys=0;
  delete magneticFieldVolumePhys;
  magneticFieldVolumePhys=0;
  delete racetrackBottomTiProtectionPhys;
  racetrackBottomTiProtectionPhys=0;
  delete microMeteoriteProtectionPhys;
  microMeteoriteProtectionPhys=0;
  delete racetrackCoilBoxPhys;
  racetrackCoilBoxPhys=0;
  delete racetrackCoilPhys;
  racetrackCoilPhys=0;
  delete racetrackCoilTiProtectionPhys;
  racetrackCoilTiProtectionPhys=0;
  delete tierodsPhys;
  tierodsPhys=0;
}

void SR2SToroidalMagneticShield::ConstructComponent(G4VPhysicalVolume* motherVolume)
{
  // Geometry definition
  pMaterial->DefineMaterials();

  G4Material* vacuum =   pMaterial -> GetMaterial("Galactic");
  G4Material* titanium = pMaterial -> GetMaterial("Titanium");	      
  G4Material* graphite = pMaterial -> GetMaterial("Graphite");	      
  G4Material* sccable =  pMaterial -> GetMaterial("SCCable");	      
  G4Material* polyacrylate = pMaterial -> GetMaterial("polyacrylate");

  if(!materialShield){
    titanium = pMaterial -> GetMaterial("Galactic");
    graphite = pMaterial -> GetMaterial("Galactic");
    sccable =  pMaterial -> GetMaterial("Galactic");
    polyacrylate = pMaterial -> GetMaterial("Galactic");
  }
  
  racetrackSupportStructure = new G4Tubs("racetrackSupportStructure",
					 racetrackSupportStructureInnerRadius,
					 racetrackSupportStructureInnerRadius+racetrackSupportStructureThickness,
					 structureBarrelHalfLength,0,2*pi);
  
  racetrackSupportStructureLog = new G4LogicalVolume(racetrackSupportStructure,
						     titanium,
						     "racetrackSupportStructureLog",
						     0,0,0);
  
  racetrackSupportStructurePhys = new G4PVPlacement(0,
						    G4ThreeVector(0.,0.,0.),
						    "racetrackSupportStructurePhys",
						    racetrackSupportStructureLog,
						    motherVolume,false,0,true); 
  
  magneticFieldVolumeBarrel = new G4Tubs("magneticFieldVolumeBarrel",
					 magneticFieldVolumeInnerRadius,
					 magneticFieldVolumeInnerRadius+magneticFieldVolumeThickness,
					 structureBarrelHalfLength,0,2*pi);
  
  magneticFieldVolumeEndcapTorus = new G4Torus("magneticFieldVolumeEndcapTorus",
					       0,microMeteoriteProtectionInnerRadius,
					       racetrackRadialDistanceFromOrigin,0,2*pi);
					       
  magneticFieldVolumeEndcapBox = new G4Box("magneticFieldVolumeEndcapBox",
					   magneticFieldVolumeInnerRadius+magneticFieldVolumeThickness,
					   magneticFieldVolumeInnerRadius+magneticFieldVolumeThickness,
					   microMeteoriteProtectionInnerRadius);
  
  
  magneticFieldVolumeEndcap = new G4SubtractionSolid("magneticFieldVolumeEndcap",
						     magneticFieldVolumeEndcapTorus,
						     magneticFieldVolumeEndcapBox,
						     0,
						     G4ThreeVector(0,0,-microMeteoriteProtectionInnerRadius));
  
  
  G4ThreeVector transM = G4ThreeVector(0,0,-structureBarrelHalfLength);
  G4RotationMatrix* rotM = new G4RotationMatrix;
  rotM->rotateY(pi);

  G4ThreeVector transP = G4ThreeVector(0,0,structureBarrelHalfLength);
  G4RotationMatrix* rotP = new G4RotationMatrix;
  rotP->rotateY(0);

  magneticFieldVolume = new G4UnionSolid("magneticFieldVolume",
					 new G4UnionSolid("partialmagneticFieldVolume",
							  magneticFieldVolumeBarrel,
							  magneticFieldVolumeEndcap,
							  rotP,transP),
					 magneticFieldVolumeEndcap,rotM,transM);
  
  magneticFieldVolumeLog = new G4LogicalVolume(magneticFieldVolume,
					       vacuum,
					       "magneticFieldVolumeLog",
					       0,0,0);

  magneticFieldVolumePhys = new G4PVPlacement(0,
					      G4ThreeVector(0.,0.,0.),
					      "magneticFieldVolumePhys",
					      magneticFieldVolumeLog,
					      motherVolume,false,0,true); 

  racetrackBottomTiProtection = new G4Tubs("racetrackBottomTiProtection",
					   racetrackBottomTiProtectionInnerRadius,
					   racetrackBottomTiProtectionInnerRadius+racetrackTiProtectionThickness,
					   structureBarrelHalfLength,0,2*pi);
  
  racetrackBottomTiProtectionLog = new G4LogicalVolume(racetrackBottomTiProtection,
						       titanium,
						       "racetrackBottomTiProtectionLog",
						       0,0,0);
  
  racetrackBottomTiProtectionPhys = new  G4PVPlacement(0,
						       G4ThreeVector(0.,0.,0.),
						       "racetrackBottomTiProtectionPhys",
						       racetrackBottomTiProtectionLog,
						       magneticFieldVolumePhys,false,0,true);
  

  racetrackCoilBarrelBox = new G4SubtractionSolid("racetrackCoilBarrelBox",
						  new G4Box("tempBareelBox1",
							    racetrackCoilLength/2.+tierodsRadius*2.,
							    racetrackTiProtectionInnerRadius+racetrackTiProtectionThickness,
							    structureBarrelHalfLength),
						  new G4Box("tempBareelBox2",
							    racetrackCoilLength/2.+tierodsRadius*2.+1,
							    racetrackTiProtectionThickness/2.,
							    structureBarrelHalfLength+1),
						  0,G4ThreeVector(0,-(racetrackTiProtectionInnerRadius+racetrackTiProtectionThickness/2.),0));
  
  racetrackCoilEndcapBox = new G4Tubs("racetrackCoilEndcapBox",
				      0,
				      microMeteoriteProtectionInnerRadius,
				      racetrackCoilLength/2.+tierodsRadius*2.,
				      0,pi);
  
  G4RotationMatrix* rotRTCM = new G4RotationMatrix;
  rotRTCM->rotateY(pi/2.);
  rotRTCM->rotateZ(-pi/2.);

  G4RotationMatrix* rotRTCP = new G4RotationMatrix;
  rotRTCP->rotateY(-pi/2.);
  rotRTCP->rotateZ(-pi/2.);

  racetrackCoilBox = new G4UnionSolid("racetrackCoilBox",
				      new G4UnionSolid("partialracetrackCoilBox",
						       racetrackCoilBarrelBox,
						       racetrackCoilEndcapBox,rotRTCP,transP),
				      racetrackCoilEndcapBox,
				      rotRTCM,transM);

  racetrackCoilBoxLog = new G4LogicalVolume(racetrackCoilBox,
					    vacuum,
					    "racetrackCoilBoxLog",
					    0,0,0);
  
  racetrackCoilBarrel = new G4SubtractionSolid("racetrackCoilBarrel",
					       new G4Box("BarerlOuter",
							 racetrackCoilLength/2.,
							 racetrackTiProtectionInnerRadius,
							 structureBarrelHalfLength),
					       new G4Box("BarrelInner",
							 racetrackCoilLength/2.+1,
							 racetrackCoilInnerRadius,
							 structureBarrelHalfLength+1));
  
  racetrackCoilEndcap = new G4Tubs("racetrackCoilEndcap",
				   racetrackCoilInnerRadius,
				   racetrackCoilInnerRadius+racetrackCoilThickness,
				   racetrackCoilLength/2.,
				   0,pi);

  racetrackCoil = new G4UnionSolid("racetrackCoil",
				   new G4UnionSolid("partialracetrackCoil",
						    racetrackCoilBarrel,
						    racetrackCoilEndcap,rotRTCP,transP),
				   racetrackCoilEndcap,rotRTCM,transM);
  
  racetrackCoilLog = new G4LogicalVolume(racetrackCoil,
					 sccable,
					 "racetrackCoilLog",
					 0,0,0);
  
  racetrackCoilTiProtectionBarrel = new G4SubtractionSolid("racetrackCoilTiProtectionBarrel",
							   new G4Box("BarrelOuterTi",
								     racetrackCoilLength/2.,
								     racetrackTiProtectionInnerRadius+racetrackTiProtectionThickness,
								     structureBarrelHalfLength),
							   new G4Box("BarrelInnerTi",
								     racetrackCoilLength/2.+1,
								     racetrackTiProtectionInnerRadius+racetrackTiProtectionThickness,
								     structureBarrelHalfLength+1),
							   0,G4ThreeVector(0,-racetrackTiProtectionThickness,0));

  racetrackCoilTiProtectionEndcap = new G4Tubs("racetrackCoilTiProtectionEndcap",
					       racetrackTiProtectionInnerRadius,
					       racetrackTiProtectionInnerRadius+racetrackTiProtectionThickness,
					       racetrackCoilLength/2.,
					       0,pi);
  
  racetrackCoilTiProtection = new G4UnionSolid("racetrackCoilTiProtection",
					       new G4UnionSolid("partialracetrackCoilTiProtection",
								racetrackCoilTiProtectionBarrel,
								racetrackCoilTiProtectionEndcap,rotRTCP,transP),
					       racetrackCoilTiProtectionEndcap,rotRTCM,transM);

  racetrackCoilTiProtectionLog = new G4LogicalVolume(racetrackCoilTiProtection,
						     titanium,
						     "racetrackCoilTiProtectionLog",
						     0,0,0);

  tierods = new G4Tubs("tierods",
		       0,
		       tierodsRadius,
		       tierodsHalfLength,
		       0,2*pi);

  tierodsLog = new  G4LogicalVolume(tierods,
				    graphite,
				    "tierodsLog",
				    0,0,0);
  
  G4RotationMatrix* tierodsRot = new G4RotationMatrix;
  tierodsRot->rotateX(-pi/2.);
  
  for(int i=0;i<racetrackCoilNumber;i++){
    G4double localAngle = i*coilAngle;
    G4RotationMatrix* boxRot = new G4RotationMatrix;
    boxRot->rotateZ(localAngle);
    
    racetrackCoilBoxPhys = new G4PVPlacement(boxRot,
						G4ThreeVector(racetrackRadialDistanceFromOrigin*sin(localAngle),racetrackRadialDistanceFromOrigin*cos(localAngle),0.),
						"racetrackCoilBoxPhys",
						racetrackCoilBoxLog,
						magneticFieldVolumePhys,
						false,i,true);

  }
    
  racetrackCoilPhys = new G4PVPlacement(0,
					G4ThreeVector(0.,0.,0.),
					"racetrackCoilPhys", 
					racetrackCoilLog,
					racetrackCoilBoxPhys,
					false,0,true); 
  
  racetrackCoilTiProtectionPhys = new G4PVPlacement(0,
						    G4ThreeVector(0.,0.,0.),
						    "racetrackCoilTiProtectionPhys", 
						    racetrackCoilTiProtectionLog,
						    racetrackCoilBoxPhys,
						    false,0,true);
  
  for(int j=0;j<tierodsNumber;j++){
    if(j<tierodsNumber/2.){
      tierodsPhys = new G4PVPlacement(tierodsRot,
				      G4ThreeVector(-(tierodsRadius+racetrackCoilLength/2.),0,-structureBarrelHalfLength+j*tierodsStep),
				      "tierodsPhys",
				      tierodsLog,
				      racetrackCoilBoxPhys,
				      false,j,true);
    }else{
      tierodsPhys = new G4PVPlacement(tierodsRot,
				      G4ThreeVector(tierodsRadius+racetrackCoilLength/2.,0,-structureBarrelHalfLength+(j-(tierodsNumber/2.))*tierodsStep),
				      "tierodsPhys",
				      tierodsLog,
				      racetrackCoilBoxPhys,
				      false,j,true);
    }
  }
  

  microMeteoriteProtectionBarrel = new G4Tubs("microMeteoriteProtectionBarrel",
					      racetrackRadialDistanceFromOrigin+microMeteoriteProtectionInnerRadius,
					      racetrackRadialDistanceFromOrigin+microMeteoriteProtectionInnerRadius+microMeteoriteProtectionThickness,
					      structureBarrelHalfLength,
					      0,2*pi);
  
  microMeteoriteProtectionEndcapTorus = new G4Torus("microMeteoriteProtectionEndcapTorus",
						    microMeteoriteProtectionInnerRadius,
						    microMeteoriteProtectionInnerRadius+microMeteoriteProtectionThickness,
						    racetrackRadialDistanceFromOrigin,
						    0,2*pi);
  
  microMeteoriteProtectionEndcapBox = new G4Box("microMeteoriteProtectionEndcapBox",
						racetrackRadialDistanceFromOrigin+microMeteoriteProtectionInnerRadius+microMeteoriteProtectionThickness+1,
						racetrackRadialDistanceFromOrigin+microMeteoriteProtectionInnerRadius+microMeteoriteProtectionThickness+1,
						microMeteoriteProtectionInnerRadius+microMeteoriteProtectionThickness);

  microMeteoriteProtectionEndcap = new G4SubtractionSolid("microMeteoriteProtectionEndcap",
							  microMeteoriteProtectionEndcapTorus,
							  microMeteoriteProtectionEndcapBox,
							  0,G4ThreeVector(0,0,-(microMeteoriteProtectionInnerRadius+microMeteoriteProtectionThickness)));

  microMeteoriteProtection = new G4UnionSolid("microMeteoriteProtection",
					      new G4UnionSolid("partialmicroMeteoriteProtection",
							       microMeteoriteProtectionBarrel,
							       microMeteoriteProtectionEndcap,rotP,transP),
					      microMeteoriteProtectionEndcap,rotM,transM);
  
  microMeteoriteProtectionLog = new G4LogicalVolume(microMeteoriteProtection,
						    polyacrylate,
						    "microMeteoriteProtectionLog",
						    0,0,0);
  
  microMeteoriteProtectionPhys = new G4PVPlacement(0,
						   G4ThreeVector(0,0,0),
						   "microMeteoriteProtectionPhys",
						   microMeteoriteProtectionLog,
						   motherVolume,false,0,true);

  G4VisAttributes* layerVisAttMagenta = new G4VisAttributes(G4Colour::Magenta());
  layerVisAttMagenta->SetVisibility(true);
  //layerVisAttMagenta->SetForceAuxEdgeVisible(true);
  layerVisAttMagenta->SetForceSolid(true);
  G4VisAttributes* layerVisAttGreen = new G4VisAttributes(G4Colour::Green());
  layerVisAttGreen->SetVisibility(true);
  //layerVisAttGreen->SetForceAuxEdgeVisible(true);
  layerVisAttGreen->SetForceSolid(true);
  G4VisAttributes* layerVisAttBlue = new G4VisAttributes(G4Colour::Blue());
  layerVisAttBlue->SetVisibility(true);
  //layerVisAttBlue->SetForceAuxEdgeVisible(true);
  layerVisAttBlue->SetForceSolid(true);
  G4VisAttributes* layerVisAttYellow = new G4VisAttributes(G4Colour::Yellow());
  layerVisAttYellow->SetVisibility(true);
  //layerVisAttYellow->SetForceAuxEdgeVisible(true);
  layerVisAttYellow->SetForceSolid(true);
  G4VisAttributes* layerVisAttRed = new G4VisAttributes(G4Colour::Red());
  layerVisAttRed->SetVisibility(true);
  //layerVisAttRed->SetForceAuxEdgeVisible(true);
  layerVisAttRed->SetForceSolid(true);
  G4VisAttributes* layerVisAttGray = new G4VisAttributes(G4Colour::Gray());
  layerVisAttGray->SetVisibility(true);
  //layerVisAttGray->SetForceAuxEdgeVisible(true);
  layerVisAttGray->SetForceSolid(true);
  G4VisAttributes* layerVisAttInvisible = new G4VisAttributes();
  layerVisAttInvisible->SetVisibility(false);
  //layerVisAttInvisible->SetForceAuxEdgeVisible(false);
  layerVisAttInvisible->SetForceSolid(false);

  racetrackSupportStructureLog->SetVisAttributes(layerVisAttGray);
  //racetrackSupportStructureLog->SetVisAttributes(layerVisAttInvisible);

  //magneticFieldVolumeLog->SetVisAttributes(layerVisAttBlue);
  magneticFieldVolumeLog->SetVisAttributes(layerVisAttInvisible);

  racetrackBottomTiProtectionLog->SetVisAttributes(layerVisAttGray);
  //racetrackBottomTiProtectionLog->SetVisAttributes(layerVisAttInvisible);

  //racetrackCoilBoxLog->SetVisAttributes(layerVisAttMagenta);
  racetrackCoilBoxLog->SetVisAttributes(layerVisAttInvisible);

  racetrackCoilLog->SetVisAttributes(layerVisAttRed);
  //racetrackCoilLog->SetVisAttributes(layerVisAttInvisible);

  racetrackCoilTiProtectionLog->SetVisAttributes(layerVisAttGray);
  //racetrackCoilTiProtectionLog->SetVisAttributes(layerVisAttInvisible);

  tierodsLog->SetVisAttributes(layerVisAttYellow);
  //tierodsLog->SetVisAttributes(layerVisAttInvisible);

  //microMeteoriteProtectionLog->SetVisAttributes(layerVisAttGreen);
  microMeteoriteProtectionLog->SetVisAttributes(layerVisAttInvisible);
  
}

void SR2SToroidalMagneticShield::SetFieldManager(G4FieldManager* fieldManager)
{
  magneticFieldVolumeLog->SetFieldManager(fieldManager,true);
}
 
