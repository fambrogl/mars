//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id$
//
// Code developed by: S.Guatelli, susanna@uow.edu.au
//
#include "SR2SSolenoidalMagneticShield.hh"
#include "MarsMaterial.hh"

#include "G4SystemOfUnits.hh"
#include "G4Material.hh"
#include "G4MaterialTable.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "G4RunManager.hh"
#include "G4VisAttributes.hh"
#include "SR2SSolenoidalMagneticShieldMessenger.hh"
#include "G4Torus.hh"
#include "G4UnionSolid.hh"
#include "G4SubtractionSolid.hh"

SR2SSolenoidalMagneticShield::SR2SSolenoidalMagneticShield()
{
  innerSolenoidRadius=3.1*m; //3*m;
  innerSolenoidThickness=5*cm;
  outerSolenoidRadius=4.5*m;
  outerSolenoidThickness=5*cm;
  solenoidHalfLength=5*m;

  magneticFieldVolumeInnerRadius=3.10*m;
  magneticFieldVolumeThickness=(4.5-3.1)*m;

  materialShield=true;
  pMaterial = new MarsMaterial();
  messenger = new SR2SSolenoidalMagneticShieldMessenger(this);
}

void SR2SSolenoidalMagneticShield::SetSR2SShieldStructureParameter(G4double aInnerSolenoidRadius,
								   G4double aInnerSolenoidThickness,
								   G4double aOuterSolenoidRadius,
								   G4double aOuterSolenoidThickness,
								   G4double aSolenoidHalfLength,
								   G4bool aMaterial)
{
  innerSolenoidRadius=aInnerSolenoidRadius;
  innerSolenoidThickness=aInnerSolenoidThickness;
  outerSolenoidRadius=aOuterSolenoidRadius;
  outerSolenoidThickness=aOuterSolenoidThickness;
  solenoidHalfLength=aSolenoidHalfLength;

  magneticFieldVolumeInnerRadius=aInnerSolenoidRadius;
  magneticFieldVolumeThickness=(aOuterSolenoidRadius-aInnerSolenoidRadius);
  materialShield=aMaterial;

}

SR2SSolenoidalMagneticShield::~SR2SSolenoidalMagneticShield()
{
  delete pMaterial;
  delete messenger;
}

void SR2SSolenoidalMagneticShield::DestroyComponent()
{
  delete innerSolenoid;
  innerSolenoid=0;
  delete outerSolenoid;
  outerSolenoid=0;
  delete magneticFieldVolume;
  magneticFieldVolume=0;

  delete innerSolenoidLog;
  innerSolenoidLog=0;
  delete outerSolenoidLog;
  outerSolenoidLog=0;
  delete magneticFieldVolumeLog;
  magneticFieldVolumeLog=0;

  delete innerSolenoidPhys;
  innerSolenoidPhys=0;
  delete outerSolenoidPhys;
  outerSolenoidPhys=0;
  delete magneticFieldVolumePhys;
  magneticFieldVolumePhys=0;
}

void SR2SSolenoidalMagneticShield::ConstructComponent(G4VPhysicalVolume* motherVolume)
{
  // Geometry definition
  pMaterial->DefineMaterials();

  G4Material* vacuum =   pMaterial -> GetMaterial("Galactic");
  G4Material* sccable =  pMaterial -> GetMaterial("SCCable");
  //G4Material* titanium = pMaterial -> GetMaterial("Titanium");
  //G4Material* graphite = pMaterial -> GetMaterial("Graphite");
  //G4Material* polyacrylate = pMaterial -> GetMaterial("polyacrylate");
  if(!materialShield){
    sccable =  pMaterial -> GetMaterial("Galactic");
    //titanium = pMaterial -> GetMaterial("Galactic");
    //graphite = pMaterial -> GetMaterial("Galactic");
    //polyacrylate = pMaterial -> GetMaterial("Galactic");
  }
  

  magneticFieldVolume = new G4Tubs("magneticFieldVolume",
				   magneticFieldVolumeInnerRadius,
				   magneticFieldVolumeInnerRadius+magneticFieldVolumeThickness,
				   solenoidHalfLength,
				   0,2*pi);
  
  magneticFieldVolumeLog = new G4LogicalVolume(magneticFieldVolume,
					       vacuum,
					       "magneticFieldVolume",
					       0,0,0);

  magneticFieldVolumePhys = new G4PVPlacement(0,
					      G4ThreeVector(0.,0.,0.),
					      "magneticFieldVolumePhys",
					      magneticFieldVolumeLog,
					      motherVolume,false,0,true); 

  innerSolenoid = new G4Tubs("innerSolenoid",
			     innerSolenoidRadius,
			     innerSolenoidRadius+innerSolenoidThickness,
			     solenoidHalfLength,
			     0,2*pi);
  
  innerSolenoidLog = new G4LogicalVolume(innerSolenoid,
					 sccable,
					 "innerSolenoidLog",
					 0,0,0);
  
  innerSolenoidPhys = new G4PVPlacement(0,
					G4ThreeVector(0.,0.,0.),
					"innerSolenoidPhys",
					innerSolenoidLog,
					magneticFieldVolumePhys,false,0,true); 

  outerSolenoid = new G4Tubs("outerSolenoid",
			     outerSolenoidRadius-outerSolenoidThickness,
			     outerSolenoidRadius,
			     solenoidHalfLength,
			     0,2*pi);
  
  outerSolenoidLog = new G4LogicalVolume(outerSolenoid,
					 sccable,
					 "outerSolenoidLog",
					 0,0,0);
  
  outerSolenoidPhys = new G4PVPlacement(0,
					G4ThreeVector(0.,0.,0.),
					"outerSolenoidPhys",
					outerSolenoidLog,
					magneticFieldVolumePhys,false,0,true); 
  

  G4VisAttributes* layerVisAttMagenta = new G4VisAttributes(G4Colour::Magenta());
  layerVisAttMagenta->SetVisibility(true);
  //layerVisAttMagenta->SetForceAuxEdgeVisible(true);
  layerVisAttMagenta->SetForceSolid(true);
  G4VisAttributes* layerVisAttGreen = new G4VisAttributes(G4Colour::Green());
  layerVisAttGreen->SetVisibility(true);
  //layerVisAttGreen->SetForceAuxEdgeVisible(true);
  layerVisAttGreen->SetForceSolid(true);
  G4VisAttributes* layerVisAttBlue = new G4VisAttributes(G4Colour::Blue());
  layerVisAttBlue->SetVisibility(true);
  //layerVisAttBlue->SetForceAuxEdgeVisible(true);
  layerVisAttBlue->SetForceSolid(true);
  G4VisAttributes* layerVisAttYellow = new G4VisAttributes(G4Colour::Yellow());
  layerVisAttYellow->SetVisibility(true);
  //layerVisAttYellow->SetForceAuxEdgeVisible(true);
  layerVisAttYellow->SetForceSolid(true);
  G4VisAttributes* layerVisAttRed = new G4VisAttributes(G4Colour::Red());
  layerVisAttRed->SetVisibility(true);
  //layerVisAttRed->SetForceAuxEdgeVisible(true);
  layerVisAttRed->SetForceSolid(true);
  G4VisAttributes* layerVisAttGray = new G4VisAttributes(G4Colour::Gray());
  layerVisAttGray->SetVisibility(true);
  //layerVisAttGray->SetForceAuxEdgeVisible(true);
  layerVisAttGray->SetForceSolid(true);
  G4VisAttributes* layerVisAttInvisible = new G4VisAttributes();
  layerVisAttInvisible->SetVisibility(false);
  //layerVisAttInvisible->SetForceAuxEdgeVisible(false);
  layerVisAttInvisible->SetForceSolid(false);

  innerSolenoidLog->SetVisAttributes(layerVisAttBlue);
  //innerSolenoidLog->SetVisAttributes(layerVisAttInvisible);

  outerSolenoidLog->SetVisAttributes(layerVisAttRed);
  //outerSolenoidLog->SetVisAttributes(layerVisAttInvisible);

  magneticFieldVolumeLog->SetVisAttributes(layerVisAttMagenta);
  //magneticFieldVolumeLog->SetVisAttributes(layerVisAttInvisible);
}

void SR2SSolenoidalMagneticShield::SetFieldManager(G4FieldManager* fieldManager)
{
  magneticFieldVolumeLog->SetFieldManager(fieldManager,true);
}
 
