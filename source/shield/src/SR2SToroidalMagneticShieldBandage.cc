//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id$
//
// Code developed by: Filippo Ambroglini, filippo.ambroglini@pg.infn.it
//
#include "SR2SToroidalMagneticShieldBandage.hh"
#include "MarsMaterial.hh"

#include "G4SystemOfUnits.hh"
#include "G4Material.hh"
#include "G4MaterialTable.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "G4RunManager.hh"
#include "G4VisAttributes.hh"
#include "SR2SToroidalMagneticShieldBandageMessenger.hh"
#include "G4Torus.hh"
#include "G4UnionSolid.hh"
#include "G4SubtractionSolid.hh"

SR2SToroidalMagneticShieldBandage::SR2SToroidalMagneticShieldBandage()
{

  racetrackSupportStructureMiddlePosition = 2686*mm;
  racetrackSupportStructureThickness      = 130*mm;
  racetrackSupportStructureInnerRadius    = racetrackSupportStructureMiddlePosition
    -racetrackSupportStructureThickness/2.;
  racetrackSupportStructureOuterRadius    = racetrackSupportStructureMiddlePosition
    +racetrackSupportStructureThickness/2.;
  racetrackSupportStructureLength         = 9000*mm;

  bandageThickness                        = 1*mm;
  shortBandageLength                      = 10002*mm;
  shortBandageHeight                      = 2200*mm;
  shortBandageWidth                       = 124*mm;
  longBandageLength                       = 8600*mm;
  longBandageHeight                       = 3602*mm;
  longBandageWidth                        = 126*mm;

  coilDistanceFromCenter                   = 2752*mm;
  coilBarrelLength                         = 8600*mm;
  coilBarrelWidth                          = 122*mm;
  coilThickness                            = 62*mm;
  coilBarrelHeight                         = 3600*mm;
  coilEndcapRadius                         = 700*mm;
  coilSupportThickness                     = 6*mm;
  
  racetrackCoilNumber                      = 120;
  coilAngle                                = (2*pi)/float(racetrackCoilNumber);
  

  cableBarrelLength                        = coilBarrelLength;
  cableBarrelWidth                         = coilBarrelWidth-coilSupportThickness*2;
  cableThickness                           = coilThickness-coilSupportThickness*2;
  cableBarrelHeight                        = coilBarrelHeight-coilSupportThickness*2;
  cableEndcapRadius                        = coilEndcapRadius-coilSupportThickness;

  coilMaterial                            ="Aluminium";
  coreCableMaterial                       ="SCCable";
  bandageMaterial                         ="kevlar";
  SupportStructureMaterial                ="Aluminium";
  
  materialShield=true;
  
  pMaterial = new MarsMaterial();
  messenger = new SR2SToroidalMagneticShieldBandageMessenger(this);
}


void SR2SToroidalMagneticShieldBandage::SetSR2SSupportStructureParameter(G4double radius,G4double thickness,
									     G4double length, G4String material)
{
  racetrackSupportStructureMiddlePosition = radius;
  racetrackSupportStructureThickness      = thickness;
  racetrackSupportStructureInnerRadius    = racetrackSupportStructureMiddlePosition
    -racetrackSupportStructureThickness/2.;
  racetrackSupportStructureOuterRadius    = racetrackSupportStructureMiddlePosition
    +racetrackSupportStructureThickness/2.;
  racetrackSupportStructureLength         = length;
  
  SupportStructureMaterial                = material; 
}

void SR2SToroidalMagneticShieldBandage::SetSR2SCoilStructureParameter(G4double radialDistance,G4double length,
								      G4double width,G4double thickness,
								      G4double  height,G4double endcapR,
								      G4double envelopeThickness,
								      G4int nCoil, G4String materialCoil,
								      G4String materialCable)
{
  coilDistanceFromCenter                   = radialDistance;
  coilBarrelLength                         = length;
  coilBarrelWidth                          = width;
  coilThickness                            = thickness;
  coilBarrelHeight                         = height;
  coilEndcapRadius                         = endcapR;
  coilSupportThickness                    = envelopeThickness;

  cableBarrelLength                        = coilBarrelLength;
  cableBarrelWidth                         = coilBarrelWidth-coilSupportThickness*2;
  cableThickness                           = coilThickness-coilSupportThickness*2;
  cableBarrelHeight                        = coilBarrelHeight-coilSupportThickness*2;
  cableEndcapRadius                        = coilEndcapRadius-coilSupportThickness;
  
  racetrackCoilNumber                      = nCoil;
  coilAngle                                = (2*pi)/float(racetrackCoilNumber);

  coilMaterial                             = materialCoil;
  coreCableMaterial                        = materialCable;
}

void SR2SToroidalMagneticShieldBandage::SetSR2SBandageStructureParameter(G4double thickness,
									 G4double short_length,
									 G4double short_height,
									 G4double short_width,
									 G4double long_length,
									 G4double long_height,
									 G4double long_width,
									 G4String material)
{

  bandageThickness                        = thickness;
  shortBandageLength                      = short_length;
  shortBandageHeight                      = short_height;
  shortBandageWidth                       = short_width;
  longBandageLength                       = long_length;
  longBandageHeight                       = long_height;
  longBandageWidth                        = long_width;
  
  bandageMaterial                         = material;
}


SR2SToroidalMagneticShieldBandage::~SR2SToroidalMagneticShieldBandage()
{
  delete pMaterial;
  delete messenger;
}

void SR2SToroidalMagneticShieldBandage::DestroyComponent()
{
  /*
  delete racetrackSupportStructure;
  racetrackSupportStructure=0;

  delete magneticFieldVolume;
  magneticFieldVolume=0;
  delete racetrackCoilBoxBarrelExternal;
  racetrackCoilBoxBarrelExternal=0;
  delete racetrackCoilBoxBarrelInternal;
  racetrackCoilBoxBarrelInternal=0;
  delete racetrackCoilBoxBarrel;
  racetrackCoilBoxBarrel=0;
  delete racetrackCoilBoxEndcapTubs;
  racetrackCoilBoxEndcapTubs=0;
  delete racetrackCoilBoxEndcapBox;
  racetrackCoilBoxEndcapBox=0;
  delete racetrackCoilBoxEndcap;
  racetrackCoilBoxEndcap=0;
  delete racetrackCoil;
  racetrackCoil=0;
  delete racetrackCableBoxBarrelExternal;
  racetrackCableBoxBarrelExternal=0;
  delete racetrackCableBoxBarrelInternal;
  racetrackCableBoxBarrelInternal=0;
  delete racetrackCableBoxBarrel;
  racetrackCableBoxBarrel=0;
  delete racetrackCableBoxEndcapTubs;
  racetrackCableBoxEndcapTubs=0;
  delete racetrackCableBoxEndcapBox;
  racetrackCableBoxEndcapBox=0;
  delete racetrackCableBoxEndcap;
  racetrackCableBoxEndcap=0;
  delete racetrackCable;
  racetrackCable=0;
  delete  bandageBoxLongExternal;
  bandageBoxLongExternal=0;
  delete  bandageBoxLongInternal;
  bandageBoxLongInternal=0;
  delete  bandageBoxLong;
  bandageBoxLong=0;
  delete  bandageBoxShortExternal;
  bandageBoxShortExternal=0;
  delete  bandageBoxShortInternal;
  bandageBoxShortInternal=0;
  delete  bandageBoxShort;
  bandageBoxShort=0;


  delete racetrackSupportStructureLog;
  racetrackSupportStructureLog=0;
  delete magneticFieldVolumeLog;
  magneticFieldVolumeLog=0;
  delete racetrackCoilLog;
  racetrackCoilLog=0;
  delete racetrackCableLog;
  racetrackCableLog=0;
  delete  bandageBoxShortLog;
  bandageBoxShortLog=0;
  delete  bandageBoxLongLog;
  bandageBoxLongLog=0;

  delete racetrackSupportStructurePhys;
  racetrackSupportStructurePhys=0;
  delete magneticFieldVolumePhys;
  magneticFieldVolumePhys=0;
  delete racetrackCoilPhys;
  racetrackCoilPhys=0;
  delete racetrackCablePhys;
  racetrackCablePhys=0;
  delete  bandageBoxShortPhys;
  bandageBoxShortPhys=0;
  delete  bandageBoxLongPhys;
  bandageBoxLongPhys=0;
  */

}

void SR2SToroidalMagneticShieldBandage::ConstructComponent(G4VPhysicalVolume* motherVolume)
{
  // Geometry definition
  pMaterial->DefineMaterials();

  G4Material* vacuum =   pMaterial -> GetMaterial("Galactic");
  G4Material* support = pMaterial -> GetMaterial(SupportStructureMaterial);	      
  G4Material* bandage = pMaterial -> GetMaterial(bandageMaterial);	      
  G4Material* cable = pMaterial -> GetMaterial(coreCableMaterial);	      
  G4Material* coil =  pMaterial -> GetMaterial(coilMaterial);	      

  if(!materialShield){
    support = pMaterial -> GetMaterial("Galactic");
    bandage =  pMaterial -> GetMaterial("Galactic");
    cable = pMaterial -> GetMaterial("Galactic");
    coil = pMaterial -> GetMaterial("Galactic");
  }
  

  magneticFieldVolume = new G4Tubs("magneticFieldVolume",
				   racetrackSupportStructureOuterRadius,
				   racetrackSupportStructureOuterRadius+longBandageHeight+3*mm,
				   shortBandageLength/2.+2*mm,0,2*pi);
  
 
  
  magneticFieldVolumeLog = new G4LogicalVolume(magneticFieldVolume,
					       vacuum,
					       "magneticFieldVolumeLog",
					       0,0,0);

  magneticFieldVolumePhys = new G4PVPlacement(0,
					      G4ThreeVector(0.,0.,0.),
					      "magneticFieldVolumePhys",
					      magneticFieldVolumeLog,
					      motherVolume,false,0,true); 

  racetrackSupportStructure = new G4Tubs("racetrackSupportStructure",
					 racetrackSupportStructureInnerRadius,
					 racetrackSupportStructureOuterRadius,
					 racetrackSupportStructureLength/2.,0,2*pi);
  
  
  racetrackSupportStructureLog = new G4LogicalVolume(racetrackSupportStructure,
						     support,
						     "racetrackSupportStructureLog",
						     0,0,0);

  
  racetrackSupportStructurePhys = new G4PVPlacement(0,
						    G4ThreeVector(0.,0.,0.),
						    "racetrackSupportStructurePhys",
						    racetrackSupportStructureLog,
						    motherVolume,false,0,true); 
 
  
  racetrackCoilBoxBarrelExternal = new G4Box("racetrackCoilBoxBarrelExternal",
					     coilBarrelWidth/2.,
					     coilBarrelHeight/2.,
					     coilBarrelLength/2.);

  racetrackCoilBoxBarrelInternal = new G4Box("racetrackCoilBoxBarrelInternal",
					     coilBarrelWidth/2.+10*mm,
					     (coilBarrelHeight-coilThickness*2)/2.,
					     coilBarrelLength/2.+10*mm);

  racetrackCoilBoxBarrel = new G4SubtractionSolid("racetrackCoilBoxBarrel",
						  racetrackCoilBoxBarrelExternal,
						  racetrackCoilBoxBarrelInternal);
  
  racetrackCoilBoxEndcapTubs = new G4Tubs("racetrackCoilBoxEndcapTubs",
					  coilEndcapRadius-coilThickness,
					  coilEndcapRadius,
					  coilBarrelWidth/2.,0,pi/2.);
  
  racetrackCoilBoxEndcapBox = new G4Box("racetrackCoilBoxEndcapBox",
					coilBarrelHeight/2.-coilEndcapRadius,
					coilThickness/2.,
					coilBarrelWidth/2);
  
  G4ThreeVector transEP = G4ThreeVector(coilBarrelHeight/2.-coilEndcapRadius,-(coilEndcapRadius-coilThickness/2.),0);
  G4ThreeVector transEM = G4ThreeVector(-(coilBarrelHeight/2.-coilEndcapRadius),-(coilEndcapRadius-coilThickness/2.),0);
  G4RotationMatrix* rotEP = new G4RotationMatrix;
  G4RotationMatrix* rotEM = new G4RotationMatrix;
  rotEM->rotateY(-pi);
  
  racetrackCoilBoxEndcap = new G4UnionSolid("racetrackCoilBoxEndcap",
					    new G4UnionSolid("partialracetrackCoilBoxEndcap",
							     racetrackCoilBoxEndcapBox,
							     racetrackCoilBoxEndcapTubs,
							     rotEP,transEP),
					    racetrackCoilBoxEndcapTubs,rotEM,transEM);
  
  G4ThreeVector transCM = G4ThreeVector(0,0,-(coilBarrelLength/2.+coilEndcapRadius-coilThickness/2.));
  G4RotationMatrix* rotCM = new G4RotationMatrix;
  rotCM->rotateZ(pi/2);
  rotCM->rotateX(pi/2);
  
  G4ThreeVector transCP = G4ThreeVector(0,0,coilBarrelLength/2.+coilEndcapRadius-coilThickness/2.);
  G4RotationMatrix* rotCP = new G4RotationMatrix;
  rotCP->rotateZ(pi/2);
  rotCP->rotateX(-pi/2);
  
  racetrackCoil = new G4UnionSolid("racetrackCoil",
				   new G4UnionSolid("partilaracetrackCoil",
						    racetrackCoilBoxBarrel,
						    racetrackCoilBoxEndcap,
						    rotCP,transCP),
				   racetrackCoilBoxEndcap,rotCM,transCM);
  
  
  racetrackCoilLog = new G4LogicalVolume(racetrackCoil,
					 coil,
					 "racetrackCoilLog",
					 0,0,0);
 

  bandageBoxLongExternal = new G4Box("BandageBoxLongExternal",
				     longBandageWidth/2.,
				     longBandageHeight/2.,
				     longBandageLength/2.);

  bandageBoxLongInternal = new G4Box("BandageBoxLongInternal",
				     longBandageWidth/2.-bandageThickness,
				     longBandageHeight/2.-bandageThickness,
				     longBandageLength/2.+20*mm);

    
  bandageBoxLong = new G4SubtractionSolid("BandageBoxLong",
					  bandageBoxLongExternal,
					  bandageBoxLongInternal);

  bandageBoxLongLog = new G4LogicalVolume(bandageBoxLong,
					  bandage,
					  "bandageBoxLongLog",
					  0,0,0);

  bandageBoxShortExternal = new G4Box("BandageBoxShortExternal",
				     shortBandageWidth/2.,
				     shortBandageHeight/2.,
				     shortBandageLength/2.);

  bandageBoxShortInternal = new G4Box("BandageBoxShortInternal",
				      shortBandageWidth/2.-bandageThickness,
				      shortBandageHeight/2.+20*mm,
				      shortBandageLength/2.-bandageThickness);

  
  bandageBoxShort = new G4SubtractionSolid("BandageBoxShort",
					   bandageBoxShortExternal,
					   bandageBoxShortInternal);
  
  bandageBoxShortLog = new G4LogicalVolume(bandageBoxShort,
					   bandage,
					   "bandageBoxShortLog",
					   0,0,0);
  


 
  for(int i=0;i<racetrackCoilNumber;i++){
    G4double localAngle = i*coilAngle;
    G4RotationMatrix* boxRot = new G4RotationMatrix;
    G4ThreeVector boxTras= G4ThreeVector((coilDistanceFromCenter+coilBarrelHeight/2.)*sin(localAngle),
							(coilDistanceFromCenter+coilBarrelHeight/2.)*cos(localAngle),
					 0.);
    boxRot->rotateZ(localAngle);
    racetrackCoilPhys = new G4PVPlacement(boxRot,boxTras,
					  "racetrackCoilPhys",
					  racetrackCoilLog,
					  magneticFieldVolumePhys,
					  false,i,true);    

    bandageBoxShortPhys = new G4PVPlacement(boxRot,boxTras,
					    "bandageBoxShortPhys",
					    bandageBoxShortLog,
					    magneticFieldVolumePhys,
					    false,i,true);    

    bandageBoxLongPhys = new G4PVPlacement(boxRot,boxTras,
					    "bandageBoxLongPhys",
					    bandageBoxLongLog,
					    magneticFieldVolumePhys,
					    false,i,true);    
  }


  racetrackCableBoxBarrelExternal = new G4Box("racetrackCableBoxBarrelExternal",
					     cableBarrelWidth/2.,
					     cableBarrelHeight/2.,
					     cableBarrelLength/2.);

  racetrackCableBoxBarrelInternal = new G4Box("racetrackCableBoxBarrelInternal",
					     cableBarrelWidth/2.+10*mm,
					     (cableBarrelHeight-cableThickness*2)/2.,
					     cableBarrelLength/2.+10*mm);

  racetrackCableBoxBarrel = new G4SubtractionSolid("racetrackCableBoxBarrel",
						  racetrackCableBoxBarrelExternal,
						  racetrackCableBoxBarrelInternal);
  
  racetrackCableBoxEndcapTubs = new G4Tubs("racetrackCableBoxEndcapTubs",
					  cableEndcapRadius-cableThickness,
					  cableEndcapRadius,
					  cableBarrelWidth/2.,0,pi/2.);
  
  racetrackCableBoxEndcapBox = new G4Box("racetrackCableBoxEndcapBox",
					cableBarrelHeight/2.-cableEndcapRadius,
					cableThickness/2.,
					cableBarrelWidth/2);

  transEP = G4ThreeVector(cableBarrelHeight/2.-cableEndcapRadius,-(cableEndcapRadius-cableThickness/2.),0);
  transEM = G4ThreeVector(-(cableBarrelHeight/2.-cableEndcapRadius),-(cableEndcapRadius-cableThickness/2.),0);

  racetrackCableBoxEndcap = new G4UnionSolid("racetrackCableBoxEndcap",
					    new G4UnionSolid("partialracetrackCableBoxEndcap",
							     racetrackCableBoxEndcapBox,
							     racetrackCableBoxEndcapTubs,
							     rotEP,transEP),
					    racetrackCableBoxEndcapTubs,rotEM,transEM);
  
  transCM = G4ThreeVector(0,0,-(cableBarrelLength/2.+cableEndcapRadius-cableThickness/2.));
    
  transCP = G4ThreeVector(0,0,cableBarrelLength/2.+cableEndcapRadius-cableThickness/2.);
    
  racetrackCable = new G4UnionSolid("racetrackCable",
				   new G4UnionSolid("partilaracetrackCable",
						    racetrackCableBoxBarrel,
						    racetrackCableBoxEndcap,
						    rotCP,transCP),
				   racetrackCableBoxEndcap,rotCM,transCM);
  
  
  racetrackCableLog = new G4LogicalVolume(racetrackCable,
					 cable,
					 "racetrackCableLog",
					 0,0,0);
  
  racetrackCablePhys = new G4PVPlacement(0,
					 G4ThreeVector(0.,0.,0.),
					 "racetrackCablePhys",
					 racetrackCableLog,
					 racetrackCoilPhys,
					 false,0,true);
  
  
  G4VisAttributes* layerVisAttMagenta = new G4VisAttributes(G4Colour::Magenta());
  layerVisAttMagenta->SetVisibility(true);
  //layerVisAttMagenta->SetForceAuxEdgeVisible(true);
  layerVisAttMagenta->SetForceSolid(true);
  G4VisAttributes* layerVisAttGreen = new G4VisAttributes(G4Colour::Green());
  layerVisAttGreen->SetVisibility(true);
  //layerVisAttGreen->SetForceAuxEdgeVisible(true);
  layerVisAttGreen->SetForceSolid(true);
  G4VisAttributes* layerVisAttBlue = new G4VisAttributes(G4Colour::Blue());
  layerVisAttBlue->SetVisibility(true);
  //layerVisAttBlue->SetForceAuxEdgeVisible(true);
  layerVisAttBlue->SetForceSolid(true);
  G4VisAttributes* layerVisAttYellow = new G4VisAttributes(G4Colour::Yellow());
  layerVisAttYellow->SetVisibility(true);
  //layerVisAttYellow->SetForceAuxEdgeVisible(true);
  layerVisAttYellow->SetForceSolid(true);
  G4VisAttributes* layerVisAttRed = new G4VisAttributes(G4Colour::Red());
  layerVisAttRed->SetVisibility(true);
  //layerVisAttRed->SetForceAuxEdgeVisible(true);
  layerVisAttRed->SetForceSolid(true);
  G4VisAttributes* layerVisAttGray = new G4VisAttributes(G4Colour::Gray());
  layerVisAttGray->SetVisibility(true);
  //layerVisAttGray->SetForceAuxEdgeVisible(true);
  layerVisAttGray->SetForceSolid(true);
  G4VisAttributes* layerVisAttCyan = new G4VisAttributes(G4Colour::Cyan());
  layerVisAttCyan->SetVisibility(true);
  //layerVisAttCyan->SetForceAuxEdgeVisible(true);
  layerVisAttCyan->SetForceSolid(true);
  G4VisAttributes* layerVisAttInvisible = new G4VisAttributes();
  layerVisAttInvisible->SetVisibility(false);
  //layerVisAttInvisible->SetForceAuxEdgeVisible(false);
  layerVisAttInvisible->SetForceSolid(false);

  racetrackCoilLog->SetVisAttributes(layerVisAttYellow);
  racetrackCableLog->SetVisAttributes(layerVisAttRed);
  bandageBoxLongLog->SetVisAttributes(layerVisAttCyan);
  bandageBoxShortLog->SetVisAttributes(layerVisAttCyan);
  magneticFieldVolumeLog->SetVisAttributes(layerVisAttInvisible);
  racetrackSupportStructureLog->SetVisAttributes(layerVisAttGray); 

  G4double coilSuppMass = ((racetrackCoilLog->GetSolid()->GetCubicVolume())-(racetrackCableLog->GetSolid()->GetCubicVolume()))*(racetrackCoilLog->GetMaterial()->GetDensity())/kg;
  G4double cableMass = (racetrackCableLog->GetSolid()->GetCubicVolume())*(racetrackCableLog->GetMaterial()->GetDensity())/kg;
  G4double supportMass = (racetrackSupportStructureLog->GetSolid()->GetCubicVolume())*(racetrackSupportStructureLog->GetMaterial()->GetDensity())/kg;
  G4double bandageMass = ((bandageBoxLongLog->GetSolid()->GetCubicVolume())+(bandageBoxShortLog->GetSolid()->GetCubicVolume()))*(bandageBoxLongLog->GetMaterial()->GetDensity())/kg;

  std::cout<<"Mass of Geometry"<<std::endl;
  std::cout<<"Single Cable Mass = "<<cableMass/1000<<" Tons; Total Mass = "<<cableMass*racetrackCoilNumber/1000<<" Tons;"<<std::endl;
  std::cout<<"Single CoilSupport Mass = "<<coilSuppMass/1000<<" Tons; Total Mass = "<<coilSuppMass*racetrackCoilNumber/1000<<" Tons;"<<std::endl;
  std::cout<<"Single RaceTrack Mass = "<<(cableMass+coilSuppMass)/1000<<" Tons; Total Mass = "<<(cableMass+coilSuppMass)*racetrackCoilNumber/1000<<" Tons;"<<std::endl;
  std::cout<<"Single Bandage Mass = "<<bandageMass/1000<<" Tons; Total Mass = "<<bandageMass*racetrackCoilNumber/1000<<" Tons;"<<std::endl;
  std::cout<<"Support structure Mass = "<<supportMass/1000<<" Tons;"<<std::endl;
  std::cout<<"Total Shield Mass = "<<((cableMass+coilSuppMass+bandageMass)*racetrackCoilNumber+supportMass)/1000<<" Tons;"<<std::endl;
}

void SR2SToroidalMagneticShieldBandage::SetFieldManager(G4FieldManager* fieldManager)
{
  magneticFieldVolumeLog->SetFieldManager(fieldManager,true);
}
 
