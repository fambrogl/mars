#include "MarsSolenoidalMagneticField.hh"
 #include "globals.hh"
#include "geomdefs.hh"
#include "time.h"
#include "G4ios.hh"
#include "fstream"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"

////////////////////////////////////////////////////////////////////////////////
//
MarsSolenoidalMagneticField::MarsSolenoidalMagneticField():SolenoidalField(0)
{ 
  SolenoidalBx = 0;
  SolenoidalBy = 0;
  SolenoidalBz = 0;
}
 
////////////////////////////////////////////////////////////////////////
//
MarsSolenoidalMagneticField::~MarsSolenoidalMagneticField()
{ 
}
///////////////////////////////////////////////////////////////////////
//
void MarsSolenoidalMagneticField::GetFieldValue( const G4double yTrack[7], G4double B[3]) const 
{ 
  G4ThreeVector position = G4ThreeVector(yTrack[0],yTrack[1],yTrack[2]);
  G4ThreeVector BField = GetFieldValue(position); 
  B[0]=BField.x();
  B[1]=BField.y();
  B[2]=BField.z();
 
  return;
 
}
/////////////////////////////////////////////////////////////////////////////////
//
G4ThreeVector MarsSolenoidalMagneticField::GetFieldValue(const G4ThreeVector) const 
{ 
  return SolenoidalField;
}


////////////////////////////////////////////////////////////////////////////////
//
void MarsSolenoidalMagneticField::SetMagneticFieldParameter(const G4String name_file)
{
  std::fstream File_Input(name_file,std::ios::in);
  G4String unit;
  G4double Xfield,Yfield,Zfield;
  while (!File_Input.eof()){
    File_Input>>Xfield>>Yfield>>Zfield>>unit;
  }
  File_Input.close(); 
  SolenoidalBx = Xfield*G4UnitDefinition::GetValueOf(unit);
  SolenoidalBy = Yfield*G4UnitDefinition::GetValueOf(unit);
  SolenoidalBz = Zfield*G4UnitDefinition::GetValueOf(unit);
  SolenoidalField = G4ThreeVector(SolenoidalBx,SolenoidalBy,SolenoidalBz);
  return;
}
