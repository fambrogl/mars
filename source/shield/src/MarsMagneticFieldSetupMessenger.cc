#include "MarsMagneticFieldSetup.hh"
#include "MarsMagneticFieldSetupMessenger.hh"
#include "G4UImessenger.hh"
#include "G4UIdirectory.hh"
#include "G4UIcommand.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWith3Vector.hh"
#include "G4UIcmdWith3VectorAndUnit.hh"
#include "G4UIcmdWithABool.hh"

//units
#include "G4UnitsTable.hh"

MarsMagneticFieldSetupMessenger::MarsMagneticFieldSetupMessenger (MarsMagneticFieldSetup* aField )
{ 
  theMotherField = aField;

  G4String candidates;
  G4String guidance;
  G4String cmd_name;
  
  //directories
  ///////////////
 
  IntegrationDir= new G4UIdirectory("/bfieldintegration/");
  IntegrationDir->SetGuidance("Numerical integration control");
  
  MagnetoDir = new G4UIdirectory("/bfieldmodels/");
  MagnetoDir->SetGuidance("Magnetic field control"); 
   
  // Integration control commands
  /////////////////////////////////

  cmd_name = "/bfieldintegration/SetPrecision";
  SetEpsilonCmd = new G4UIcmdWithADouble(cmd_name,this);
  SetEpsilonCmd->SetGuidance("Set the relative precision for integration min and max value");
  SetEpsilonCmd->SetParameterName("precision",false);
  SetEpsilonCmd->SetRange("precision <= 1.e-6  && precision >=1.e-12");
  SetEpsilonCmd->AvailableForStates(G4State_PreInit,G4State_Idle); 
  
  cmd_name = "/bfieldintegration/SetDeltaChordStep";
  SetDeltaChordCmd = new G4UIcmdWithADoubleAndUnit(cmd_name,this);
  SetDeltaChordCmd->SetGuidance("Set the maximal integrating step allowed for G4 integration");
  SetDeltaChordCmd->SetParameterName("DeltaChord",false);
  SetDeltaChordCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  SetDeltaChordCmd->SetUnitCategory("Length");
  
  cmd_name = "/bfieldintegration/SetDeltaIntersection"; 
  SetDeltaIntersectionCmd = new G4UIcmdWithADoubleAndUnit(cmd_name,this);
  SetDeltaIntersectionCmd->SetGuidance("Set the precision for crossing boundary");
  SetDeltaIntersectionCmd->SetParameterName("DeltaIntersection",false);
  SetDeltaIntersectionCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  SetDeltaIntersectionCmd->SetUnitCategory("Length");
  
  cmd_name = "/bfieldintegration/SetDefaultIntegrationParameters";
  ResetIntegrationParametersCmd=  new G4UIcmdWithoutParameter(cmd_name,this);
  ResetIntegrationParametersCmd->SetGuidance("Set the integration parameters to their default values");
  ResetIntegrationParametersCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  cmd_name = "/bfieldintegration/SetStepperModel";
  SetStepperCmd = new G4UIcmdWithAString(cmd_name,this);
  SetStepperCmd->SetGuidance("Set the stepper model for the G4Integration method ");
  SetStepperCmd->SetParameterName("choice",true);
  SetStepperCmd->SetDefaultValue("ClassicalRK4");
  SetStepperCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  candidates = "ExplicitEuler ImplicitEuler SimpleRunge ClassicalRK4 ";
  candidates += "CashKarpRKF45 HelixSimpleRunge HelixImplicitEuler HelixHeum";
  SetStepperCmd->SetCandidates(candidates);  
  
  // magnetic field model commands
  /////////////////////////////////////
  
  cmd_name = "/bfieldmodels/SetMagneticFieldModel";
  SetMagneticFieldModelCmd = new G4UIcmdWithAString(cmd_name,this);
  guidance =  "Set the model for the magnetic field and the path to the grid file";
  guidance += "Available models are: ";
  std::vector<G4String> ListOfModels = theMotherField->GetListOfFieldModels();
  for (unsigned int i=0;i<ListOfModels.size();i++){
    guidance+=" "+ListOfModels[i];
  }
  SetMagneticFieldModelCmd->SetGuidance(guidance);
  SetMagneticFieldModelCmd->SetParameterName("FieldModel",false);
  SetMagneticFieldModelCmd->SetDefaultValue("SOLENOIDAL");  
  SetMagneticFieldModelCmd->AvailableForStates(G4State_PreInit,G4State_Idle); 

  cmd_name = "/bfieldmodels/SetMagneticFieldParameter";
  SetMagneticFieldParameterCmd = new G4UIcmdWithAString(cmd_name,this);
  guidance =  "Set the the path to the parametr file for BField";
  SetMagneticFieldParameterCmd->SetGuidance(guidance);
  SetMagneticFieldParameterCmd->SetParameterName("ParameterFile",false);
  SetMagneticFieldParameterCmd->AvailableForStates(G4State_PreInit,G4State_Idle); 
    
  cmd_name = "/bfieldmodels/SwitchOn";
  SwitchOnFieldCmd= new G4UIcmdWithoutParameter(cmd_name,this);
  SwitchOnFieldCmd->SetGuidance("Activate the effect of the magnetic field");
  SwitchOnFieldCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  cmd_name = "/bfieldmodels/SwitchOff";
  SwitchOffFieldCmd= new G4UIcmdWithoutParameter(cmd_name,this);
  SwitchOffFieldCmd->SetGuidance("Desactivate the effect of the magnetic field");
  SwitchOffFieldCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
    
}
////////////////////////////////////////////////////////////////////////////////
//
MarsMagneticFieldSetupMessenger::~MarsMagneticFieldSetupMessenger()
{ 
  delete SetEpsilonCmd;
  delete SetDeltaChordCmd; 
  delete SetDeltaIntersectionCmd;
  delete ResetIntegrationParametersCmd;
  delete SetStepperCmd;
  delete SetMagneticFieldModelCmd;
  delete SetMagneticFieldParameterCmd;
  delete SwitchOnFieldCmd; 
  delete SwitchOffFieldCmd;
  delete mainDir;
  delete IntegrationDir;
  delete MagnetoDir;
}		  
////////////////////////////////////////////////////////////////////////////////
//
void MarsMagneticFieldSetupMessenger::SetNewValue(G4UIcommand * command,G4String newValues)
{  
  
  // integration parameters command 
  if (command == SetEpsilonCmd){ 
    const char* paramString=newValues;
    G4double EpsMin,EpsMax;
    std::istringstream is((char*)paramString);
    is >>EpsMin >> EpsMax;
    theMotherField->SetEpsilon(EpsMin,EpsMax);
  }else if (command == SetDeltaChordCmd){ 
    theMotherField->SetDeltaChord(SetDeltaChordCmd->GetNewDoubleValue(newValues));   
  }else if (command == SetDeltaIntersectionCmd) {
    theMotherField->SetDeltaIntersection(SetDeltaIntersectionCmd->GetNewDoubleValue(newValues));   
  }else if (command == ResetIntegrationParametersCmd){
    theMotherField->ResetIntegrationParameters();  
  }else if ( command == SetStepperCmd ){
    theMotherField->SetStepper(newValues);
  }else if ( command == SetMagneticFieldModelCmd ){
    theMotherField->SetMagneticField(newValues);
  }else if ( command == SetMagneticFieldParameterCmd ){
    theMotherField->SetMagneticFieldParameter(newValues);
  }else if( command ==  SwitchOnFieldCmd){
    theMotherField->SwitchOn(); 
  }else if( command ==  SwitchOffFieldCmd){ 
    theMotherField->SwitchOff();  
  }
}












