#include "MarsMapMagneticField.hh"
#include "globals.hh"
#include "geomdefs.hh"
#include "time.h"
#include "G4ios.hh"
#include "fstream"
#include "G4UnitsTable.hh"
#include "TFile.h"
#include "TH3.h"

////////////////////////////////////////////////////////////////////////////////
//
MarsMapMagneticField::MarsMapMagneticField()
{ 
  Xmin=0;
  Xmax=0;
  Ymin=0;
  Ymax=0;
  Zmin=0;
  Zmax=0;
}

////////////////////////////////////////////////////////////////////////
//
MarsMapMagneticField::~MarsMapMagneticField()
{ 
  fileMap->Close(); 
}
///////////////////////////////////////////////////////////////////////
//
void MarsMapMagneticField::GetFieldValue( const G4double yTrack[7], G4double B[3]) const 
{
  G4double Bx = 0;
  G4double By = 0;
  G4double Bz = 0;
  
  G4double posX = yTrack[0]/m;
  G4double posY = yTrack[1]/m;
  G4double posZ = yTrack[2]/m;
  
  if((posX<Xmin&&posX>Xmax)||(posY<Ymin&&posY>Ymax)||(posZ<Zmin&&posZ>Zmax)){
    G4cout<<"The position is out of the region where the BField map is defined then the BField is set to 0"<<G4endl; 
    Bx = 0;
    By = 0;
    Bz = 0;
  }else{
    Bx= histoBxComponent->Interpolate(posX,posY,posZ);
    By= histoByComponent->Interpolate(posX,posY,posZ);
    Bz= histoBzComponent->Interpolate(posX,posY,posZ);
  }
  
  B[0]=Bx*G4UnitDefinition::GetValueOf("tesla");
  B[1]=By*G4UnitDefinition::GetValueOf("tesla");
  B[2]=Bz*G4UnitDefinition::GetValueOf("tesla");

  return;
}
/////////////////////////////////////////////////////////////////////////////////
//
G4ThreeVector MarsMapMagneticField::GetFieldValue(const G4ThreeVector) const 
{
  return G4ThreeVector(0,0,0);
}


////////////////////////////////////////////////////////////////////////////////
//
void MarsMapMagneticField::SetMagneticFieldParameter(const G4String name_file)
{
  fileMap = new TFile(name_file);

  histoBxComponent = (TH3F*)fileMap->Get("BFieldMapHistoBx");
  histoByComponent = (TH3F*)fileMap->Get("BFieldMapHistoBx");
  histoBzComponent = (TH3F*)fileMap->Get("BFieldMapHistoBx");

  Xmin = histoBxComponent->GetXaxis()->GetXmin();
  Xmax = histoBxComponent->GetXaxis()->GetXmax();
  Ymin = histoBxComponent->GetYaxis()->GetXmin();
  Ymax = histoBxComponent->GetYaxis()->GetXmax();
  Zmin = histoBxComponent->GetZaxis()->GetXmin();
  Zmax = histoBxComponent->GetZaxis()->GetXmax();
}
