//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// Code developed by:
//  S.Guatelli
//
//    *********************************
//    *                               *
//    *    SR2SToroidalMagneticShieldMessenger.cc *
//    *                               *
//    *********************************
//
//
// $Id$
//
// 

#include "SR2SToroidalMagneticShieldMessenger.hh"
#include "SR2SToroidalMagneticShield.hh"
#include "G4UIdirectory.hh"
#include "G4UIparameter.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcommand.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UnitsTable.hh"

SR2SToroidalMagneticShieldMessenger::SR2SToroidalMagneticShieldMessenger( SR2SToroidalMagneticShield* sr2s): sr2sShield(sr2s)
{ 
  sr2sShieldDir = new G4UIdirectory("/SR2SShield/");
  sr2sShieldDir->SetGuidance("SR2S Shield control.");
   
  sr2sMagneticShieldStructureCmd = new G4UIcommand("/SR2SShield/ToroidalShieldStructure",this);
  sr2sMagneticShieldStructureCmd->SetGuidance("[usage] /SR2SShield/ToroidalShieldStructure BarrelLength unit RTSuppIR unit RTSuppThick unit RTCoilNumber RTCoilIR unit RTCoilThick unit RTCoilLength unit RTCoilTiPThick unit TRodsNumber TRodsR unit mMeteoritesPThick unit");
  sr2sMagneticShieldStructureCmd->AvailableForStates(G4State_Idle); 
  param = new G4UIparameter("BarrelLength",'d',false);
  sr2sMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("length_unit",'s',false);
  sr2sMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("RTSuppIR",'d',false);
  sr2sMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("length_unit",'s',false);
  sr2sMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("RTSuppThick",'d',false);
  sr2sMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("length_unit",'s',false);
  sr2sMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("RTCoilNumber",'i',false);
  sr2sMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("RTCoilIR",'d',false);
  sr2sMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("length_unit",'s',false);
  sr2sMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("RTCoilThick",'d',false);
  sr2sMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("length_unit",'s',false);
  sr2sMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("RTCoilLength",'d',false);
  sr2sMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("length_unit",'s',false);
  sr2sMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("RTCoilTiPThick",'d',false);
  sr2sMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("length_unit",'s',false);
  sr2sMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("TRodsNumber",'i',false);
  sr2sMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("TRodsR",'d',false);
  sr2sMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("length_unit",'s',false);
  sr2sMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("mMeteoritesPThick",'d',false);
  sr2sMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("length_unit",'s',false);
  sr2sMagneticShieldStructureCmd->SetParameter(param);
  param = new G4UIparameter("material",'s',false);
  sr2sMagneticShieldStructureCmd->SetParameter(param);
  
}

SR2SToroidalMagneticShieldMessenger::~SR2SToroidalMagneticShieldMessenger()
{
  delete sr2sShield;
  delete sr2sShieldDir; 
  delete sr2sMagneticShieldStructureCmd;
}

void SR2SToroidalMagneticShieldMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 
  if(command == sr2sMagneticShieldStructureCmd){
    const char* paramString;
    paramString=newValue;
    G4double aBarrelHalfLength,rtSuppIR,rtSuppThick,rtcInnerRadius,rtcThick,rtcLength,rtcTiPThick,tieRadius,mmpThick;
    G4int rtcNumber,tieNumber;
    G4String unit1,unit2,unit3,unit4,unit5,unit6,unit7,unit8,unit9,material;
    G4bool existMaterial = true;
    std::istringstream is((char*)paramString);
    is >> aBarrelHalfLength >> unit1
       >> rtSuppIR >> unit2 >> rtSuppThick >> unit3
       >> rtcNumber >> rtcInnerRadius >> unit4 >> rtcThick >> unit5 >> rtcLength >> unit6
       >> rtcTiPThick >> unit7
       >> tieNumber >> tieRadius >> unit8
       >> mmpThick >> unit9 >> material;
    aBarrelHalfLength *=G4UnitDefinition::GetValueOf(unit1);
    rtSuppIR*=G4UnitDefinition::GetValueOf(unit2);
    rtSuppThick*=G4UnitDefinition::GetValueOf(unit3);
    rtcInnerRadius*=G4UnitDefinition::GetValueOf(unit4);
    rtcThick*=G4UnitDefinition::GetValueOf(unit5);
    rtcLength*=G4UnitDefinition::GetValueOf(unit6);
    rtcTiPThick*=G4UnitDefinition::GetValueOf(unit7);
    tieRadius*=G4UnitDefinition::GetValueOf(unit8);
    mmpThick*=G4UnitDefinition::GetValueOf(unit9);

    if(material=="materialOff")
      existMaterial=false;
    else if(material=="materialOn")
      existMaterial=true;

    sr2sShield->SetSR2SShieldStructureParameter(aBarrelHalfLength,rtSuppIR,rtSuppThick,rtcNumber,rtcInnerRadius,rtcThick,rtcLength,rtcTiPThick,tieNumber,tieRadius,mmpThick,existMaterial);
  }
}
 
 
