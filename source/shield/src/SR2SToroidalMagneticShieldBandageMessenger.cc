//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// Code developed by:
//  S.Guatelli
//
//    *********************************
//    *                               *
//    *    SR2SToroidalMagneticShieldBandageMessenger.cc *
//    *                               *
//    *********************************
//
//
// $Id$
//
// 

#include "SR2SToroidalMagneticShieldBandageMessenger.hh"
#include "SR2SToroidalMagneticShieldBandage.hh"
#include "G4UIdirectory.hh"
#include "G4UIparameter.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcommand.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UnitsTable.hh"

SR2SToroidalMagneticShieldBandageMessenger::SR2SToroidalMagneticShieldBandageMessenger( SR2SToroidalMagneticShieldBandage* sr2s): sr2sShield(sr2s)
{ 
  sr2sShieldDir = new G4UIdirectory("/SR2SShieldBandage/");
  sr2sShieldDir->SetGuidance("SR2S Shield control.");
   
  sr2sSupportStructureCmd = new G4UIcommand("/SR2SShieldBandage/SupportStructure",this);
  sr2sSupportStructureCmd->SetGuidance("[usage] /SR2SShieldBandage/SupportStructure Radius Thicknes Length unit Material");
  sr2sSupportStructureCmd->AvailableForStates(G4State_Idle); 
  param = new G4UIparameter("Radius",'d',false);
  sr2sSupportStructureCmd->SetParameter(param);
  param = new G4UIparameter("Thickness",'d',false);
  sr2sSupportStructureCmd->SetParameter(param);
  param = new G4UIparameter("Length",'d',false);
  sr2sSupportStructureCmd->SetParameter(param);
  param = new G4UIparameter("length_unit",'s',false);
  sr2sSupportStructureCmd->SetParameter(param);
  param = new G4UIparameter("Material",'s',false);
  sr2sSupportStructureCmd->SetParameter(param);
    
  sr2sCoilStructureCmd = new G4UIcommand("/SR2SShieldBandage/CoilStructure",this);
  sr2sCoilStructureCmd->SetGuidance("[usage] /SR2SShieldBandage/CoilStructure RadialDistance Length Width Thickness Heigth EndcapRadius EnvelopeThickness unit CoilNumber CoilMaterial CableMaterial");
  sr2sCoilStructureCmd->AvailableForStates(G4State_Idle); 
  param = new G4UIparameter("RadialDistance",'d',false);
  sr2sCoilStructureCmd->SetParameter(param);
  param = new G4UIparameter("Length",'d',false);
  sr2sCoilStructureCmd->SetParameter(param);
  param = new G4UIparameter("Width",'d',false);
  sr2sCoilStructureCmd->SetParameter(param);
  param = new G4UIparameter("Thickness",'d',false);
  sr2sCoilStructureCmd->SetParameter(param);
  param = new G4UIparameter("Height",'d',false);
  sr2sCoilStructureCmd->SetParameter(param);
  param = new G4UIparameter("EndcapRadius",'d',false);
  sr2sCoilStructureCmd->SetParameter(param);
  param = new G4UIparameter("EnvelopeThockness",'d',false);
  sr2sCoilStructureCmd->SetParameter(param);
  param = new G4UIparameter("length_unit",'s',false);
  sr2sCoilStructureCmd->SetParameter(param);
  param = new G4UIparameter("CoilNumber",'i',false);
  sr2sCoilStructureCmd->SetParameter(param);
  param = new G4UIparameter("CoilMaterial",'s',false);
  sr2sCoilStructureCmd->SetParameter(param);
  param = new G4UIparameter("CableMaterial",'s',false);
  sr2sCoilStructureCmd->SetParameter(param);
  
  sr2sBandageStructureCmd = new G4UIcommand("/SR2SShieldBandage/BandageStructure",this);
  sr2sBandageStructureCmd->SetGuidance("[usage] /SR2SShieldBandage/BandageStructure Thickness ShortLength ShortHeight ShortWidth LongLength LongHeight LongWidth  unit Material");
  sr2sBandageStructureCmd->AvailableForStates(G4State_Idle); 
  param = new G4UIparameter("Thickness",'d',false);
  sr2sBandageStructureCmd->SetParameter(param);
  param = new G4UIparameter("ShortLength",'d',false);
  sr2sBandageStructureCmd->SetParameter(param);
  param = new G4UIparameter("ShortHeight",'d',false);
  sr2sBandageStructureCmd->SetParameter(param);
  param = new G4UIparameter("ShortWidth",'d',false);
  sr2sBandageStructureCmd->SetParameter(param);
  param = new G4UIparameter("LongLength",'d',false);
  sr2sBandageStructureCmd->SetParameter(param);
  param = new G4UIparameter("LongHeight",'d',false);
  sr2sBandageStructureCmd->SetParameter(param);
  param = new G4UIparameter("LongWidth",'d',false);
  sr2sBandageStructureCmd->SetParameter(param);
  param = new G4UIparameter("length_unit",'s',false);
  sr2sBandageStructureCmd->SetParameter(param);
  param = new G4UIparameter("Material",'s',false);
  sr2sBandageStructureCmd->SetParameter(param);
  
  sr2sNoMaterialCmd = new G4UIcmdWithoutParameter("/SR2SShieldBandage/NoMaterial",this);
  sr2sNoMaterialCmd->SetGuidance("Switched off all teh materila used for the shield");
  sr2sNoMaterialCmd->AvailableForStates(G4State_Idle); 
}

SR2SToroidalMagneticShieldBandageMessenger::~SR2SToroidalMagneticShieldBandageMessenger()
{
  delete sr2sShield;
  delete sr2sShieldDir; 
  delete sr2sCoilStructureCmd;
  delete sr2sSupportStructureCmd;
  delete sr2sBandageStructureCmd;
  delete sr2sNoMaterialCmd;
}

void SR2SToroidalMagneticShieldBandageMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 
  if(command == sr2sCoilStructureCmd){
    const char* paramString;
    paramString=newValue;
    G4double RadialDistance,Length,Width,Thickness,Heigth,EndcapRadius,EnvelopeThickness;
    G4int CoilNumber;
    G4String unit,materialcoil,materialcable;
    std::istringstream is((char*)paramString);
    is>>RadialDistance>>Length>>Width>>Thickness>>Heigth>>EndcapRadius>>EnvelopeThickness>>unit>>CoilNumber>>materialcoil>>materialcable;
    RadialDistance*=G4UnitDefinition::GetValueOf(unit);
    Length*=G4UnitDefinition::GetValueOf(unit);
    Width*=G4UnitDefinition::GetValueOf(unit);
    Thickness*=G4UnitDefinition::GetValueOf(unit);
    Heigth*=G4UnitDefinition::GetValueOf(unit);
    EndcapRadius*=G4UnitDefinition::GetValueOf(unit);
    EnvelopeThickness*=G4UnitDefinition::GetValueOf(unit);

    sr2sShield->SetSR2SCoilStructureParameter(RadialDistance,Length,Width,Thickness,Heigth,EndcapRadius,EnvelopeThickness,CoilNumber,materialcoil,materialcable);
  }else if(command == sr2sSupportStructureCmd){
    const char* paramString;
    paramString=newValue;
    G4double Radius,Thickness,Length; 
    G4String unit,material;
    std::istringstream is((char*)paramString);
    is>>Radius>>Thickness>>Length>>unit>>material;
    Radius*=G4UnitDefinition::GetValueOf(unit);
    Thickness*=G4UnitDefinition::GetValueOf(unit);
    Length*=G4UnitDefinition::GetValueOf(unit);
    
    sr2sShield->SetSR2SSupportStructureParameter(Radius,Thickness,Length,material);
}else if(command == sr2sBandageStructureCmd){
    const char* paramString;
    paramString=newValue;
    G4double Thickness,ShortLength,ShortHeight,ShortWidth,LongLength,LongHeight,LongWidth;
    G4String unit,material;
    std::istringstream is((char*)paramString);
    is>>Thickness>>ShortLength>>ShortHeight>>ShortWidth>>LongLength>>LongHeight>>LongWidth>>unit>>material;
    Thickness*=G4UnitDefinition::GetValueOf(unit);
    ShortLength*=G4UnitDefinition::GetValueOf(unit);
    ShortHeight*=G4UnitDefinition::GetValueOf(unit);
    ShortWidth*=G4UnitDefinition::GetValueOf(unit);
    LongLength*=G4UnitDefinition::GetValueOf(unit);
    LongHeight*=G4UnitDefinition::GetValueOf(unit);
    LongWidth*=G4UnitDefinition::GetValueOf(unit);

    sr2sShield->SetSR2SBandageStructureParameter(Thickness,ShortLength,ShortHeight,ShortWidth,LongLength,LongHeight,LongWidth,material);
  }else if(command == sr2sNoMaterialCmd)
    sr2sShield->SetSR2SNoMaterial();
}

