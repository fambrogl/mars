//
 // ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id$
//
// Code developed by: S.Guatelli, guatelli@ge.infn.it
//
#ifndef SR2SToroidalMagneticShieldBandage_h
#define SR2SToroidalMagneticShieldBandage_h 1
#include "globals.hh"
#include "MarsMagneticShield.hh"
#include <vector>
#include "G4FieldManager.hh"

class G4VPhysicalVolume;
class G4Tubs;
class G4Box;
class G4LogicalVolume;
class G4Material;
class MarsMaterial;
class MarsDecorator;
class G4VPhysicalVolume;
class G4VisAttributes;
class SR2SToroidalMagneticShieldBandageMessenger;
class G4Torus;
class G4UnionSolid;
class G4SubtractionSolid;

class SR2SToroidalMagneticShieldBandage: public MarsMagneticShield
{
public:
  SR2SToroidalMagneticShieldBandage();
  ~SR2SToroidalMagneticShieldBandage();
  void ConstructComponent(G4VPhysicalVolume*);
  void DestroyComponent(); 
  
  void SetSR2SBandageStructureParameter(G4double,G4double,G4double,G4double,G4double,G4double,G4double,G4String);
  void SetSR2SSupportStructureParameter(G4double,G4double,G4double,G4String);
  void SetSR2SCoilStructureParameter(G4double,G4double,G4double,G4double,G4double,G4double,G4double,
				     G4int,G4String,G4String);
  
  inline void SetSR2SNoMaterial(){materialShield=false;}

  void SetFieldManager(G4FieldManager* fieldManager);
  
private:
  MarsMaterial* pMaterial;
  SR2SToroidalMagneticShieldBandageMessenger* messenger;
  
  G4double racetrackSupportStructureMiddlePosition;
  G4double racetrackSupportStructureThickness;
  G4double racetrackSupportStructureInnerRadius;
  G4double racetrackSupportStructureOuterRadius;
  G4double racetrackSupportStructureLength;
  
  G4double bandageThickness;
  G4double shortBandageLength;
  G4double shortBandageHeight;
  G4double shortBandageWidth;
  G4double longBandageLength;
  G4double longBandageHeight;
  G4double longBandageWidth;

  G4double coilDistanceFromCenter;
  G4double coilBarrelLength;    
  G4double coilBarrelWidth;     
  G4double coilThickness;       
  G4double coilBarrelHeight;    
  G4double coilEndcapRadius;
  G4double coilSupportThickness;

  G4double cableBarrelLength;    
  G4double cableBarrelWidth;     
  G4double cableThickness;       
  G4double cableBarrelHeight;    
  G4double cableEndcapRadius;


  G4int racetrackCoilNumber; 
  G4double coilAngle;           
  
  G4String coilMaterial;
  G4String coreCableMaterial;
  G4String bandageMaterial;
  G4String SupportStructureMaterial;

  G4bool materialShield;


  G4Tubs* racetrackSupportStructure;
  G4Tubs*  magneticFieldVolume;
  G4Box* racetrackCoilBoxBarrelExternal;
  G4Box* racetrackCoilBoxBarrelInternal;
  G4SubtractionSolid* racetrackCoilBoxBarrel;
  G4Tubs* racetrackCoilBoxEndcapTubs;
  G4Box* racetrackCoilBoxEndcapBox;
  G4UnionSolid* racetrackCoilBoxEndcap;
  G4UnionSolid* racetrackCoil;

  G4Box* racetrackCableBoxBarrelExternal;
  G4Box* racetrackCableBoxBarrelInternal;
  G4SubtractionSolid* racetrackCableBoxBarrel;
  G4Tubs* racetrackCableBoxEndcapTubs;
  G4Box* racetrackCableBoxEndcapBox;
  G4UnionSolid* racetrackCableBoxEndcap;
  G4UnionSolid* racetrackCable;

  G4Box* bandageBoxLongExternal;
  G4Box* bandageBoxLongInternal;
  G4SubtractionSolid* bandageBoxLong;
  G4Box* bandageBoxShortExternal;
  G4Box* bandageBoxShortInternal;
  G4SubtractionSolid* bandageBoxShort;

  G4LogicalVolume* racetrackSupportStructureLog;
  G4LogicalVolume* magneticFieldVolumeLog;
  G4LogicalVolume* racetrackCoilLog;
  G4LogicalVolume* racetrackCableLog;
  G4LogicalVolume* bandageBoxLongLog;
  G4LogicalVolume* bandageBoxShortLog;


  G4VPhysicalVolume* racetrackSupportStructurePhys;
  G4VPhysicalVolume* magneticFieldVolumePhys;
  G4VPhysicalVolume* racetrackCoilPhys;
  G4VPhysicalVolume* racetrackCablePhys;
  G4VPhysicalVolume* bandageBoxLongPhys;
  G4VPhysicalVolume* bandageBoxShortPhys;
};
#endif
