//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id$
//
// Code developed by: S.Guatelli, guatelli@ge.infn.it
//
#ifndef NIACMagneticShield_h
#define NIACMagneticShield_h 1

#include "globals.hh"
#include "MarsMagneticShield.hh"

#include <vector>
#include "G4FieldManager.hh"

class G4VPhysicalVolume;
class G4Tubs;
class G4Box;
class G4LogicalVolume;
class G4Material;
class MarsMaterial;
class MarsDecorator;
class G4VPhysicalVolume;
class G4VisAttributes;
class NIACMagneticShieldMessenger;

class NIACMagneticShield: public MarsMagneticShield
{
public:
  NIACMagneticShield();
  ~NIACMagneticShield();
  void ConstructComponent(G4VPhysicalVolume*);
  void DestroyComponent(); 
  
  void SetNIACShieldStructureParameter(G4double length, G4double compradius,G4double eThick, G4double iThick,G4int number,G4double radius,G4double thick,G4double cradius,G4double cthick,G4double rthick,G4int rnumb,G4bool aMaterial);
  void SetFieldManager(G4FieldManager* fieldManager);
  
private:
  MarsMaterial* pMaterial; 
  NIACMagneticShieldMessenger* messenger;

  G4double compensationSolenoidRadius;
  G4double compensationSolenoidExternalThickness;
  G4double compensationSolenoidInternalThickness;
  G4double halfSolenoidLength;
  G4double solenoidRadius;
  G4double solenoidThickness;
  G4double solenoidCentralSupportRadius;
  G4double solenoidCentralSupportThickness;
  G4double solenoidRadialSupportShortSide;
  G4double solenoidRadialSupportThickness;
  G4double radialDistanceToSpacecraft;
  G4double angleRadialSupport;
  G4double angleSolenoid; 
  G4int nSolenoid;
  G4int nRadialSolenoidSupport;

  G4bool materialShield;
  
  G4Tubs* compensationSolenoidExternal;
  G4Tubs* compensationSolenoidInternal;
  G4Tubs* solenoid;
  G4Tubs* superconductingCoil;
  G4Tubs* centralSolenoidSupport;
  G4Box*  radialSolenoidSupport;

  G4LogicalVolume* compensationSolenoidExternalLog;
  G4LogicalVolume* compensationSolenoidInternalLog;
  G4LogicalVolume* solenoidLog;
  G4LogicalVolume* superconductingCoilLog;
  G4LogicalVolume* centralSolenoidSupportLog;
  G4LogicalVolume* radialSolenoidSupportLog;

  G4VPhysicalVolume* compensationSolenoidExternalPhys;
  G4VPhysicalVolume* compensationSolenoidInternalPhys;
  G4VPhysicalVolume* solenoidPhys;
  G4VPhysicalVolume* superconductingCoilPhys;
  G4VPhysicalVolume* centralSolenoidSupportPhys;
  G4VPhysicalVolume* radialSolenoidSupportPhys;
};
#endif
