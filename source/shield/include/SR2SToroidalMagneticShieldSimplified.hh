//
 // ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id$
//
// Code developed by: S.Guatelli, guatelli@ge.infn.it
//
#ifndef SR2SToroidalMagneticShieldSimplified_h
#define SR2SToroidalMagneticShieldSimplified_h 1
#include "globals.hh"
#include "MarsMagneticShield.hh"
#include <vector>
#include "G4FieldManager.hh"

class G4VPhysicalVolume;
class G4Tubs;
class G4Box;
class G4LogicalVolume;
class G4Material;
class MarsMaterial;
class MarsDecorator;
class G4VPhysicalVolume;
class G4VisAttributes;
class SR2SToroidalMagneticShieldSimplifiedMessenger;
class G4Torus;
class G4UnionSolid;
class G4SubtractionSolid;

class SR2SToroidalMagneticShieldSimplified: public MarsMagneticShield
{
public:
  SR2SToroidalMagneticShieldSimplified();
  ~SR2SToroidalMagneticShieldSimplified();
  void ConstructComponent(G4VPhysicalVolume*);
  void DestroyComponent(); 
  
  void SetSR2STitaniumStructureParameter(G4double,G4double,G4double,G4String);
  void SetSR2SHydrogenStructureParameter(G4double,G4double,G4double,G4String);
  void SetSR2SSupportStructureParameter(G4double,G4double,G4double,G4String);
  void SetSR2SCoilStructureParameter(G4double,G4double,G4double,G4double,G4double,G4double,G4int,G4String);
  
  inline void SetSR2SNoMaterial(){materialShield=false;}

  void SetFieldManager(G4FieldManager* fieldManager);
  
private:
  MarsMaterial* pMaterial;
  SR2SToroidalMagneticShieldSimplifiedMessenger* messenger;
  
  G4double racetrackSupportStructureMiddlePosition;
  G4double racetrackSupportStructureThickness;
  G4double racetrackSupportStructureInnerRadius;
  G4double racetrackSupportStructureOuterRadius;
  G4double racetrackSupportStructureLength;

  G4double racetrackTitaniumStructureMiddlePosition;
  G4double racetrackTitaniumStructureThickness;
  G4double racetrackTitaniumStructureInnerRadius;
  G4double racetrackTitaniumStructureOuterRadius;
  G4double racetrackTitaniumStructureLength;

  G4double racetrackHydrogenStructureMiddlePosition;
  G4double racetrackHydrogenStructureThickness;
  G4double racetrackHydrogenStructureInnerRadius;
  G4double racetrackHydrogenStructureOuterRadius;
  G4double racetrackHydrogenStructureLength;

  G4double coilDistanceFromCenter;
  G4double coilBarrelLength;    
  G4double coilBarrelWidth;     
  G4double coilThickness;       
  G4double coilBarrleHeight;    
  G4double coilEndcapRadius;    
  G4int racetrackCoilNumber; 
  G4double coilAngle;           
  
  G4String coilMaterial;
  G4String Ti_StructureMaterial;
  G4String H_StructureMaterial;
  G4String SupportStructureMaterial;

  G4bool materialShield;


  G4Tubs* racetrackSupportStructure;
  G4Tubs* racetrackTitaniumStructure;
  G4Tubs* racetrackHydrogenStructure;
  G4Tubs*  magneticFieldVolumeBarrel;
  G4Torus* magneticFieldVolumeEndcap;
  G4UnionSolid* magneticFieldVolume;
  G4Box* racetrackCoilBoxBarrelExternal;
  G4Box* racetrackCoilBoxBarrelInternal;
  G4SubtractionSolid* racetrackCoilBoxBarrel;
  G4Tubs* racetrackCoilBoxEndcapTubs;
  G4Box* racetrackCoilBoxEndcapBox;
  G4UnionSolid* racetrackCoilBoxEndcap;
  G4UnionSolid* racetrackCoil;

  G4LogicalVolume* racetrackSupportStructureLog;
  G4LogicalVolume* racetrackTitaniumStructureLog;
  G4LogicalVolume* racetrackHydrogenStructureLog;
  G4LogicalVolume* magneticFieldVolumeLog;
  G4LogicalVolume* racetrackCoilLog;


  G4VPhysicalVolume* racetrackSupportStructurePhys;
  G4VPhysicalVolume* racetrackTitaniumStructurePhys;
  G4VPhysicalVolume* racetrackHydrogenStructurePhys;
  G4VPhysicalVolume* magneticFieldVolumePhys;
  G4VPhysicalVolume* racetrackCoilPhys;
};
#endif
