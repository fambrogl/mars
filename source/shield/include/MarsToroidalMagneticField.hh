#ifndef MarsToroidalMagneticField_h 
#define MarsToroidalMagneticField_h 1 

#include "globals.hh"
#include "G4ios.hh"
#include "G4ThreeVector.hh"
#include "vector"
#include "G4strstreambuf.hh"
#include "MarsMagneticField.hh"

class MarsToroidalMagneticField : public MarsMagneticField
{
public:
  //constructor destructor	       
  MarsToroidalMagneticField();
  ~MarsToroidalMagneticField();
	     
  // Gives the magnetic field B at a given position defined by yTrack
  void GetFieldValue(const G4double yTrack[],G4double B[]) const;
  G4ThreeVector GetFieldValue(const G4ThreeVector position) const; 
  
  void SetMagneticFieldParameter(const G4String name_file);

private:
  G4double BdLParameter;
}; 

#endif
