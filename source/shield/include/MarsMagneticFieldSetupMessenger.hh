#ifndef MarsMagneticFieldSetupMessenger_h
#define MarsMagneticFieldSetupMessenger_h 1

#include "globals.hh"
#include "G4UImessenger.hh"

class MarsMagneticFieldSetup;
class G4UIdirectory;
class G4UIparameter;
class G4UIcommand;
class G4UIcmdWithADouble;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithAnInteger;
class G4UIcmdWithABool;
class G4UIcmdWithAString;
class G4UIcmdWithoutParameter;
class G4UIcmdWith3Vector;
class G4UIcmdWith3VectorAndUnit;

class MarsMagneticFieldSetupMessenger : public G4UImessenger
{public:
  MarsMagneticFieldSetupMessenger(MarsMagneticFieldSetup* aField);
  ~MarsMagneticFieldSetupMessenger();
  
  void SetNewValue(G4UIcommand * command,G4String newValues);
  
protected:
  
  G4UIdirectory*             mainDir;
  G4UIdirectory*             IntegrationDir;
  G4UIdirectory*             MagnetoDir;
  G4UIparameter*             param;
  
  MarsMagneticFieldSetup* theMotherField;

  //integration command
  
  G4UIcmdWithADouble*        SetEpsilonCmd;
  G4UIcmdWithADoubleAndUnit* SetDeltaChordCmd; 
  G4UIcmdWithADoubleAndUnit* SetDeltaIntersectionCmd;
  G4UIcmdWithoutParameter*   ResetIntegrationParametersCmd;
  G4UIcmdWithAString*        SetStepperCmd;
     
  // magnetic field model command
     
  G4UIcmdWithAString*        SetMagneticFieldModelCmd;
  G4UIcmdWithAString*        SetMagneticFieldParameterCmd;
  G4UIcmdWithoutParameter*   SwitchOnFieldCmd; 
  G4UIcmdWithoutParameter*   SwitchOffFieldCmd;
    
};
#endif

