//
 // ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id$
//
// Code developed by: S.Guatelli, guatelli@ge.infn.it
//
#ifndef SR2SToroidalMagneticShield_h
#define SR2SToroidalMagneticShield_h 1
#include "globals.hh"
#include "MarsMagneticShield.hh"
#include <vector>
#include "G4FieldManager.hh"

class G4VPhysicalVolume;
class G4Tubs;
class G4Box;
class G4LogicalVolume;
class G4Material;
class MarsMaterial;
class MarsDecorator;
class G4VPhysicalVolume;
class G4VisAttributes;
class SR2SToroidalMagneticShieldMessenger;
class G4Torus;
class G4UnionSolid;
class G4SubtractionSolid;

class SR2SToroidalMagneticShield: public MarsMagneticShield
{
public:
  SR2SToroidalMagneticShield();
  ~SR2SToroidalMagneticShield();
  void ConstructComponent(G4VPhysicalVolume*);
  void DestroyComponent(); 
  
  void SetSR2SShieldStructureParameter(G4double,G4double,G4double,G4int,G4double,G4double,G4double,G4double,G4int,G4double,G4double,G4bool);
  
  void SetFieldManager(G4FieldManager* fieldManager);
  
private:
  MarsMaterial* pMaterial;
  SR2SToroidalMagneticShieldMessenger* messenger;

  G4double structureBarrelHalfLength;
  G4double racetrackSupportStructureInnerRadius;
  G4double racetrackSupportStructureThickness;
  G4int    racetrackCoilNumber;
  G4double coilAngle;
  G4double racetrackCoilInnerRadius;
  G4double racetrackCoilThickness;
  G4double racetrackCoilLength;
  G4double racetrackRadialDistanceFromOrigin;
  G4double racetrackBottomTiProtectionInnerRadius;
  G4double racetrackTiProtectionInnerRadius;
  G4double racetrackTiProtectionThickness;
  G4double microMeteoriteProtectionInnerRadius;
  G4double microMeteoriteProtectionThickness;
  G4int    tierodsNumber;
  G4double tierodsRadius;
  G4double tierodsHalfLength;
  G4double tierodsStep;

  G4double magneticFieldVolumeInnerRadius;
  G4double magneticFieldVolumeThickness;

  G4bool materialShield;

  G4Tubs*  racetrackSupportStructure;
  G4Tubs*  magneticFieldVolumeBarrel;
  G4Torus* magneticFieldVolumeEndcapTorus;
  G4Box*   magneticFieldVolumeEndcapBox;
  G4SubtractionSolid* magneticFieldVolumeEndcap;
  G4UnionSolid* magneticFieldVolume;
  G4Tubs*  racetrackBottomTiProtection;
  G4SubtractionSolid* racetrackCoilBarrelBox;
  G4Tubs*  racetrackCoilEndcapBox;
  G4UnionSolid* racetrackCoilBox;
  G4SubtractionSolid* racetrackCoilBarrel;
  G4Tubs*  racetrackCoilEndcap;
  G4UnionSolid* racetrackCoil;
  G4SubtractionSolid* racetrackCoilTiProtectionBarrel;
  G4Tubs*  racetrackCoilTiProtectionEndcap;
  G4UnionSolid* racetrackCoilTiProtection;
  G4Tubs*  tierods;
  G4Tubs*  microMeteoriteProtectionBarrel;
  G4Torus* microMeteoriteProtectionEndcapTorus;
  G4Box*   microMeteoriteProtectionEndcapBox;
  G4SubtractionSolid* microMeteoriteProtectionEndcap;
  G4UnionSolid* microMeteoriteProtection;

  G4LogicalVolume* racetrackSupportStructureLog;
  G4LogicalVolume* magneticFieldVolumeLog;
  G4LogicalVolume* racetrackBottomTiProtectionLog;
  G4LogicalVolume* racetrackCoilBoxLog;
  G4LogicalVolume* racetrackCoilLog;
  G4LogicalVolume* racetrackCoilTiProtectionLog;
  G4LogicalVolume* tierodsLog;
  G4LogicalVolume* microMeteoriteProtectionLog;

  G4VPhysicalVolume* racetrackSupportStructurePhys;
  G4VPhysicalVolume* magneticFieldVolumePhys;
  G4VPhysicalVolume* racetrackBottomTiProtectionPhys;
  G4VPhysicalVolume* racetrackCoilBoxPhys;
  G4VPhysicalVolume* racetrackCoilPhys;
  G4VPhysicalVolume* racetrackCoilTiProtectionPhys;
  G4VPhysicalVolume* tierodsPhys;
  G4VPhysicalVolume* microMeteoriteProtectionPhys;
};
#endif
