#ifndef MarsMagneticFieldSetup_h 
#define MarsMagneticFieldSetup_h 1

#include "globals.hh"
#include "G4ios.hh"
#include "MarsMagneticField.hh"
#include "G4ThreeVector.hh"
#include "vector"
#include "G4strstreambuf.hh"
#include "G4UniformMagField.hh"

class G4ChordFinder;
class G4Mag_UsualEqRhs;
class G4MagIntegratorStepper;
class MarsMagneticFieldSetupMessenger;
class G4FieldManager;
class G4TransportationManager;
class G4PropagatorInField;

class MarsMagneticFieldSetup
{
public:
  //constructor destructor	       
  MarsMagneticFieldSetup();
  ~MarsMagneticFieldSetup();
	 
  //Set methods 
  ///////////////////
  void SetMagneticField(G4String aModel);
  void SetMagneticFieldParameter(G4String aParam);

  //Integration paremeters
  void ResetIntegrationParameters();
  void SetEpsilon(G4double aEpsMin,G4double aEpsMax);
  void SetDeltaChord(G4double aVal);
  void SetDeltaIntersection(G4double aVal);
  void SetStepper(G4String );
  	 
  //Get methods
  /////////////////////
	 
  inline std::vector< G4String > GetListOfFieldModels() {return ListOfFieldModels;}
  inline G4String GetFieldModelName(){return FieldModelName;}
  G4FieldManager* GetGlobalFieldManager();
  G4FieldManager* GetLocalFieldManager();

  void PrintBField(G4ThreeVector pos) const; 
 	 
  //SwitchOn, Switch off field
  void SwitchOn();
  void SwitchOff();

protected:

  //Magnetic field model parameters  
	
  std::vector< G4String > ListOfFieldModels;
        

  G4String FieldModelName;
	
  //attribute for the integration method 
  ///////////////////////////////////////
	
  G4ChordFinder* fChordFinder;
  G4ChordFinder* fLocalChordFinder;
  G4Mag_UsualEqRhs* fEquationOfMotion;
  G4Mag_UsualEqRhs* fLocalEquationOfMotion;
  G4MagIntegratorStepper* fStepper;
  G4MagIntegratorStepper* fLocalStepper;
  MarsMagneticFieldSetupMessenger* fFieldMessenger;
  G4UniformMagField* fMagneticField;
  MarsMagneticField* fLocalMagneticField;
  G4FieldManager* fFieldManager;
  G4FieldManager* fLocalFieldManager;

  G4double DefaultDeltaChord;
  G4double DeltaChord;
  G4double DefaultDeltaIntersection;
  G4double DefaultEpsilonMin,DefaultEpsilonMax;

  //protected methods
protected:
  
} ;

#endif
