#ifndef MarsMagneticField_h 
#define MarsMagneticField_h 1

#include "globals.hh"
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"
#include "G4ios.hh"
#include "G4MagneticField.hh"
#include "G4ThreeVector.hh"
#include "vector"
#include "G4strstreambuf.hh"

class MarsMagneticField : public G4MagneticField
{
public:
  //constructor destructor	       
  MarsMagneticField();
  ~MarsMagneticField();
  
  // Gives the magnetic field B at a given position defined by yTrack
  virtual void GetFieldValue(const G4double yTrack[],G4double B[]) const = 0;
  virtual G4ThreeVector GetFieldValue(const G4ThreeVector position) const = 0; 

  virtual void SetMagneticFieldParameter(const G4String aParam) = 0;

};

#endif
