#ifndef MarsSolenoidalMagneticField_h
#define MarsSolenoidalMagneticField_h 1 

#include "globals.hh"
#include "G4ios.hh"
#include "G4ThreeVector.hh"
#include "vector"
#include "G4strstreambuf.hh"
#include "MarsMagneticField.hh"

class MarsSolenoidalMagneticField : public MarsMagneticField
{
public:
  //constructor destructor	       
  MarsSolenoidalMagneticField();
  ~MarsSolenoidalMagneticField();
	     
  // Gives the magnetic field B at a given position defined by yTrack
  void GetFieldValue(const G4double yTrack[],G4double B[]) const;
  G4ThreeVector GetFieldValue(const G4ThreeVector position) const; 

  void SetMagneticFieldParameter(const G4String name_file);	 

  G4double SolenoidalBx,SolenoidalBy,SolenoidalBz;
  G4ThreeVector SolenoidalField;
};

#endif
