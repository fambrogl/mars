//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
//
// $Id$
//
// 
#ifndef SR2SOpenToroidalMagneticShieldPumpkinMessenger_h
#define SR2SOpenToroidalMagneticShieldPumpkinMessenger_h 1

#include "globals.hh"
#include "G4UImessenger.hh"

class SR2SOpenToroidalMagneticShieldPumpkin;
class G4UIdirectory;
class G4UIparameter;
class G4UIcmdWithAString;
class G4UIcommad;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithoutParameter;
class SR2SOpenToroidalMagneticShieldPumpkinMessenger: public G4UImessenger
{
public:
  SR2SOpenToroidalMagneticShieldPumpkinMessenger(SR2SOpenToroidalMagneticShieldPumpkin* );
  ~SR2SOpenToroidalMagneticShieldPumpkinMessenger();
    
  void SetNewValue(G4UIcommand*, G4String);
  
private:
  SR2SOpenToroidalMagneticShieldPumpkin* sr2sShield;
  G4UIparameter*               param;
  G4UIdirectory*               sr2sShieldDir; // control the geometry configuration
  G4UIcommand*                 sr2sBandageStructureCmd;
  G4UIcommand*                 sr2sSupportRingCmd;
  G4UIcommand*                 sr2sSupportCylinderCmd;
  G4UIcommand*                 sr2sCoilStructureCmd;
  G4UIcmdWithoutParameter*     sr2sNoMaterialCmd; 
};
#endif

