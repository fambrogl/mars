//
 // ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id$
//
// Code developed by: S.Guatelli, guatelli@ge.infn.it
//
#ifndef SR2SSolenoidalMagneticShield_h
#define SR2SSolenoidalMagneticShield_h 1
#include "globals.hh"
#include "MarsMagneticShield.hh"
#include <vector>
#include "G4FieldManager.hh"

class G4VPhysicalVolume;
class G4Tubs;
class G4Box;
class G4LogicalVolume;
class G4Material;
class MarsMaterial;
class MarsDecorator;
class G4VPhysicalVolume;
class G4VisAttributes;
class SR2SSolenoidalMagneticShieldMessenger;
class G4Torus;
class G4UnionSolid;
class G4SubtractionSolid;

class SR2SSolenoidalMagneticShield: public MarsMagneticShield
{
public:
  SR2SSolenoidalMagneticShield();
  ~SR2SSolenoidalMagneticShield();
  void ConstructComponent(G4VPhysicalVolume*);
  void DestroyComponent(); 
  
  void SetSR2SShieldStructureParameter(G4double,G4double,G4double,G4double,G4double,G4bool);
  
  void SetFieldManager(G4FieldManager* fieldManager);
  
private:
  MarsMaterial* pMaterial;
  SR2SSolenoidalMagneticShieldMessenger* messenger;

  
  G4double innerSolenoidRadius;
  G4double innerSolenoidThickness;
  G4double outerSolenoidRadius;
  G4double outerSolenoidThickness;
  G4double solenoidHalfLength;

  G4double magneticFieldVolumeInnerRadius;
  G4double magneticFieldVolumeThickness;

  G4bool materialShield;

  G4Tubs* innerSolenoid;
  G4Tubs* outerSolenoid;
  G4Tubs* magneticFieldVolume;

  G4LogicalVolume* innerSolenoidLog;
  G4LogicalVolume* outerSolenoidLog;
  G4LogicalVolume* magneticFieldVolumeLog;


  G4VPhysicalVolume* innerSolenoidPhys;
  G4VPhysicalVolume* outerSolenoidPhys;
  G4VPhysicalVolume* magneticFieldVolumePhys;

};
#endif
