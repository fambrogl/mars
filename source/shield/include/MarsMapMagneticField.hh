#ifndef MarsMapMagneticField_h 
#define MarsMapMagneticField_h 1 

#include "globals.hh"
#include "G4ios.hh"
#include "G4ThreeVector.hh"
#include "vector"
#include "G4strstreambuf.hh"
#include "MarsMagneticField.hh"
#include <map>
#include <TFile.h>
#include <TH3F.h>

class MarsMapMagneticField : public MarsMagneticField
{
public:
  //constructor destructor	       
  MarsMapMagneticField();
  ~MarsMapMagneticField();
	     
  // Gives the magnetic field B at a given position defined by yTrack
  void GetFieldValue(const G4double yTrack[],G4double B[]) const;
  G4ThreeVector GetFieldValue(const G4ThreeVector position) const; 
  
  void SetMagneticFieldParameter(const G4String name_file);

private:

  G4double Xmin,Xmax,Ymin,Ymax,Zmin,Zmax;
  TFile* fileMap;
  TH3F* histoBxComponent;
  TH3F* histoByComponent;
  TH3F* histoBzComponent;
}; 

#endif
