//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// $Id$

#include "G4RunManager.hh"
#include "G4UImanager.hh"
#include "MarsGeometryConstruction.hh"
#include "MarsPhysicsList.hh"
#include "MarsPrimaryGeneratorAction.hh"
#include "MarsEventAction.hh"
#include "MarsRunAction.hh"
#include "MarsSteppingAction.hh"
#include "MarsTrackingAction.hh"
#include "MarsAnalysisManager.hh"
#include "G4ScoringManager.hh"

#ifdef G4VIS_USE
#include "G4VisExecutive.hh"
#endif
#ifdef G4UI_USE
#include "G4UIExecutive.hh"
#endif
#include "QGSP_BIC_HP.hh"

int main(int argc,char** argv)
{
  // Construct the default run manager
  G4RunManager* runManager = new G4RunManager;

  // To be check it cause a crash
  // Access to the Scoring Manager pointer
  //G4ScoringManager::GetScoringManager();

  // Geometry
  MarsGeometryConstruction* geometry = new MarsGeometryConstruction();
  runManager->SetUserInitialization(geometry);
  
  // Physics
   runManager->SetUserInitialization(new MarsPhysicsList);
  
  // Set mandatory user action class

  // Primary particles
  MarsPrimaryGeneratorAction* primary = new MarsPrimaryGeneratorAction();
  primary->SetGeometry(geometry);
  runManager->SetUserAction(primary);
 
  // Set optional user action class
  MarsRunAction* run = new MarsRunAction();
  runManager->SetUserAction(run);
  MarsEventAction* event = new MarsEventAction();
  runManager->SetUserAction(event);
  MarsSteppingAction* stepping = new MarsSteppingAction(primary);
  runManager->SetUserAction(stepping);
  MarsTrackingAction* tracking = new MarsTrackingAction();
  runManager->SetUserAction(tracking);

  MarsAnalysisManager* theAnalysisManager = MarsAnalysisManager::GetInstance();
    
#ifdef G4VIS_USE
   // Visualisation
   G4VisManager* visManager = new G4VisExecutive;
   visManager->Initialize();
#endif
 
  // get the pointer to the UI manager and set verbosities
  G4UImanager* UImanager = G4UImanager::GetUIpointer();
   
  if(argc == 1)
    // Define (G)UI terminal for interactive mode  
    { 
#ifdef G4UI_USE
      G4UIExecutive* ui = new G4UIExecutive(argc, argv);
#ifdef G4VIS_USE
      UImanager->ApplyCommand("/control/execute setup/gui.mac");     
#endif
      ui->SessionStart();
      delete ui;
#endif
    }
  else
    // Batch mode
    { 
      G4String command = "/control/execute ";
      G4String fileName = argv[1];
      UImanager->ApplyCommand(command+fileName);
    }


#ifdef G4VIS_USE
  delete visManager;
#endif

  return 0;
}

