#ifndef MarsAnalysisManager_HH
#define MarsAnalysisManager_HH

#include"G4ios.hh"
#include"G4strstreambuf.hh"
#include <vector>
#include"globals.hh"
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"
#include <fstream>
#include"G4ThreeVector.hh"
#include"G4Event.hh"
#include"G4Run.hh"

#include "TROOT.h"
#include "TFile.h"
#include "TH1.h"
#include "TH2.h"
#include "TTree.h"
#include "TNtuple.h"
#include "TDirectory.h"
#include "TVector3.h"

#include "MarsEventAction.hh"
#include "MarsSteppingAction.hh"
#include "MarsRootEvent.hh"
#include "MarsTrack.hh"
#include "MarsRootTrack.hh"
#include <string>

class G4Step;
class MarsAnalysisMessenger;
class MarsAnalysisManager
{
public:
  
  ~MarsAnalysisManager();
  static MarsAnalysisManager* GetInstance();
   
  //Event actions
  //-------------
  void BeginOfRunAction(const G4Run*);
  void BeginOfEventAction(const G4Event*);
  void EndOfEventAction(const G4Event*);
    
  void SetRootFile(G4String aFileName,G4String aDirName);
  void StoreRootFile();
  void SetDoseTree();
  void SetSetupTree();
  void SetAstronautGeometryInfoTree();
  
  inline void SetAutoSaveLimit(G4int aAutoSave){theAutoSaveLimit=aAutoSave;}
  inline void SetGeometryConfigurationParameter(G4String aVal) {GeometryConfiguration=aVal;}
  inline void SetBFieldConfigurationParameter(G4String aVal) {BFieldConfiguration=aVal;}
  void SetWorldDimensions(G4double X, G4double Y, G4double Z);
  void SetActiveWall(G4bool Xp,G4bool Xm,G4bool Yp,G4bool Ym,G4bool Zp,G4bool Zm);

private:
  static MarsAnalysisManager* instance;

private:
  MarsAnalysisManager(); 
 
private:
  MarsAnalysisMessenger* theMessenger;

  G4int trackingHitCollID;
  G4int fluenceHitCollID;
  G4int trackCollID;
  G4int theAutoSaveLimit;

  G4bool DoseFill;
  G4bool SetupFill;
  G4bool AstronautGeometryInfoFill;

  std::vector<std::string> hpVolumeName;
  std::vector<G4double> hpVolumeMass;
  

  G4double HitdEdx,HitTrackId;
  G4int TrackTrackId,DoseGeneratorId;

  G4int eventNumber;
  std::vector<MarsRootTrack> theTrackContainer;
  std::vector<MarsRootFluenceHit> theFluenceHitContainer;
  std::map<int,std::vector<MarsRootHit> > theTrackIdvsHitMap;
  std::map<G4int,MarsTrack*> theTrackAccessMap;
  std::map<int,std::vector<int> > theDoseGeneratorMap;
  MarsRootEvent* theMarsRootEvent;
  std::vector<double> theWorldDimensions;
  std::vector<int> theActiveWall;


  std::string GeometryConfiguration;
  std::string BFieldConfiguration;
 
  TFile* theRootFile;
  TTree* theDoseTree;
  TTree* theSetupTree;
  TTree* theAstronautGeometryInfoTree;
  TDirectory* thePathDir;
  
};

#endif




