#include "MarsAnalysisManager.hh"
#include "MarsAnalysisMessenger.hh"

//Analyser
#include "G4RunManager.hh"
#include "G4ParticleTable.hh"
#include "G4SDManager.hh"
#include "G4EventManager.hh"
#include "G4RunManager.hh"

#include "G4Step.hh"
#include "G4Timer.hh"
#include "Randomize.hh"

#include "MarsFluenceHit.hh"
#include "MarsTrackingHit.hh"
#include "MarsRootHit.hh"
#include "MarsRootFluenceHit.hh"
#include <map>
#include "G4PhysicalVolumeStore.hh"
#include "G4VPhysicalVolume.hh"
#include "G4LogicalVolume.hh"
#include <iterator>

MarsAnalysisManager* MarsAnalysisManager::instance = 0;

////////////////////////////////////////////////////////////////////////////////
//
MarsAnalysisManager::MarsAnalysisManager():theMarsRootEvent(0),theRootFile(0),theDoseTree(0),theSetupTree(0),theAstronautGeometryInfoTree(0),thePathDir(0)
{
  theMessenger = new MarsAnalysisMessenger(this);
  
  trackingHitCollID=-1;	
  fluenceHitCollID=-1;	
  trackCollID=-1;	
  theAutoSaveLimit = 1000;
  DoseFill=false;
  SetupFill=false;
  AstronautGeometryInfoFill=false;
  eventNumber=0;
  //thePathDir="/";
}
////////////////////////////////////////////////////////////////////////////////
//  
MarsAnalysisManager::~MarsAnalysisManager() 
{
  delete theMessenger;
  delete theMarsRootEvent;
  delete theDoseTree;
  delete theSetupTree;
  delete theAstronautGeometryInfoTree;
  delete theRootFile;
}
////////////////////////////////////////////////////////////////////////////////
//

void MarsAnalysisManager::SetRootFile(G4String aFileName,G4String aDirName) 
{
  theRootFile = new TFile(aFileName,"RECREATE","Storing of Radiation information");
  
  G4String aNameDir = aDirName;
  thePathDir = theRootFile->mkdir(aNameDir);
}

void MarsAnalysisManager::SetWorldDimensions(G4double X, G4double Y, G4double Z){
  theWorldDimensions.push_back(X);
  theWorldDimensions.push_back(Y);
  theWorldDimensions.push_back(Z);
}

void MarsAnalysisManager::SetActiveWall(G4bool Xp,G4bool Xm,G4bool Yp,G4bool Ym,G4bool Zp,G4bool Zm){
  if(Xp)
    theActiveWall.push_back(1);
  else
    theActiveWall.push_back(0);
  if(Xm)
    theActiveWall.push_back(1);
  else
    theActiveWall.push_back(0);
  if(Yp)
    theActiveWall.push_back(1);
  else
    theActiveWall.push_back(0);
  if(Ym)
    theActiveWall.push_back(1);
  else
    theActiveWall.push_back(0);
  if(Zp)
    theActiveWall.push_back(1);
  else
    theActiveWall.push_back(0);
  if(Zm)
    theActiveWall.push_back(1);
  else
    theActiveWall.push_back(0);
}

void MarsAnalysisManager::SetDoseTree()
{
  DoseFill=true;

  thePathDir->cd();

  theDoseTree = new TTree("Dose","The Tree with the variable used to performe the dose calculation on astronaut ");
  theDoseTree->Branch("Event","MarsRootEvent",&theMarsRootEvent);
  theDoseTree->SetAutoSave(theAutoSaveLimit);
}

void MarsAnalysisManager::SetSetupTree() 
{
  SetupFill=true;

  thePathDir->cd();

  theSetupTree = new TTree("Setup","The Tree with the info of general setup of geometry and b-field");
  theSetupTree->Branch("GeometryConfiguration",&GeometryConfiguration);
  theSetupTree->Branch("BFieldConfiguration",&BFieldConfiguration);
  theSetupTree->Branch("WorldDimensions",&theWorldDimensions);
  theSetupTree->Branch("ActiveWall",&theActiveWall);
}
void MarsAnalysisManager::SetAstronautGeometryInfoTree() 
{
  AstronautGeometryInfoFill=true;

  thePathDir->cd();
  
  theAstronautGeometryInfoTree = new TTree("AstronautGeometry","The Tree with the info of human astronaut geometry");
  hpVolumeName.clear();
  hpVolumeMass.clear();
  for(std::vector<G4VPhysicalVolume*>::iterator i = G4PhysicalVolumeStore::GetInstance()->begin();i!=G4PhysicalVolumeStore::GetInstance()->end(); i++){
    if((*i)->GetMotherLogical()){
      G4String motherName = (*i)->GetMotherLogical()->GetName().c_str();
      if(motherName.contains("stronaut")&&(*i)->GetCopyNo()==0){
	hpVolumeName.push_back((*i)->GetName());
	hpVolumeMass.push_back((*i)->GetLogicalVolume()->GetMass()/kg);
      }else{
	for(unsigned int j=0;j<hpVolumeName.size();j++){
	  G4String mother = (*i)->GetMotherLogical()->GetName().c_str();
	  mother.erase(0,7);
	  if(mother=="")
	    mother="m";
	  G4String oldmother = hpVolumeName[j].c_str();
	  oldmother.erase(0,8);
	  if(oldmother=="")
	    oldmother="om";
	  if( mother == oldmother  && (*i)->GetCopyNo()==0){
	    hpVolumeName.push_back((*i)->GetName());
	    hpVolumeMass.push_back((*i)->GetLogicalVolume()->GetMass()/kg);
	  }
	}
      }
    }
  }
  theAstronautGeometryInfoTree->Branch("HumanAstronautVolumeName",&hpVolumeName);
  theAstronautGeometryInfoTree->Branch("HumanAstronautVolumeMass",&hpVolumeMass);
}

MarsAnalysisManager* MarsAnalysisManager::GetInstance()
{
  if (instance == 0) instance = new MarsAnalysisManager;
  return instance;
}
////////////////////////////////////////////////////////////////////////////////
//
void MarsAnalysisManager::BeginOfEventAction(const G4Event*)
{
  G4SDManager * SDman = G4SDManager::GetSDMpointer();
  if(trackingHitCollID<0||trackCollID<0||fluenceHitCollID<0){
    fluenceHitCollID  = SDman->GetCollectionID("FluenceHitCollection");
    trackingHitCollID = SDman->GetCollectionID("TrackingHitCollection");
    trackCollID       = SDman->GetCollectionID("TrackCollection");
  } 
}  


void MarsAnalysisManager::StoreRootFile()
{
  theRootFile->Write(0,TObject::kOverwrite);
  theRootFile->Close();
}
void MarsAnalysisManager::BeginOfRunAction(const G4Run*)
{
  if(SetupFill)
    theSetupTree->Fill();
  
  if(AstronautGeometryInfoFill)
    theAstronautGeometryInfoTree->Fill();

  eventNumber=0;
}
///////////////////////////////////////////////////////////////////////////////
//
void MarsAnalysisManager::EndOfEventAction(const G4Event* evt)
{ 
  eventNumber++;
  if(DoseFill){ 
    theTrackIdvsHitMap.clear();
    theTrackAccessMap.clear();
    theDoseGeneratorMap.clear();
    theTrackContainer.clear();
    theFluenceHitContainer.clear();
 
    if(trackingHitCollID<0||trackCollID<0||fluenceHitCollID<0) return;
    G4HCofThisEvent * HCE = evt->GetHCofThisEvent();
    MarsFluenceHitCollection*  FluenceHC  = 0;
    MarsTrackingHitCollection* TrackingHC = 0;
    MarsTrackCollection*       TrackC     = 0;
    bool hittedAstronaut = false;
    int trackNumber = 0;
    int hitNumber = 0;
    int fluenceHitNumber = 0;
    if(HCE){
      FluenceHC  = (MarsFluenceHitCollection*)(HCE->GetHC(fluenceHitCollID));
      TrackingHC = (MarsTrackingHitCollection*)(HCE->GetHC(trackingHitCollID));
      TrackC     = (MarsTrackCollection*)(HCE->GetHC(trackCollID));    

      hitNumber = TrackingHC->entries();
      if(hitNumber>0){
	hittedAstronaut = true;
	for(int i=0;i<hitNumber;i++){
	  HitdEdx=(*TrackingHC)[i]->GetdEdx();
	  HitTrackId=(*TrackingHC)[i]->GetTrackId();
	  if(!(HitdEdx>-0.1))
	    std::cout<<"HitdEdx = "<<HitdEdx<<" MeV/mm "<<std::endl;
	  theTrackIdvsHitMap[HitTrackId].push_back(MarsRootHit((*TrackingHC)[i]->GetVolumeName(),
							       (*TrackingHC)[i]->GetCopyNumber(),
							       (*TrackingHC)[i]->GetELoss(),
							       HitdEdx)
						   );
	}
      }
      
      trackNumber = TrackC->entries();
      if(hittedAstronaut){
	int FirstInteractionId = -1;
	for(int j=0;j<trackNumber;j++){
	  TrackTrackId=(*TrackC)[j]->GetTrackId();
	  if(j==1)
	    FirstInteractionId=TrackTrackId;
	  DoseGeneratorId = (*TrackC)[j]->GetDoseGeneratorId();
	  theTrackAccessMap.insert(std::pair<G4int,MarsTrack*>(TrackTrackId,(*TrackC)[j]));
	  if(  DoseGeneratorId == TrackTrackId ){
	    std::vector<int> temp;
	    theDoseGeneratorMap.insert(std::pair<int,std::vector<int> >(TrackTrackId,temp));
	  }else if(theDoseGeneratorMap.find(DoseGeneratorId) != theDoseGeneratorMap.end() ){
	    theDoseGeneratorMap[DoseGeneratorId].push_back(TrackTrackId);
	  }else{
	    G4cout<<"WARNING A TRACK IS ORPHAN!!!"<<G4endl;
	  }
	}
	
	for (std::map<int,std::vector<int> >::iterator it=theDoseGeneratorMap.begin(); it!=theDoseGeneratorMap.end(); ++it){
	  G4int mainId = it->first;
	  std::vector<G4int> secondaryId = it->second;
	  std::vector<MarsRootHit> myDoseHits;
	  std::vector<MarsRootHit>::iterator itMRH;
	  if(theTrackIdvsHitMap.find(mainId)!=theTrackIdvsHitMap.end()){
	    itMRH = myDoseHits.end();
	    myDoseHits.insert(itMRH,((*theTrackIdvsHitMap.find(mainId)).second).begin(),((*theTrackIdvsHitMap.find(mainId)).second).end());
	  }
	  if(secondaryId.size()){
	    for(unsigned int k = 0; k<secondaryId.size();k++){
	      if(theTrackIdvsHitMap.find(secondaryId[k])!=theTrackIdvsHitMap.end()){
		itMRH = myDoseHits.end();
		myDoseHits.insert(itMRH,((*theTrackIdvsHitMap.find(secondaryId[k])).second).begin(),((*theTrackIdvsHitMap.find(secondaryId[k])).second).end());
	      }
	    }
	  }
	  if(myDoseHits.size()||mainId==1||mainId==FirstInteractionId){
	    MarsTrack* myTrack = theTrackAccessMap[mainId];
	    TVector3 VtxPos(myTrack->GetVertexPosition().getX(),
			    myTrack->GetVertexPosition().getY(),
			    myTrack->GetVertexPosition().getZ());
	    bool firstInteractionVertex = false;
	    if(mainId==FirstInteractionId)
	      firstInteractionVertex=true;
	    theTrackContainer.push_back(MarsRootTrack(myTrack->GetTrackId(),
						      myTrack->GetParticleName(),
						      myTrack->GetZ(),
						      myTrack->GetA(),
						      myTrack->GetKineticEnergy(),
						      VtxPos,
						      myTrack->GetVolumeAtVertex(),
						      myTrack->GetProcessCreatorName(),
						      firstInteractionVertex,
						      myDoseHits
						      )
					);
	  }
	}
      }else{
	for(int j=0;j<trackNumber;j++){
	  TrackTrackId=(*TrackC)[j]->GetTrackId();
	  if(TrackTrackId==1){
	    std::vector<MarsRootHit> myDoseHits;
	    TVector3 VtxPos((*TrackC)[j]->GetVertexPosition().getX(),
			    (*TrackC)[j]->GetVertexPosition().getY(),
			    (*TrackC)[j]->GetVertexPosition().getZ());
	    theTrackContainer.push_back(MarsRootTrack((*TrackC)[j]->GetTrackId(),
						      (*TrackC)[j]->GetParticleName(),
						      (*TrackC)[j]->GetZ(),
						      (*TrackC)[j]->GetA(),
						      (*TrackC)[j]->GetKineticEnergy(),
						      VtxPos,
						      (*TrackC)[j]->GetVolumeAtVertex(),
						      (*TrackC)[j]->GetProcessCreatorName(),
						      false,
						      myDoseHits
						      )
					);
	    break;
	  }
	}
      }

      fluenceHitNumber = FluenceHC->entries();
      if(fluenceHitNumber>0){
	for(int i=0;i<fluenceHitNumber;i++){
	  theFluenceHitContainer.push_back(MarsRootFluenceHit((*FluenceHC)[i]->GetParticleName(),
							      (*FluenceHC)[i]->GetTrackId(),
							      (*FluenceHC)[i]->GetZ(),
							      (*FluenceHC)[i]->GetA(),
							      (*FluenceHC)[i]->GetKineticEnergy()));
	}
      }
      
      theMarsRootEvent->SetEventNumber(eventNumber);
      theMarsRootEvent->SetFluenceHit(theFluenceHitContainer);
      theMarsRootEvent->SetHitAstronaut(hittedAstronaut);
      theMarsRootEvent->SetTracks(theTrackContainer);
      theDoseTree->Fill();
    }
    
    std::map<int,std::vector<MarsRootHit> >().swap(theTrackIdvsHitMap);
    std::map<G4int,MarsTrack*>().swap(theTrackAccessMap);
    std::map<int,std::vector<int> >().swap(theDoseGeneratorMap);
    std::vector<MarsRootTrack>().swap(theTrackContainer);
    std::vector<MarsRootFluenceHit>().swap(theFluenceHitContainer);
    
    theTrackIdvsHitMap.clear();
    theTrackAccessMap.clear();
    theDoseGeneratorMap.clear();
    theTrackContainer.clear();
    theFluenceHitContainer.clear();
  }
}
