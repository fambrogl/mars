#include "MarsAnalysisMessenger.hh"
#include "MarsAnalysisManager.hh"
#include "G4UIdirectory.hh"
#include "G4UIcommand.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWith3VectorAndUnit.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UImanager.hh"
#include "G4UnitsTable.hh"
#include "G4UserStackingAction.hh"
#include "G4RunManager.hh"
#include "G4UIparameter.hh"


MarsAnalysisMessenger::MarsAnalysisMessenger
(MarsAnalysisManager* theManager)
  :pAnalysisManager(theManager)
{

  G4UIparameter* param;
  //command directory 
  //-------------------
  analysisDir = new G4UIdirectory("/analysis/");
  analysisDir->SetGuidance("Analysis control");
  
  //commands
  //--------
  SaveDoseTreeCmd =  new G4UIcmdWithoutParameter("/analysis/SaveDose",this);
  SaveDoseTreeCmd->SetGuidance("Save the set of variables used to performe teh dose calculation");
  SaveDoseTreeCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SaveSetupTreeCmd =  new G4UIcmdWithoutParameter("/analysis/SaveSetupInfo",this);
  SaveSetupTreeCmd->SetGuidance("Save the information of the general Setup as Geomety and BField configuration");
  SaveSetupTreeCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SaveAstronautGeometryInfoTreeCmd =  new G4UIcmdWithoutParameter("/analysis/SaveAstronautGeometryInfo",this);
  SaveAstronautGeometryInfoTreeCmd->SetGuidance("Save the information of Human astronaut geomety");
  SaveAstronautGeometryInfoTreeCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SetGeometryConfigurationCmd = new G4UIcmdWithAString("/analysis/SetGeometryConfiguration",this);
  SetGeometryConfigurationCmd->SetGuidance("Set the string that identify the Geometry configuration to be stored in the root file");
  SetGeometryConfigurationCmd->SetParameterName("GeometryType",true);
  SetGeometryConfigurationCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SetBFieldConfigurationCmd = new G4UIcmdWithAString("/analysis/SetBFieldConfiguration",this);
  SetBFieldConfigurationCmd->SetGuidance("Set the string that identify the BField configuration to be stored in the root file");
  SetBFieldConfigurationCmd->SetParameterName("BFieldType",true);
  SetBFieldConfigurationCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SetActiveWallCmd = new G4UIcommand("/analysis/SetActiveWall",this);
  SetActiveWallCmd->SetGuidance("Set the active waal of the world volume from where the particle si generated.");
  SetActiveWallCmd->SetGuidance("[usage] /analysis/SetActiveWall X+ X- Y+ Y- Z+ Z- [bool] "); 
  param = new G4UIparameter("X+",'B',false);
  SetActiveWallCmd->SetParameter(param);
  param = new G4UIparameter("X-",'B',false);
  SetActiveWallCmd->SetParameter(param);
  param = new G4UIparameter("Y+",'B',false);
  SetActiveWallCmd->SetParameter(param);
  param = new G4UIparameter("Y-",'B',false);
  SetActiveWallCmd->SetParameter(param);
  param = new G4UIparameter("Z+",'B',false);
  SetActiveWallCmd->SetParameter(param);
  param = new G4UIparameter("Z-",'B',false);
  SetActiveWallCmd->SetParameter(param);
  SetActiveWallCmd->AvailableForStates(G4State_PreInit,G4State_Idle); 

  SetWorldDimensionsCmd = new G4UIcommand("/analysis/SetWorldDimensions",this);
  SetWorldDimensionsCmd->SetGuidance("Set the dimension of the world specifing the X,Y,Z dimension and the unit");
  SetWorldDimensionsCmd->SetGuidance("[usage] /analysis/SetWorldDimensions X Y Z length_unit");
  param = new G4UIparameter("X",'d',false);
  SetWorldDimensionsCmd->SetParameter(param);
  param = new G4UIparameter("Y",'d',false);
  SetWorldDimensionsCmd->SetParameter(param);
  param = new G4UIparameter("Z",'d',false);
  SetWorldDimensionsCmd->SetParameter(param);
  param = new G4UIparameter("length_unit",'s',false);
  SetWorldDimensionsCmd->SetParameter(param);
  SetWorldDimensionsCmd->AvailableForStates(G4State_Idle);


  SetRootFileCmd =  new G4UIcommand("/analysis/SetRootFile",this);
  SetRootFileCmd->SetGuidance("Save the information for radiation dose analysis in a ROOT file");
  param = new G4UIparameter("FileName",'s',false);
  SetRootFileCmd->SetParameter(param);
  param = new G4UIparameter("DirectoryName",'s',false);
  SetRootFileCmd->SetParameter(param);
  SetRootFileCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  StoreRootFileCmd =  new G4UIcmdWithoutParameter("/analysis/StoreRootFile",this);
  StoreRootFileCmd->SetGuidance("Save the ROOT file");
  StoreRootFileCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SetAutoSaveLimitCmd = new G4UIcommand("/analysis/SetAutoSaveLimit",this);
  SetAutoSaveLimitCmd->SetGuidance("Set the AutoSave limit and mode you can choose between Event and Memory");
  param = new G4UIparameter("Limit",'i',false);
  SetAutoSaveLimitCmd->SetParameter(param);
  param = new G4UIparameter("Mode",'s',false);
  SetAutoSaveLimitCmd->SetParameter(param);
  SetAutoSaveLimitCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
}

MarsAnalysisMessenger::~MarsAnalysisMessenger()
{  
  delete analysisDir;
}

void MarsAnalysisMessenger::SetNewValue(G4UIcommand * command,G4String newValues)
{ 
  if(command == SaveDoseTreeCmd)
    pAnalysisManager->SetDoseTree();
  else if(command == SaveSetupTreeCmd)
    pAnalysisManager->SetSetupTree();
  else if(command == SaveAstronautGeometryInfoTreeCmd)
    pAnalysisManager->SetAstronautGeometryInfoTree();
  else if(command == StoreRootFileCmd)
    pAnalysisManager->StoreRootFile();
  else if(command == SetGeometryConfigurationCmd)
    pAnalysisManager->SetGeometryConfigurationParameter(newValues);
  else if(command == SetBFieldConfigurationCmd)
    pAnalysisManager->SetBFieldConfigurationParameter(newValues);
  else if(command == SetWorldDimensionsCmd){
    const char* paramString;
    paramString=newValues;
    G4double X,Y,Z;
    G4String length_unit;
    std::istringstream is((char*)paramString);
    is>>X>>Y>>Z>>length_unit;
    X*=G4UnitDefinition::GetValueOf(length_unit);
    Y*=G4UnitDefinition::GetValueOf(length_unit);
    Z*=G4UnitDefinition::GetValueOf(length_unit);
    pAnalysisManager->SetWorldDimensions(X,Y,Z);
  }  else if(command == SetActiveWallCmd){
    const char* paramString=newValues;
    G4bool Xp,Xm,Yp,Ym,Zp,Zm;
    std::istringstream is((char*)paramString);
    is >>Xp>>Xm>>Yp>>Ym>>Zp>>Zm;
    pAnalysisManager->SetActiveWall(Xp,Xm,Yp,Ym,Zp,Zm);
  }else if(command == SetRootFileCmd){
    const char* paramString=newValues;
    G4String fileName,dirName;
    std::istringstream is((char*)paramString);
    is >>fileName>>dirName;
    pAnalysisManager->SetRootFile(fileName,dirName);
  }else if(command == SetAutoSaveLimitCmd){
   const char* paramString=newValues;
   G4String autosaveMode;
   G4int number;
   std::istringstream is((char*)paramString);
   is >>number>>autosaveMode;
   if(autosaveMode=="Event")
     pAnalysisManager->SetAutoSaveLimit(number);
   else if(autosaveMode=="Memory")
     pAnalysisManager->SetAutoSaveLimit(-1*number);
   else{
     G4cout<<"You have select the wrong AutoSave mode "<<autosaveMode<<";"<<std::endl;
     G4cout<<"The possibility is Event or Memory by default Event has been chose"<<std::endl;
     pAnalysisManager->SetAutoSaveLimit(number);
   }
  }
}

