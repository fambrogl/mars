////////////////////////////////////////////////////////////////////////////////
//
#include "MarsRootTrack.hh"

ClassImp(MarsRootTrack)

////////////////////////////////////////////////////////////////////////////////
//
MarsRootTrack::MarsRootTrack ():theTrackId(0),
  theHitContainer(0)
{
  theVolumeAtVertex="";
  theProcessCreator="";
  thePartName="";
}
////////////////////////////////////////////////////////////////////////////////
//
MarsRootTrack::MarsRootTrack (int aTrackId, TString aPartName, 
			      int aZ, int aA,
			      double aKineticEnergy,
			      TVector3 aVertexPosition,
			      TString aVolumeAtVertex,
			      TString aProcessCreator,
			      bool aFirstInteraction)
{ 
  theTrackId             = aTrackId;	 
  thePartName            = aPartName;
  theZ                   = aZ;
  theA                   = aA;
  theKineticEnergy       = aKineticEnergy;
  theVolumeAtVertex      = aVolumeAtVertex;
  theVertexPosition      = aVertexPosition;
  theProcessCreator      = aProcessCreator;
  theFirstInteraction    = aFirstInteraction;
}
////////////////////////////////////////////////////////////////////////////////
//
MarsRootTrack::MarsRootTrack (int aTrackId, TString aPartName,
			      int aZ, int aA,
			      double aKineticEnergy,
			      TVector3 aVertexPosition,
			      TString aVolumeAtVertex,
			      TString aProcessCreator,
			      bool aFirstInteraction,
			      std::vector<MarsRootHit> aHitContainer)
{ 
  theTrackId             = aTrackId;	 
  thePartName            = aPartName;
  theZ                   = aZ;
  theA                   = aA;
  theKineticEnergy       = aKineticEnergy;
  theVertexPosition      = aVertexPosition;
  theVolumeAtVertex      = aVolumeAtVertex;
  theProcessCreator      = aProcessCreator;
  theFirstInteraction    = aFirstInteraction;
  theHitContainer        = aHitContainer;
}
////////////////////////////////////////////////////////////////////////////////
//
MarsRootTrack::~MarsRootTrack ()
{}
////////////////////////////////////////////////////////////////////////////////
//
MarsRootTrack::MarsRootTrack (const MarsRootTrack& right) : TObject()
{
  theTrackId              = right.theTrackId;
  thePartName             = right.thePartName;
  theZ                    = right.theZ;
  theA                    = right.theA;
  theKineticEnergy        = right.theKineticEnergy;
  theVertexPosition       = right.theVertexPosition;
  theVolumeAtVertex       = right.theVolumeAtVertex;
  theProcessCreator       = right.theProcessCreator;
  theFirstInteraction     = right.theFirstInteraction;
  theHitContainer         = right.theHitContainer;
}
////////////////////////////////////////////////////////////////////////////////
//
