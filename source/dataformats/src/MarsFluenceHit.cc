////////////////////////////////////////////////////////////////////////////////
//
#include "MarsFluenceHit.hh"


G4Allocator<MarsFluenceHit> MarsFluenceHitAllocator;
////////////////////////////////////////////////////////////////////////////////
//
MarsFluenceHit::MarsFluenceHit ():theTrackId(0),theZ(0),theA(0),
				  theKinEnergy(0)
{
  theName="";
}
////////////////////////////////////////////////////////////////////////////////
//
MarsFluenceHit::MarsFluenceHit (G4String aName,G4int aTrackId,G4int aZ,G4int aA, 
				G4double aKinEnergy)
{ 
  theName            = aName;
  theTrackId         = aTrackId;
  theZ               = aZ;
  theA               = aA;
  theKinEnergy       = aKinEnergy;	
}
////////////////////////////////////////////////////////////////////////////////
//
MarsFluenceHit::~MarsFluenceHit ()
{}
////////////////////////////////////////////////////////////////////////////////
//
MarsFluenceHit::MarsFluenceHit (const MarsFluenceHit& right) : G4VHit()
{
  theName            = right.theName;
  theTrackId         = right.theTrackId;
  theZ               = right.theZ;
  theA               = right.theA;
  theKinEnergy       = right.theKinEnergy;
}
////////////////////////////////////////////////////////////////////////////////
//
const MarsFluenceHit& MarsFluenceHit::operator= (const MarsFluenceHit& right)
{ 
  theName            = right.theName;
  theTrackId         = right.theTrackId;
  theZ               = right.theZ;
  theA               = right.theA;
  theKinEnergy       = right.theKinEnergy;	 
  return *this;
}
////////////////////////////////////////////////////////////////////////////////
//
int MarsFluenceHit::operator== (const MarsFluenceHit& ) const
{
  return 0;
}

