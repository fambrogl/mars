////////////////////////////////////////////////////////////////////////////////
//

#include "MarsRootEvent.hh"

ClassImp(MarsRootEvent)

////////////////////////////////////////////////////////////////////////////////
//
MarsRootEvent::MarsRootEvent ():eventNumber(0),hitAstronaut(0)
{
  theTrackCollection.clear();
  theFluenceHitCollection.clear();
}
////////////////////////////////////////////////////////////////////////////////
//
MarsRootEvent::MarsRootEvent (int aEventNumber,bool aHitAstronaut, std::vector<MarsRootTrack> aTracks,std::vector<MarsRootFluenceHit> aFluenceHit)
{ 
  eventNumber             = aEventNumber;
  hitAstronaut            = aHitAstronaut;
  theTrackCollection      = aTracks;
  theFluenceHitCollection = aFluenceHit ;
}
////////////////////////////////////////////////////////////////////////////////
//
MarsRootEvent::~MarsRootEvent ()
{}
////////////////////////////////////////////////////////////////////////////////
//
MarsRootEvent::MarsRootEvent (const MarsRootEvent& right): TObject()
{
  eventNumber             = right.eventNumber;
  hitAstronaut            = right.hitAstronaut;
  theTrackCollection      = right.theTrackCollection;
  theFluenceHitCollection = right.theFluenceHitCollection;
  
}
////////////////////////////////////////////////////////////////////////////////
//
