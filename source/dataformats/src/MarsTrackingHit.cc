////////////////////////////////////////////////////////////////////////////////
//
#include "MarsTrackingHit.hh"


G4Allocator<MarsTrackingHit> MarsTrackingHitAllocator;
////////////////////////////////////////////////////////////////////////////////
//
MarsTrackingHit::MarsTrackingHit ():theCopyNumber(0),thePDG(0),
				    theKinEnergy(0),theELoss(0),thePosition(0),
				    theStepLength(0),thedEdx(0),theG4EMdEdx(0),
				    theTrackId(0),theEventId(0)
{
  theVolumeName="";
}
////////////////////////////////////////////////////////////////////////////////
//
MarsTrackingHit::MarsTrackingHit (G4String aVolumeName,G4int aCopyNo,G4int aPDG, 
				  G4double aKinEnergy,G4double aELoss,
				  G4ThreeVector aPosition,G4double aStepLength,G4double adEdx,G4double aG4EMdEdx,
				  G4int aTrackId,G4int aEventId)
{ 
  theVolumeName  = aVolumeName;
  theCopyNumber  = aCopyNo;
  thePDG         = aPDG;	  
  theKinEnergy   = aKinEnergy;	  
  theELoss       = aELoss;
  thePosition    = aPosition;	 
  theStepLength  = aStepLength;
  thedEdx        = adEdx;	  
  theG4EMdEdx    = aG4EMdEdx;	  
  theTrackId     = aTrackId;	 
  theEventId     = aEventId;	 
}
////////////////////////////////////////////////////////////////////////////////
//
MarsTrackingHit::~MarsTrackingHit ()
{}
////////////////////////////////////////////////////////////////////////////////
//
MarsTrackingHit::MarsTrackingHit (const MarsTrackingHit& right) : G4VHit()
{
  theVolumeName     = right.theVolumeName;
  theCopyNumber     = right.theCopyNumber;
  thePDG            = right.thePDG;	 
  theKinEnergy      = right.theKinEnergy;	 
  theELoss          = right.theELoss;
  thePosition       = right.thePosition;	 
  theStepLength     = right.theStepLength;
  thedEdx           = right.thedEdx;	 
  theG4EMdEdx       = right.theG4EMdEdx;	 
  theTrackId        = right.theTrackId;	 
  theEventId        = right.theEventId;	 
}
////////////////////////////////////////////////////////////////////////////////
//
const MarsTrackingHit& MarsTrackingHit::operator= (const MarsTrackingHit& right)
{ 
  theVolumeName     = right.theVolumeName;
  theCopyNumber     = right.theCopyNumber;
  thePDG            = right.thePDG;	 
  theKinEnergy      = right.theKinEnergy;	 
  theELoss          = right.theELoss;
  thePosition       = right.thePosition;	 
  theStepLength     = right.theStepLength;
  thedEdx           = right.thedEdx;	 
  theG4EMdEdx       = right.theG4EMdEdx;	 
  theTrackId        = right.theTrackId;	 
  theEventId        = right.theEventId;	 
  return *this;
}
////////////////////////////////////////////////////////////////////////////////
//
int MarsTrackingHit::operator== (const MarsTrackingHit& ) const
{
  return 0;
}

