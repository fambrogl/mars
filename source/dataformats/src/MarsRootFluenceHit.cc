////////////////////////////////////////////////////////////////////////////////
//
#include "MarsRootFluenceHit.hh"

ClassImp(MarsRootFluenceHit)

////////////////////////////////////////////////////////////////////////////////
//
MarsRootFluenceHit::MarsRootFluenceHit ():theTrackId(0),theZ(0),theA(0),theKinEnergy(0)
  
{
  theName="";
  
}
////////////////////////////////////////////////////////////////////////////////
//
MarsRootFluenceHit::MarsRootFluenceHit(TString aName,int aTrackId,int aZ,int aA,double aKinEnergy)
{ 
  theName            = aName;
  theTrackId         = aTrackId;
  theZ               = aZ;
  theA               = aA;
  theKinEnergy       = aKinEnergy;
}
////////////////////////////////////////////////////////////////////////////////
//
MarsRootFluenceHit::~MarsRootFluenceHit ()
{}
////////////////////////////////////////////////////////////////////////////////
//
MarsRootFluenceHit::MarsRootFluenceHit (const MarsRootFluenceHit& right) : TObject()
{  
  theName            = right.theName;
  theTrackId         = right.theTrackId;
  theZ               = right.theZ;
  theA               = right.theA;
  theKinEnergy       = right.theKinEnergy;
}
////////////////////////////////////////////////////////////////////////////////
//
