////////////////////////////////////////////////////////////////////////////////
//
#include "MarsTrack.hh"

G4Allocator<MarsTrack> MarsTrackAllocator;
////////////////////////////////////////////////////////////////////////////////
//
MarsTrack::MarsTrack ():theTrackId(0),
			theVertexPosition(0),theMomentum(0),
			theMotherTrackId(0),
			theEventId(0),theHitContainer(0)
{
  theVolumeAtVertex="";
  theProcessCreatorName="";
  thePartName="";
}
////////////////////////////////////////////////////////////////////////////////
//
MarsTrack::MarsTrack (G4int aTrackId, G4String aPartName,G4double aPDGmass,
		      G4int aCharge,G4int aZ, G4int aA, 
		      G4ThreeVector aMomentum, 
		      double aTotalEnergy,
		      double aKineticEnergy,
		      G4ThreeVector aVertexPosition,
		      G4String aVolumeAtVertex,
		      G4int aMotherTrackId,G4int aEventId,
		      G4String aPorcessCreator,
		      G4int aDoseGeneratorId )
{ 
  theTrackId             = aTrackId;	 
  thePartName            = aPartName;
  thePDGmass             = aPDGmass;
  theCharge              = aCharge;
  theZ                   = aZ;
  theA                   = aA;
  theMomentum            = aMomentum;
  theTotalEnergy         = aTotalEnergy;
  theKineticEnergy       = aKineticEnergy;
  theVertexPosition      = aVertexPosition;	 
  theVolumeAtVertex      = aVolumeAtVertex;
  theMotherTrackId       = aMotherTrackId;
  theEventId             = aEventId;
  theProcessCreatorName  = aPorcessCreator;
  theDoseGeneratorId     = aDoseGeneratorId;
}
////////////////////////////////////////////////////////////////////////////////
//
MarsTrack::~MarsTrack ()
{}
////////////////////////////////////////////////////////////////////////////////
//
MarsTrack::MarsTrack (const MarsTrack& right) : G4VHit()
{
  theTrackId              = right.theTrackId;
  thePartName             = right.thePartName;
  thePDGmass              = right.thePDGmass;	 
  theCharge               = right.theCharge;
  theZ                    = right.theZ;
  theA                    = right.theA;
  theMomentum             = right.theMomentum;
  theTotalEnergy          = right.theTotalEnergy;
  theKineticEnergy        = right.theKineticEnergy;
  theVertexPosition       = right.theVertexPosition;	 
  theVolumeAtVertex       = right.theVolumeAtVertex;
  theMotherTrackId        = right.theMotherTrackId;
  theEventId              = right.theEventId;
  theProcessCreatorName   = right.theProcessCreatorName;
  theHitContainer         = right.theHitContainer;
  theDoseGeneratorId      = right.theDoseGeneratorId;
}
////////////////////////////////////////////////////////////////////////////////
//
const MarsTrack& MarsTrack::operator= (const MarsTrack& right)
{ 
  theTrackId              = right.theTrackId;
  thePartName             = right.thePartName;	 
  thePDGmass              = right.thePDGmass;	 
  theCharge               = right.theCharge;	 
  theZ                    = right.theZ;
  theA                    = right.theA;
  theMomentum             = right.theMomentum;
  theTotalEnergy          = right.theTotalEnergy;
  theKineticEnergy        = right.theKineticEnergy;
  theVertexPosition       = right.theVertexPosition;	 
  theVolumeAtVertex       = right.theVolumeAtVertex;
  theMotherTrackId        = right.theMotherTrackId;
  theEventId              = right.theEventId;
  theProcessCreatorName   = right.theProcessCreatorName;
  theHitContainer         = right.theHitContainer;
  theDoseGeneratorId      = right.theDoseGeneratorId;
  return *this;
}
////////////////////////////////////////////////////////////////////////////////
//
int MarsTrack::operator== (const MarsTrack& ) const
{
  return 0;
}

