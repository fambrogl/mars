#ifndef MarsRootFluenceHit_h
#define MarsRootFluenceHit_h 1
////////////////////////////////////////////////////////////////////////////////
//
#include <vector>

#include "TObject.h"
#include "TString.h"

////////////////////////////////////////////////////////////////////////////////
//
class MarsRootFluenceHit : public TObject
{
public:
  MarsRootFluenceHit ();
  MarsRootFluenceHit (TString aName, int aTarckId, int aZ, int aA, double aKinEnergy);
  
  ~MarsRootFluenceHit ();
  MarsRootFluenceHit (const MarsRootFluenceHit&);

  inline TString      GetParticleName(){return theName;}
  inline int          GetTrackId(){return theTrackId;}
  inline int          GetZ(){return theZ;}
  inline int          GetA(){return theA;}
  inline double       GetKineticEnergy(){return theKinEnergy;}
  
  ClassDef(MarsRootFluenceHit,4);

private:
  TString      theName;
  int          theTrackId;
  int          theZ;	 
  int          theA;	 
  double       theKinEnergy;

};

#endif
