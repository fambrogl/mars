#ifndef MarsTrack_h
#define MarsTrack_h 1
////////////////////////////////////////////////////////////////////////////////
//
#include "globals.hh"
#include <vector>

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"
#include "MarsRootHit.hh"

#include "G4HCofThisEvent.hh"

////////////////////////////////////////////////////////////////////////////////
//
class MarsTrack : public G4VHit
{
public:
  MarsTrack ();
  MarsTrack (G4int aTrackId, G4String aPartName, G4double aPDGmass,
	     G4int aCharge, G4int aZ, G4int aA,
	     G4ThreeVector aMomentum, 
	     G4double aTotalEnergy,G4double aKineticEnergy,
	     G4ThreeVector aVertexPosition,
	     G4String aVolumeAtVertex,
	     G4int aMotherTrackId,G4int aEventId,
	     G4String aPorcessCreator,
	     G4int aDoseGeneratorId);
  
  ~MarsTrack ();
  MarsTrack (const MarsTrack&);
  const MarsTrack& operator= (const MarsTrack&);
  int operator== (const MarsTrack&) const;

  inline void* operator new(size_t);
  inline void  operator delete(void*);
  
  inline void SetHits(std::vector<MarsRootHit> aHits){theHitContainer=aHits;}

  inline G4int          GetTrackId(){return theTrackId;}
  inline G4String       GetParticleName(){return thePartName;}
  inline G4double       GetPDGMass(){return thePDGmass;}
  inline G4double       GetCharge(){return theCharge;}
  inline G4int          GetZ(){return theZ;}
  inline G4int          GetA(){return theA;}
  inline G4ThreeVector  GetVertexPosition(){return theVertexPosition;}
  inline G4ThreeVector  GetMomentum(){return theMomentum;}
  inline G4double       GetTotalEnergy(){return theTotalEnergy;}
  inline G4double       GetKineticEnergy(){return theKineticEnergy;}
  inline G4String       GetVolumeAtVertex(){return theVolumeAtVertex;}
  inline G4int          GetMotherTrackId(){return theMotherTrackId;}
  inline G4int          GetEventId(){return theEventId;}
  inline G4String       GetProcessCreatorName(){return theProcessCreatorName;}
  inline G4int          GetDoseGeneratorId(){return theDoseGeneratorId;}
  inline std::vector<MarsRootHit> GetHits(){return theHitContainer;}
  

  void Draw () {};
  void Print () {};
  void clear () {};
  void DrawAll () {};
  void PrintAll () {};

private:
  G4int          theTrackId;	 
  G4String       thePartName;            
                                     
  G4double       thePDGmass;
  G4int          theCharge;
  G4int          theZ;
  G4int          theA;
  G4ThreeVector  theVertexPosition;	 
  G4ThreeVector  theMomentum;
  G4double       theTotalEnergy;
  G4double       theKineticEnergy;
  G4String       theVolumeAtVertex;
  G4int          theMotherTrackId;
  G4int          theEventId;
  G4String       theProcessCreatorName;
  G4int          theDoseGeneratorId;
  std::vector<MarsRootHit> theHitContainer;
};

typedef G4THitsCollection<MarsTrack> MarsTrackCollection;

extern G4Allocator<MarsTrack> MarsTrackAllocator;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

inline void* MarsTrack::operator new(size_t)
{
  void *aHit;
  aHit = (void *) MarsTrackAllocator.MallocSingle();
  return aHit;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

inline void MarsTrack::operator delete(void *aHit)
{
  MarsTrackAllocator.FreeSingle((MarsTrack*) aHit);
}


////////////////////////////////////////////////////////////////////////////////
#endif
