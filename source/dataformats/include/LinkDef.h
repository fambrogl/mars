#ifdef __CINT__

#pragma link off all  globals;
#pragma link off all  classes;
#pragma link off all  functions;

#pragma link C++ class MarsRootFluenceHit+;
#pragma link C++ class std::vector<MarsRootFluenceHit>+;
#pragma link C++ class MarsRootHit+;
#pragma link C++ class std::vector<MarsRootHit>+;
#pragma link C++ class MarsRootTrack+;
#pragma link C++ class std::vector<MarsRootTrack>+;
#pragma link C++ class MarsRootEvent+;

#endif 
