#ifndef MarsRootTrack_h
#define MarsRootTrack_h 1
////////////////////////////////////////////////////////////////////////////////
//
#include <vector>

#include "TObject.h"
#include "TString.h"
#include "TVector3.h"
#include "MarsRootHit.hh"


////////////////////////////////////////////////////////////////////////////////
//
class MarsRootTrack :  public TObject
{
public:
  MarsRootTrack ();
  MarsRootTrack (int aTrackId, TString aPartName, 
		 int aZ, int aA,
		 double aKineticEnergy,
		 TVector3 aVertexPosition,
		 TString aVolumeAtVertex,
		 TString aProcessCreator,
		 bool aFirstInteraction);

  MarsRootTrack (int aTrackId, TString aPartName,
		 int aZ, int aA,
		 double aKineticEnergy,
		 TVector3 aVertexPosition,
		 TString aVolumeAtVertex,
		 TString aProcessCreator,
		 bool aFirstInteraction,		 
		 std::vector<MarsRootHit> aHits);
  
  ~MarsRootTrack ();
  MarsRootTrack (const MarsRootTrack&);
  
  inline void SetHits(std::vector<MarsRootHit> aHits){theHitContainer=aHits;}

  inline int          GetTrackId(){return theTrackId;}
  inline TString      GetParticleName(){return thePartName;}
  inline int          GetZ(){return theZ;}
  inline int          GetA(){return theA;}
  inline double       GetKineticEnergy(){return theKineticEnergy;}
  inline TVector3     GetVertexPosition(){return theVertexPosition;}
  inline TString      GetVolumeAtVertex(){return theVolumeAtVertex;}
  inline TString      GetProcessCreator(){return theProcessCreator;}
  inline bool         GetIsFirstInteraction(){return theFirstInteraction;}
  inline std::vector<MarsRootHit> GetHits(){return theHitContainer;}
  

  ClassDef(MarsRootTrack,8);

private:

  int          theTrackId;	 
  TString      thePartName;            
                                      
  int          theZ;
  int          theA;
  double       theKineticEnergy;
  TVector3     theVertexPosition;
  TString      theVolumeAtVertex;
  TString      theProcessCreator;
  bool         theFirstInteraction;
  std::vector<MarsRootHit> theHitContainer;

};

////////////////////////////////////////////////////////////////////////////////
#endif
