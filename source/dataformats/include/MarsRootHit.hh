#ifndef MarsRootHit_h
#define MarsRootHit_h 1
////////////////////////////////////////////////////////////////////////////////
//
#include <vector>

#include "TObject.h"
#include "TString.h"

////////////////////////////////////////////////////////////////////////////////
//
class MarsRootHit : public TObject
{
public:
  MarsRootHit ();
  MarsRootHit (TString aVolumeName,int aCopyNumber,
	       double aELoss,double adEdx);
  
  ~MarsRootHit ();
  MarsRootHit (const MarsRootHit&);

  
  inline TString      GetVolumeName(){return theVolumeName;}
  inline int          GetCopyNumber(){return theCopyNumber;}
  inline double       GetELoss(){return theELoss;}
  inline double       GetdEdx(){return thedEdx;}
  
  ClassDef(MarsRootHit,7);

private:
  TString      theVolumeName;
  int          theCopyNumber;	 
  double       theELoss;	 
  double       thedEdx;	 

};

#endif
