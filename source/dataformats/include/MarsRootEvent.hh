#ifndef MarsRootEvent_h
#define MarsRootEvent_h 1
////////////////////////////////////////////////////////////////////////////////
//
#include <vector>
#include "TObject.h"
#include "MarsRootTrack.hh"
#include "MarsRootFluenceHit.hh"



////////////////////////////////////////////////////////////////////////////////
//
class MarsRootEvent : public TObject
{
public:
  MarsRootEvent ();
  MarsRootEvent (int aEventNumber, bool aHitAstronaut, std::vector<MarsRootTrack> aTracks,std::vector<MarsRootFluenceHit> aFluenceHit);
  
  ~MarsRootEvent ();
  MarsRootEvent (const MarsRootEvent&);
  
  inline int EventNumber() {return eventNumber;}
  inline bool HitAstronaut() {return hitAstronaut;}
  inline std::vector<MarsRootTrack>  GetTracks(){return theTrackCollection;}
  inline std::vector<MarsRootFluenceHit> GetFluenceHits(){return theFluenceHitCollection;}

  inline void SetEventNumber(int aEventNumber) {eventNumber=aEventNumber;}
  inline void SetFluenceHit(std::vector<MarsRootFluenceHit> aFluenceHit) {theFluenceHitCollection=aFluenceHit;}
  inline void SetHitAstronaut(bool aHitAstronaut) {hitAstronaut=aHitAstronaut;}
  inline void SetTracks(std::vector<MarsRootTrack> aTracks) {theTrackCollection=aTracks;}

  ClassDef(MarsRootEvent,6);
  
private:

  int eventNumber;
  bool hitAstronaut;
  std::vector<MarsRootTrack> theTrackCollection;
  std::vector<MarsRootFluenceHit> theFluenceHitCollection;

};

#endif
