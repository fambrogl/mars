#ifndef MarsTrackingHit_h
#define MarsTrackingHit_h 1
////////////////////////////////////////////////////////////////////////////////
//
#include "globals.hh"
#include <vector>

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"

#include "G4HCofThisEvent.hh"

////////////////////////////////////////////////////////////////////////////////
//
class MarsTrackingHit : public G4VHit
{
public:
  MarsTrackingHit ();
  MarsTrackingHit (G4String aVolumeName,G4int aCopyNumber,G4int aPDG, G4double aKinEnergy, 
		   G4double aeLoss, G4ThreeVector aPosition,
		   G4double aStepLength,G4double adEdx,G4double adG4EMEdx,
		   G4int aTrackId,G4int aEventId);
  
  ~MarsTrackingHit ();
  MarsTrackingHit (const MarsTrackingHit&);
  const MarsTrackingHit& operator= (const MarsTrackingHit&);
  int operator== (const MarsTrackingHit&) const;

  inline void* operator new(size_t);
  inline void  operator delete(void*);
  
  inline G4String       GetVolumeName(){return theVolumeName;}
  inline G4int          GetCopyNumber(){return theCopyNumber;}
  inline G4int          GetPDGCode(){return thePDG;}
  inline G4double       GetKineticEnergy(){return theKinEnergy;}
  inline G4double       GetELoss(){return theELoss;}
  inline G4double       GetdEdx(){return thedEdx;}
  inline G4double       GetG4EMdEdx(){return theG4EMdEdx;}
  inline G4ThreeVector  GetPosition(){return thePosition;}
  inline G4double       GetStepLength(){return theStepLength;}
  inline G4int          GetTrackId(){return theTrackId;}
  inline G4int          GetEventId(){return theEventId;}
  

  void Draw () {};
  void Print () {};
  void clear () {};
  void DrawAll () {};
  void PrintAll () {};

private:
  G4String       theVolumeName;
  G4int          theCopyNumber;              
  G4int          thePDG;              //Using this code we can in ROOT to the built in table and create a 
                                      //TParticlePDG that contain particleMass particleCharge 	 
  G4double       theKinEnergy;	 
  G4double       theELoss;	 
  G4ThreeVector  thePosition;	 
  G4double       theStepLength;
  G4double       thedEdx;	 
  G4double       theG4EMdEdx;	 
  G4int          theTrackId;	 
  G4int          theEventId;	 

};

typedef G4THitsCollection<MarsTrackingHit> MarsTrackingHitCollection;

extern G4Allocator<MarsTrackingHit> MarsTrackingHitAllocator;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

inline void* MarsTrackingHit::operator new(size_t)
{
  void *aHit;
  aHit = (void *) MarsTrackingHitAllocator.MallocSingle();
  return aHit;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

inline void MarsTrackingHit::operator delete(void *aHit)
{
  MarsTrackingHitAllocator.FreeSingle((MarsTrackingHit*) aHit);
}


////////////////////////////////////////////////////////////////////////////////
#endif
