#ifndef MarsFluenceHit_h
#define MarsFluenceHit_h 1
////////////////////////////////////////////////////////////////////////////////
//
#include "globals.hh"
#include <vector>

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"

#include "G4HCofThisEvent.hh"

////////////////////////////////////////////////////////////////////////////////
//
class MarsFluenceHit : public G4VHit
{
public:
  MarsFluenceHit ();
  MarsFluenceHit (G4String aName,G4int aTrackId,G4int aZ,G4int aA, G4double aKinEnergy);
  
  ~MarsFluenceHit ();
  MarsFluenceHit (const MarsFluenceHit&);
  const MarsFluenceHit& operator= (const MarsFluenceHit&);
  int operator== (const MarsFluenceHit&) const;

  inline void* operator new(size_t);
  inline void  operator delete(void*);
  
  inline G4String       GetParticleName(){return theName;}
  inline G4int          GetTrackId(){return theTrackId;}
  inline G4int          GetZ(){return theZ;}
  inline G4int          GetA(){return theA;}
  inline G4double       GetKineticEnergy(){return theKinEnergy;}

  void Draw () {};
  void Print () {};
  void clear () {};
  void DrawAll () {};
  void PrintAll () {};

private:
  G4String       theName;
  G4int          theTrackId;
  G4int          theZ;
  G4int          theA;                      
  G4double       theKinEnergy;	 

};

typedef G4THitsCollection<MarsFluenceHit> MarsFluenceHitCollection;

extern G4Allocator<MarsFluenceHit> MarsFluenceHitAllocator;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

inline void* MarsFluenceHit::operator new(size_t)
{
  void *aHit;
  aHit = (void *) MarsFluenceHitAllocator.MallocSingle();
  return aHit;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

inline void MarsFluenceHit::operator delete(void *aHit)
{
  MarsFluenceHitAllocator.FreeSingle((MarsFluenceHit*) aHit);
}


////////////////////////////////////////////////////////////////////////////////
#endif
