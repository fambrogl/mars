// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
//    **************************************
//    *                                    *
//    *    MarsGeometryConstruction.cc   *
//    *                                    *          
//    **************************************
//
#include "MarsGeometryConstruction.hh"
#include "MarsMaterial.hh"
#include "MarsGeometryMessenger.hh"
#include "MarsMagneticFieldSetup.hh"
#include "globals.hh"
#include "G4SystemOfUnits.hh"
#include "G4Material.hh"
#include "G4MaterialTable.hh"
#include "G4Box.hh"
#include "G4Sphere.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4RunManager.hh"
#include "G4Colour.hh"
#include "MarsVGeometryComponent.hh"
#include "MarsColumbusSpacecraft.hh"
#include "MarsSimplifiedSpacecraft.hh"
#include "MarsDetailedSpacecraft.hh"
#include "G4VisAttributes.hh"
#include "MarsMagneticShield.hh"
#include "MarsAstronaut.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4VPhysicalVolume.hh"
#include "NIACMagneticShield.hh"
#include "SR2SToroidalMagneticShield.hh"
#include "SR2SToroidalMagneticShieldSimplified.hh"
#include "SR2SToroidalMagneticShieldBandage.hh"
#include "SR2SOpenToroidalMagneticShieldPumpkin.hh"
#include "SR2SOpenToroidalMagneticShieldPumpkinV2.hh"
#include "SR2SSolenoidalMagneticShield.hh"
#include <iterator>
#include "G4GDMLParser.hh"
#include "G4SDManager.hh"

MarsGeometryConstruction::MarsGeometryConstruction()
  :  experimentalHall_log(0), experimentalHall_phys(0),astronautMotherVolume(0),
     spacecraft(0),magneticFieldSetup(0),astronaut(0),magneticShielding(0)
{
  pMaterial = new MarsMaterial();
  messenger = new MarsGeometryMessenger(this);

  spacecraftType="none";

  worldHalfX=15.0*m;
  worldHalfY=15.0*m;
  worldHalfZ=15.0*m;

  worldDimensions.push_back(worldHalfX);
  worldDimensions.push_back(worldHalfY);
  worldDimensions.push_back(worldHalfZ);

  magneticFieldSetup = new MarsMagneticFieldSetup();
  astronaut = new MarsAstronaut();
}

MarsGeometryConstruction::~MarsGeometryConstruction()
{  
  /*
    if (magneticShielding) delete magneticShielding;
    if (astronaut) delete astronaut;
    if (messenger) delete messenger;
    if (spacecraft) delete spacecraft;
  */
  if (pMaterial) delete pMaterial;
  if (magneticFieldSetup) delete magneticFieldSetup;
}

void MarsGeometryConstruction::SetWorldDimensions(G4double aHalfX,G4double aHalfY,G4double aHalfZ)
{
  
  if(worldHalfX!=aHalfX||worldHalfY!=aHalfY||worldHalfZ!=aHalfZ){
    worldHalfX=aHalfX;
    worldHalfY=aHalfY;
    worldHalfZ=aHalfZ;
    
    if(worldDimensions.size())
      worldDimensions.clear();
    
    worldDimensions.push_back(worldHalfX);
    worldDimensions.push_back(worldHalfY);
    worldDimensions.push_back(worldHalfZ);
    
    delete experimentalHall_log;
    delete experimentalHall_phys;
    pMaterial -> DefineMaterials();
    G4Material* vacuum = pMaterial -> GetMaterial("Galactic");
    G4Box* experimentalHall_box = new G4Box("world",worldHalfX,worldHalfY,worldHalfZ);
    experimentalHall_log = new G4LogicalVolume(experimentalHall_box,vacuum,"world");
    experimentalHall_phys = new G4PVPlacement(0,G4ThreeVector(),"world",experimentalHall_log,0,false,0);
    G4RunManager::GetRunManager()->DefineWorldVolume(experimentalHall_phys);
  }
}

G4VPhysicalVolume* MarsGeometryConstruction::Construct()
{ 
  pMaterial -> DefineMaterials();
  G4Material* vacuum = pMaterial -> GetMaterial("Galactic");
  G4Box* experimentalHall_box = new G4Box("world",worldHalfX,worldHalfY,worldHalfZ);
  experimentalHall_log = new G4LogicalVolume(experimentalHall_box,vacuum,"world");
  experimentalHall_phys = new G4PVPlacement(0,G4ThreeVector(),"world",experimentalHall_log,0,false,0);
  
  return experimentalHall_phys;
}


void MarsGeometryConstruction::ConstructVolume()
{ 
  if(spacecraftType == "none"){
    astronaut->ConstructComponent(experimentalHall_phys);
  }else{
    spacecraft->ConstructComponent(experimentalHall_phys);
    for(std::vector<G4VPhysicalVolume*>::iterator i = G4PhysicalVolumeStore::GetInstance()->begin(); i!=G4PhysicalVolumeStore::GetInstance()->end(); i++){
      if((*i)->GetName()=="spacecraftFluenceVolumePhys")
	astronautMotherVolume = (*i);
    } 
    astronaut->ConstructComponent(astronautMotherVolume);
  }
}
void MarsGeometryConstruction::BuildSpacecraftAndAstronaut()
{
  ConstructVolume();
  G4RunManager::GetRunManager()->DefineWorldVolume(experimentalHall_phys);
}


void MarsGeometryConstruction::BuildMagneticShield()
{
  if(magneticShielding != 0){
    magneticShielding->ConstructComponent(experimentalHall_phys); 
    magneticShielding->SetFieldManager(magneticFieldSetup->GetLocalFieldManager());
    G4RunManager::GetRunManager()->DefineWorldVolume(experimentalHall_phys);
  }else{
    G4cout<<" The magnetic shielding doesn't exists! Try to set up it before!!!"<<G4endl;
  }
}

void MarsGeometryConstruction::MagneticShieldConfiguration(G4String value)
{ 
  if(magneticShielding == 0){ 
    if(value == "NIAC"){
      magneticShielding = new NIACMagneticShield();
    }else if(value == "SR2SToroidal"){
      magneticShielding = new SR2SToroidalMagneticShield();
    }else if(value == "SR2SSolenoidal"){
      magneticShielding = new SR2SSolenoidalMagneticShield();
    }else if(value == "SR2SToroidalSimplified"){
      magneticShielding = new SR2SToroidalMagneticShieldSimplified();
    }else if(value == "SR2SToroidalBandage"){
      magneticShielding = new SR2SToroidalMagneticShieldBandage();
    }else if(value == "SR2SOpenToroidalPumpkin"){
      magneticShielding = new SR2SOpenToroidalMagneticShieldPumpkin(worldHalfX,worldHalfY,worldHalfZ);
    }else if(value == "SR2SOpenToroidalPumpkinV2"){
      magneticShielding = new SR2SOpenToroidalMagneticShieldPumpkinV2(worldHalfX,worldHalfY,worldHalfZ);
    }
    else if(value == "NONE"){
      G4cout<<" No magnetic shielding!"<<G4endl;
    }
  }else{
    if(value == "NONE"){
      magneticShielding->DestroyComponent(); 
      G4RunManager::GetRunManager()->DefineWorldVolume(experimentalHall_phys);
      magneticShielding = 0;
      G4cout<<" Destroing the alread existing magnetic shielding!"<<G4endl;
    }
    G4cout<<" The magnetic shielding alread exists!"<<G4endl;
  }
}

void MarsGeometryConstruction::CheckOverlaps()
{
  for(std::vector<G4VPhysicalVolume*>::iterator i = G4PhysicalVolumeStore::GetInstance()->begin(); i!=G4PhysicalVolumeStore::GetInstance()->end(); i++)
    (*i)->CheckOverlaps();
}

void MarsGeometryConstruction::SpacecraftConfiguration(G4String value)
{
  spacecraftType = value;
  if(spacecraft == 0){ 
    if(value == "simplified") 
      spacecraft = new MarsSimplifiedSpacecraft();
    else if(value == "detailed")
      spacecraft = new MarsDetailedSpacecraft();
    else if(value == "columbus")
      spacecraft = new MarsColumbusSpacecraft();
  }else 
    G4cout<< "The configurations can not be switched" << G4endl;
}
