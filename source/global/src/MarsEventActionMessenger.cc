//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
//$Id: MarsEventActionMessenger.cc,v 1.5 2006-06-29 16:24:27 gunter Exp $// GEANT4 tag $Name: not supported by cvs2svn $
//
// Code developed by: S.Guatelli, guatelli@ge.infn.it
//

#include "MarsEventActionMessenger.hh"
#include "MarsEventAction.hh"

#include "globals.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWithAnInteger.hh"

MarsEventActionMessenger::MarsEventActionMessenger(MarsEventAction* EA)
:eventAction(EA)
{
  eventDirectory = new G4UIdirectory("/event/");

  timerCmd = new G4UIcmdWithoutParameter("/event/timerVerbose",this);
  timerCmd -> SetGuidance("Activate the timer");
  timerCmd -> AvailableForStates(G4State_PreInit,G4State_Idle); 
  
  printModuleCmd = new G4UIcmdWithAnInteger("/event/printModul",this);
  printModuleCmd -> SetGuidance("Set the print module event based");
  printModuleCmd -> SetParameterName("printModul", true);
  printModuleCmd -> SetDefaultValue(500);
  printModuleCmd -> AvailableForStates(G4State_PreInit,G4State_Idle); 

}


MarsEventActionMessenger::~MarsEventActionMessenger()
{
  delete printModuleCmd;
  delete timerCmd;
  delete eventDirectory;
}

void MarsEventActionMessenger::SetNewValue(G4UIcommand* command, G4String newValue)
{
  if(command == timerCmd) 
    eventAction -> SetTimer();
  if(command == printModuleCmd) 
    eventAction -> SetPrintModul(printModuleCmd->GetNewIntValue(newValue));
}

