//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// Code developed by:
//  S.Guatelli
//
//    *********************************
//    *                               *
//    *    MarsGeometryMessenger.cc *
//    *                               *
//    *********************************
//
//
// $Id$
//
// 

#include "MarsGeometryMessenger.hh"
#include "MarsGeometryConstruction.hh"
#include "G4UIdirectory.hh"
#include "G4UIparameter.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcommand.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UnitsTable.hh"

MarsGeometryMessenger::MarsGeometryMessenger( MarsGeometryConstruction* Geo): geometry(Geo)
{ 
  geometryConfigDir = new G4UIdirectory("/configuration/");
  geometryConfigDir->SetGuidance("geometry control.");
  
  G4UIparameter* gdmlModeParam = new G4UIparameter("gdmlMode",'s',false);
  gdmlModeParam->SetDefaultValue("Read");
  gdmlModeParam->SetParameterCandidates("Read Write");
  
  worldDimensionCmd = new G4UIcommand("/configuration/SetWorldDimension",this);
  worldDimensionCmd->SetGuidance("Set the dimension of the world specifing the half X,Y,Z dimension and the unit");
  worldDimensionCmd->SetGuidance("[usage] /configuration/SetWorldDimension HalfX HalfY HalfZ length_unit");
  param = new G4UIparameter("HalfX",'d',false);
  worldDimensionCmd->SetParameter(param);
  param = new G4UIparameter("HalfY",'d',false);
  worldDimensionCmd->SetParameter(param);
  param = new G4UIparameter("HalfZ",'d',false);
  worldDimensionCmd->SetParameter(param);
  param = new G4UIparameter("length_unit",'s',false);
  worldDimensionCmd->SetParameter(param);
  worldDimensionCmd->AvailableForStates(G4State_Idle);

  checkOverlapsCmd = new G4UIcmdWithoutParameter("/configuration/checkoverlaps",this);
  checkOverlapsCmd->SetGuidance("Perform the checkoverlaps test over all the logical volume."); 
  checkOverlapsCmd->AvailableForStates(G4State_Idle);
  
  spacecraftCmd = new G4UIcmdWithAString("/configuration/Spacecraft",this);
  spacecraftCmd->SetGuidance("Assign the geometrical set-up to G4RunManager."); 
  spacecraftCmd->SetParameterName("type",true);
  spacecraftCmd->SetCandidates("simplified detailed columbus none");
  spacecraftCmd->AvailableForStates(G4State_Idle);

  magneticShieldingCmd =  new G4UIcmdWithAString("/configuration/MagneticShield",this); 
  magneticShieldingCmd->SetGuidance("Add the magnetic shielding structure."); 
  magneticShieldingCmd->SetParameterName("choice",true);
  magneticShieldingCmd->SetCandidates("NIAC SR2SToroidal SR2SSolenoidal SR2SToroidalSimplified SR2SToroidalBandage SR2SOpenToroidalPumpkin SR2SOpenToroidalPumpkinV2 NONE");
  magneticShieldingCmd->AvailableForStates(G4State_Idle); 

  buildMagneticShieldCmd = new G4UIcmdWithoutParameter("/configuration/BuildMagneticShield",this);
  buildMagneticShieldCmd->SetGuidance("Build the Magnetic Shield");
  buildMagneticShieldCmd->AvailableForStates(G4State_Idle);

  buildSpacecraftAndAstronautCmd = new G4UIcmdWithoutParameter("/configuration/BuildSpacecraftAndAstronaut",this);
  buildSpacecraftAndAstronautCmd->SetGuidance("Built the Astronaut and the Spacecraft if any ");
  buildSpacecraftAndAstronautCmd->AvailableForStates(G4State_Idle);
}

MarsGeometryMessenger::~MarsGeometryMessenger()
{
  delete geometryConfigDir; 
  delete worldDimensionCmd;
  delete checkOverlapsCmd;
  delete spacecraftCmd;
  delete magneticShieldingCmd; 
  delete buildMagneticShieldCmd;
  delete buildSpacecraftAndAstronautCmd;
}

void MarsGeometryMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 
  if(command == spacecraftCmd)
    geometry->SpacecraftConfiguration(newValue); 
  if(command == checkOverlapsCmd)
    geometry->CheckOverlaps();
  if(command == magneticShieldingCmd)
    geometry->MagneticShieldConfiguration(newValue); 
  if(command==worldDimensionCmd){
    const char* paramString;
    paramString=newValue;
    G4double DimX,DimY,DimZ;
    G4String length_unit;
    std::istringstream is((char*)paramString);
    is>>DimX>>DimY>>DimZ>>length_unit;
    DimX*=G4UnitDefinition::GetValueOf(length_unit);
    DimY*=G4UnitDefinition::GetValueOf(length_unit);
    DimZ*=G4UnitDefinition::GetValueOf(length_unit);
    geometry->SetWorldDimensions(DimX/2.,DimY/2.,DimZ/2.);
  }
  if(command == buildMagneticShieldCmd)
    geometry->BuildMagneticShield();
  if(command == buildSpacecraftAndAstronautCmd)
    geometry->BuildSpacecraftAndAstronaut();
}

