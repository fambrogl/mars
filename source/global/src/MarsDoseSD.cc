#include "MarsDoseSD.hh"
#include "G4Step.hh"
#include "G4HCofThisEvent.hh"
#include "G4Track.hh"
#include "G4RunManager.hh"
#include "G4SDManager.hh"
#include "G4ios.hh"
#include "G4ParticleDefinition.hh"
#include "G4VProcess.hh"
#include "G4SDManager.hh"
#include "G4EmCalculator.hh"

//#include "G4ProcessTable.hh"
class G4Step;

MarsDoseSD::MarsDoseSD(G4String name)
  :G4VSensitiveDetector(name)
{ 
  collectionName.insert("TrackingHitCollection");
  collectionName.insert("TrackCollection");
}
////////////////////////////////////////////////////////////////////////////////
//
MarsDoseSD::~MarsDoseSD()
{
}
////////////////////////////////////////////////////////////////////////////////
//
void MarsDoseSD::Initialize(G4HCofThisEvent*HCE)
{ 
  static int HCID = -1;

  TrackingHitCollection = new MarsTrackingHitCollection
    (SensitiveDetectorName,collectionName[0]);

  TrackCollection = new MarsTrackCollection
    (SensitiveDetectorName,collectionName[1]);
  
  HCID = GetCollectionID(0);
  HCE->AddHitsCollection(HCID,TrackingHitCollection); 

  HCID = GetCollectionID(1);
  HCE->AddHitsCollection(HCID,TrackCollection); 

}
////////////////////////////////////////////////////////////////////////////////
//
G4bool MarsDoseSD::ProcessHits(G4Step*aStep,G4TouchableHistory*)
{

  G4TouchableHandle theTouchable = aStep->GetPreStepPoint()->GetTouchableHandle();
  G4int copyNumber = 0;
  for(int i = 1; i<4;i++){
    if(theTouchable->GetVolume(i)->GetName()=="astronaut"){
      copyNumber = theTouchable->GetCopyNumber(i);
      break;
    }
  }

  G4EmCalculator emCalc;
  const G4Region* astroRegion = emCalc.FindRegion("Astronaut");
  G4String volumeName = aStep->GetTrack()->GetVolume()->GetName(); 
  G4Material* material = aStep->GetTrack()->GetVolume()->GetLogicalVolume()->GetMaterial();
  const G4ParticleDefinition* particle = aStep->GetTrack()->GetParticleDefinition();
  G4int pdg = particle->GetPDGEncoding();
  G4double kinE = aStep->GetTrack()->GetKineticEnergy();
  G4double eLoss = aStep->GetTotalEnergyDeposit();
  G4ThreeVector position = aStep->GetPreStepPoint()->GetPosition();
  G4double stepLength = aStep->GetStepLength();
  G4int TrackId = aStep->GetTrack()->GetTrackID();
  G4int eventId = G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID();
  G4double dedx = 0;
  G4double emCalc_dedx = 0;
  if(particle->GetPDGCharge()!= 0.) {
    if(stepLength==0)
      dedx = 0;
    else
      dedx = eLoss/stepLength;
    emCalc_dedx     = emCalc.GetDEDX(kinE,particle,material,astroRegion);
  }
  MarsTrackingHit* aHit = new MarsTrackingHit(volumeName,copyNumber,pdg,kinE,eLoss,position,stepLength,dedx,emCalc_dedx,TrackId,eventId);
  TrackingHitCollection->insert(aHit);
  /*
  std::cout<<"MarsDoseSD::ProcessHits BEGIN"<<std::endl;
  std::cout<<"volumeName =  "<<volumeName<<std::endl;
  std::cout<<"copyNumber =  "<<copyNumber<<std::endl;
  std::cout<<"pdg =  "<<pdg<<std::endl;
  std::cout<<"ELoss = "<<eLoss<<std::endl;
  std::cout<<"stepLength = "<<stepLength<<std::endl;
  std::cout<<"dEdx =  "<<dedx<<std::endl;
  std::cout<<"emCalc_dedx = "<<emCalc_dedx<<std::endl;
  std::cout<<"MarsDoseSD::ProcessHits END"<<std::endl;
  */
  return true;   
}  
////////////////////////////////////////////////////////////////////////////////
//
void MarsDoseSD::EndOfEvent(G4HCofThisEvent*)
{
}
