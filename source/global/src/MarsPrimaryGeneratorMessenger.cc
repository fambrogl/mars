//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// Code developed by:  S.Guatelli, guatelli@ge.infn.it
//
//    *****************************************
//    *                                       *
//    *    MarsPrimaryGeneratorMessenger.cc *
//    *                                       *
//    *****************************************
//
//
// $Id$
//
// 

#include "MarsPrimaryGeneratorMessenger.hh"
#include "MarsPrimaryGeneratorAction.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcommand.hh"
#include "G4UIparameter.hh"

MarsPrimaryGeneratorMessenger::MarsPrimaryGeneratorMessenger( MarsPrimaryGeneratorAction* prim): primary(prim)
{  

  G4UIparameter* param;
  
  gunDir = new G4UIdirectory("/gun/");
  gunDir->SetGuidance("Select the generation configuration of primary particles.");
 
  dataCmd = new G4UIcmdWithAString("/gun/data",this);
  dataCmd->SetGuidance("Primary particle spectrum"); 
  dataCmd->SetParameterName("choice",true);
  dataCmd->AvailableForStates(G4State_PreInit,G4State_Idle); 

  customDataCmd = new G4UIcmdWithAString("/gun/customData",this);
  customDataCmd->SetGuidance("Custom Primary particle spectrum"); 
  customDataCmd->SetParameterName("choice",true);
  customDataCmd->AvailableForStates(G4State_PreInit,G4State_Idle); 

  zminzmaxCmd = new G4UIcommand("/gun/ZminZmax",this);
  zminzmaxCmd->SetGuidance("Set the range of Ions Z N.B. in case of a single ion Zmin must be equal Zmax i.e. Zmin = 15 Zmax = 15"); 
  param = new G4UIparameter("ZMin",'i',false);
  zminzmaxCmd->SetParameter(param);
  param = new G4UIparameter("ZMax",'i',false);
  zminzmaxCmd->SetParameter(param);
  zminzmaxCmd->AvailableForStates(G4State_PreInit,G4State_Idle); 

  generationPlaneCmd = new G4UIcommand("/gun/SelectGenerationPlane",this);
  generationPlaneCmd->SetGuidance("Set the plane where generate the particle"); 
  generationPlaneCmd->SetGuidance("[usage] /gun/SelectGenerationPlane X+ X- Y+ Y- Z+ Z- [bool] "); 
  param = new G4UIparameter("X+",'B',false);
  generationPlaneCmd->SetParameter(param);
  param = new G4UIparameter("X-",'B',false);
  generationPlaneCmd->SetParameter(param);
  param = new G4UIparameter("Y+",'B',false);
  generationPlaneCmd->SetParameter(param);
  param = new G4UIparameter("Y-",'B',false);
  generationPlaneCmd->SetParameter(param);
  param = new G4UIparameter("Z+",'B',false);
  generationPlaneCmd->SetParameter(param);
  param = new G4UIparameter("Z-",'B',false);
  generationPlaneCmd->SetParameter(param);
  generationPlaneCmd->AvailableForStates(G4State_PreInit,G4State_Idle); 

  directionCmd = new G4UIcmdWithoutParameter("/gun/point_to_center",this);
  directionCmd->SetGuidance("Generate particle pointing to center of world volume");
  directionCmd->AvailableForStates(G4State_PreInit,G4State_Idle); 
  
 }

MarsPrimaryGeneratorMessenger::~MarsPrimaryGeneratorMessenger()
{
  delete dataCmd;
  delete gunDir;
} 
 
void MarsPrimaryGeneratorMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 
 if (command == dataCmd) 
   primary->ReadProbability(newValue);
 else if(command == customDataCmd) 
   primary->ReadCustomProbability(newValue);
 else if (command == zminzmaxCmd){
   const char* paramString=newValue;
   G4int zmin,zmax;
   std::istringstream is((char*)paramString);
   is >>zmin>>zmax;
   primary->SetZminZmax(zmin,zmax);
 }
 else if (command == generationPlaneCmd){
   const char* paramString=newValue;
   G4bool Xp,Xm,Yp,Ym,Zp,Zm;
   std::istringstream is((char*)paramString);
   is >>Xp>>Xm>>Yp>>Ym>>Zp>>Zm;
   primary->SetGenerationPlane(Xp,Xm,Yp,Ym,Zp,Zm);
 }
 else if (command == directionCmd) primary->SetDirectionToCenter();
}

