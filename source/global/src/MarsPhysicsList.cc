//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id$
//
// Author: Susanna Guatelli, susanna@uow.edu.au

#include "MarsPhysicsList.hh"
#include "MarsPhysicsListMessenger.hh"

#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"

#include "ParticlesBuilder.hh"

#include "G4EmStandardPhysics.hh"
#include "G4EmStandardPhysics_option1.hh"
#include "G4EmStandardPhysics_option2.hh"
#include "G4EmStandardPhysics_option3.hh"
#include "G4EmStandardPhysics_option4.hh"
#include "G4EmLivermorePhysics.hh"
#include "G4EmPenelopePhysics.hh"

#include "G4EmExtraPhysics.hh"
#include "G4DecayPhysics.hh"
#include "G4RadioactiveDecayPhysics.hh"

#include "G4IonPhysics.hh"
#include "G4IonQMDPhysics.hh"
#include "G4IonINCLXXPhysics.hh"

#include "G4HadronElasticPhysics.hh"
#include "G4HadronElasticPhysicsHP.hh"
#include "G4HadronElasticPhysicsXS.hh"

#include "G4HadronPhysicsFTFP_BERT.hh"
#include "G4HadronPhysicsFTFP_BERT_HP.hh"
#include "G4HadronPhysicsFTFP_BERT_TRV.hh"
#include "G4HadronPhysicsQGSP_BERT.hh"
#include "G4HadronPhysicsQGSP_BERT_HP.hh"
#include "G4HadronPhysicsQGSP_BIC.hh"
#include "G4HadronPhysicsQGSP_BIC_HP.hh"
#include "G4HadronInelasticQBBC.hh"
#include "G4HadronPhysicsShielding.hh"

#include "G4StoppingPhysics.hh"

#include "G4PhysListFactory.hh"

#include "G4Region.hh"
#include "G4RegionStore.hh"

#include "G4StepLimiterPhysics.hh"

#include "G4LossTableManager.hh"
#include "G4EmProcessOptions.hh"
#include "G4EmConfigurator.hh"

MarsPhysicsList::MarsPhysicsList(): G4VModularPhysicsList(){

  helIsRegisted = false;
  bicIsRegisted = false;
  ionIsRegisted = false;
  gnucIsRegisted= false;
  lenIsRegisted = false;
  stopIsRegisted= false;
  radDecayIsRegisted = false;

  messenger = new MarsPhysicsListMessenger(this);

  SetVerboseLevel(1);

  G4LossTableManager* man = G4LossTableManager::Instance();
  man->SetVerbose(1);
  em_config = man->EmConfigurator();

  defaultCutValue = 0.1*mm;
  cutForGamma     = defaultCutValue;
  cutForElectron  = defaultCutValue;
  cutForPositron  = defaultCutValue;
  cutForProton    = defaultCutValue;

  productionCutsEnergyRangeLow  = 0.99 * keV;
  productionCutsEnergyRangeHigh = 100. * TeV;

  // EM physics
  emName = G4String("emstandard_opt4");
  emPhysicsList = new G4EmStandardPhysics_option4(verboseLevel);

  // Decay physics
  //
  decay_List = new G4DecayPhysics();
  radioactiveDecay_List = new G4RadioactiveDecayPhysics();
}


MarsPhysicsList::~MarsPhysicsList()
{
  delete messenger;
  delete emPhysicsList;
  delete decay_List;  
  delete radioactiveDecay_List;
  hadronPhys.clear();
  for(size_t i=0; i<hadronPhys.size(); i++){
    delete hadronPhys[i];
  }
}

/////////////////////////////////////////////////////////////////////////////
void MarsPhysicsList::ConstructParticle(){
  if(verboseLevel > 0) { G4cout << "[MarsPhysicsList::ConstructParticle]" << G4endl; }
  decay_List->ConstructParticle();
}

void MarsPhysicsList::ConstructProcess(){
  if(verboseLevel > 0) { G4cout << "[MarsPhysicsList::ConstructProcess]" << G4endl; }

  AddTransportation();

  emPhysicsList->ConstructProcess();
  em_config->AddModels();

  decay_List->ConstructProcess();
  
  for(size_t i=0; i < hadronPhys.size(); i++){
    hadronPhys[i] -> ConstructProcess();
  }
  
  return;
}

void MarsPhysicsList::AddPackage(const G4String& name)
{
  G4PhysListFactory factory;
  G4VModularPhysicsList* phys =factory.GetReferencePhysList(name);
  G4int i=0;
  const G4VPhysicsConstructor* elem= phys->GetPhysics(i);
  G4VPhysicsConstructor* tmp = const_cast<G4VPhysicsConstructor*> (elem);
  while (elem !=0)
	{
	  RegisterPhysics(tmp);
	  elem= phys->GetPhysics(++i) ;
	  tmp = const_cast<G4VPhysicsConstructor*> (elem);
	}
}

void MarsPhysicsList::AddPhysicsList(const G4String& name){
  if(name == emName) { return; }  
  if (verboseLevel>1) {
    G4cout << "MarsPhysicsList::AddPhysicsList: <" << name << ">" << G4endl;
  }

  if (name == "em_standard") {
    emName = name;
    delete emPhysicsList;
    emPhysicsList = new G4EmStandardPhysics(verboseLevel);
  }  else if (name == "em_standard_opt1") {
    emName = name;
    delete emPhysicsList;
    emPhysicsList = new G4EmStandardPhysics_option1(verboseLevel);
  } else if (name == "em_standard_opt2") {
    emName = name;
    delete emPhysicsList;
    emPhysicsList = new G4EmStandardPhysics_option2(verboseLevel);
  } else if (name == "em_standard_opt3") {
    emName = name;
    delete emPhysicsList;
    emPhysicsList = new G4EmStandardPhysics_option3(verboseLevel);
  } else if (name == "em_standard_opt4") {
    emName = name;
    delete emPhysicsList;
    emPhysicsList = new G4EmStandardPhysics_option4(verboseLevel);
  } else if (name == "em_livermore") {
    emName = name;
    delete emPhysicsList;
    emPhysicsList = new G4EmLivermorePhysics(verboseLevel);
  } else if (name == "em_penelope") {
    emName = name;
    delete emPhysicsList;
    emPhysicsList = new G4EmPenelopePhysics(verboseLevel);
  }  else if (name == "binary" || name == "binary_had") {
    AddDecayIonsStoppingGN(name,"binary_ion");
    if (!helIsRegisted) {
      hadronPhys.push_back(new G4HadronElasticPhysics(verboseLevel));
      helIsRegisted = true;
    }
  } else if (name == "binary_hp") {
    AddDecayIonsStoppingGN(name,"binary_ion");
    if (!helIsRegisted) {
      hadronPhys.push_back(new G4HadronElasticPhysicsHP(verboseLevel));
      helIsRegisted = true;
    }
  } else if (name == "bertini") {
    AddDecayIonsStoppingGN(name,"binary_ion");
    if (!helIsRegisted) {
      hadronPhys.push_back(new G4HadronElasticPhysics(verboseLevel));
      helIsRegisted = true;
    }
  } else if (name == "bertini_hp") {
    AddDecayIonsStoppingGN(name,"binary_ion");
    if (!helIsRegisted) {
      hadronPhys.push_back(new G4HadronElasticPhysicsHP(verboseLevel));
      helIsRegisted = true;
    }
  } else if (name == "bertini_preco") {
    AddDecayIonsStoppingGN(name,"binary_ion");
    if (!helIsRegisted) {
      hadronPhys.push_back(new G4HadronElasticPhysics(verboseLevel));
      helIsRegisted = true;
    }
  } else if (name == "FTFP_BERT") {
    AddDecayIonsStoppingGN(name,"binary_ion");
    if(!helIsRegisted) {
      hadronPhys.push_back(new G4HadronElasticPhysics(verboseLevel));
      helIsRegisted = true;
    }
    if(!bicIsRegisted) {
      hadronPhys.push_back(new G4HadronPhysicsFTFP_BERT(verboseLevel));
      bicIsRegisted = true;
    }

  } else if (name == "FTFP_BERT_HP") {
    AddDecayIonsStoppingGN(name,"binary_ion");
    if(!helIsRegisted) {
      hadronPhys.push_back(new G4HadronElasticPhysicsHP(verboseLevel));
      helIsRegisted = true;
    }
    if(!bicIsRegisted) {
      hadronPhys.push_back(new G4HadronPhysicsFTFP_BERT_HP(verboseLevel));
      bicIsRegisted = true;
    }

  } else if (name == "FTFP_BERT_TRV") {
    AddDecayIonsStoppingGN(name,"binary_ion");
    if(!helIsRegisted) {
      hadronPhys.push_back(new G4HadronElasticPhysics(verboseLevel));
      helIsRegisted = true;
    }
    if(!bicIsRegisted) {
      hadronPhys.push_back(new G4HadronPhysicsFTFP_BERT_TRV(verboseLevel));
      bicIsRegisted = true;
    }

  } else if (name == "QBBC") {
    if(!helIsRegisted) {
      hadronPhys.push_back(new G4HadronElasticPhysicsXS(verboseLevel));
      helIsRegisted = true;
    }
    if(!bicIsRegisted) {
      AddDecayIonsStoppingGN(name,"binary_ion");
      hadronPhys.push_back(new G4HadronInelasticQBBC(verboseLevel));
      bicIsRegisted = true;
    }

  } else if (name == "QGSP_BERT") {
    AddDecayIonsStoppingGN(name,"binary_ion");
    if(!helIsRegisted) {
      hadronPhys.push_back(new G4HadronElasticPhysics(verboseLevel));
      helIsRegisted = true;
    }
    if(!bicIsRegisted) {
      hadronPhys.push_back(new G4HadronPhysicsQGSP_BERT(verboseLevel));
      bicIsRegisted = true;
    }

  } else if (name == "QGSP_BERT_HP") {
    AddDecayIonsStoppingGN(name,"binary_ion");
    if(!helIsRegisted) {
      hadronPhys.push_back(new G4HadronElasticPhysicsHP(verboseLevel));
      helIsRegisted = true;
    }
    if(!bicIsRegisted) {
      hadronPhys.push_back(new G4HadronPhysicsQGSP_BERT_HP(verboseLevel));
      bicIsRegisted = true;
    }

  } else if (name == "QGSP_BIC") {
    AddDecayIonsStoppingGN(name,"binary_ion");
    if(!helIsRegisted) {
      hadronPhys.push_back(new G4HadronElasticPhysics(verboseLevel));
      helIsRegisted = true;
    }
    if(!bicIsRegisted) {
      hadronPhys.push_back(new G4HadronPhysicsQGSP_BIC(verboseLevel));
      bicIsRegisted = true;
    }

  } else if (name == "QGSP_BIC_HP") {
    AddDecayIonsStoppingGN(name,"binary_ion");
    if(!helIsRegisted) {
      hadronPhys.push_back(new G4HadronElasticPhysicsHP(verboseLevel));
      helIsRegisted = true;
    }
    if(!bicIsRegisted) {
      hadronPhys.push_back(new G4HadronPhysicsQGSP_BIC_HP(verboseLevel));
      bicIsRegisted = true;
    }

  } else if (name == "QGSP_QMD_HP") {
    if(!helIsRegisted) {
      hadronPhys.push_back(new G4HadronElasticPhysicsHP(verboseLevel));
      helIsRegisted = true;
    }
    if(!ionIsRegisted) {
      hadronPhys.push_back(new G4IonQMDPhysics(verboseLevel));
      ionIsRegisted = true;
    }
  } else if (name == "Shielding") {
    if(!helIsRegisted) {
      hadronPhys.push_back(new G4HadronElasticPhysicsHP(verboseLevel));
      helIsRegisted = true;
    }
    if(!ionIsRegisted) {
      hadronPhys.push_back(new G4IonQMDPhysics(verboseLevel));
      ionIsRegisted = true;
    }
    if(!bicIsRegisted) {
      // The ion component will not be use in this call as it is 
      // already declared above
      AddDecayIonsStoppingGN(name,"ion");
      hadronPhys.push_back(new G4HadronPhysicsShielding(verboseLevel));
      bicIsRegisted = true;
    }
    if(!radDecayIsRegisted) {
      hadronPhys.push_back(new G4RadioactiveDecayPhysics(verboseLevel));
      radDecayIsRegisted = true;
    }
  } else if (name == "stopping") {
    if(!stopIsRegisted) {
      hadronPhys.push_back(new G4StoppingPhysics(verboseLevel));
      stopIsRegisted = true;
      if(verboseLevel>0) {
	G4cout << "PhysicsList::AddPhysicsList <" << name << ">" << G4endl;
      }
    }

  } else if (name == "elastic") {
    if(!helIsRegisted) {
      hadronPhys.push_back(new G4HadronElasticPhysics(verboseLevel));
      helIsRegisted = true;
      if(verboseLevel>0) {
	G4cout << "PhysicsList::AddPhysicsList <" << name << ">" << G4endl;
      }
    }

  } else if (name == "elasticHP") {
    if(!helIsRegisted) {
      hadronPhys.push_back(new G4HadronElasticPhysicsHP(verboseLevel));
      helIsRegisted = true;
      if(verboseLevel>0) {
	G4cout << "PhysicsList::AddPhysicsList <" << name << ">" << G4endl;
      }
    }

  } else if (name == "binary_ion") {
    if(!ionIsRegisted) {
      hadronPhys.push_back(new G4IonPhysics(verboseLevel));
      ionIsRegisted = true;
      if(verboseLevel>0) {
	G4cout << "PhysicsList::AddPhysicsList <" << name << ">" << G4endl;
      }
    }
  } else if (name == "qmd_ion") {
    if(!ionIsRegisted) {
      hadronPhys.push_back(new G4IonQMDPhysics(verboseLevel));
      ionIsRegisted = true;
      if(verboseLevel>0) {
	G4cout << "PhysicsList::AddPhysicsList <" << name << ">" << G4endl;
      }
    }
  } else if (name == "incl_ion") {
    if(!ionIsRegisted) {
      hadronPhys.push_back(new G4IonINCLXXPhysics(verboseLevel));
      ionIsRegisted = true;
      if(verboseLevel>0) {
	G4cout << "PhysicsList::AddPhysicsList <" << name << ">" << G4endl;
      }
    }
  } else if (name == "gamma_nuc") {
    if(!gnucIsRegisted) {
      hadronPhys.push_back(new G4EmExtraPhysics(verboseLevel));
      gnucIsRegisted = true;
      if(verboseLevel>0) {
	G4cout << "PhysicsList::AddPhysicsList <" << name << ">" << G4endl;
      }
    }
  } else {
    G4cout << "PhysicsList::AddPhysicsList ERROR: <" << name << ">"
	   << " fail - module is unknown " << G4endl;
  }
}

void MarsPhysicsList::SetCuts() {
  G4ProductionCutsTable::GetProductionCutsTable()->
    SetEnergyRange(productionCutsEnergyRangeLow, productionCutsEnergyRangeHigh);
  
  SetCutValue(cutForGamma, "gamma");
  SetCutValue(cutForElectron, "e-");
  SetCutValue(cutForPositron, "e+");
  SetCutValue(cutForProton, "proton");
  
  if (verboseLevel>0) DumpCutValuesTable();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void MarsPhysicsList::SetCutForGamma(G4double cut)
{
  cutForGamma = cut;
  SetParticleCuts(cutForGamma, G4Gamma::Gamma());
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void MarsPhysicsList::SetCutForElectron(G4double cut)
{
  cutForElectron = cut;
  SetParticleCuts(cutForElectron, G4Electron::Electron());
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void MarsPhysicsList::SetCutForPositron(G4double cut)
{
  cutForPositron = cut;
  SetParticleCuts(cutForPositron, G4Positron::Positron());
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void MarsPhysicsList::SetCutForProton(G4double cut)
{
  cutForProton = cut;
  SetParticleCuts(cutForProton, G4Proton::Proton());
}


void MarsPhysicsList::AddDecayIonsStoppingGN(const G4String& name, const G4String& ion)
{
  if (!gnucIsRegisted) {
    hadronPhys.push_back(new G4EmExtraPhysics(verboseLevel));
    gnucIsRegisted = true;
  }
  if (!ionIsRegisted) {
    if(ion == "ion_incl") { hadronPhys.push_back(new G4IonINCLXXPhysics(verboseLevel)); }
    else { hadronPhys.push_back(new G4IonPhysics(verboseLevel)); }
    ionIsRegisted = true;
  }
  if (!stopIsRegisted) {
    hadronPhys.push_back(new G4StoppingPhysics(verboseLevel));
    stopIsRegisted = true;
  }
  if(verboseLevel>0) {
    G4cout << "MarsPhysicsList::AddPhysicsList <" << name << ">" << G4endl;
  }
}

void MarsPhysicsList::SetProductionCutsLowestEnergy(G4double low)
{
  productionCutsEnergyRangeLow  = low; 
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void MarsPhysicsList::SetVerbose(G4int aVal)
{
  SetVerboseLevel(aVal);
}



void MarsPhysicsList::SetProductionCutsAstronautRegion(G4double prodCuts){
  
  G4Region* region;
  G4String regName;
  
  G4ProductionCuts* cuts;
  cuts = new G4ProductionCuts;
  //cuts->SetProductionCut(prodCuts);
  cuts->SetProductionCut(prodCuts,G4Gamma::Gamma());
  cuts->SetProductionCut(prodCuts,G4Electron::Electron());
  cuts->SetProductionCut(prodCuts,G4Positron::Positron());
  cuts->SetProductionCut(prodCuts,G4Proton::Proton());
  regName = "Astronaut";
  region = G4RegionStore::GetInstance()->GetRegion(regName);
  region->SetProductionCuts(cuts);
  
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......



