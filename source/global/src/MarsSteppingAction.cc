//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// $Id$
//
//
// Author: Susanna Guatelli (susanna@uow.edu.au)
//

#include "MarsSteppingAction.hh"
#include "MarsPrimaryGeneratorAction.hh"
#include "MarsSteppingActionMessenger.hh"
#include "MarsAnalysis.hh"

#include "G4ios.hh"
#include "G4SystemOfUnits.hh"
#include "G4SteppingManager.hh"
#include "G4Step.hh"
#include "G4Track.hh"
#include "G4StepPoint.hh"
#include "G4ParticleDefinition.hh"
#include "G4VPhysicalVolume.hh"
#include "G4TrackStatus.hh"

MarsSteppingAction::MarsSteppingAction(MarsPrimaryGeneratorAction* primary):
  primaryAction(primary)
{
  maxTrackLength=10000*m;
  messenger = new MarsSteppingActionMessenger(this);
}

MarsSteppingAction::~MarsSteppingAction()
{
  delete messenger;
}

void MarsSteppingAction::UserSteppingAction(const G4Step* aStep)
{ 

  if(aStep->GetTrack()->GetTrackLength()>maxTrackLength){
    aStep->GetTrack()->SetTrackStatus(fStopAndKill);
    G4cout<<"Stop  at track length = "<<aStep->GetTrack()->GetTrackLength()/m<< " m"<<std::endl;
    return;
  }
  
  /*
  G4String volumeName = aStep->GetTrack()->GetVolume()->GetName();
  G4int pdg = aStep->GetTrack()->GetParticleDefinition()->GetPDGEncoding();
  G4double dEdx = aStep->GetTotalEnergyDeposit();
  G4double stepLength = aStep->GetStepLength();
  std::cout<<"MarsSteppingAction::UserSteppingAction BEGIN"<<std::endl;  
  std::cout<<"volumeName =  "<<volumeName<<std::endl;
  std::cout<<"pdg =  "<<pdg<<std::endl;
  std::cout<<"dEdx =  "<<dEdx<<std::endl;
  std::cout<<"stepLength =  "<<stepLength<<std::endl;
  std::cout<<"MarsSteppingAction::UserSteppingAction END"<<std::endl;   
  */
}
 
