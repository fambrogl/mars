//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// $Id: MarsPrimaryGeneratorAction.cc,v 1.17 2006-06-29 16:24:09 gunter Exp $
// Author: Filippo Ambroglini, filippo.ambroglini@pg.infn.it
#include <iostream>
#include <cmath>
#include <algorithm>

#include "MarsPrimaryGeneratorAction.hh"
#include "MarsPrimaryGeneratorMessenger.hh"

#include "MarsGeometryConstruction.hh"
#include "G4SystemOfUnits.hh"
#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4IonTable.hh"
#include "G4ParticleDefinition.hh"
#include "Randomize.hh"
#include "G4DataVector.hh"

MarsPrimaryGeneratorAction::MarsPrimaryGeneratorAction():geometry(0)
{
  readFile = false;
  
  useZplus=true;
  useZminus=true;
  useXplus=true;
  useXminus=true;
  useYplus=true;
  useYminus=true;
  XplusLimit=1;
  XminusLimit=2;
  YplusLimit=3;
  YminusLimit=4;
  ZplusLimit=5;
  ZminusLimit=6;
  
  baryon = 0;
  
  centerpointing=false;
  
  particleGun = new G4ParticleGun(1);
  messenger = new MarsPrimaryGeneratorMessenger(this);
  particleTable = G4ParticleTable::GetParticleTable();

  G4String ParticleName = "proton";
  G4ParticleDefinition* particle = particleTable->FindParticle(ParticleName);

  particleGun->SetParticleDefinition(particle); 

  // Default values of the primary generator
  G4ThreeVector position(0,0,-15*m);
  particleGun -> SetParticlePosition(position);
  G4ThreeVector direction(0,0,15*m);
  particleGun -> SetParticleMomentumDirection(direction.unit());
  particleGun -> SetParticleEnergy(250. * MeV); 

  for(int i=1;i<29;i++){
    if(i==1)
      atomicChargeAndMassMap[i]=1;
    else if(i==4)
      atomicChargeAndMassMap[i]=7;
    else if(i==9)
      atomicChargeAndMassMap[i]=19;
    else if(i==11)
      atomicChargeAndMassMap[i]=23;
    else if(i==13)
      atomicChargeAndMassMap[i]=27;
    else if(i==15)
      atomicChargeAndMassMap[i]=31;
    else if(i==17)
      atomicChargeAndMassMap[i]=35;
    else if(i==19)
      atomicChargeAndMassMap[i]=39;
    else if(i==21)
      atomicChargeAndMassMap[i]=21;
    else if(i==22)
      atomicChargeAndMassMap[i]=48;
    else if(i==23)
      atomicChargeAndMassMap[i]=51;
    else if(i==24)
      atomicChargeAndMassMap[i]=52;
    else if(i==25)
      atomicChargeAndMassMap[i]=55;
    else if(i==26)
      atomicChargeAndMassMap[i]=56;
    else if(i==27)
      atomicChargeAndMassMap[i]=59;
    else if(i==28)
      atomicChargeAndMassMap[i]=58;
    else
      atomicChargeAndMassMap[i]=i*2;
  }
}


void MarsPrimaryGeneratorAction::SetGenerationPlane(G4bool aXplus, G4bool aXminus,G4bool aYplus, G4bool aYminus,G4bool aZplus, G4bool aZminus){
  useXplus=aXplus;
  useXminus=aXminus;
  useYplus=aYplus;
  useYminus=aYminus;
  useZplus=aZplus;
  useZminus=aZminus;
}

MarsPrimaryGeneratorAction::~MarsPrimaryGeneratorAction()
{
  delete messenger;
  delete particleGun;
  delete particleTable;
}

G4double MarsPrimaryGeneratorAction::GetInitialEnergy()
{
  G4double primaryParticleEnergy = particleGun -> GetParticleEnergy();
  return primaryParticleEnergy;   
}

void MarsPrimaryGeneratorAction::SetParticleTypeAndEnergy()
{
  if(IONZMin==IONZMax){
    SetParticleEnergy(cumulativeFlux[IONZMin],baryon);
  }else{
    G4bool notGenerated = true;
    do{
      G4int ionZ = IONZMin+floor((IONZMax-IONZMin+1)*G4UniformRand());
      if(G4UniformRand()<ionsProbabilityChoice[ionZ-1]){
	G4int ionA = atomicChargeAndMassMap[ionZ];
	G4String ParticleName;
	if(ionZ==1){
	  ParticleName = "proton";
	  G4ParticleDefinition* particle = particleTable->FindParticle(ParticleName);
	  baryon = particle->GetBaryonNumber();
	  particleGun->SetParticleDefinition(particle); 
	}else{
	  G4ParticleDefinition* particle= particleTable->GetIonTable()->GetIon(ionZ,ionA,0);
	  ParticleName = particle->GetParticleName();
	  baryon = particle->GetBaryonNumber();
	  particleGun->SetParticleDefinition(particle); 
	}
	SetParticleEnergy(cumulativeFlux[ionZ],baryon);
	notGenerated = false;
      }
      
    }while(notGenerated);
  } 
}

void MarsPrimaryGeneratorAction::SetParticleEnergy(std::vector<G4double> usedFlux, G4int myBaryon)
{
  // Generate the energy spectrum of primary particles according to 
  // the flux reported in the .txt file (firt column = Energy (MeV/nucl),
  // second column = differential flux). The fluxes are derived from CREME96  
  // Uniform number between 0. and 1.
  G4double random = G4UniformRand();
  G4int nbelow = 0;	  // largest k such that I[k] is known to be <= rand
  G4int nabove = usedFlux.size()-1;  // largest k such that I[k] is known to be >  rand
  G4int middle;
  while(nabove > nbelow+1){
    middle = (nabove + nbelow+1)>>1;
    if(random >= usedFlux[middle]){
      nbelow = middle;
    }else{
      nabove = middle;
    }
  }

  // Linear interpolation
  G4double energy_gun = 0. * MeV;
  G4double binMeasure = usedFlux[nabove] - usedFlux[nbelow];
  if( binMeasure == 0 ){ 
    energy_gun = (nucleonEnergy[nbelow] + random*(nucleonEnergy[nabove] - nucleonEnergy[nbelow]));
    //std::cout<<"BinMeasure is zero !!!!! energy_gun = "<<energy_gun<<std::endl;
    //G4cout<<"BinMeasure is zero !!!!!"<<G4endl;
  }else{
    G4double binFraction = (random - usedFlux[nbelow]) / binMeasure;
    energy_gun = nucleonEnergy[nbelow] + binFraction *(nucleonEnergy[nabove] - nucleonEnergy[nbelow]);
    //std::cout<<"binFraction = "<<binFraction<<"; energy_gun = "<<energy_gun<<std::endl;
  }
  /*
  if(energy_gun==0){
      std::cout<<"binMeasure = "<<binMeasure<<";usedFlux[nabove] = "<<usedFlux[nabove]<<";  usedFlux[nbelow] = "<< usedFlux[nbelow]<< std::endl;
      std::cout<<" Energy Particle = "<<energy_gun * baryon<<std::endl;
  }
  */
  particleGun -> SetParticleEnergy(energy_gun * myBaryon); 
}
void MarsPrimaryGeneratorAction::ComputeIonsProbability()
{
  if(IONZMin==IONZMax){
    G4int Z = IONZMin;
    G4int A = atomicChargeAndMassMap[Z];
    G4String ParticleName;
    if(Z==1){
      ParticleName = "proton";
      G4ParticleDefinition* particle = particleTable->FindParticle(ParticleName);
      baryon = particle->GetBaryonNumber();
      particleGun->SetParticleDefinition(particle); 
    }else{
      G4ParticleDefinition* particle= particleTable->GetIonTable()->GetIon(Z,A,0);
      ParticleName = particle->GetParticleName();
      baryon = particle->GetBaryonNumber();
      particleGun->SetParticleDefinition(particle); 
      std::cout<<"ParticleName = "<<ParticleName<<"; baryon = "<<baryon<<";"<<std::endl;
    }
    //std::cout<<"Z = "<<Z<<"; A = "<<A<<"; baryon = "<<baryon<<" ParticleName = "<<ParticleName<<std::endl;
  }else{
    G4double norm = 0;
    for(int i=IONZMin-1;i<IONZMax;i++){
      norm +=ionsProbabilityChoice[i];
    }
    for(unsigned int j=0;j<ionsProbabilityChoice.size();j++){
      if(int(j)>=IONZMin-1&&int(j)<IONZMax){
	ionsProbabilityChoice[j]/=norm;
      }else{
	ionsProbabilityChoice[j]=0;
      }
    }
  }
}
void MarsPrimaryGeneratorAction::SetZminZmax(G4int aZmin,G4int aZmax)
{
  IONZMin=aZmin; 
  IONZMax=aZmax;
  ComputeIonsProbability();
}
void MarsPrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
  if(readFile == true){ 
    SetParticleTypeAndEnergy();
  }
  std::vector<G4double> worldDimensions = geometry->GetWorldDimensions();
  G4double Xmax = worldDimensions[0];
  G4double Ymax = worldDimensions[1];
  G4double Zmax = worldDimensions[2];

  G4double phi = 2*pi*G4RandFlat::shoot();
  G4double theta = std::sqrt(G4RandFlat::shoot());
  theta = std::acos(theta);
  
  G4double XYplane = (Xmax*2)*(Ymax*2);
  G4double ZYplane = (Zmax*2)*(Ymax*2);
  G4double XZplane = (Xmax*2)*(Zmax*2);
  G4double surface[] = {XYplane,ZYplane,XZplane};
  G4double minarea = *std::min_element(surface,surface+3);
  G4double maxarea = *std::max_element(surface,surface+3);

  G4double length;
  G4ThreeVector position;
  G4ThreeVector direction;

  if(minarea==maxarea){
    length=6;
  }else{
    length = 2*(ZYplane/minarea + XZplane/minarea + XYplane/minarea);
    XplusLimit = ZYplane/minarea;
    XminusLimit = XplusLimit+ZYplane/minarea;
    YplusLimit = XminusLimit+XZplane/minarea;
    YminusLimit = YplusLimit+XZplane/minarea;
    ZplusLimit = YminusLimit+XYplane/minarea;
    ZminusLimit = ZplusLimit+XYplane/minarea;
  }
  G4bool ok = true;
  
  do{
    G4double side = G4RandFlat::shoot(0.,length);
    if(side<=XplusLimit&&useXplus){
      ok=false;
      position=G4ThreeVector(Xmax,-Ymax+2*Ymax*G4RandFlat::shoot(),-Zmax+2*Zmax*G4RandFlat::shoot());
      if(centerpointing)
	direction=-1*position;
      else
	direction=G4ThreeVector(-cos(theta),cos(phi)*sin(theta),sin(phi)*sin(theta));	      
    }else if(side<=XminusLimit&&useXminus){
      ok=false;
      position=G4ThreeVector(-Xmax,-Ymax+2*Ymax*G4RandFlat::shoot(),-Zmax+2*Zmax*G4RandFlat::shoot());
      if(centerpointing)
	direction=-1*position;
      else
	direction=G4ThreeVector(cos(theta),cos(phi)*sin(theta),sin(phi)*sin(theta));	      
    }else if(side<=YplusLimit&&useYplus){
      ok=false;
      position=G4ThreeVector(-Xmax+2*Xmax*G4RandFlat::shoot(),Ymax,-Zmax+2*Zmax*G4RandFlat::shoot());
      if(centerpointing)
	direction=-1*position;
      else
	direction=G4ThreeVector(cos(phi)*sin(theta),-cos(theta),sin(phi)*sin(theta));	      
    }else if(side<=YminusLimit&&useYminus){
      ok=false;
      position=G4ThreeVector(-Xmax+2*Xmax*G4RandFlat::shoot(),-Ymax,-Zmax+2*Zmax*G4RandFlat::shoot());
      if(centerpointing)
	direction=-1*position;
      else
	direction=G4ThreeVector(cos(phi)*sin(theta),cos(theta),sin(phi)*sin(theta));	      
    }else if(side<=ZplusLimit&&useZplus){
      ok=false;
      position=G4ThreeVector(-Xmax+2*Xmax*G4RandFlat::shoot(),-Ymax+2*Ymax*G4RandFlat::shoot(),Zmax);
      if(centerpointing)
	direction=-1*position;
      else
	direction=G4ThreeVector(cos(phi)*sin(theta),sin(phi)*sin(theta),-cos(theta));	      	      
    }else if(side<=ZminusLimit&&useZminus){
      ok=false;
      position=G4ThreeVector(-Xmax+2*Xmax*G4RandFlat::shoot(),-Ymax+2*Ymax*G4RandFlat::shoot(),-Zmax);
      if(centerpointing)
	direction=-1*position;
      else
	direction=G4ThreeVector(cos(phi)*sin(theta),sin(phi)*sin(theta),cos(theta));	      	      
    }
  }while(ok);
  
  particleGun->SetParticlePosition(position);
  particleGun->SetParticleMomentumDirection(direction.unit());
  particleGun->GeneratePrimaryVertex(anEvent);
}

void MarsPrimaryGeneratorAction::ReadProbability(G4String fileName)
{
  // This method allows to read the .txt files containing the
  // fluxes of the galactic cosmic rays derived form CREME96
  readFile = true;
  
  std::fstream file(fileName, std::ios::in);
  std::filebuf* lsdp = file.rdbuf();
  
  if(! (lsdp->is_open()) ){
    G4String excep = "MarsPrimaryGenerator - data file: " 
      + fileName + " not found";
    G4Exception("MarsPrimaryGeneratorAction::ReadProbability()",
		"RadioP001",FatalException,excep);
  }

  while(!file.eof()){
    G4double ene = -9;
    file >>ene;
    if(ene!=-9 ){
      nucleonEnergy.push_back(ene*MeV);
      for(int i=1;i<29;i++){
	G4double flux = -9;
	file>>flux;
	if(flux!=-9){
	  fluxes[i].push_back(flux);
	}
      }
    }
  }
  file.close();
  for(int i=1;i<29;i++){
    std::vector<G4double> tmpFlux = fluxes[i];
    G4double weight=0;
    for(unsigned int j=0;j<tmpFlux.size()-1;j++){
      weight += (tmpFlux[j+1]+tmpFlux[j]/2.)*(nucleonEnergy[j+1]-nucleonEnergy[j]);
      cumulativeFlux[i].push_back(weight);
    }
    unsigned int tmpSize = cumulativeFlux[i].size();
    ionsProbabilityChoice.push_back(cumulativeFlux[i][tmpSize-1]);
    for(unsigned int j=0;j<tmpSize;j++){
      cumulativeFlux[i][j]/=cumulativeFlux[i][tmpSize-1];
    }
  }
}


void MarsPrimaryGeneratorAction::ReadCustomProbability(G4String fileName)
{
  readFile = true;
  
  std::fstream file(fileName, std::ios::in);
  std::filebuf* lsdp = file.rdbuf();
  
  if(! (lsdp->is_open()) ){
    G4String excep = "MarsPrimaryGenerator - data file: " 
      + fileName + " not found";
    G4Exception("MarsPrimaryGeneratorAction::ReadProbability()",
		"RadioP001",FatalException,excep);
  }
  G4int line = 0;
  G4int Z = -9;
  while(!file.eof()){
    G4double ene = -9;
    G4double flux = -9;
    if(line==0){
      file>>Z;
      line++;
    }
    file >> ene;
    file >> flux;
    if(ene!=-9 && flux!=-9 ){
      nucleonEnergy.push_back(ene*MeV);
      fluxes[Z].push_back(flux*10000/(4*3.14));
    }
  }
  file.close();
  std::vector<G4double> tmpFlux = fluxes[Z];
  G4double weight=0;
  for(unsigned int j=0;j<tmpFlux.size()-1;j++){
    weight += (tmpFlux[j+1]+tmpFlux[j]/2.)*(nucleonEnergy[j+1]-nucleonEnergy[j]);
    cumulativeFlux[Z].push_back(weight);
  }
  unsigned int tmpSize = cumulativeFlux[Z].size();
  ionsProbabilityChoice.push_back(cumulativeFlux[Z][tmpSize-1]);
  for(unsigned int j=0;j<tmpSize;j++){
    cumulativeFlux[Z][j]/=cumulativeFlux[Z][tmpSize-1];
  }
}
