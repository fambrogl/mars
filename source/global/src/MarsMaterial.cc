//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// Code developed by:
//  S.Guatelli
//
//    *******************************
//    *                             *
//    *    MarsMaterial.cc        *
//    *                             *
//    *******************************
//
// $Id$
//

#include "MarsMaterial.hh"

#include "globals.hh"
#include "G4SystemOfUnits.hh"
#include "G4MaterialPropertiesTable.hh"
#include "G4MaterialPropertyVector.hh"
#include "G4MaterialTable.hh"
#include "G4RunManager.hh"
#include "G4Element.hh"
#include "G4ElementTable.hh"

MarsMaterial::MarsMaterial():
  matAir(0), matAl(0), matTi(0), matGraphite(0),matH2O(0),matMgB2(0),matSiO2(0),matSCCable(0),matCu(0),
  nylon(0), mylar(0), 
  beta(0), nextel(0), kevlar(0),
  vacuum(0),deepSpace(0), betaCloth(0), eterogeneousNextel(0), kevlarDeepSpace(0),
  polyethylene(0), polyacrylate(0), evoh(0), nomex(0), nomexAir(0), 
  kevlarAir(0), moon(0),
  soft(0),skeleton(0),lung(0),adipose(0),glandular(0),
  adipose_glandular(0),tissue4(0),skin(0),coilequivalent(0),carboepoxy(0),alb4c(0),solidH(0)
{;}

MarsMaterial::~MarsMaterial()
{
  delete moon;
  delete kevlarAir;
  delete nomexAir;
  delete nomex;
  delete evoh;
  delete polyacrylate;
  delete polyethylene;
  delete kevlarDeepSpace;
  delete eterogeneousNextel;
  delete betaCloth;
  delete vacuum;
  delete deepSpace;
  delete kevlar;
  delete nextel;
  delete beta;
  delete mylar;
  delete nylon;
  delete matH2O;
  delete matAir;
  delete matAl;
  delete matTi;
  delete matGraphite;
  delete matSCCable;
  delete matSiO2;
  delete matCu;
  delete soft;
  delete skeleton; 
  delete lung;
  delete adipose;
  delete glandular;
  delete adipose_glandular;
  delete tissue4;
  delete skin;
  delete coilequivalent;
  delete carboepoxy;
  delete alb4c;
  delete solidH;
}

void MarsMaterial::DefineMaterials()
{
  // Define required materials

  G4double A;  // atomic mass
  G4double Z;  // atomic number
  G4double d;  // density
 
  // General elements
   A = 1.01*g/mole;
  G4Element* elH = new G4Element ("Hydrogen","H",Z = 1.,A);

  A = 12.011*g/mole;
  G4Element* elC = new G4Element("Carbon","C",Z = 6.,A);  

  A = 14.01*g/mole;
  G4Element* elN = new G4Element("Nitrogen","N",Z = 7.,A);

  A = 16.00*g/mole;
  G4Element* elO = new G4Element("Oxygen","O",Z = 8.,A);

  A = 22.99*g/mole;
  G4Element* elNa = new G4Element("Sodium","Na",Z = 11.,A);

  A = 24.305*g/mole;
  G4Element* elMg = new G4Element("Magnesium","Mg",Z = 12.,A);

  A = 30.974*g/mole;
  G4Element* elP = new G4Element("Phosphorus","P",Z = 15.,A);
 
  A = 32.064*g/mole;
  G4Element* elS = new G4Element("Sulfur","S",Z = 16.,A);
 
  A = 35.453*g/mole;
  G4Element* elCl = new G4Element("Chlorine","Cl",Z = 17.,A);
 
  A = 39.098*g/mole;
  G4Element* elK = new G4Element("Potassium","K",Z = 19.,A);

  A = 40.08*g/mole;
  G4Element* elCa = new G4Element("Calcium","Ca",Z = 20.,A);

  A = 55.85*g/mole;
  G4Element* elFe  = new G4Element("Iron","Fe",Z = 26.,A);
 
  A = 65.38*g/mole;
  G4Element* elZn = new G4Element("Zinc","Zn",Z = 30.,A);

  A = 85.47 *g/mole;
  G4Element* elRb = new G4Element("Rb","Rb",Z = 37.,A);

  A = 87.62 *g/mole;
  G4Element* elSr = new G4Element("Sr","Sr",Z = 38.,A);

  A = 91.22 *g/mole;
  G4Element* elZr = new G4Element("Zr","Zr",Z = 40.,A);

  A = 207.19 *g/mole;
  G4Element* elPb = new G4Element("Lead","Pb", Z = 82.,A);

  A = 28.09*g/mole;
  G4Element* elSi  = new G4Element("Silicon","Si",Z = 14.,A);

  A = 10.811*g/mole;
  G4Element* elB = new G4Element ("Boro","B",Z = 5.,A);

  A = 26.98*g/mole;
  G4Element* elAl = new G4Element("Aluminum","Al", Z = 13.,A);

  A = 47.867*g/mole;
  G4Element* elTi = new G4Element("Titanium","Ti",Z = 22.,A);

  A = 63.546*g/mole;
  G4Element* elCu = new G4Element("Copper","Cu",Z = 29., A);

  A = 39.948*g/mole;
  G4Element* elAr = new G4Element("Argon","Ar",Z = 18.,A);
  
  // Air material
  G4double airDensity = 1.205*mg/cm3;
  matAir = new G4Material("Air",airDensity,4);
  matAir -> AddElement(elC,0.000124);
  matAir -> AddElement(elN,0.755267);
  matAir -> AddElement(elO,0.231781);
  matAir -> AddElement(elAr,0.012827);

  // Aluminium
  d = 2.699*g/cm3;
  matAl = new G4Material("Aluminium",d,1);
  matAl->AddElement(elAl,1);
  
  //Titanium
  d = 4.540*g/cm3;
  matTi = new G4Material("Titanium",d,1);
  matTi->AddElement(elTi,1);

  //MgB2
  d = 2.500*g/cm3;
  matMgB2 = new G4Material("MgB2",d,2);
  matMgB2->AddElement(elMg,1);
  matMgB2->AddElement(elB,2);

  //Graphite
  d = 2.210*g/cm3;
  matGraphite = new G4Material("Graphite",d,1);
  matGraphite->AddElement(elC,1);
  
  //SiO2
  d = 1.220*g/cm3;
  matSiO2 = new G4Material("SiO2",d,2);
  matSiO2->AddElement(elSi,1);
  matSiO2->AddElement(elO,2);

  //SCCable
  d = 3.000*g/cm3;
  matSCCable = new G4Material("SCCable",d,4);
  matSCCable->AddMaterial(matMgB2,0.086);
  matSCCable->AddElement(elTi,0.23);
  matSCCable->AddElement(elAl,0.574);
  matSCCable->AddMaterial(matSiO2,0.11);

  //Copper
  d = 8.960*g/cm3;
  matCu = new G4Material("Copper",d,1);
  matCu->AddElement(elCu,1);
  
  // Water
  d = 1.000*g/cm3;
  matH2O = new G4Material("Water",d,2);
  matH2O->AddElement(elH,2);
  matH2O->AddElement(elO,1);
  matH2O->GetIonisation()->SetMeanExcitationEnergy(75.0*eV);
  
  //nylon (alenia spazio)
  d = 1.14 *g/cm3;
  nylon = new G4Material("nylon",d,4);
  nylon -> AddElement(elH,0.108);
  nylon -> AddElement(elC,0.68);
  nylon -> AddElement(elN,0.099);
  nylon -> AddElement(elO,0.113);

  //mylar (alenia spazio)
  d= 1.4 *g/cm3;
  mylar = new G4Material("mylar",d,3);
  mylar -> AddElement(elH,0.042);
  mylar -> AddElement(elC,0.625);
  mylar -> AddElement(elO,0.333);

  //beta cloth
  G4double betaDensity = 2.3 *g/cm3;
  beta = new G4Material("beta",betaDensity,2);
  beta -> AddElement(elO,0.53);
  beta -> AddElement(elSi,0.47);
 
  G4double nextelDensity = 2.7 * g/cm3;
  nextel = new G4Material("nextel",nextelDensity,4);
  nextel -> AddElement(elB,0.04);
  nextel -> AddElement(elO,0.52);
  nextel -> AddElement(elAl,0.33);
  nextel -> AddElement(elSi,0.11);

  //kevlar
  G4double kevlarDensity = 1.44 *g/cm3;
  kevlar = new G4Material("kevlar",d,4);
  kevlar -> AddElement(elH,0.04);
  kevlar -> AddElement(elC,0.71);
  kevlar -> AddElement(elO,0.12);
  kevlar -> AddElement(elN,0.13);
  

  //vacuum
  G4double vacuumDensity = 1.e-25 *g/cm3;
  G4double pressure = 3.e-18*pascal;
  G4double temperature = 2.73*kelvin;
  vacuum = new G4Material("VACUUM", Z=1., A=1.*g/mole,vacuumDensity,kStateGas,temperature,pressure);
  //deepSpace
  G4double deepSpaceDensity = 1.e-25 *g/cm3;
  deepSpace = new G4Material("Galactic", Z=1., A=1.01*g/mole,deepSpaceDensity,kStateGas,temperature,pressure);
  d = (deepSpaceDensity*0.44)+ (betaDensity*0.56);
  betaCloth = new G4Material("betacloth",d,2);
  betaCloth -> AddMaterial (beta, 0.56);
  betaCloth -> AddMaterial (deepSpace,0.44);

  d = (deepSpaceDensity*0.73)+ (nextelDensity*0.27); 
  eterogeneousNextel = new G4Material("Nextel312AF62",d,2);
  eterogeneousNextel -> AddMaterial (deepSpace, 0.73);
  eterogeneousNextel -> AddMaterial (nextel, 0.27);

  d = (deepSpaceDensity*0.44)+ (kevlarDensity*0.56);
  kevlarDeepSpace = new G4Material("kevlarDeepSpace",d,2);
  kevlarDeepSpace -> AddMaterial (deepSpace, 0.44);
  kevlarDeepSpace -> AddMaterial (kevlar, 0.56);

  d = 0.94 * g/cm3;
  polyethylene = new G4Material("polyethylene",d,2);
  polyethylene -> AddElement(elH,0.14);
  polyethylene -> AddElement(elC,0.86);
  
  d = 1.19 * g/cm3;
  polyacrylate = new G4Material("polyacrylate",d,3);
  polyacrylate -> AddElement(elH,0.08);
  polyacrylate -> AddElement(elC,0.60);
  polyacrylate -> AddElement(elO,0.32);
 
  d = 1.17 * g/cm3;
  evoh = new G4Material("evoh",d,3);
  evoh -> AddElement(elH,0.11);
  evoh -> AddElement(elC,0.67);
  evoh -> AddElement(elO,0.22);
  
  G4double nomexDensity = 0.98 * g/cm3;
  nomex = new G4Material("nomex",nomexDensity,5);
  nomex -> AddElement(elH,0.04);
  nomex -> AddElement(elC,0.54);
  nomex -> AddElement(elN,0.09);
  nomex -> AddElement(elO,0.10);
  nomex -> AddElement(elCl,0.23);

  d = 0.45*(nomexDensity)+ 0.55*(airDensity);
  nomexAir = new G4Material("nomexAir",d,2);
  nomexAir -> AddMaterial(nomex,0.45);
  nomexAir -> AddMaterial(matAir,0.55);

  d =0.56*(kevlarDensity)+ 0.44*(airDensity);
  kevlarAir = new G4Material("kevlarAir",d,2);
  kevlarAir -> AddMaterial(kevlar,0.56);
  kevlarAir -> AddMaterial(matAir,0.44);

  d = 1.5*g/cm3;
  moon =  new G4Material("moon",d,6);
  moon -> AddElement(elO,0.45);
  moon -> AddElement(elMg,0.05);
  moon -> AddElement(elAl,0.13);
  moon -> AddElement(elSi,0.21);
  moon -> AddElement(elCa,0.10);
  moon -> AddElement(elFe,0.06); 

  //TISSUE4
  d = 1.0 * g/cm3;
  tissue4 = new G4Material("G4TISSUE4",d,4);
  tissue4 -> AddElement(elO,0.761828);
  tissue4 -> AddElement(elC,0.111000);
  tissue4 -> AddElement(elN,0.026000);
  tissue4 -> AddElement(elH,0.101172);

  //G4SKIN
  d = 1.1 * g/cm3;
  skin = new G4Material("G4SKIN",d,13);
  skin -> AddElement(elH,0.100588);
  skin -> AddElement(elC,0.228250);
  skin -> AddElement(elN,0.046420);
  skin -> AddElement(elO,0.619002);
  skin -> AddElement(elNa,0.000070);
  skin -> AddElement(elMg,0.000060);
  skin -> AddElement(elP,0.000330);
  skin -> AddElement(elS,0.001590);
  skin -> AddElement(elCl,0.002670);
  skin -> AddElement(elK,0.000850);
  skin -> AddElement(elCa,0.000150);
  skin -> AddElement(elFe,0.000010);
  skin -> AddElement(elZn,0.000010);

  // MIRD soft tissue
  d = 0.9869 * g/cm3;
  soft = new G4Material("soft_tissue",d,16);
  soft->AddElement(elH,0.1047);
  soft->AddElement(elC,0.2302);
  soft->AddElement(elN,0.0234);
  soft->AddElement(elO,0.6321);
  soft->AddElement(elNa,0.0013);
  soft->AddElement(elMg,0.00015);
  soft->AddElement(elP,0.0024);
  soft->AddElement(elS,0.0022);
  soft->AddElement(elCl,0.0014);
  soft->AddElement(elK,0.0021);
  soft->AddElement(elFe,0.000063);
  soft->AddElement(elZn,0.000032);
  soft->AddElement(elRb,0.0000057);
  soft->AddElement(elSr,0.00000034);
  soft->AddElement(elZr,0.000008);
  soft->AddElement(elPb,0.00000016);
 
  // MIRD Skeleton

  d = 1.4862*g/cm3;
  skeleton = new G4Material("skeleton",d,15);
  skeleton -> AddElement(elH,0.0704);
  skeleton -> AddElement(elC,0.2279);
  skeleton -> AddElement(elN,0.0387);
  skeleton -> AddElement(elO,0.4856);
  skeleton -> AddElement(elNa,0.0032); 
  skeleton -> AddElement(elMg,0.0011); 
  skeleton -> AddElement(elP,0.0694);
  skeleton -> AddElement(elS,0.0017);
  skeleton -> AddElement(elCl,0.0014);
  skeleton -> AddElement(elK,0.0015);
  skeleton -> AddElement(elCa,0.0991);
  skeleton -> AddElement(elFe,0.00008);
  skeleton -> AddElement(elZn,0.000048);
  skeleton -> AddElement(elSr,0.000032);
  skeleton -> AddElement(elPb,0.000011);
 
  // MIRD lung material
  d = 0.2958 *g/cm3;
  lung = new G4Material("lung_material", d,16);
  lung -> AddElement(elH, 0.1021);
  lung -> AddElement(elC, 0.1001);
  lung -> AddElement(elN,0.028);
  lung -> AddElement(elO,0.7596);
  lung -> AddElement(elNa,0.0019);
  lung -> AddElement(elMg,0.000074);
  lung -> AddElement(elP,0.00081);
  lung -> AddElement(elS,0.0023);
  lung -> AddElement(elCl,0.0027);
  lung -> AddElement(elK,0.0020);
  lung -> AddElement(elCa,0.00007);
  lung -> AddElement(elFe,0.00037);
  lung -> AddElement(elZn,0.000011);
  lung -> AddElement(elRb,0.0000037);
  lung -> AddElement(elSr,0.000000059);
  lung -> AddElement(elPb,0.00000041);

  G4double density_adipose = 0.93 *g/cm3;
  adipose = new G4Material("adipose", density_adipose,8);  
  adipose -> AddElement(elH, 0.112);
  adipose -> AddElement(elC, 0.619);
  adipose -> AddElement(elN, 0.017);
  adipose -> AddElement(elO, 0.251);
  adipose -> AddElement(elS, 0.00025);
  adipose -> AddElement(elP, 0.00025);
  adipose -> AddElement(elK, 0.00025);
  adipose -> AddElement(elCa,0.00025);

  G4double density_glandular = 1.04 * g/cm3;
  glandular = new G4Material("glandular", density_glandular,8);
  glandular -> AddElement(elH, 0.1);
  glandular -> AddElement(elC,0.184);
  glandular -> AddElement(elN, 0.032);
  glandular -> AddElement(elO, 0.679);
  glandular -> AddElement(elS, 0.00125);
  glandular -> AddElement(elP, 0.00125);
  glandular -> AddElement(elK, 0.00125);
  glandular -> AddElement(elCa,0.00125);


  d = (density_adipose * 0.5) + (density_glandular * 0.5);
  adipose_glandular = new G4Material("adipose_glandular", d, 2);
  adipose_glandular -> AddMaterial(adipose, 0.5);
  adipose_glandular -> AddMaterial(glandular, 0.5);

  d = 4.08* g/cm3;
  coilequivalent = new G4Material("coil_equivalent",d,6);
  coilequivalent ->AddElement(elTi,0.79);
  coilequivalent ->AddElement(elAl,0.11);
  coilequivalent ->AddElement(elMg,0.032);
  coilequivalent ->AddElement(elB,0.028);
  coilequivalent ->AddElement(elH,0.01);
  coilequivalent ->AddElement(elO,0.03);

  d = 1.5 * g/cm3;
  carboepoxy = new G4Material("carbo_epoxy",d,3);
  carboepoxy -> AddElement(elC,0.261);
  carboepoxy -> AddElement(elH,0.13);
  carboepoxy -> AddElement(elN,0.609);

  d = 2.592 * g/cm3;
  alb4c = new G4Material("AlB4C",d,3);
  alb4c -> AddElement(elAl,0.2);
  alb4c -> AddElement(elC,0.2);
  alb4c -> AddElement(elB,0.6);

  d = 0.086 *g/cm3;
  solidH = new G4Material("SolidH",d,1,kStateSolid,13.0*kelvin);
  solidH -> AddElement(elH,1);

}

G4Material* MarsMaterial::GetMaterial(G4String material)
{
  G4Material* pttoMaterial = G4Material::GetMaterial(material); 
  return pttoMaterial; 
}
