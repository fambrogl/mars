//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
//$Id: MarsSteppingActionMessenger.cc,v 1.5 2006-06-29 16:24:27 gunter Exp $// GEANT4 tag $Name: not supported by cvs2svn $
//
// Code developed by: Filippo Ambroglini, filippo.ambroglini@pg.infn.it
//

#include "MarsSteppingActionMessenger.hh"
#include "MarsSteppingAction.hh"

#include "globals.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"

MarsSteppingActionMessenger::MarsSteppingActionMessenger(MarsSteppingAction* SA)
:steppingAction(SA)
{
  stepDirectory = new G4UIdirectory("/step/");
  stepDirectory -> SetGuidance("Step control command.");

  SetMaxTrackLengthCmd = new G4UIcmdWithADoubleAndUnit("/step/SetMaxTrackLength",this);
  SetMaxTrackLengthCmd -> SetGuidance("Set the maximum length of the track");
  SetMaxTrackLengthCmd -> SetParameterName("MaxTrackLength", true);
  SetMaxTrackLengthCmd ->SetUnitCategory("Length");
  SetMaxTrackLengthCmd ->SetRange("MaxTrackLength>0.0");
  SetMaxTrackLengthCmd -> AvailableForStates(G4State_PreInit,G4State_Idle); 
}
MarsSteppingActionMessenger::~MarsSteppingActionMessenger()
{
  delete SetMaxTrackLengthCmd;
  delete stepDirectory;
}

void MarsSteppingActionMessenger::SetNewValue(G4UIcommand* command, G4String newValue)
{
 if(command == SetMaxTrackLengthCmd) 
   {
   steppingAction -> SetMaxTrackLength(SetMaxTrackLengthCmd->GetNewDoubleValue(newValue));
 }
}

