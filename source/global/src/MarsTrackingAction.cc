#include "MarsTrackingAction.hh"
#include "G4ThreeVector.hh"
#include "G4TransportationManager.hh"
#include "G4FieldManager.hh"
#include "G4RunManager.hh"
#include "G4SDManager.hh"
#include "MarsDoseSD.hh"

////////////////////////////////////////////////////////////////////////////////
//
MarsTrackingAction::MarsTrackingAction (){
  theIdVertexMap.clear();
  outsideDetector=false;
}
////////////////////////////////////////////////////////////////////////////////
//
MarsTrackingAction::~MarsTrackingAction ()
{;}
////////////////////////////////////////////////////////////////////////////////
//
void MarsTrackingAction::PreUserTrackingAction (const G4Track* aTrack)
{ 
  if(aTrack->GetTrackID()==1&&theIdVertexMap.size()!=0){
    theIdVertexMap.clear(); 
  }
  outsideDetector=false;
  G4int doseGeneratorId = 0;
  G4int TrackId = aTrack->GetTrackID();
  G4String ParticleName = aTrack->GetDefinition()->GetParticleName();
  G4double PDGmass = aTrack->GetDefinition()->GetPDGMass();
  G4double Charge = aTrack->GetDefinition()->GetPDGCharge();
  G4int Z = aTrack->GetDefinition()->GetAtomicNumber();
  G4int A = aTrack->GetDefinition()->GetAtomicMass();
  G4ThreeVector momentum = aTrack->GetMomentum();
  G4double totalE = aTrack->GetTotalEnergy();
  G4double kinE = aTrack->GetKineticEnergy();
  G4ThreeVector vertexPos = aTrack->GetVertexPosition();
  G4int motherTrackId = aTrack->GetParentID();
  G4String volumeAtVertex = aTrack->GetLogicalVolumeAtVertex()->GetName();
  G4int eventId = G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID();
  G4String processName = "unknown";
  if (aTrack->GetCreatorProcess() != 0){
    processName = aTrack->GetCreatorProcess()->GetProcessName();
  }
  std::pair<G4int, G4String> tempPair(motherTrackId,volumeAtVertex);
  theIdVertexMap.insert(std::pair<G4int,std::pair<G4int,G4String> >(TrackId,tempPair));

  G4int searchId = TrackId;
  while(outsideDetector == false){
    if(theIdVertexMap.find(searchId)!=theIdVertexMap.end()){
      std::pair<G4int,G4String> temp = theIdVertexMap.find(searchId)->second;
      if(temp.second!="bfo"&&temp.second!="skin"&&temp.second!="body"&&temp.second!="organs"){
	outsideDetector=true;
	doseGeneratorId = searchId;
      }else{
	searchId=temp.first;
      }
    }else{
      G4cout<<"WHERE IS GONE THE TRACK WITH TRACK ID = "<<searchId<<G4endl;
    }
  }

  MarsTrack* storeTrack = new MarsTrack(TrackId,ParticleName,PDGmass,Charge,Z,A,momentum,totalE,kinE,vertexPos,volumeAtVertex,motherTrackId,eventId,processName,doseGeneratorId);

  MarsDoseSD* sd  = dynamic_cast<MarsDoseSD*>(G4SDManager::GetSDMpointer()->FindSensitiveDetector("astronautSD"));
  sd->GetTrackCollection()->insert(storeTrack);
}

////////////////////////////////////////////////////////////////////////////////
//
void MarsTrackingAction::PostUserTrackingAction (const G4Track* aTrack)
{;}
