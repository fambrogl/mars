#include "MarsFluenceSD.hh"
#include "G4Step.hh"
#include "G4HCofThisEvent.hh"
#include "G4Track.hh"
#include "G4RunManager.hh"
#include "G4SDManager.hh"
#include "G4ios.hh"
#include "G4ParticleDefinition.hh"
#include "G4VProcess.hh"
#include "G4SDManager.hh"

//#include "G4ProcessTable.hh"
class G4Step;

MarsFluenceSD::MarsFluenceSD(G4String name)
  :G4VSensitiveDetector(name)
{ 
  collectionName.insert("FluenceHitCollection");
}
////////////////////////////////////////////////////////////////////////////////
//
MarsFluenceSD::~MarsFluenceSD()
{
}
////////////////////////////////////////////////////////////////////////////////
//
void MarsFluenceSD::Initialize(G4HCofThisEvent*HCE)
{ 
  static int HCID = -1;

  FluenceHitCollection = new MarsFluenceHitCollection
    (SensitiveDetectorName,collectionName[0]);

  HCID = GetCollectionID(0);
  HCE->AddHitsCollection(HCID,FluenceHitCollection); 

}
////////////////////////////////////////////////////////////////////////////////
//
G4bool MarsFluenceSD::ProcessHits(G4Step*aStep,G4TouchableHistory*)
{
  G4int trackID = aStep->GetTrack()->GetTrackID();
  if(previousTrackID.find(trackID)==previousTrackID.end()){
    previousTrackID[trackID]+=1;
    G4double kinE = aStep->GetTrack()->GetKineticEnergy();
    G4String Name = aStep->GetTrack()->GetDefinition()->GetParticleName();
    G4int Z = aStep->GetTrack()->GetDefinition()->GetAtomicNumber();
    G4int A = aStep->GetTrack()->GetDefinition()->GetAtomicMass();
    MarsFluenceHit* aHit = new MarsFluenceHit(Name,trackID,Z,A,kinE);
    FluenceHitCollection->insert(aHit);
  }
  return true;   
}  
////////////////////////////////////////////////////////////////////////////////
//
void MarsFluenceSD::EndOfEvent(G4HCofThisEvent*)
{
  previousTrackID.clear();
}
