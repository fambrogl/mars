//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//    **********************************
//    *                                *
//    *    MarsEventAction.cc        *
//    *                                *
//    **********************************
//
//
// $Id$
//
// Author : Susanna Guatelli, guatelli@ge.infn.it
// 
#include <algorithm>
#include <iostream>
#include "MarsEventAction.hh"
#include "G4Event.hh"
#include "G4Timer.hh"
#include "G4EventManager.hh"
#include "G4ios.hh"
#include "MarsAnalysisManager.hh"
#include "MarsEventActionMessenger.hh"

//##include "Randomize.hh"
//#include "CLHEP/Random/RandEngine.h"
MarsEventAction::MarsEventAction():evtNo(-1),printModul(500)
{
  messenger= new MarsEventActionMessenger(this);

  timer = new G4Timer;
  usetimer = false;
}
 
MarsEventAction::~MarsEventAction()
{
  delete timer;
}
 
void MarsEventAction::BeginOfEventAction(const G4Event* evt)
{ 
  evtNo = evt -> GetEventID(); 
  if (evtNo%printModul == 0) 
    G4cout << "\n---> Begin Of Event: " << evtNo << G4endl;
  MarsAnalysisManager::GetInstance()->BeginOfEventAction(evt);
  if(usetimer)
    timer->Start();
}

void MarsEventAction::EndOfEventAction(const G4Event* evt)
{
  if (evtNo%printModul == 0) 
    G4cout << "\n---> End Of Event: " << evtNo <<G4endl;
  if(usetimer){
    timer->Stop();
    G4cout << G4endl;
    G4cout << "******************************************";
    G4cout << G4endl;
    G4cout << "Event: " << evtNo;
    G4cout << G4endl;
    G4cout << "Total Real Elapsed Time is: "<< timer->GetRealElapsed();
    G4cout << G4endl;
    G4cout << "Total System Elapsed Time: " << timer->GetSystemElapsed();
    G4cout << G4endl;
    G4cout << "Total GetUserElapsed Time: " << timer->GetUserElapsed();
    G4cout << G4endl;
    G4cout << "******************************************";
    G4cout << G4endl;
  }
  MarsAnalysisManager::GetInstance()->EndOfEventAction(evt);
}
