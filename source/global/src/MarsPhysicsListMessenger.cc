//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id$
//
// Author: Susanna Guatelli, guatelli@ge.infn.it

#include "MarsPhysicsListMessenger.hh"
#include "MarsPhysicsList.hh"

#include "G4UIdirectory.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcommand.hh"
#include "G4UIparameter.hh"
#include "G4UIdirectory.hh"
#include <sstream>


MarsPhysicsListMessenger::MarsPhysicsListMessenger(MarsPhysicsList * List)
:mPhysicsList(List)
{
  physDir = new G4UIdirectory("/mars/physics/");
  physDir -> SetGuidance("physics commands");

  gammaCutCmd = new G4UIcmdWithADoubleAndUnit("/mars/physics/setGCut",this);  
  gammaCutCmd->SetGuidance("Set gamma cut.");
  gammaCutCmd->SetParameterName("Gcut",false);
  gammaCutCmd->SetUnitCategory("Length");
  gammaCutCmd->SetRange("Gcut>=0.0");
  gammaCutCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  electCutCmd = new G4UIcmdWithADoubleAndUnit("/mars/physics/setECut",this);  
  electCutCmd->SetGuidance("Set electron cut.");
  electCutCmd->SetParameterName("Ecut",false);
  electCutCmd->SetUnitCategory("Length");
  electCutCmd->SetRange("Ecut>=0.0");
  electCutCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  posCutCmd = new G4UIcmdWithADoubleAndUnit("/mars/physics/setPCut",this);  
  posCutCmd->SetGuidance("Set positron cut.");
  posCutCmd->SetParameterName("Pcut",false);
  posCutCmd->SetUnitCategory("Length");
  posCutCmd->SetRange("Pcut>=0.0");
  posCutCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  

  protoCutCmd = new G4UIcmdWithADoubleAndUnit("/mars/physics/setPrCut",this);  
  protoCutCmd->SetGuidance("Set proton cut.");
  protoCutCmd->SetParameterName("PRcut",false);
  protoCutCmd->SetUnitCategory("Length");
  protoCutCmd->SetRange("PRcut>=0.0");
  protoCutCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  

  allCutCmd = new G4UIcmdWithADoubleAndUnit("/mars/physics/setCuts",this);  
  allCutCmd->SetGuidance("Set cut for all.");
  allCutCmd->SetParameterName("cut",false);
  allCutCmd->SetUnitCategory("Length");
  allCutCmd->SetRange("cut>0.0");
  allCutCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  


  ARCutCmd = new G4UIcmdWithADoubleAndUnit("/mars/physics/setAstronautRegionCuts",this);  
  ARCutCmd->SetGuidance("Set cut for all in the Astronaut Region");
  ARCutCmd->SetParameterName("ARCut",false);
  ARCutCmd->SetUnitCategory("Length");
  ARCutCmd->SetRange("ARCut>0.0");
  ARCutCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  

  pListCmd = new G4UIcmdWithAString("/mars/physics/addPhysics",this);  
  pListCmd->SetGuidance("Add predefine Physics List or its component ");
  pListCmd->SetGuidance("(list command shows all options)");
  pListCmd->SetParameterName("PList",false);
  pListCmd->AvailableForStates(G4State_PreInit);  

  pVerCmd = new G4UIcmdWithAnInteger("/mars/physics/verbose",this);  
  pVerCmd->SetGuidance("Define verbose level for PhysicsList");
  pVerCmd->SetParameterName("PLver",false);
  pVerCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  

  energyRangeCmd = 
    new G4UIcmdWithADoubleAndUnit("/mars/physics/productionCutsLowestEnergy",this);
  energyRangeCmd->SetGuidance("Set lowest energy for production cut.");
  energyRangeCmd->SetParameterName("lowUnit",false);
  energyRangeCmd->SetUnitCategory("Energy");
  energyRangeCmd->SetRange("lowUnit>=0.0");
  energyRangeCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

}

MarsPhysicsListMessenger::~MarsPhysicsListMessenger()
{  
  delete physDir;
  delete gammaCutCmd;
  delete electCutCmd;
  delete posCutCmd;
  delete protoCutCmd;
  delete allCutCmd;
  delete ARCutCmd;
  delete pListCmd;
  delete pVerCmd;
  delete energyRangeCmd;

}
  
void MarsPhysicsListMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{
  if( command == gammaCutCmd ) {
    mPhysicsList->SetCutForGamma(gammaCutCmd->GetNewDoubleValue(newValue));
  } else if( command == electCutCmd ) {
    mPhysicsList->SetCutForElectron(electCutCmd->GetNewDoubleValue(newValue));
  } else if( command == posCutCmd ) {
    mPhysicsList->SetCutForPositron(posCutCmd->GetNewDoubleValue(newValue));
  } else if( command == protoCutCmd ) {
    mPhysicsList->SetCutForProton(protoCutCmd->GetNewDoubleValue(newValue));
  } else if( command == allCutCmd ) {
    G4double cut = allCutCmd->GetNewDoubleValue(newValue);
    mPhysicsList->SetCutForGamma(cut);
    mPhysicsList->SetCutForElectron(cut);
    mPhysicsList->SetCutForPositron(cut);
    mPhysicsList->SetCutForProton(cut);
  } else if( command == ARCutCmd){
    mPhysicsList->SetProductionCutsAstronautRegion(ARCutCmd->GetNewDoubleValue(newValue));
  }else if( command == pListCmd){ 
    mPhysicsList->AddPhysicsList(newValue);
  } else if( command == pVerCmd){ 
    mPhysicsList->SetVerbose(pVerCmd->GetNewIntValue(newValue));
  } else if( command == energyRangeCmd) {
    mPhysicsList->SetProductionCutsLowestEnergy(energyRangeCmd->GetNewDoubleValue(newValue));
  }
}






