#ifndef MarsDoseSD_h
#define MarsDoseSD_h 1

#include "G4VSensitiveDetector.hh"
#include "G4ThreeVector.hh"
#include "MarsTrackingHit.hh"
#include "MarsTrack.hh"

class G4Step;
class G4HCofThisEvent;
class G4StepPoint;


#include"G4ios.hh"

class MarsDoseSD : public G4VSensitiveDetector
{

public:
  MarsDoseSD(G4String name);
  ~MarsDoseSD();

  void Initialize(G4HCofThisEvent*HCE);
  G4bool ProcessHits(G4Step*aStep,G4TouchableHistory*ROhist);
  void EndOfEvent(G4HCofThisEvent*HCE);
      
  inline MarsTrackingHitCollection* GetTrackingHitCollection()
  {return TrackingHitCollection;}
  inline MarsTrackCollection* GetTrackCollection()
  {return TrackCollection;}

private:
  MarsTrackingHitCollection* TrackingHitCollection;
  MarsTrackCollection*        TrackCollection;
};




#endif

