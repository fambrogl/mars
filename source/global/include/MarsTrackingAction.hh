#ifndef MarsTrackingAction_h
#define MarsTrackingAction_h 1

#include "G4UserTrackingAction.hh"
#include "G4TrackingManager.hh"
#include "globals.hh"
#include "MarsTrack.hh"

////////////////////////////////////////////////////////////////////////////////
//
class MarsTrackingAction : public G4UserTrackingAction
{
public:
  MarsTrackingAction ();
  ~MarsTrackingAction ();
  
  void PreUserTrackingAction (const G4Track* theTrack);
  void PostUserTrackingAction (const G4Track* theTrack);
  
private:
  G4bool outsideDetector;
  std::map<G4int, std::pair<G4int, G4String> >  theIdVertexMap;
};
////////////////////////////////////////////////////////////////////////////////
#endif
