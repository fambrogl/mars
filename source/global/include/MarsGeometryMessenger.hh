//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
//    *****************************************
//    *                                       *
//    *      MarsDetectrorMessenger.hh      *
//    *                                       *
//    *****************************************
//
// $Id$
//
// 
#ifndef MarsGeometryMessenger_h
#define MarsGeometryMessenger_h 1

#include "globals.hh"
#include "G4UImessenger.hh"

class MarsGeometryConstruction;
class G4UIdirectory;
class G4UIparameter;
class G4UIcmdWithAString;
class G4UIcommad;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithoutParameter;
class MarsGeometryMessenger: public G4UImessenger
{
public:
  MarsGeometryMessenger(MarsGeometryConstruction* );
  ~MarsGeometryMessenger();
    
  void SetNewValue(G4UIcommand*, G4String);
  
private:
  MarsGeometryConstruction*    geometry;//pointer to detector
  G4UIparameter*               param;
  G4UIdirectory*               geometryConfigDir; // control the geometry configuration
  G4UIcommand*                 worldDimensionCmd;
  G4UIcmdWithAString*          spacecraftCmd; //change set-up configuration 
  G4UIcmdWithAString*          magneticShieldingCmd;//add the shielding layer
  G4UIcmdWithoutParameter*     checkOverlapsCmd;
  G4UIcmdWithoutParameter*     buildMagneticShieldCmd;
  G4UIcmdWithoutParameter*     buildSpacecraftAndAstronautCmd;
};
#endif

