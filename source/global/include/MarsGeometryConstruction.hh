//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//    **************************************
//    *                                    *
//    *    MarsGeometryConstruction.hh   *
//    *                                    *          
//    **************************************
//
// $Id$
//
// Author:Filippo Ambroglini, filippo.ambroglini@pg.infn.it
//
#ifndef MarsGeometryConstruction_H
#define MarsGeometryConstruction_H 1

#include "G4VUserDetectorConstruction.hh"
#include "globals.hh"

class G4LogicalVolume;
class G4VPhysicalVolume;
class MarsMaterial;
class MarsGeometryMessenger;
class G4Box;
class G4VisAttributes;
class MarsVGeometryComponent;
class MarsMagneticFieldSetup;
class MarsMagneticShield;
class G4Sphere;

class MarsGeometryConstruction : public G4VUserDetectorConstruction
{
public:

  MarsGeometryConstruction();
  ~MarsGeometryConstruction();

  G4VPhysicalVolume* Construct();

  void ConstructVolume();
  void MagneticShieldConfiguration(G4String);
  void SpacecraftConfiguration(G4String);

  void BuildSpacecraftAndAstronaut();
  void BuildMagneticShield();
  void CheckOverlaps();
  
  void SetWorldDimensions(G4double aHalfX,G4double aHalfY,G4double aHalfZ);
  inline std::vector<G4double> GetWorldDimensions(){return worldDimensions;}

private:

  G4double worldHalfX,worldHalfY,worldHalfZ;
  std::vector<G4double> worldDimensions;
  
  G4LogicalVolume* experimentalHall_log;
  G4VPhysicalVolume* experimentalHall_phys;
  G4VPhysicalVolume* astronautMotherVolume;
  MarsMaterial*  pMaterial;
  MarsGeometryMessenger* messenger; 
  G4String spacecraftType;
  MarsVGeometryComponent* spacecraft;
  MarsMagneticFieldSetup* magneticFieldSetup;
  MarsVGeometryComponent* astronaut;
  MarsMagneticShield*     magneticShielding; 


};
#endif

