#ifndef MarsFluenceSD_h
#define MarsFluenceSD_h 1

#include "G4VSensitiveDetector.hh"
#include "G4ThreeVector.hh"
#include "MarsFluenceHit.hh"
#include <map>

class G4Step;
class G4HCofThisEvent;
class G4StepPoint;


#include"G4ios.hh"

class MarsFluenceSD : public G4VSensitiveDetector
{

public:
  MarsFluenceSD(G4String name);
  ~MarsFluenceSD();

  void Initialize(G4HCofThisEvent*HCE);
  G4bool ProcessHits(G4Step*aStep,G4TouchableHistory*ROhist);
  void EndOfEvent(G4HCofThisEvent*HCE);
      
  inline MarsFluenceHitCollection* GetFluenceHitCollection()
  {return FluenceHitCollection;}

private:
  MarsFluenceHitCollection* FluenceHitCollection;
  std::map<G4int,G4int> previousTrackID;
};




#endif

