// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// $Id: MarsPrimaryGeneratorAction.hh,v 1.14 2006-06-29 16:23:07 gunter Exp $// GEANT4 tag $Name: not supported by cvs2svn $
//
// Author: Susanna Guatelli, guatelli@ge.infn.it
//
#ifndef MarsPrimaryGeneratorAction_h
#define MarsPrimaryGeneratorAction_h 1

#include "G4VUserPrimaryGeneratorAction.hh"
#include "globals.hh"
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"
#include <vector>
#include <map>
#include <iostream>

class G4ParticleGun;
class G4ParticleTable;
class G4Event;
class G4DataVector;
class MarsPrimaryGeneratorMessenger;
class MarsGeometryConstruction;
class MarsPrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction
{
public:
  MarsPrimaryGeneratorAction();
  ~MarsPrimaryGeneratorAction();

public:
  G4double GetInitialEnergy();
  void GeneratePrimaries(G4Event* anEvent);
  void ReadProbability(G4String);
  void ReadCustomProbability(G4String);
  void SetGenerationPlane(G4bool aXplus, G4bool aXminus,G4bool aYplus, G4bool aYminus,G4bool aZplus, G4bool aZminus);
  inline void SetGeometry(MarsGeometryConstruction* Geom){ geometry = Geom;}
  inline void SetDirectionToCenter(){centerpointing=true;}
  void SetZminZmax(G4int aZmin,G4int aZmax);
private:  
  void SetParticleTypeAndEnergy();
  void SetParticleEnergy(std::vector<G4double> flux ,G4int baryon);
  void ComputeIonsProbability();
  G4ParticleTable* particleTable;
  G4ParticleGun* particleGun;
  MarsPrimaryGeneratorMessenger* messenger;
  MarsGeometryConstruction* geometry;
  G4bool readFile;
  G4bool useXplus,useXminus,useYplus,useYminus,useZplus,useZminus;
  G4double XplusLimit,XminusLimit,YplusLimit,YminusLimit,ZplusLimit,ZminusLimit;
  G4bool centerpointing;
  //G4double* cumulate;
  //G4double* energy;
  //G4double worldHalfSize;
  G4int IONZMin,IONZMax;
  
  //stores the probability given in input
  //G4DataVector* data;
  //stores the energy data 
  //G4DataVector* energies;
  
  //G4int size;
  G4int baryon;
  std::vector<G4double> nucleonEnergy;
  std::map<G4int,std::vector<G4double> > fluxes;
  std::map<G4int,std::vector<G4double> > cumulativeFlux;
  std::map<G4int,G4int> atomicChargeAndMassMap; 
  std::vector<G4double> ionsProbabilityChoice;
};
#endif


